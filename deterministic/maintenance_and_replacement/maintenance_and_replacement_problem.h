#ifndef MAINTENANCE_AND_REPLACEMENT_PROBLEM_H_
#define MAINTENANCE_AND_REPLACEMENT_PROBLEM_H_

#include <action.h>
#include <algorithm>
#include <deterministic_dynamic_program.h>
#include <limits>
#include <state_loop.h>
#include <string>
#include <vector>

struct MaintenanceAndReplacementParameters {
  double purchase_cost;
  std::vector<double> salvage_value;
  std::vector<double> trade_in_value;
  std::vector<double> operating_cost;
};

class MaintenanceAndReplacementProblem
    : public DeterministicDynamicProgram<unsigned int> {

  typedef unsigned int StateType;

public:
  MaintenanceAndReplacementProblem() = delete;
  MaintenanceAndReplacementProblem(const unsigned int &T,
                                   const unsigned int &oldest_state);
  MaintenanceAndReplacementProblem(
      const unsigned int &T, const unsigned int &oldest_state,
      const MaintenanceAndReplacementParameters &params);

  double value_function(const stage &t,
                        const StateType &state) const override;

  void set_salvage_value(const std::vector<double> &salvage_value);
  void set_purchase_cost(const double &purchase_cost);
  void set_trade_in_value(const std::vector<double> &trade_in_value);
  void set_operating_cost(const std::vector<double> &operating_cost);

protected:
  unsigned int oldest_state() const;

  double purchase_cost() const;
  const std::vector<double> &trade_in_value() const;
  const std::vector<double> &operating_cost() const;

  virtual std::vector<Action<StateType>>
  generate_actions(const stage &t, const StateType &state) const override;

  Action<StateType> make_replacement_action(const StateType &state) const;
  Action<StateType> make_maintenance_action(const StateType &state) const;

private:
  StateType oldest_state_;
  double purchase_cost_;
  std::vector<double> trade_in_value_;
  std::vector<double> operating_cost_;
};

#endif // MAINTENANCE_AND_REPLACEMENT_PROBLEM_H_