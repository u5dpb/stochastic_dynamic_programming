#include "maintenance_and_replacement_problem.h"

MaintenanceAndReplacementProblem::MaintenanceAndReplacementProblem(
    const unsigned int &T, const unsigned int &oldest_state_)
    : DeterministicDynamicProgram(T,
                                  StateLoop<StateType>(1, oldest_state_)) {
  this->oldest_state_ = oldest_state_;
  this->purchase_cost_ = std::numeric_limits<double>::max();
  trade_in_value_.resize(oldest_state_ + 1u, 0);
  operating_cost_.resize(oldest_state_ + 1u, std::numeric_limits<double>::max());
}

MaintenanceAndReplacementProblem::MaintenanceAndReplacementProblem(
    const unsigned int &T, const unsigned int &oldest_state_,
    const MaintenanceAndReplacementParameters &params)
    : DeterministicDynamicProgram(T,
                                  StateLoop<unsigned int>(1, oldest_state_)) {
  this->oldest_state_ = oldest_state_;
  this->purchase_cost_ = params.purchase_cost;
  this->trade_in_value_ = params.trade_in_value;
  this->operating_cost_ = params.operating_cost;
  for (auto state = 1u; state < params.salvage_value.size(); ++state) {
    set_v0(state, -params.salvage_value[state]);
  }
}

void MaintenanceAndReplacementProblem::set_salvage_value(
    const std::vector<double> &salvage_value) {
  for (auto state = 1u; state < salvage_value.size(); ++state) {
    set_v0(state, -salvage_value[state]);
  }
}

void MaintenanceAndReplacementProblem::set_purchase_cost(
    const double &purchase_cost_) {
  this->purchase_cost_ = purchase_cost_;
}

void MaintenanceAndReplacementProblem::set_trade_in_value(
    const std::vector<double> &trade_in_value) {
  this->trade_in_value_ = trade_in_value;
}

void MaintenanceAndReplacementProblem::set_operating_cost(
    const std::vector<double> &operating_cost) {
  this->operating_cost_ = operating_cost;
}

double MaintenanceAndReplacementProblem::value_function(
    const stage &t, const StateType &state) const {
  return v(t, state);
}

// in the following function const unsigned int &t is unused but must
// be included in the function definition

std::vector<Action<MaintenanceAndReplacementProblem::StateType>>
MaintenanceAndReplacementProblem::generate_actions(
    const stage &t, const StateType &state) const {

 (void) t;
 
  if (state < oldest_state_) {
    return {make_replacement_action(state), make_maintenance_action(state)};
  } else {
    return {make_replacement_action(state)};
  }
}

Action<MaintenanceAndReplacementProblem::StateType> MaintenanceAndReplacementProblem::make_maintenance_action(
    const StateType &state) const {
  return {operating_cost_[state], {{state + 1u, 1.0}}, 0u};
}

Action<MaintenanceAndReplacementProblem::StateType> MaintenanceAndReplacementProblem::make_replacement_action(
    const StateType &state) const {
  return {purchase_cost_ - trade_in_value_[state] + operating_cost_[0],
          {{1, 1.0}},
          1u};
}

unsigned int MaintenanceAndReplacementProblem::oldest_state() const {
  return oldest_state_;
}

double MaintenanceAndReplacementProblem::purchase_cost() const {
  return purchase_cost_;
}

const std::vector<double> &MaintenanceAndReplacementProblem::trade_in_value() const
{
  return trade_in_value_;
}

const std::vector<double> &MaintenanceAndReplacementProblem::operating_cost() const
{
  return operating_cost_;
}