#ifndef MAINTENANCE_AND_REPLACEMENT_PROBLEM_MULTI_H_
#define MAINTENANCE_AND_REPLACEMENT_PROBLEM_MULTI_H_

#include <action.h>
#include <algorithm>
#include <deterministic_dynamic_program.h>
#include <limits>
#include <numeric>
#include <state_loop_vector.h>
#include <string>
#include <vector>

struct MaintenanceAndReplacementMultiParameters {
  std::vector<double> purchase_cost;
  std::vector<double> salvage_value;
  std::vector<double> trade_in_value;
  std::vector<double> operating_cost;
};

class MaintenanceAndReplacementProblemMulti
    : public DeterministicDynamicProgram<std::vector<unsigned int>> {

  typedef std::vector<unsigned int> StateType;

public:
  MaintenanceAndReplacementProblemMulti(const unsigned int &T,
                                        const unsigned int &oldest_state,
                                        const unsigned int &no_machines);
  MaintenanceAndReplacementProblemMulti(
      const unsigned int &T, const unsigned int &oldest_state,
      const unsigned int &no_machines,
      const MaintenanceAndReplacementMultiParameters &params);

  void set_salvage_value(const std::vector<double> &salvage_value);
  void set_purchase_cost(const std::vector<double> &purchase_cost);
  void set_trade_in_value(const std::vector<double> &trade_in_value);
  void set_operating_cost(const std::vector<double> &operating_cost);

  double value_function(const stage &t, const std::vector<unsigned int> &state) const override;

protected:
  unsigned int no_machines() const;
  unsigned int oldest_state() const;
  const std::vector<double> &purchase_cost() const;
  const std::vector<double> &trade_in_value() const;
  const std::vector<double> &operating_cost() const;

  std::vector<Action<StateType>>
  generate_actions(const stage &t,
                   const StateType &state) const override;

private:
  unsigned int no_machines_;
  unsigned int oldest_state_;
  std::vector<double> purchase_cost_;
  std::vector<double> trade_in_value_;
  std::vector<double> operating_cost_;
};

#endif // MAINTENANCE_AND_REPLACEMENT_PROBLEM_MULTI_H_