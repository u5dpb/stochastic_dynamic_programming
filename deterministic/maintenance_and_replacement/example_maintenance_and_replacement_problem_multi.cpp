#include "maintenance_and_replacement_problem_multi.h"

#include <iostream>
#include <vector>

int main() {
  const auto oldest_state = 7u;
  const auto no_stages = 500u;
  const auto no_machines = 3u;

  const std::vector<double> salvage_value = {50, 40, 30, 20, 10, 0, 0, 0};
  const std::vector<double> trade_in_value = {0, 30, 15, 5, 0, 0, 0};
  const std::vector<double> operating_cost = {5, 7, 11, 22, 35, 50, 75};
  const std::vector<double> purchase_cost = {60, 118, 175};

  MaintenanceAndReplacementProblemMulti problem(no_stages, oldest_state,
                                                no_machines);
  problem.use_action_matrix();

  problem.set_salvage_value(salvage_value);
  problem.set_trade_in_value(trade_in_value);
  problem.set_operating_cost(operating_cost);
  problem.set_purchase_cost(purchase_cost);

  problem.calculate();

  StateLoop<std::vector<unsigned int>> state_loop(
      {1, 1, 1}, {oldest_state, oldest_state, oldest_state});
  for (auto state : state_loop) {
    if (state[0] == 3 && state[1] == 3) {
      for (auto t = 20u; t > 0u; --t) {
        std::cout << problem.action_matrix(stage(t), state) << " ";
      }
      std::cout << "\n";
    }
  }

  exit(0);
}