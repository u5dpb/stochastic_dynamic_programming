#include "maintenance_and_replacement_problem_multi.h"

MaintenanceAndReplacementProblemMulti::MaintenanceAndReplacementProblemMulti(
    const unsigned int &T, const unsigned int &oldest_state,
    const unsigned int &no_machines)
    : DeterministicDynamicProgram(
          T, StateLoop<std::vector<unsigned int>>(
                 std::vector<unsigned int>(no_machines, 1),
                 std::vector<unsigned int>(no_machines, oldest_state))),
      no_machines_(no_machines), oldest_state_(oldest_state) {
  this->purchase_cost_ =
      std::vector<double>(no_machines, std::numeric_limits<double>::max());
  trade_in_value_.resize(oldest_state_ + 1u, 0);
  operating_cost_.resize(oldest_state_ + 1u,
                         std::numeric_limits<double>::max());
}
MaintenanceAndReplacementProblemMulti::MaintenanceAndReplacementProblemMulti(
    const unsigned int &T, const unsigned int &oldest_state,
    const unsigned int &no_machines,
    const MaintenanceAndReplacementMultiParameters &params)
    : DeterministicDynamicProgram(
          T, StateLoop<std::vector<unsigned int>>(
                 std::vector<unsigned int>(no_machines, 1),
                 std::vector<unsigned int>(no_machines, oldest_state))),
      no_machines_(no_machines), oldest_state_(oldest_state) {
  this->purchase_cost_ = params.purchase_cost;
  this->trade_in_value_ = params.trade_in_value;
  this->operating_cost_ = params.operating_cost;
  set_salvage_value(params.salvage_value);
}

void MaintenanceAndReplacementProblemMulti::set_salvage_value(
    const std::vector<double> &salvage_value) {

  for (auto state : state_loop_) {
    double total_salvage = 0.0;
    for (const auto &age : state)
      total_salvage += salvage_value[age];
    set_v0(state, -total_salvage);
  }
}

double MaintenanceAndReplacementProblemMulti::value_function(
    const stage &t, const StateType &state) const {
  return v(t, state);
}

void MaintenanceAndReplacementProblemMulti::set_purchase_cost(
    const std::vector<double> &purchase_cost) {
  this->purchase_cost_ = purchase_cost;
}

void MaintenanceAndReplacementProblemMulti::set_trade_in_value(
    const std::vector<double> &trade_in_value) {
  this->trade_in_value_ = trade_in_value;
}

void MaintenanceAndReplacementProblemMulti::set_operating_cost(
    const std::vector<double> &operating_cost) {
  this->operating_cost_ = operating_cost;
}

// in the following function const unsigned int &t is unused but must
// be included in the function definition

std::vector<Action<MaintenanceAndReplacementProblemMulti::StateType>>
MaintenanceAndReplacementProblemMulti::generate_actions(
    const stage &t, const StateType &state) const {

  (void) t;
  
  std::vector<Action<StateType>> action_set;

  StateLoop<StateType> choices(
      std::vector<unsigned int>(state.size(), 0),
      std::vector<unsigned int>(state.size(), 1));
  // choices[i] = 0 if machine i is replaced
  //            = 1 if machine i is kept
  // Note: This may be different elsewhere in the code
  //       but this notation makes some of the calculations
  //       easier
  for (const auto &choice : choices) {
    unsigned int no_purchases =
        no_machines_ - std::accumulate(choice.begin(), choice.end(), 0u);
    if (no_purchases < purchase_cost_.size()) {
      Action<StateType> action;
      action.cost = 0;
      StateType subsequent_state = state;

      for (auto machine = 0u; machine < no_machines_; ++machine) {
        if (choice[machine] == 1) {
          action.cost += operating_cost_[state[machine]];
          subsequent_state[machine]++;
        } else {
          action.cost += -trade_in_value_[state[machine]] + operating_cost_[0];
          subsequent_state[machine] = 1;
        }
      }
      action.cost += purchase_cost_[no_purchases];
      action.subsequent_state = {{subsequent_state, 1.0}};
      action.type = (int)no_purchases;
      if (valid(stage(t - 1), subsequent_state))
        action_set.push_back(action);
    }
  }

  return action_set;
}

unsigned int MaintenanceAndReplacementProblemMulti::no_machines() const {
  return no_machines_;
}

unsigned int MaintenanceAndReplacementProblemMulti::oldest_state() const {
  return oldest_state_;
}

const std::vector<double> &
MaintenanceAndReplacementProblemMulti::purchase_cost() const {
  return purchase_cost_;
}

const std::vector<double> &
MaintenanceAndReplacementProblemMulti::trade_in_value() const {
  return trade_in_value_;
}

const std::vector<double> &
MaintenanceAndReplacementProblemMulti::operating_cost() const {
  return operating_cost_;
}
