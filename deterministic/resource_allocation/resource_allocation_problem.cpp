#include "resource_allocation_problem.h"

ResourceAllocationProblem::ResourceAllocationProblem(
    const unsigned int &total_resource, const unsigned int &no_activities,
    const std::vector<std::vector<double>> &reward_matrix)
    : DeterministicDynamicProgram(no_activities,
                                  StateLoop<StateType>(0u, total_resource)),
      reward_matrix_(reward_matrix) {
  assert(reward_matrix.size() == no_activities);
  for (const auto &state : state_loop_)
    set_v0(state, 0.0);
}

double
ResourceAllocationProblem::value_function(const stage &t,
                                          const StateType &state) const {
  return -v(t, state);
}

std::vector<Action<ResourceAllocationProblem::StateType>>
ResourceAllocationProblem::generate_actions(const stage &t,
                                            const StateType &state) const {
  std::vector<Action<StateType>> actions;
  for (auto allocation = 0u;
       allocation <= state && allocation < reward_matrix_[t - 1].size();
       ++allocation) {
    actions.push_back(make_allocation(state, allocation, t));
  }
  return actions;
}

Action<ResourceAllocationProblem::StateType>
ResourceAllocationProblem::make_allocation(const StateType &state,
                                           const unsigned int &allocation,
                                           const stage &t) const {
  return {-reward_matrix_[t - 1][allocation],
          {{state - allocation, 1.0}},
          (int)allocation};
}

const std::vector<std::vector<double>> &
ResourceAllocationProblem::reward_matrix() const {
  return reward_matrix_;
}