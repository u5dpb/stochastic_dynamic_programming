#ifndef RESOURCE_ALLOCATION_PROBLEM_H_
#define RESOURCE_ALLOCATION_PROBLEM_H_

#include <action.h>
#include <deterministic_dynamic_program.h>
#include <state_loop.h>
#include <vector>
#include <cassert>

class ResourceAllocationProblem : public DeterministicDynamicProgram<unsigned int>{

  typedef unsigned int StateType;
public:
  ResourceAllocationProblem(
      const unsigned int &total_resource, const unsigned int &no_activities,
      const std::vector<std::vector<double>> &reward_matrix);

  double value_function(const stage &t, const StateType &state) const override;


protected:
  const std::vector<std::vector<double>> &reward_matrix() const;
  
  virtual std::vector<Action<StateType>>
  generate_actions(const stage &t, const StateType &state) const override;

  Action<StateType> make_allocation(const StateType &state,
                                       const unsigned int &allocation,
                                       const stage &t) const;
private:
  std::vector<std::vector<double>> reward_matrix_;
};

#endif // RESOURCE_ALLOCATION_PROBLEM_H_