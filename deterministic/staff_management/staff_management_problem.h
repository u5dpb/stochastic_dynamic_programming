#ifndef STAFF_MANAGEMENT_PROBLEM_H_
#define STAFF_MANAGEMENT_PROBLEM_H_

#include <deterministic_dynamic_program.h>
#include <state_loop.h>
#include <vector>

class StaffManagementProblem
    : public DeterministicDynamicProgram<unsigned int> {

  typedef unsigned int StateType;

public:
  struct day {
    explicit day(unsigned int v) { number = v; }
    operator unsigned int() { return number; }
    operator const unsigned int() const { return number; }

    unsigned int number;
  };
  StaffManagementProblem(const day &no_days, const unsigned int &no_workers);


  int action_matrix(const day &d, const unsigned int &state) const;
  int action_matrix(const stage &d, const unsigned int &state) const;
  void set_requirements(const std::vector<unsigned int> &requirements);
  void set_hire_cost(const double &hire_cost_fixed,
                     const double &hire_cost_variable);
  void set_wage_cost(const double &wage_cost);

  double value_function(const stage &t, const StateType &state) const override;
  double value_function(const day &d, const StateType &state) const;



protected:
  std::vector<Action<StateType>>
  generate_actions(const stage &t, const StateType &state) const override;
  const std::vector<unsigned int> &requirements() const;

private:
  std::vector<unsigned int> requirements_;
  day no_days_;
  unsigned int no_workers;

  double hire_cost_fixed;
  double hire_cost_variable;
  double wage_cost;
};

#endif // STAFF_MANAGEMENT_PROBLEM_H_S