cmake_minimum_required(VERSION 3.0.0)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(compiler_error_flags -Werror -Wall -Wextra -pedantic-errors -Wdeprecated 
-Wextra-semi -Wconversion -Wno-error=unused-parameter -Wno-error=unused-variable
-Wno-error=unused-but-set-variable -Wno-error=unused-function)
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")

include_directories(../../include)
