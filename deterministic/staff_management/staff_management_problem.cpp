#include "staff_management_problem.h"

StaffManagementProblem::StaffManagementProblem(const day &no_days,
                                               const unsigned int &no_workers)
    : DeterministicDynamicProgram(no_days, StateLoop<StateType>(0, no_workers)),
      no_days_(no_days), no_workers(no_workers) {
  for (const auto &state : state_loop_)
    set_v0(state, 0.0);
  wage_cost = 0.0;
  hire_cost_fixed = 0.0;
  hire_cost_variable = 0.0;
}

void StaffManagementProblem::set_requirements(
    const std::vector<unsigned int> &requirements) {
  this->requirements_ = requirements;
  std::reverse(this->requirements_.begin(), this->requirements_.end());
}

void StaffManagementProblem::set_hire_cost(const double &hire_cost_fixed,
                                           const double &hire_cost_variable) {
  this->hire_cost_fixed = hire_cost_fixed;
  this->hire_cost_variable = hire_cost_variable;
}

void StaffManagementProblem::set_wage_cost(const double &wage_cost) {
  this->wage_cost = wage_cost;
}

double StaffManagementProblem::value_function(const stage &t,
                                              const StateType &state) const {
  return v(t, state);
}

int StaffManagementProblem::action_matrix(const day &d, const unsigned int &state) const
{
  return DeterministicDynamicProgram::action_matrix(stage{no_days_-d+1},state);
}

int StaffManagementProblem::action_matrix(const stage &s, const unsigned int &state) const
{
  return DeterministicDynamicProgram::action_matrix(s,state);
}

double StaffManagementProblem::value_function(const day &d, const StateType &state) const
{
  return v(stage{no_days_-d+1},state);
}

std::vector<Action<StaffManagementProblem::StateType>>
StaffManagementProblem::generate_actions(const stage &t,
                                         const StateType &state) const {
  std::vector<Action<StateType>> actions;
  if (state < requirements_[t - 1]) { // hire
    for (auto n = requirements_[t - 1] - state; state + n <= no_workers; ++n) {
      Action<StateType> action = {hire_cost_fixed +
                                      double(n) * hire_cost_variable +
                                      double(state + n) * wage_cost,
                                  {{state + n, 1.0}},
                                  (int)n};
      actions.push_back(action);
    }
  } else { // fire
    for (auto n = 0u; state - n >= requirements_[t - 1]; ++n) {
      Action<StateType> action = {
          double(state - n) * wage_cost, {{state - n, 1.0}}, -int(n)};
      actions.push_back(action);
    }
  }
  return actions;
}

const std::vector<unsigned int> &StaffManagementProblem::requirements() const {
  return requirements_;
}