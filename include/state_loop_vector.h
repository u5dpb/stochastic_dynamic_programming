#ifndef STATE_LOOP_VECTOR_UNSIGNED_INT_H_
#define STATE_LOOP_VECTOR_UNSIGNED_INT_H_

#include <array>
#include <cassert>
#include <state_loop.h>
#include <vector>

template <class T> class StateLoop<std::vector<T>> {
public:
  struct iterator {
    using iterator_category = std::forward_iterator_tag;
    using difference_type = std::ptrdiff_t;
    using value_type = std::vector<T>;
    using pointer_to_const = std::vector<T> const *;
    using const_pointer_to_const = std::vector<T> const *const;
    using const_reference = const std::vector<T> &;

    iterator(pointer_to_const ptr, const_pointer_to_const begin,
             const_pointer_to_const end)
        : m_ptr(ptr), m_ptr_begin(begin), m_ptr_end(end), middle_value(*begin),
          use_middle_value(false), middle_value_ptr(&middle_value) {}
    iterator(const iterator &other)
        : m_ptr(other.m_ptr), m_ptr_begin(other.m_ptr_begin),
          m_ptr_end(other.m_ptr_end), middle_value(other.middle_value),
          use_middle_value(other.use_middle_value),
          middle_value_ptr(&this->middle_value) {}
    const_reference operator*() const {
      if (use_middle_value)
        return *middle_value_ptr;
      else
        return *m_ptr;
    }
    const_pointer_to_const operator->() {
      if (use_middle_value)
        return middle_value_ptr;
      else
        return m_ptr;
    }

    iterator &operator++() {
      if (m_ptr == m_ptr_begin) {
        m_ptr++;
        middle_value = *(m_ptr - 1);
        use_middle_value = true;
      }
      if (m_ptr != m_ptr_end) {
        ++middle_value[0];
        size_t i = 0;
        while (i < middle_value.size() && middle_value[i] > (*(m_ptr + 1))[i]) {
          middle_value[i] = (*(m_ptr - 1))[i];
          middle_value[++i]++;
        }
        if (i >= (*m_ptr).size()) {
          middle_value = (*(m_ptr + 1));
        }
        if (middle_value == *(m_ptr + 1)) {
          m_ptr++;
          use_middle_value = false;
        }
      } else {
        m_ptr++;
      }
      return *this;
    }

    iterator operator++(int) {
      iterator tmp = *this;
      ++(*this);
      return tmp;
    }

    friend bool operator==(const iterator &a, const iterator &b) {
      return a.m_ptr == b.m_ptr;
    }
    friend bool operator!=(const iterator &a, const iterator &b) {
      return a.m_ptr != b.m_ptr;
    }
    iterator &operator=(iterator other) {
      swap(other);
      return *this;
    }

  private:
    pointer_to_const m_ptr;
    const_pointer_to_const m_ptr_begin;
    const_pointer_to_const m_ptr_end;
    value_type middle_value;
    bool use_middle_value;
    const_pointer_to_const middle_value_ptr;
  };

  iterator begin() const {
    return iterator(&i_value[0], &i_value[0], &i_value[2]);
  }
  iterator end() const {
    return iterator(&i_value[3], &i_value[0], &i_value[2]);
  }

  StateLoop() = delete;
  StateLoop(const std::vector<T> &min, const std::vector<T> &max)
      : min_(min), max_(max), i_value({min, min, max}) {}
  StateLoop(const StateLoop &v) = default;
  ~StateLoop() = default;

  std::vector<T> max() const { return max_; }
  std::vector<T> min() const { return min_; }
  size_t size() const { return size_t(reference(max_) - reference(min_) + 1); }
  size_t reference(const std::vector<T> &value) const {
    size_t ref = 0;
    size_t multiple = 1;
    for (auto i = 0u; i < value.size(); ++i) {
      ref += multiple * size_t(value[i] - min_[i]);
      multiple *= size_t(max_[i] - min_[i]) + 1u;
    }
    return ref;
  }
  bool valid(const std::vector<T> &check_value) const {
    for (auto i = 0u; i < check_value.size(); ++i) {
      if (check_value[i] < min_[i] || check_value[i] > max_[i])
        return false;
    }
    return true;
  }

  StateLoop &operator=(const StateLoop &rhs) = default;

private:
  std::vector<T> min_;
  std::vector<T> max_;
  std::array<std::vector<T>, 3> i_value;
};

#endif // STATE_LOOP_VECTOR_UNSIGNED_INT_H_