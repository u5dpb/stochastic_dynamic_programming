#ifndef STOCHASTIC_DYNAMIC_PROGRAM_H_
#define STOCHASTIC_DYNAMIC_PROGRAM_H_

#include <algorithm>
#include <cassert>
#include <functional>
#include <iostream>
#include <limits>
#include <map>
#include <numeric>
#include <set>
#include <string>
#include <unordered_set>
#include <vector>

#include "action.h"
#include "event.h"
#include "scrolling_array.h"
#include "state_loop.h"
#include "strong_types.h"

template <class State, class CostType = double, class ActionStorageType = int> class StochasticDynamicProgram {

  using ActionType = Action<State>;

public:
  StochasticDynamicProgram() = delete;
  StochasticDynamicProgram(const unsigned int &T,
                           const StateLoop<State> &state_loop);

  virtual CostType value_function(const stage &t, const State &state) const {
    return v(t, state);
  }

  void calculate();
  void calculate(const stage &max_T);
  void calculate(const epsilon &stopping_epsilon);

  ActionStorageType action_matrix(const stage &t, const State &state) const;

  stage T() const { return T_; }

  void set_v0(const State &state, const CostType &value) {
    value_function_[0][state_loop_.reference(state)] = value;
  }

  void set_value_function_size(const size_t &new_size);
  void use_action_matrix(const size_t &new_size);

protected:
  const StateLoop<State> state_loop_;
  CostType v(const stage &t, const State &state) const;
  virtual std::vector<ActionType> generate_actions(const stage &t,
                                                   const State &state) = 0;

private:
  stage T_;
  ScrollingArray<std::vector<CostType>> value_function_;
  ScrollingArray<std::vector<ActionStorageType>> action_matrix_;

  void calculate(std::function<bool(const stage &)> stopping_function);
  void calculate_stage(const stage &t);

  ActionType get_min_cost_action(const std::vector<ActionType> &actions,
                                 const stage &t);
  CostType get_action_cost(const ActionType &action, const stage &t) const;
};

template <class State, class CostType, class ActionStorageType>
StochasticDynamicProgram<State, CostType, ActionStorageType>::StochasticDynamicProgram(
    const unsigned int &T, const StateLoop<State> &state_loop)
    : state_loop_(state_loop), T_(T),
      value_function_(T + 1,
                      std::vector<CostType>(state_loop_.size(),
                                          std::numeric_limits<CostType>::max()),
                      true),
      action_matrix_(
          0,
          std::vector<ActionStorageType>(state_loop_.size(), std::numeric_limits<ActionStorageType>::max()),
          true)
       {}

template <class State, class CostType, class ActionStorageType>
void StochasticDynamicProgram<State,CostType, ActionStorageType>::use_action_matrix(
    const size_t &new_size) {
  action_matrix_.resize(
      new_size,
      std::vector<ActionStorageType>(state_loop_.size(), std::numeric_limits<ActionStorageType>::max()));
}

template <class State, class CostType, class ActionStorageType>
void StochasticDynamicProgram<State,CostType, ActionStorageType>::set_value_function_size(
    const size_t &new_size) {
  value_function_.resize(
      new_size + 1, std::vector<CostType>(state_loop_.size(),
                                        std::numeric_limits<CostType>::max()));
}

template <class State, class CostType, class ActionStorageType>
CostType StochasticDynamicProgram<State, CostType, ActionStorageType>::v(const stage &t,
                                          const State &state) const {
  assert(t <= T_);
  return value_function_[t][state_loop_.reference(state)];
}

template <class State, class CostType, class ActionStorageType>
ActionStorageType StochasticDynamicProgram<State, CostType, ActionStorageType>::action_matrix(const stage &t,
                                                   const State &state) const {
  assert(t <= T_);
  return action_matrix_[t][state_loop_.reference(state)];
}

template <class State, class CostType, class ActionStorageType>
void StochasticDynamicProgram<State, CostType, ActionStorageType>::calculate_stage(const stage &t) {
  for (auto state : state_loop_) {
    std::vector<ActionType> actions = generate_actions(t, state);

    if (actions.size() > 0) {

      ActionType min_cost_action = get_min_cost_action(actions, t);
      value_function_[t][state_loop_.reference(state)] =
          get_action_cost(min_cost_action, t);

      if (action_matrix_.size() > 0)
        action_matrix_[t][state_loop_.reference(state)] = min_cost_action.type;
    }
  }
}

template <class State, class CostType, class ActionStorageType> void StochasticDynamicProgram<State, CostType, ActionStorageType>::calculate() {
  stage max_T = T_;
  calculate(max_T);
}

template <class State, class CostType, class ActionStorageType>
void StochasticDynamicProgram<State, CostType, ActionStorageType>::calculate(

    std::function<bool(const stage &)> stopping_function) {
  T_ = stage(1);
  calculate_stage(T_);
  while (stopping_function(T_)) {
    ++T_;
    calculate_stage(T_);
  }
}

template <class State, class CostType, class ActionStorageType>
void StochasticDynamicProgram<State, CostType, ActionStorageType>::calculate(const stage &max_T) {
  calculate([&max_T](const stage &t) -> bool { return t < max_T; });
}

template <class State, class CostType, class ActionStorageType>
void StochasticDynamicProgram<State, CostType, ActionStorageType>::calculate(
    const epsilon &stopping_epsilon) {
  calculate([this, &stopping_epsilon](const stage &t) -> bool {
    if (t <= 2)
      return true;
    CostType max = 0.0;
    for (auto state : state_loop_)
      max = std::max(
          max,
          std::abs(
              (value_function_[t][state_loop_.reference(state)] / CostType(t)) -
              (value_function_[stage(t - 1)][state_loop_.reference(state)] /
               CostType(t - 1))));
    return max >= stopping_epsilon;
  });
}

template <class State, class CostType, class ActionStorageType>
Action<State> StochasticDynamicProgram<State, CostType, ActionStorageType>::get_min_cost_action(
    const std::vector<ActionType> &actions, const stage &t) {
  return *std::min_element(
      actions.begin(), actions.end(), [&](ActionType a, ActionType b) {
        return get_action_cost(a, t) < get_action_cost(b, t);
      });
}

template <class State, class CostType, class ActionStorageType>
CostType
StochasticDynamicProgram<State, CostType, ActionStorageType>::get_action_cost(const ActionType &action,
                                                 const stage &t) const {
  CostType cost = action.cost;
  for (auto i = 0u; i < action.subsequent_state.size(); ++i)
    cost += action.subsequent_state[i].probability *
            value_function_[stage(t - 1)][state_loop_.reference(
                action.subsequent_state[i].state)];
  return cost;
}

#endif // STOCHASTIC_DYNAMIC_PROGRAM_H_