#ifndef SCROLLING_ARRAY_H_
#define SCROLLING_ARRAY_H_

#include <vector>
#include <string>
#include <cassert>

template <class T> class ScrollingArray {
public:
  ScrollingArray() {
    head_ = 0;
    size_ = 0;
  }
  ScrollingArray(std::size_t s, bool auto_scroll = false)
      : size_(s), array(s), head_(s), auto_scroll(auto_scroll) {}
  ScrollingArray(std::size_t s, const T &initial, bool auto_scroll = false)
      : size_(s), array(s, initial), head_(s), auto_scroll(auto_scroll) {}
  std::size_t head() const { return head_; }
  std::size_t size() const { return size_; }

  void resize(const std::size_t size) {
    head_ += size - size_;
    size_ = size;
    array.resize(size);
  }

  void resize(const std::size_t size, const T &initial) {
    head_ += size - size_;
    size_ = size;
    array.resize(size, initial);
  }

  T &operator[](std::size_t index) {
    while (auto_scroll && index >= head_)
      head_++;
    assert(valid_index(index));
    return array[index % size_];
  }

  const T &operator[](std::size_t index) const {
    assert(valid_index(index));
    return array[index % size_];
  }

  ScrollingArray &operator++() {
    ++head_;
    return *this;
  }
  
  bool valid_index(std::size_t index) const {
    return index < head_ && index >= head_ - size_;
  }

private:
  std::size_t size_;
  std::vector<T> array;
  std::size_t head_;

  bool auto_scroll;

};

#endif // SCROLLING_ARRAY_H_