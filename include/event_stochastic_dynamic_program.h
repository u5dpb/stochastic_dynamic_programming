#ifndef EVENT_STOCHASTIC_DYNAMIC_PROGRAM_H_
#define EVENT_STOCHASTIC_DYNAMIC_PROGRAM_H_

#include <algorithm>
#include <cassert>
#include <functional>
#include <iostream>
#include <limits>
#include <map>
#include <numeric>
#include <set>
#include <string>
#include <unordered_set>
#include <vector>

#include "action.h"
#include "event.h"
#include "scrolling_array.h"
#include "state_loop.h"
#include "strong_types.h"

template <class State, class CostType = double, class ActionStorageType = int,
          class EventStorageType = int>
class EventStochasticDynamicProgram {

  using ActionType = Action<State>;

public:
  EventStochasticDynamicProgram() = delete;
  EventStochasticDynamicProgram(const unsigned int &T,
                                const StateLoop<State> &state_loop);

  virtual double value_function(const stage &t, const State &state) const {
    return v(t, state);
  }

  void calculate();
  void calculate(const stage &max_T);
  void calculate(const epsilon &stopping_epsilon);

  ActionStorageType event_action_matrix(const stage &t, const State &state,
                                        const EventStorageType &event_type) const;

  stage T() const { return T_; }

  void set_v0(const State &state, const CostType &value) {
    value_function_[0][state_loop_.reference(state)] = value;
  }

  void set_value_function_size(const size_t &new_size);
  void use_event_action_matrix(const size_t &new_size);
  void use_event_action_matrix(
      const size_t &new_size,
      const std::unordered_set<EventStorageType> &event_type_to_store);

protected:
  CostType v(const stage &t, const State &state) const;

  virtual std::vector<Event<State>>
  generate_events(const stage &t, const State &state) const = 0;

  const StateLoop<State> state_loop_;

private:
  stage T_;
  ScrollingArray<std::vector<CostType>> value_function_;

  ScrollingArray<std::vector<std::map<EventStorageType, ActionStorageType>>>
      event_action_matrix_;
  std::unordered_set<EventStorageType> event_type_to_store;

  void calculate(std::function<bool(const stage &)> stopping_function);
  void calculate_stage(const stage &t);

  ActionType get_min_cost_action(const std::vector<ActionType> &actions,
                                 const stage &t) const;
  CostType get_action_cost(const ActionType &action, const stage &t) const;
};

template <class State, class CostType, class ActionStorageType,
          class EventStorageType>
EventStochasticDynamicProgram<State, CostType, ActionStorageType,
                              EventStorageType>::
    EventStochasticDynamicProgram(const unsigned int &T,
                                  const StateLoop<State> &state_loop)
    : state_loop_(state_loop), T_(T),
      value_function_(
          T + 1,
          std::vector<CostType>(state_loop_.size(),
                                std::numeric_limits<CostType>::max()),
          true),
      event_action_matrix_(
          0,
          std::vector<std::map<EventStorageType, ActionStorageType>>(
              state_loop_.size(),
              std::map<EventStorageType, ActionStorageType>()),
          true) {}

template <class State, class CostType, class ActionStorageType,
          class EventStorageType>
void EventStochasticDynamicProgram<
    State, CostType, ActionStorageType,
    EventStorageType>::use_event_action_matrix(const size_t &new_size) {
  event_action_matrix_.resize(
      new_size,
      std::vector<std::map<EventStorageType, ActionStorageType>>(
          state_loop_.size(), std::map<EventStorageType, ActionStorageType>()));
}

template <class State, class CostType, class ActionStorageType,
          class EventStorageType>
void EventStochasticDynamicProgram<State, CostType, ActionStorageType,
                                   EventStorageType>::
    use_event_action_matrix(
        const size_t &new_size,
        const std::unordered_set<EventStorageType> &event_type_to_store) {
  event_action_matrix_.resize(
      new_size,
      std::vector<std::map<EventStorageType, ActionStorageType>>(
          state_loop_.size(), std::map<EventStorageType, ActionStorageType>()));
  this->event_type_to_store = event_type_to_store;
}

template <class State, class CostType, class ActionStorageType,
          class EventStorageType>
void EventStochasticDynamicProgram<
    State, CostType, ActionStorageType,
    EventStorageType>::set_value_function_size(const size_t &new_size) {
  value_function_.resize(
      new_size + 1,
      std::vector<CostType>(state_loop_.size(),
                            std::numeric_limits<CostType>::max()));
}

template <class State, class CostType, class ActionStorageType,
          class EventStorageType>
CostType
EventStochasticDynamicProgram<State, CostType, ActionStorageType,
                              EventStorageType>::v(const stage &t,
                                                   const State &state) const {
  assert(t <= T_);
  return value_function_[t][state_loop_.reference(state)];
}

template <class State, class CostType, class ActionStorageType,
          class EventStorageType>
ActionStorageType EventStochasticDynamicProgram<
    State, CostType, ActionStorageType,
    EventStorageType>::event_action_matrix(const stage &t, const State &state,
                                           const EventStorageType &event_type)
    const {
  assert(t <= T_);
  assert(event_type_to_store.size() == 0 ||
         event_type_to_store.find(event_type) != event_type_to_store.end());
  return event_action_matrix_[t][state_loop_.reference(state)].at(event_type);
}

template <class State, class CostType, class ActionStorageType,
          class EventStorageType>
void EventStochasticDynamicProgram<
    State, CostType, ActionStorageType, EventStorageType>::calculate_stage(const stage &t) {

  for (const auto &state : state_loop_) {
    value_function_[t][state_loop_.reference(state)] = 0.0;

    const auto events = generate_events(t, state);

    if (events.size() == 0u)
      value_function_[t][state_loop_.reference(state)] =
          std::numeric_limits<CostType>::max();
    for (const auto &event : events) {
      if (event.subsequent_actions.size() > 0) {
        auto min_cost_action = get_min_cost_action(event.subsequent_actions, t);
        value_function_[t][state_loop_.reference(state)] +=

            event.probability * get_action_cost(min_cost_action, t);
        if (event_action_matrix_.size() > 0 &&
            (event_type_to_store.size() == 0 ||
             event_type_to_store.find(event.type) != event_type_to_store.end()))
          event_action_matrix_[t][state_loop_.reference(state)][event.type] =
              min_cost_action.type;
      }
    }
  }
}

template <class State, class CostType, class ActionStorageType,
          class EventStorageType>
void EventStochasticDynamicProgram<State, CostType,
                                   ActionStorageType, EventStorageType>::calculate() {
  // note: this variable is required as T_ will be reset and incremented
  //       and max_T will be used to evaluate the stopping condition
  const auto max_T = T_;
  calculate(max_T);
}

template <class State, class CostType, class ActionStorageType,
          class EventStorageType>
void EventStochasticDynamicProgram<State, CostType, ActionStorageType,
                                   EventStorageType>::
    calculate(std::function<bool(const stage &)> stopping_function) {
  T_ = stage(1);
  calculate_stage(T_);
  while (stopping_function(T_)) {
    ++T_;
    calculate_stage(T_);
  }
}

template <class State, class CostType, class ActionStorageType,
          class EventStorageType>
void EventStochasticDynamicProgram<
    State, CostType, ActionStorageType, EventStorageType>::calculate(const stage &max_T) {

  calculate([&max_T](const stage &t) -> bool { return t < max_T; });
}

template <class State, class CostType, class ActionStorageType,
          class EventStorageType>
Action<State> EventStochasticDynamicProgram<State, CostType, ActionStorageType,
                                            EventStorageType>::
    get_min_cost_action(const std::vector<ActionType> &actions,
                        const stage &t) const {
  return *std::min_element(actions.begin(), actions.end(),
                           [&](const ActionType &a, const ActionType &b) {
                             return get_action_cost(a, t) <
                                    get_action_cost(b, t);
                           });
}

template <class State, class CostType, class ActionStorageType,
          class EventStorageType>
void EventStochasticDynamicProgram<
    State, CostType, ActionStorageType,
    EventStorageType>::calculate(const epsilon &stopping_epsilon) {
  calculate([this, &stopping_epsilon](const stage &t) -> bool {
    if (t <= 2)
      return true;
    CostType max = 0.0;
    for (const auto &state : state_loop_)
      max = std::max(max, std::abs((v(t, state) / CostType(t)) -
                                   (v(stage(t - 1), state) / CostType(t - 1))));
    return max >= stopping_epsilon;
  });
}

template <class State, class CostType, class ActionStorageType,
          class EventStorageType>
CostType EventStochasticDynamicProgram<
    State, CostType, ActionStorageType,
    EventStorageType>::get_action_cost(const ActionType &action,
                                       const stage &t) const {
  CostType cost = action.cost;
  for (const auto &subsequent_state : action.subsequent_state)
    cost +=
        subsequent_state.probability * v(stage(t - 1), subsequent_state.state);
  return cost;
}

#endif // EVENT_STOCHASTIC_DYNAMIC_PROGRAM_H_