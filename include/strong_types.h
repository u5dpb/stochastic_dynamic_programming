#ifndef STRONG_TYPES_H_
#define STRONG_TYPES_H_

struct stage{
  explicit stage(const unsigned int value) { t = value; }
  operator unsigned int() { return t; }
  operator const unsigned int() const { return t; }

  stage &operator++() {
    ++t;
    return *this;
  }
  stage operator++(int) {
    stage temp = *this;
    ++*this;
    return temp;
  }

  unsigned int t;
};

struct epsilon{
  explicit epsilon(const double v) {  value = v; }
  operator double() const { return value; }
  double value;
};

#endif // STRONG_TYPES_H_