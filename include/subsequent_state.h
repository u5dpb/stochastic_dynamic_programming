#ifndef SUBSEQUENT_STATE_H_
#define SUBSEQUENT_STATE_H_

template <class State> struct SubsequentState {
  State state;
  double probability;
};

#endif // SUBSEQUENT_STATE_H_