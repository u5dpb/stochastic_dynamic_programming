#ifndef DETERMINISTIC_DYNAMIC_PROGRAM_H_
#define DETERMINISTIC_DYNAMIC_PROGRAM_H_

#include <algorithm>
#include <functional>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

#include "action.h"
#include "scrolling_array.h"
#include "state_loop.h"
#include "strong_types.h"

template <class State, class CostType = double, class ActionStorageType = int>
class DeterministicDynamicProgram {

  using ActionType = Action<State,CostType,ActionStorageType>;

public:
  DeterministicDynamicProgram() = delete;
  DeterministicDynamicProgram(const unsigned int &T,
                              const StateLoop<State> &state_loop);

  virtual CostType value_function(const stage &t, const State &state) const = 0;

  bool valid(const stage &t, const State &state) const;
  bool valid(const stage &t) const;

  void calculate();
  void calculate(const stage &new_T);
  void calculate(const epsilon &stopping_epsilon);

  ActionStorageType action_matrix(const stage &t, const State &state) const;

  void set_v0(const State &state, const CostType &value) {
    value_function_[0][state_loop_.reference(state)] = value;
  }

  void set_value_function_size(const size_t new_size);
  void use_action_matrix(const size_t new_size);
  void use_action_matrix();

  stage T() const { return T_; }

protected:
  CostType v(const stage &t, const State &state) const;
  virtual std::vector<ActionType> generate_actions(const stage &t,
                                                   const State &) const = 0;
  const StateLoop<State> state_loop_;

private:
  stage T_;
  ScrollingArray<std::vector<CostType>> value_function_;

  bool generate_action_matrix;
  ScrollingArray<std::vector<ActionStorageType>> action_matrix_;
  CostType get_action_cost(const ActionType &action, const stage &t) const;

  void calculate_stage(const stage &t);
  void calculate(std::function<bool(const stage &)> stopping_function);
};

template <class State, class CostType, class ActionStorageType>
DeterministicDynamicProgram<State, CostType, ActionStorageType>::
    DeterministicDynamicProgram(const unsigned int &T,
                                const StateLoop<State> &state_loop)
    : state_loop_(state_loop), T_(T),
      value_function_(
          T + 1,
          std::vector<CostType>(state_loop_.size(),
                                std::numeric_limits<CostType>::max()),
          true),
      action_matrix_(
          ActionStorageType(),
          std::vector<ActionStorageType>(state_loop_.size(),
                           std::numeric_limits<ActionStorageType>::max()),
          true) {}

template <class State, class CostType, class ActionStorageType>
void DeterministicDynamicProgram<State, CostType, ActionStorageType>::
    set_value_function_size(const size_t new_size) {
  value_function_.resize(
      new_size + 1, std::vector<CostType>(state_loop_.size(),
                                        std::numeric_limits<CostType>::max()));
}

template <class State, class CostType, class ActionStorageType>
void DeterministicDynamicProgram<State, CostType, ActionStorageType>::
    use_action_matrix(const size_t new_size) {

  action_matrix_.resize(
      new_size,
      std::vector<ActionStorageType>(state_loop_.size(), std::numeric_limits<ActionStorageType>::max()));
}

template <class State, class CostType, class ActionStorageType>
void DeterministicDynamicProgram<State, CostType,
                                 ActionStorageType>::use_action_matrix() {
  use_action_matrix(T_);
}

template <class State, class CostType, class ActionStorageType>
CostType DeterministicDynamicProgram<State, CostType, ActionStorageType>::v(
    const stage &t, const State &state) const {
  return value_function_[t][state_loop_.reference(state)];
}

template <class State, class CostType, class ActionStorageType>
bool DeterministicDynamicProgram<State, CostType, ActionStorageType>::valid(
    const stage &t, const State &state) const {
  return state_loop_.valid(state) && value_function_.valid_index(t) &&
         value_function_[t][state_loop_.reference(state)] <
             std::numeric_limits<CostType>::max();
}

template <class State, class CostType, class ActionStorageType>
bool DeterministicDynamicProgram<State, CostType, ActionStorageType>::valid(
    const stage &t) const {
  return value_function_.valid_index(t);
}

template <class State, class CostType, class ActionStorageType>
ActionStorageType DeterministicDynamicProgram<State, CostType, ActionStorageType>::
    action_matrix(const stage &t, const State &state) const {
  return action_matrix_[t][state_loop_.reference(state)];
}

template <class State, class CostType, class ActionStorageType>
void DeterministicDynamicProgram<
    State, CostType, ActionStorageType>::calculate_stage(const stage &t) {
  for (auto state : state_loop_) {
    value_function_[t][state_loop_.reference(state)] =
        std::numeric_limits<CostType>::max();
    std::vector<ActionType> actions = generate_actions(t, state);
    if (actions.size() > 0) {

      ActionType min_cost_action = *std::min_element(
          actions.begin(), actions.end(), [=](ActionType a, ActionType b) {
            return get_action_cost(a, t) < get_action_cost(b, t);
          });
      value_function_[t][state_loop_.reference(state)] =
          get_action_cost(min_cost_action, t);

      if (action_matrix_.size() > 0)
        action_matrix_[t][state_loop_.reference(state)] = min_cost_action.type;
    }
  }
}

template <class State, class CostType, class ActionStorageType>
void DeterministicDynamicProgram<State, CostType,
                                 ActionStorageType>::calculate() {
  stage max_T = T_;
  calculate(max_T);
}

template <class State, class CostType, class ActionStorageType>
void DeterministicDynamicProgram<State, CostType, ActionStorageType>::calculate(
    std::function<bool(const stage &)> stopping_function) {
  T_ = stage(1);
  calculate_stage(T_);
  while (stopping_function(T_)) {
    ++T_;
    calculate_stage(T_);
  }
}

template <class State, class CostType, class ActionStorageType>
void DeterministicDynamicProgram<State, CostType, ActionStorageType>::calculate(
    const stage &max_T) {
  calculate([&max_T](const stage &t) -> bool { return t < max_T; });
}

template <class State, class CostType, class ActionStorageType>
void DeterministicDynamicProgram<State, CostType, ActionStorageType>::calculate(
    const epsilon &stopping_epsilon) {
  calculate([this, &stopping_epsilon](const stage &t) -> bool {
    if (t <= 2)
      return true;
    CostType max = 0.0;
    for (const auto &state : state_loop_)
      max = std::max(max, std::abs((v(t, state) / CostType(t)) -
                                   (v(stage(t - 1), state) / CostType(t - 1))));
    return max >= stopping_epsilon;
  });
}

template <class State, class CostType, class ActionStorageType>
CostType DeterministicDynamicProgram<State, CostType, ActionStorageType>::
    get_action_cost(const ActionType &action, const stage &t) const {
  return action.cost + v(stage(t - 1), action.subsequent_state[0].state);
}

#endif // DETERMINISTIC_DYNAMIC_PROGRAM_H_