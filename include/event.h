#ifndef EVENT_H_
#define EVENT_H_

#include <vector>

template<class State, class EventStorageType = int>
struct Event {
  double probability;
  std::vector<Action<State>> subsequent_actions;
  EventStorageType type;
};
#endif // EVENT_H_