#ifndef ACTION_H_
#define ACTION_H_

#include <subsequent_state.h>
#include <vector>

template <class State, class CostType = double, class ActionRecordType = int> struct Action {
  CostType cost;
  std::vector<SubsequentState<State>> subsequent_state;
  ActionRecordType type;
};
#endif // ACTION_H_