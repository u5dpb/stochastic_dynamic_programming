
#ifndef STATE_LOOP_H_
#define STATE_LOOP_H_

#include <array>
#include <cassert>
#include <iterator>
#include <stddef.h>

template <class T> class StateLoop {

public:
  struct iterator {
    using iterator_category = std::forward_iterator_tag;
    using difference_type = std::ptrdiff_t;
    using value_type = T;
    using pointer = T const *;
    using const_pointer = T const *const;
    using const_reference = const T &;

    iterator(pointer ptr, const_pointer begin, const_pointer end)
        : m_ptr(ptr), m_ptr_begin(begin), m_ptr_end(end), middle_value(*begin),
          use_middle_value(false), middle_value_ptr(&middle_value) {}

    const_reference operator*() const {
      if (use_middle_value)
        return *middle_value_ptr;
      else
        return *m_ptr;
    }
    const_pointer operator->() {
      if (use_middle_value)
        return middle_value_ptr;
      else
        return m_ptr;
    }

    iterator &operator++() {
      if (m_ptr == m_ptr_begin) {
        m_ptr++;
        middle_value = *(m_ptr - 1);
        use_middle_value = true;
      }
      if (m_ptr != m_ptr_end) {
        ++middle_value;
        if (middle_value == *(m_ptr + 1)) {
          m_ptr++;
          use_middle_value = false;
        }
      } else {
        m_ptr++;
      }
      return *this;
    }

    iterator operator++(int) {
      iterator tmp = *this;
      ++(*this);
      return tmp;
    }

    friend bool operator==(const iterator &a, const iterator &b) {
      return a.m_ptr == b.m_ptr;
    }
    friend bool operator!=(const iterator &a, const iterator &b) {
      return a.m_ptr != b.m_ptr;
    }

  private:
    pointer m_ptr;
    const_pointer m_ptr_begin;
    const_pointer m_ptr_end;
    value_type middle_value;
    bool use_middle_value;
    const_pointer middle_value_ptr;
  };

  iterator begin() const {
    return iterator(&i_value[0], &i_value[0], &i_value[2]);
  }
  iterator end() const {
    return iterator(&i_value[3], &i_value[0], &i_value[2]);
  }

  StateLoop() = delete;
  StateLoop(const T &min, const T &max)
      : min_(min), max_(max), i_value({min, min, max}) {}
  StateLoop(const StateLoop &v) = default;
  ~StateLoop() = default;

  T max() const { return max_; }
  T min() const { return min_; }
  bool valid(T check_value) const {
    return check_value >= min_ && check_value <= max_;
  }
  size_t size() const { return size_t(reference(max_) - reference(min_) + 1); }
  size_t reference(const T &value) const { return size_t(value - min_); }

  StateLoop &operator=(const StateLoop &rhs) = default;

private:
  T min_;
  T max_;
  std::array<T, 3> i_value;
};

#endif // STATE_LOOP_H_