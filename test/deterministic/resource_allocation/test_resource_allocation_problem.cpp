#include <gtest/gtest.h>
#include <limits>
#include <vector>

#include <resource_allocation_problem.h>
#include <state_loop.h>

TEST(ResourceAllocationProblem, UnusedResourcesHaveNoValue) {
  ResourceAllocationProblem problem(5u, 1u, {{0, 1, 2, 2, 2}}); //
  EXPECT_EQ(problem.value_function(stage(0), 0), 0);
  EXPECT_EQ(problem.value_function(stage(0), 1), 0);
  EXPECT_EQ(problem.value_function(stage(0), 2), 0);
  EXPECT_EQ(problem.value_function(stage(0), 3), 0);
  EXPECT_EQ(problem.value_function(stage(0), 4), 0);
  EXPECT_EQ(problem.value_function(stage(0), 5), 0);
}

TEST(ResourceAllocationProblem, FinalAllocationIsCorrect) {
  ResourceAllocationProblem problem(5u, 1u, {{0, 1, 2, 2, 2, 2}}); //
  EXPECT_NO_THROW(problem.calculate());
  EXPECT_EQ(problem.value_function(stage(1), 0), 0);
  EXPECT_EQ(problem.value_function(stage(1), 1), 1);
  EXPECT_EQ(problem.value_function(stage(1), 2), 2);
  EXPECT_EQ(problem.value_function(stage(1), 3), 2);
  EXPECT_EQ(problem.value_function(stage(1), 4), 2);
  EXPECT_EQ(problem.value_function(stage(1), 5), 2);
}

TEST(ResourceAllocationProblem, FewerResourcesThanInRewardMatrix) {
  ResourceAllocationProblem problem(3u, 1u, {{0, 1, 2, 2, 2, 2}}); //
  EXPECT_NO_THROW(problem.calculate());
  EXPECT_EQ(problem.value_function(stage(1), 0), 0);

  EXPECT_EQ(problem.value_function(stage(1), 1), 1);
  EXPECT_EQ(problem.value_function(stage(1), 2), 2);
  EXPECT_EQ(problem.value_function(stage(1), 3), 2);
}

TEST(ResourceAllocationProblem, MoreResourcesThanInRewardMatrix) {
  ResourceAllocationProblem problem(5u, 1u, {{0, 1, 2}}); //
  EXPECT_NO_THROW(problem.calculate());
  EXPECT_EQ(problem.value_function(stage(1), 0), 0);

  EXPECT_EQ(problem.value_function(stage(1), 1), 1);
  EXPECT_EQ(problem.value_function(stage(1), 2), 2);
  EXPECT_EQ(problem.value_function(stage(1), 3), 2);
  EXPECT_EQ(problem.value_function(stage(1), 4), 2);
  EXPECT_EQ(problem.value_function(stage(1), 5), 2);
}

TEST(ResourceAllocationProblem, TwoStages) {
  ResourceAllocationProblem problem(5u, 2u,
                                    {
                                        {0, 1, 2, 2, 2, 2}, //
                                        {0, 2, 4, 4, 4, 4}  //
                                    });                     //
  EXPECT_NO_THROW(problem.calculate());
  EXPECT_EQ(problem.value_function(stage(2), 0), 0);
  EXPECT_EQ(problem.value_function(stage(2), 1), 2);
  EXPECT_EQ(problem.value_function(stage(2), 2), 4);
  EXPECT_EQ(problem.value_function(stage(2), 3), 5);
  EXPECT_EQ(problem.value_function(stage(2), 4), 6);
  EXPECT_EQ(problem.value_function(stage(2), 5), 6);
}

TEST(ResourceAllocationProblem, ThreeStages) {
  ResourceAllocationProblem problem(7u, 3u,
                                    {
                                        {0, 1, 2},   //
                                        {0, 2, 4},   //
                                        {0, 1, 3, 5} //
                                    });              //
  EXPECT_NO_THROW(problem.calculate());
  EXPECT_EQ(problem.value_function(stage(1), 0), 0);
  EXPECT_EQ(problem.value_function(stage(1), 1), 1);
  EXPECT_EQ(problem.value_function(stage(1), 2), 2);

  EXPECT_EQ(problem.value_function(stage(2), 0), 0);
  EXPECT_EQ(problem.value_function(stage(2), 1), 2);
  EXPECT_EQ(problem.value_function(stage(2), 2), 4);
  EXPECT_EQ(problem.value_function(stage(2), 3), 5);
  EXPECT_EQ(problem.value_function(stage(2), 4), 6);

  EXPECT_EQ(problem.value_function(stage(3), 0), 0);
  EXPECT_EQ(problem.value_function(stage(3), 1), 2);
  EXPECT_EQ(problem.value_function(stage(3), 2), 4);
  EXPECT_EQ(problem.value_function(stage(3), 3), 5);
  EXPECT_EQ(problem.value_function(stage(3), 4), 7);
  EXPECT_EQ(problem.value_function(stage(3), 5), 9);
  EXPECT_EQ(problem.value_function(stage(3), 6), 10);
  EXPECT_EQ(problem.value_function(stage(3), 7), 11);
}

TEST(ResourceAllocationProblem, ThreeStagesActionMatrix) {
  ResourceAllocationProblem problem(7u, 3u,
                                    {
                                        {0, 1, 2},   //
                                        {0, 2, 4},   //
                                        {0, 1, 3, 5} //
                                    });              //
  problem.use_action_matrix();
  EXPECT_NO_THROW(problem.calculate());
  EXPECT_EQ(problem.action_matrix(stage(1), 0), 0);
  EXPECT_EQ(problem.action_matrix(stage(1), 1), 1);
  EXPECT_EQ(problem.action_matrix(stage(1), 2), 2);
  EXPECT_EQ(problem.action_matrix(stage(2), 0), 0);
  EXPECT_EQ(problem.action_matrix(stage(2), 1), 1);
  EXPECT_EQ(problem.action_matrix(stage(2), 2), 2);
  EXPECT_EQ(problem.action_matrix(stage(2), 3), 2);
  EXPECT_EQ(problem.action_matrix(stage(2), 4), 2);
  EXPECT_EQ(problem.action_matrix(stage(3), 0), 0);
  EXPECT_EQ(problem.action_matrix(stage(3), 1), 0);
  EXPECT_EQ(problem.action_matrix(stage(3), 2), 0);
  // EXPECT_EQ(problem.action_matrix(stage(3), 3), 1); // tie break
  EXPECT_EQ(problem.action_matrix(stage(3), 4), 2);
  EXPECT_EQ(problem.action_matrix(stage(3), 5), 3);
  EXPECT_EQ(problem.action_matrix(stage(3), 6), 3);
  EXPECT_EQ(problem.action_matrix(stage(3), 7), 3);
}