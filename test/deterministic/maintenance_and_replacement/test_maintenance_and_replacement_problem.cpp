#include <gtest/gtest.h>
#include <limits>
#include <state_loop.h>
#include <vector>

#include <maintenance_and_replacement_problem.h>

double epsilon_value = 0.01;

TEST(MaintenanceAndReplacementProblem, SalvageValueCorrect) {
  MaintenanceAndReplacementProblem problem(0u, 7u);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 25, 17, 8, 0, 0, 0, 0};
  problem.set_salvage_value(salvage_value);
  EXPECT_NEAR(problem.value_function(stage(0u), 1u), -salvage_value[1], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(0u), 2u), -salvage_value[2], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(0u), 3u), -salvage_value[3], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(0u), 4u), -salvage_value[4], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(0u), 5u), -salvage_value[5], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(0u), 6u), -salvage_value[6], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(0u), 7u), -salvage_value[7], epsilon_value);
}

TEST(MaintenanceAndReplacementProblem, UncalculatedValuesCorrect) {
  MaintenanceAndReplacementProblem problem(1u, 7u);
  std::vector<double> salvage_value = {0, 25, 17, 8, 0, 0, 0, 0};
  problem.set_salvage_value(salvage_value);
  EXPECT_NEAR(problem.value_function(stage(1u), 1u), std::numeric_limits<double>::max(),
              epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(1u), 7u), std::numeric_limits<double>::max(),
              epsilon_value);
}

TEST(MaintenanceAndReplacementProblem, TRangeThrows) {
  MaintenanceAndReplacementProblem problem(3u, 7u);
  EXPECT_NO_THROW(problem.value_function(stage(3u), 1u));
  EXPECT_DEATH(problem.value_function(stage(4u), 1u), "Assertion.*failed");
}

TEST(MaintenanceAndReplacementProblem, NoCostsChooseMinSalvageCostBottom) {
  MaintenanceAndReplacementProblem problem(1u, 7u);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 25, 17, 8, 0, 0, 0, 0};
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(0);
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_operating_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.calculate();
  for (auto i = 1u; i <= 7u; ++i) {
    EXPECT_NEAR(problem.value_function(stage(1u), i), -salvage_value[1], epsilon_value);
  }
}

TEST(MaintenanceAndReplacementProblem, NoCostsChooseMinSalvageCostOlder) {
  MaintenanceAndReplacementProblem problem(1u, 7u);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 1, 2, 3, 4, 5, 6, 7};
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(0);
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_operating_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(1u), 1u), -salvage_value[2u], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(1u), 2u), -salvage_value[3u], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(1u), 3u), -salvage_value[4u], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(1u), 4u), -salvage_value[5u], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(1u), 5u), -salvage_value[6u], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(1u), 6u), -salvage_value[7u], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(1u), 7u), -salvage_value[1u], epsilon_value);
}

TEST(MaintenanceAndReplacementProblem,
     NoCostsChooseMinSalvageCostOlderTwoStage) {
  MaintenanceAndReplacementProblem problem(2u, 7u);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 1, 2, 3, 4, 5, 6, 7};
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(0);
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_operating_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(2), 1), -salvage_value[3], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(2), 2), -salvage_value[4], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(2), 3), -salvage_value[5], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(2), 4), -salvage_value[6], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(2), 5), -salvage_value[7], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(2), 7), -salvage_value[2], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(2), 6), -salvage_value[2], epsilon_value);
}

TEST(MaintenanceAndReplacementProblem,
     NoCostsChooseMinSalvageCostOlderThreeStage) {
  MaintenanceAndReplacementProblem problem(3, 7);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 1, 2, 3, 4, 5, 6, 7};
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(0);
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_operating_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(3), 1), -salvage_value[4], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(3), 2), -salvage_value[5], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(3), 3), -salvage_value[6], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(3), 4), -salvage_value[7], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(3), 7), -salvage_value[3], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(3), 6), -salvage_value[3], epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(3), 5), -salvage_value[3], epsilon_value);
}

TEST(MaintenanceAndReplacementProblem, PurchaseCostOnlyThreeStage) {
  auto oldest_state = 7u;
  MaintenanceAndReplacementProblem problem(3, oldest_state);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 4, 3, 2, 1, 0, 0, 0};
  double purchase_cost = 5;
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(purchase_cost);
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_operating_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.calculate();
  //    t = 3               t = 2               t = 1             t = 0
  //
  // 7  5-2= 3              5-3= 2              5-4= 1              0
  // 6  min{5-2,2}= 2       min{5-3,1}= 1       min{5-4,0}= 0       0
  // 5  min{5-2,1}= 1       min{5-3,0}= 0       min{5-4,0}= 0       0
  // 4  min{5-2,0}= 0       min{5-3,0}= 0       min{5-4,0}= 0       -1
  // 3  min{5-2,0}= 0       min{5-3,0}= 0       min{5-4,-1}= -1     -2
  // 2  min{5-2,0}= 0       min{5-3,-1}= -1     min{5-4,-2}= -2     -3
  // 1  min{5-2,-1}= -1     min{5-3,-2}= -2     min{5-4,-3}= -3     -4
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(1), i),
                std::min(purchase_cost + problem.value_function(stage(0), 1),
                         problem.value_function(stage(0), i + 1)),
                epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(1), 7), purchase_cost + problem.value_function(stage(0), 1),
              epsilon_value);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(2), i),
                std::min(purchase_cost + problem.value_function(stage(1), 1),
                         problem.value_function(stage(1), i + 1)),
                epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(2), 7), purchase_cost + problem.value_function(stage(1), 1),
              epsilon_value);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(3), i),
                std::min(purchase_cost + problem.value_function(stage(2), 1),
                         problem.value_function(stage(2), i + 1)),
                epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(3), 7), purchase_cost + problem.value_function(stage(2), 1),
              epsilon_value);
}

TEST(MaintenanceAndReplacementProblem, PurchaseCostAndTradeInCostThreeStage) {
  auto oldest_state = 7u;
  MaintenanceAndReplacementProblem problem(3, oldest_state);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 4, 3, 2, 1, 0, 0, 0};
  std::vector<double> trade_in_value = {
      std::numeric_limits<double>::max(), 5, 4, 3, 2, 1, 0, 0};
  double purchase_cost = 6;
  problem.set_salvage_value(salvage_value);
  problem.set_trade_in_value(trade_in_value);
  problem.set_purchase_cost(purchase_cost);
  problem.set_operating_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.calculate();
  //    t = 3               t = 2               t = 1             t = 0
  //
  // 7  6-0-2= 4              6-0-3= 3              6-0-4= 2              0
  // 6  min{6-0-2,3}= 3       min{6-0-3,2}= 2       min{6-0-4,0}= 0       0
  // 5  min{6-1-2,2}= 2       min{6-1-3,0}= 0       min{6-1-4,0}= 0       0
  // 4  min{6-2-2,0}= 0       min{6-2-3,0}= 0       min{6-2-4,0}= 0       -1
  // 3  min{6-3-2,0}= 0       min{6-3-3,0}= 0       min{6-3-4,-1}= -1     -2
  // 2  min{6-4-2,0}= 0       min{6-4-3,-1}= -1     min{6-4-4,-2}= -2     -3
  // 1  min{6-5-2,-1}= -1     min{6-5-3,-2}= -2     min{6-5-4,-3}= -3     -4
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(
        problem.value_function(stage(1), i),
        std::min(purchase_cost - trade_in_value[i] + problem.value_function(stage(0), 1),
                 problem.value_function(stage(0), i + 1)),
        epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(1), 7),
              purchase_cost - trade_in_value[7] + problem.value_function(stage(0), 1),
              epsilon_value);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(
        problem.value_function(stage(2), i),
        std::min(purchase_cost - trade_in_value[i] + problem.value_function(stage(1), 1),
                 problem.value_function(stage(1), i + 1)),
        epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(2), 7),
              purchase_cost - trade_in_value[7] + problem.value_function(stage(1), 1),
              epsilon_value);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(
        problem.value_function(stage(3), i),
        std::min(purchase_cost - trade_in_value[i] + problem.value_function(stage(2), 1),
                 problem.value_function(stage(2), i + 1)),
        epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(3), 7),
              purchase_cost - trade_in_value[7] + problem.value_function(stage(2), 1),
              epsilon_value);
}

TEST(MaintenanceAndReplacementProblem,
     PurchaseCostAndTradeInCostAndOperatingCostThreeStage) {
  auto oldest_state = 7u;
  MaintenanceAndReplacementProblem problem(3, oldest_state);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 4, 3, 2, 1, 0, 0, 0};
  std::vector<double> trade_in_value = {
      std::numeric_limits<double>::max(), 5, 4, 3, 2, 1, 0, 0};
  std::vector<double> operating_cost = {0, 1, 2, 3, 4, 5, 6, 7};
  double purchase_cost = 6;
  problem.set_salvage_value(salvage_value);
  problem.set_trade_in_value(trade_in_value);
  problem.set_purchase_cost(purchase_cost);
  problem.set_operating_cost(operating_cost);
  problem.calculate();
  //    t = 3                    t = 2                     t = 1 t = 0
  //
  // 7  6-0+0-2= 4               6-0+0-3= 3                6-0+0-4= 2 0 6
  // min{6-0+0-2,6+3}= 4      min{6-0+0-3,6+2}= 3       min{6-0+0-4,6+0}= 0 0 5
  // min{6-1+0-2,5+3}= 3      min{6-1+0-3,5+0}= 2       min{6-1+0-4,5+0}= 1 0 4
  // min{6-2+0-2,4+2}= 2      min{6-2+0-3,4+1}= 1       min{6-2+0-4,4+0}= 0 -1
  // 3  min{6-3+0-2,3+1}= 1      min{6-3+0-3,3+0}= 0       min{6-3+0-4,3-1}= -1
  // -2 2  min{6-4+0-2,2+0}= 0      min{6-4+0-3,2-1}= -1      min{6-4+0-4,2-2}=
  // -2     -3 1  min{6-5+0-2,1-1}= -1     min{6-5+0-3,1-2}= -2
  // min{6-5+0-4,1-3}= -3     -4
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(1), i),
                std::min(purchase_cost - trade_in_value[i] + operating_cost[0] +
                             problem.value_function(stage(0), 1),
                         operating_cost[i] + problem.value_function(stage(0), i + 1)),
                epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(1), 7),
              purchase_cost - trade_in_value[7] + operating_cost[0] +
                  problem.value_function(stage(0), 1),
              epsilon_value);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(2), i),
                std::min(purchase_cost - trade_in_value[i] + operating_cost[0] +
                             problem.value_function(stage(1), 1),
                         operating_cost[i] + problem.value_function(stage(1), i + 1)),
                epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(2), 7),
              purchase_cost - trade_in_value[7] + operating_cost[0] +
                  problem.value_function(stage(1), 1),
              epsilon_value);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(3), i),
                std::min(purchase_cost - trade_in_value[i] + operating_cost[0] +
                             problem.value_function(stage(2), 1),
                         operating_cost[i] + problem.value_function(stage(2), i + 1)),
                epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(3), 7),
              purchase_cost - trade_in_value[7] + operating_cost[0] +
                  problem.value_function(stage(2), 1),
              epsilon_value);
}

TEST(MaintenanceAndReplacementProblem, ParametersConstrcutorWorks) {
  MaintenanceAndReplacementParameters params;
  params.purchase_cost = 6;
  params.salvage_value = {
      std::numeric_limits<double>::max(), 4, 3, 2, 1, 0, 0, 0};
  params.trade_in_value = {
      std::numeric_limits<double>::max(), 5, 4, 3, 2, 1, 0, 0};
  params.operating_cost = {0, 1, 2, 3, 4, 5, 6, 7};
  auto oldest_state = 7u;
  MaintenanceAndReplacementProblem problem(3, oldest_state, params);
  problem.calculate();
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(1), i),
                std::min(params.purchase_cost - params.trade_in_value[i] +
                             params.operating_cost[0] + problem.value_function(stage(0), 1),
                         params.operating_cost[i] + problem.value_function(stage(0), i + 1)),
                epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(1), 7),
              params.purchase_cost - params.trade_in_value[7] +
                  params.operating_cost[0] + problem.value_function(stage(0), 1),
              epsilon_value);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(2), i),
                std::min(params.purchase_cost - params.trade_in_value[i] +
                             params.operating_cost[0] + problem.value_function(stage(1), 1),
                         params.operating_cost[i] + problem.value_function(stage(1), i + 1)),
                epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(2), 7),
              params.purchase_cost - params.trade_in_value[7] +
                  params.operating_cost[0] + problem.value_function(stage(1), 1),
              epsilon_value);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(3), i),
                std::min(params.purchase_cost - params.trade_in_value[i] +
                             params.operating_cost[0] + problem.value_function(stage(2), 1),
                         params.operating_cost[i] + problem.value_function(stage(2), i + 1)),
                epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(3), 7),
              params.purchase_cost - params.trade_in_value[7] +
                  params.operating_cost[0] + problem.value_function(stage(2), 1),
              epsilon_value);
}

TEST(MaintenanceAndReplacementProblem,
     PurchaseCostAndTradeInCostAndOperatingCostFourStage) {
  auto oldest_state = 7u;
  MaintenanceAndReplacementProblem problem(4, oldest_state);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 25, 17, 8, 0, 0, 0, 0};
  std::vector<double> trade_in_value = {
      std::numeric_limits<double>::max(), 32, 21, 11, 5, 0, 0, 0};
  std::vector<double> operating_cost = {10, 13, 20, 40, 70, 100, 100, 100};
  double purchase_cost = 50;
  problem.set_salvage_value(salvage_value);
  problem.set_trade_in_value(trade_in_value);
  problem.set_purchase_cost(purchase_cost);
  problem.set_operating_cost(operating_cost);
  problem.calculate();
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(1), i),
                std::min(purchase_cost - trade_in_value[i] + operating_cost[0] +
                             problem.value_function(stage(0), 1),
                         operating_cost[i] + problem.value_function(stage(0), i + 1)),
                epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(1), 7),
              purchase_cost - trade_in_value[7] + operating_cost[0] +
                  problem.value_function(stage(0), 1),
              epsilon_value);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(2), i),
                std::min(purchase_cost - trade_in_value[i] + operating_cost[0] +
                             problem.value_function(stage(1), 1),
                         operating_cost[i] + problem.value_function(stage(1), i + 1)),
                epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(2), 7),
              purchase_cost - trade_in_value[7] + operating_cost[0] +
                  problem.value_function(stage(1), 1),
              epsilon_value);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(3), i),
                std::min(purchase_cost - trade_in_value[i] + operating_cost[0] +
                             problem.value_function(stage(2), 1),
                         operating_cost[i] + problem.value_function(stage(2), i + 1)),
                epsilon_value);
  }
  EXPECT_NEAR(problem.value_function(stage(3), 7),
              purchase_cost - trade_in_value[7] + operating_cost[0] +
                  problem.value_function(stage(2), 1),
              epsilon_value);
  EXPECT_NEAR(problem.value_function(stage(4), 3),
              std::min(purchase_cost - trade_in_value[3] + operating_cost[0] +
                           problem.value_function(stage(3), 1),
                       operating_cost[3] + problem.value_function(stage(3), 4)),
              epsilon_value);
}

TEST(MaintenanceAndReplacementProblem, ActionMatrixCorrect) {
  auto oldest_state = 7u;
  MaintenanceAndReplacementProblem problem(4, oldest_state);
  problem.use_action_matrix();
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 25, 17, 8, 0, 0, 0, 0};
  std::vector<double> trade_in_value = {
      std::numeric_limits<double>::max(), 32, 21, 11, 5, 0, 0, 0};
  std::vector<double> operating_cost = {10, 13, 20, 40, 70, 100, 100, 100};
  double purchase_cost = 50;
  problem.set_salvage_value(salvage_value);
  problem.set_trade_in_value(trade_in_value);
  problem.set_purchase_cost(purchase_cost);
  problem.set_operating_cost(operating_cost);
  problem.calculate();
  for (auto i = 1u; i < oldest_state; ++i) {
    unsigned int action =
        (unsigned int)(purchase_cost - trade_in_value[i] + operating_cost[0] +
                           problem.value_function(stage(0), 1) <
                       operating_cost[i] + problem.value_function(stage(0), i + 1));
    EXPECT_EQ(problem.action_matrix(stage(1), i), action);
  }
  EXPECT_EQ(problem.action_matrix(stage(1), oldest_state), 1u);
  for (auto i = 1u; i < oldest_state; ++i) {
    unsigned int action =
        (unsigned int)(purchase_cost - trade_in_value[i] + operating_cost[0] +
                           problem.value_function(stage(1), 1) <
                       operating_cost[i] + problem.value_function(stage(1), i + 1));
    EXPECT_EQ(problem.action_matrix(stage(2), i), action);
  }
  EXPECT_EQ(problem.action_matrix(stage(2), oldest_state), 1u);
  for (auto i = 1u; i < oldest_state; ++i) {
    unsigned int action =
        (unsigned int)(purchase_cost - trade_in_value[i] + operating_cost[0] +
                           problem.value_function(stage(2), 1) <
                       operating_cost[i] + problem.value_function(stage(2), i + 1));
    EXPECT_EQ(problem.action_matrix(stage(3), i), action);
  }
  EXPECT_EQ(problem.action_matrix(stage(3), oldest_state), 1u);
  EXPECT_EQ(problem.action_matrix(stage(4), 3),
            ((unsigned int)(purchase_cost - trade_in_value[3] +
                                operating_cost[0] + problem.value_function(stage(3), 1) <
                            operating_cost[3] + problem.value_function(stage(3), 4))));
}

TEST(MaintenanceAndReplacementProblem,
     PurchaseCostAndTradeInCostAndOperatingCostInfinitedHorizon) {
  auto oldest_state = 7u;
  MaintenanceAndReplacementProblem problem(4, oldest_state);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 25, 17, 8, 0, 0, 0, 0};
  std::vector<double> trade_in_value = {
      std::numeric_limits<double>::max(), 32, 21, 11, 5, 0, 0, 0};
  std::vector<double> operating_cost = {10, 13, 20, 40, 70, 100, 100, 100};
  double purchase_cost = 50;
  problem.set_salvage_value(salvage_value);
  problem.set_trade_in_value(trade_in_value);
  problem.set_purchase_cost(purchase_cost);
  problem.set_operating_cost(operating_cost);
  problem.calculate(epsilon(epsilon_value));
  EXPECT_NEAR(
      problem.value_function(stage(problem.T()), 3),
      std::min(purchase_cost - trade_in_value[3] + operating_cost[0] +
                   problem.value_function(stage(problem.T() - 1), 1),
               operating_cost[3] + problem.value_function(stage(problem.T() - 1), 4)),
      epsilon_value);
  EXPECT_LT(std::abs((problem.value_function(stage(problem.T()), 3) / double(problem.T())) -
                     (problem.value_function(stage(problem.T() - 1), 3) /
                      double(problem.T() - 1))),
            0.01);
}

TEST(MaintenanceAndReplacementProblem, TestForcedPolicy) {

  class ForcedPolicy : public MaintenanceAndReplacementProblem {
  public:
    using MaintenanceAndReplacementProblem::MaintenanceAndReplacementProblem;

  private:
    std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) const override {
      if (state < 4)
        return {make_maintenance_action(state)};
      else
        return {make_replacement_action(state)};
    }
  };

  const auto oldest_state = 7u;
  const auto no_stages = 100u;

  const std::vector<double> salvage_value = {50, 40, 30, 20, 10, 0, 0, 0};
  const std::vector<double> trade_in_value = {0, 30, 15, 5, 0, 0, 0};
  const std::vector<double> operating_cost = {5, 7, 11, 22, 35, 50, 75};
  const double purchase_cost = 60;
  ForcedPolicy problem_forced_policy(no_stages, oldest_state);

  problem_forced_policy.use_action_matrix();

  problem_forced_policy.set_salvage_value(salvage_value);
  problem_forced_policy.set_trade_in_value(trade_in_value);
  problem_forced_policy.set_operating_cost(operating_cost);
  problem_forced_policy.set_purchase_cost(purchase_cost);

  problem_forced_policy.calculate();

  for (auto t = 1u; t <= 100; ++t) {

    for (auto i = 1u; i < oldest_state; ++i) {
      unsigned int action = i < 4 ? 0u : 1u;
      EXPECT_EQ(problem_forced_policy.action_matrix(stage(t), i), action);
    }
  }
}

TEST(MaintenanceAndReplacementProblem, TestParamsAndSettersTheSame) {
  const auto oldest_state = 7u;
  const auto no_stages = 100u;

  const std::vector<double> salvage_value = {50, 40, 30, 20, 10, 0, 0, 0};
  const std::vector<double> trade_in_value = {0, 30, 15, 5, 0, 0, 0};
  const std::vector<double> operating_cost = {5, 7, 11, 22, 35, 50, 75};
  const double purchase_cost = 60;

  MaintenanceAndReplacementParameters params;
  params.salvage_value = salvage_value;
  params.trade_in_value = trade_in_value;
  params.operating_cost = operating_cost;
  params.purchase_cost = purchase_cost;

  MaintenanceAndReplacementProblem problem_setters(no_stages, oldest_state);
  problem_setters.use_action_matrix();

  problem_setters.set_salvage_value(salvage_value);
  problem_setters.set_trade_in_value(trade_in_value);
  problem_setters.set_operating_cost(operating_cost);
  problem_setters.set_purchase_cost(purchase_cost);

  MaintenanceAndReplacementProblem problem_params(no_stages, oldest_state,params);
  problem_params.use_action_matrix();

  problem_setters.calculate();
  problem_params.calculate();

  for (auto state = oldest_state; state > 0; state--) {
    for (auto t = 20u; t > 0u; --t) {
      EXPECT_EQ(problem_setters.action_matrix(stage(t), state),
                problem_params.action_matrix(stage(t), state));
    }
  }
}