#include <gtest/gtest.h>
#include <limits>
#include <numeric>
#include <vector>

#include "maintenance_and_replacement_problem_multi.h"
#include "state_loop_vector.h"

double epsilon = 0.01;

TEST(MaintenanceAndReplacementProblemMultiMulti, SalvageValueCorrect) {
  MaintenanceAndReplacementProblemMulti problem(0u, 7u, 1);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 25, 17, 8, 0, 0, 0, 0};
  problem.set_salvage_value(salvage_value);
  EXPECT_NEAR(problem.value_function(stage(0u), {1}), -salvage_value[1],epsilon);
  EXPECT_NEAR(problem.value_function(stage(0u), {2}), -salvage_value[2],epsilon);
  EXPECT_NEAR(problem.value_function(stage(0u), {3}), -salvage_value[3],epsilon);
  EXPECT_NEAR(problem.value_function(stage(0u), {4}), -salvage_value[4],epsilon);
  EXPECT_NEAR(problem.value_function(stage(0u), {5}), -salvage_value[5],epsilon);
  EXPECT_NEAR(problem.value_function(stage(0u), {6}), -salvage_value[6],epsilon);
  EXPECT_NEAR(problem.value_function(stage(0u), {7}), -salvage_value[7],epsilon);
}

TEST(MaintenanceAndReplacementProblemMulti, UncalculatedValuesCorrect) {
  MaintenanceAndReplacementProblemMulti problem(1u, 7u, 1);
  std::vector<double> salvage_value = {0, 25, 17, 8, 0, 0, 0, 0};
  problem.set_salvage_value(salvage_value);
  EXPECT_NEAR(problem.value_function(stage(1u), {1}), std::numeric_limits<double>::max(),epsilon);
  EXPECT_NEAR(problem.value_function(stage(1u), {7}), std::numeric_limits<double>::max(),epsilon);
}

TEST(MaintenanceAndReplacementProblemMulti, TRangeThrows) {
  MaintenanceAndReplacementProblemMulti problem(3u, 7u, 1);
  EXPECT_NO_THROW(problem.value_function(stage(3u), {1}));
  EXPECT_DEATH(problem.value_function(stage(4u), {1}),"Assertion.*failed");
}

TEST(MaintenanceAndReplacementProblemMulti, NoCostsChooseMinSalvageCostBottom) {
  MaintenanceAndReplacementProblemMulti problem(1u, 7u, 1);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 25, 17, 8, 0, 0, 0, 0};
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost({0, 0});
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_operating_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.calculate();
  for (auto i = 1u; i <= 7u; ++i) {
    EXPECT_NEAR(problem.value_function(stage(1u), {i}), -salvage_value[1],epsilon);
  }
}

TEST(MaintenanceAndReplacementProblemMulti, NoCostsChooseMinSalvageCostOlder) {
  MaintenanceAndReplacementProblemMulti problem(1u, 7u, 1);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 1, 2, 3, 4, 5, 6, 7};
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost({0, 0});
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_operating_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(1u), {1}), -salvage_value[2],epsilon);
  EXPECT_NEAR(problem.value_function(stage(1u), {2}), -salvage_value[3],epsilon);
  EXPECT_NEAR(problem.value_function(stage(1u), {3}), -salvage_value[4],epsilon);
  EXPECT_NEAR(problem.value_function(stage(1u), {4}), -salvage_value[5],epsilon);
  EXPECT_NEAR(problem.value_function(stage(1u), {5}), -salvage_value[6],epsilon);
  EXPECT_NEAR(problem.value_function(stage(1u), {6}), -salvage_value[7],epsilon);
  EXPECT_NEAR(problem.value_function(stage(1u), {7}), -salvage_value[1],epsilon);
}

TEST(MaintenanceAndReplacementProblemMulti,
     NoCostsChooseMinSalvageCostOlderTwoStage) {
  MaintenanceAndReplacementProblemMulti problem(2u, 7u, 1);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 1, 2, 3, 4, 5, 6, 7};
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost({0, 0});
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_operating_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(2), {1}), -salvage_value[3],epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2}), -salvage_value[4],epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {3}), -salvage_value[5],epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {4}), -salvage_value[6],epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {5}), -salvage_value[7],epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {7}), -salvage_value[2],epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {6}), -salvage_value[2],epsilon);
}

TEST(MaintenanceAndReplacementProblemMulti,
     NoCostsChooseMinSalvageCostOlderThreeStage) {
  MaintenanceAndReplacementProblemMulti problem(3, 7, 1);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 1, 2, 3, 4, 5, 6, 7};
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost({0, 0});
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_operating_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(3), {1}), -salvage_value[4],epsilon);
  EXPECT_NEAR(problem.value_function(stage(3), {2}), -salvage_value[5],epsilon);
  EXPECT_NEAR(problem.value_function(stage(3), {3}), -salvage_value[6],epsilon);
  EXPECT_NEAR(problem.value_function(stage(3), {4}), -salvage_value[7],epsilon);
  EXPECT_NEAR(problem.value_function(stage(3), {7}), -salvage_value[3],epsilon);
  EXPECT_NEAR(problem.value_function(stage(3), {6}), -salvage_value[3],epsilon);
  EXPECT_NEAR(problem.value_function(stage(3), {5}), -salvage_value[3],epsilon);
}

TEST(MaintenanceAndReplacementProblemMulti, PurchaseCostOnlyThreeStage) {
  auto oldest_state = 7u;
  MaintenanceAndReplacementProblemMulti problem(3, oldest_state, 1);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 4, 3, 2, 1, 0, 0, 0};
  std::vector<double> purchase_cost = {0, 5};
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(purchase_cost);
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_operating_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.calculate();
  //    t = 3               t = 2               t = 1             t = 0
  //
  // 7  5-2= 3              5-3= 2              5-4= 1              0
  // 6  min{5-2,2}= 2       min{5-3,1}= 1       min{5-4,0}= 0       0
  // 5  min{5-2,1}= 1       min{5-3,0}= 0       min{5-4,0}= 0       0
  // 4  min{5-2,0}= 0       min{5-3,0}= 0       min{5-4,0}= 0       -1
  // 3  min{5-2,0}= 0       min{5-3,0}= 0       min{5-4,-1}= -1     -2
  // 2  min{5-2,0}= 0       min{5-3,-1}= -1     min{5-4,-2}= -2     -3
  // 1  min{5-2,-1}= -1     min{5-3,-2}= -2     min{5-4,-3}= -3     -4
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(1), {i}), std::min(purchase_cost[1] + problem.value_function(stage(0), {1}),
                                          problem.value_function(stage(0), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(1), {7}), purchase_cost[1] + problem.value_function(stage(0), {1}),epsilon);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(2), {i}), std::min(purchase_cost[1] + problem.value_function(stage(1), {1}),
                                          problem.value_function(stage(1), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(2), {7}), purchase_cost[1] + problem.value_function(stage(1), {1}),epsilon);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(3), {i}), std::min(purchase_cost[1] + problem.value_function(stage(2), {1}),
                                          problem.value_function(stage(2), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(3), {7}), purchase_cost[1] + problem.value_function(stage(2), {1}),epsilon);
}

TEST(MaintenanceAndReplacementProblemMulti,
     PurchaseCostAndTradeInCostThreeStage) {
  auto oldest_state = 7u;
  MaintenanceAndReplacementProblemMulti problem(3, oldest_state, 1);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 4, 3, 2, 1, 0, 0, 0};
  std::vector<double> trade_in_value = {
      std::numeric_limits<double>::max(), 5, 4, 3, 2, 1, 0, 0};
  std::vector<double> purchase_cost = {0, 6};
  problem.set_salvage_value(salvage_value);
  problem.set_trade_in_value(trade_in_value);
  problem.set_purchase_cost(purchase_cost);
  problem.set_operating_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.calculate();
  //    t = 3               t = 2               t = 1             t = 0
  //
  // 7  6-0-2= 4              6-0-3= 3              6-0-4= 2              0
  // 6  min{6-0-2,3}= 3       min{6-0-3,2}= 2       min{6-0-4,0}= 0       0
  // 5  min{6-1-2,2}= 2       min{6-1-3,0}= 0       min{6-1-4,0}= 0       0
  // 4  min{6-2-2,0}= 0       min{6-2-3,0}= 0       min{6-2-4,0}= 0       -1
  // 3  min{6-3-2,0}= 0       min{6-3-3,0}= 0       min{6-3-4,-1}= -1     -2
  // 2  min{6-4-2,0}= 0       min{6-4-3,-1}= -1     min{6-4-4,-2}= -2     -3
  // 1  min{6-5-2,-1}= -1     min{6-5-3,-2}= -2     min{6-5-4,-3}= -3     -4
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(1), {i}),
              std::min(purchase_cost[1] - trade_in_value[i] + problem.value_function(stage(0), {1}),
                       problem.value_function(stage(0), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(1), {7}),
            purchase_cost[1] - trade_in_value[7] + problem.value_function(stage(0), {1}),epsilon);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(2), {i}),
              std::min(purchase_cost[1] - trade_in_value[i] + problem.value_function(stage(1), {1}),
                       problem.value_function(stage(1), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(2), {7}),
            purchase_cost[1] - trade_in_value[7] + problem.value_function(stage(1), {1}),epsilon);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(3), {i}),
              std::min(purchase_cost[1] - trade_in_value[i] + problem.value_function(stage(2), {1}),
                       problem.value_function(stage(2), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(3), {7}),
            purchase_cost[1] - trade_in_value[7] + problem.value_function(stage(2), {1}),epsilon);
}

TEST(MaintenanceAndReplacementProblemMulti,
     PurchaseCostAndTradeInCostAndOperatingCostThreeStage) {
  auto oldest_state = 7u;
  MaintenanceAndReplacementProblemMulti problem(3, oldest_state, 1);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 4, 3, 2, 1, 0, 0, 0};
  std::vector<double> trade_in_value = {
      std::numeric_limits<double>::max(), 5, 4, 3, 2, 1, 0, 0};
  std::vector<double> operating_cost = {0, 1, 2, 3, 4, 5, 6, 7};
  std::vector<double> purchase_cost = {0, 6};
  problem.set_salvage_value(salvage_value);
  problem.set_trade_in_value(trade_in_value);
  problem.set_purchase_cost(purchase_cost);
  problem.set_operating_cost(operating_cost);
  problem.calculate();
  //    t = 3                    t = 2                     t = 1 t = 0
  //
  // 7  6-0+0-2= 4               6-0+0-3= 3                6-0+0-4= 2 0 6
  // min{6-0+0-2,6+3}= 4      min{6-0+0-3,6+2}= 3       min{6-0+0-4,6+0}= 0 0 5
  // min{6-1+0-2,5+3}= 3      min{6-1+0-3,5+0}= 2       min{6-1+0-4,5+0}= 1 0 4
  // min{6-2+0-2,4+2}= 2      min{6-2+0-3,4+1}= 1       min{6-2+0-4,4+0}= 0 -1
  // 3  min{6-3+0-2,3+1}= 1      min{6-3+0-3,3+0}= 0       min{6-3+0-4,3-1}= -1
  // -2 2  min{6-4+0-2,2+0}= 0      min{6-4+0-3,2-1}= -1      min{6-4+0-4,2-2}=
  // -2     -3 1  min{6-5+0-2,1-1}= -1     min{6-5+0-3,1-2}= -2
  // min{6-5+0-4,1-3}= -3     -4
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(1), {i}),
              std::min(purchase_cost[1] - trade_in_value[i] +
                           operating_cost[0] + problem.value_function(stage(0), {1}),
                       operating_cost[i] + problem.value_function(stage(0), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(1), {7}), purchase_cost[1] - trade_in_value[7] +
                                   operating_cost[0] + problem.value_function(stage(0), {1}),epsilon);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(2), {i}),
              std::min(purchase_cost[1] - trade_in_value[i] +
                           operating_cost[0] + problem.value_function(stage(1), {1}),
                       operating_cost[i] + problem.value_function(stage(1), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(2), {7}), purchase_cost[1] - trade_in_value[7] +
                                   operating_cost[0] + problem.value_function(stage(1), {1}),epsilon);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(3), {i}),
              std::min(purchase_cost[1] - trade_in_value[i] +
                           operating_cost[0] + problem.value_function(stage(2), {1}),
                       operating_cost[i] + problem.value_function(stage(2), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(3), {7}), purchase_cost[1] - trade_in_value[7] +
                                   operating_cost[0] + problem.value_function(stage(2), {1}),epsilon);
}

TEST(MaintenanceAndReplacementProblemMulti,
     PurchaseCostAndTradeInCostAndOperatingCostFourStage) {
  auto oldest_state = 7u;
  MaintenanceAndReplacementProblemMulti problem(4, oldest_state, 1);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 25, 17, 8, 0, 0, 0, 0};
  std::vector<double> trade_in_value = {
      std::numeric_limits<double>::max(), 32, 21, 11, 5, 0, 0, 0};
  std::vector<double> operating_cost = {10, 13, 20, 40, 70, 100, 100, 100};
  std::vector<double> purchase_cost = {0, 50};
  problem.set_salvage_value(salvage_value);
  problem.set_trade_in_value(trade_in_value);
  problem.set_purchase_cost(purchase_cost);
  problem.set_operating_cost(operating_cost);
  problem.calculate();
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(1), {i}),
              std::min(purchase_cost[1] - trade_in_value[i] +
                           operating_cost[0] + problem.value_function(stage(0), {1}),
                       operating_cost[i] + problem.value_function(stage(0), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(1), {7}), purchase_cost[1] - trade_in_value[7] +
                                   operating_cost[0] + problem.value_function(stage(0), {1}),epsilon);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(2), {i}),
              std::min(purchase_cost[1] - trade_in_value[i] +
                           operating_cost[0] + problem.value_function(stage(1), {1}),
                       operating_cost[i] + problem.value_function(stage(1), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(2), {7}), purchase_cost[1] - trade_in_value[7] +
                                   operating_cost[0] + problem.value_function(stage(1), {1}),epsilon);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(3), {i}),
              std::min(purchase_cost[1] - trade_in_value[i] +
                           operating_cost[0] + problem.value_function(stage(2), {1}),
                       operating_cost[i] + problem.value_function(stage(2), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(3), {7}), purchase_cost[1] - trade_in_value[7] +
                                   operating_cost[0] + problem.value_function(stage(2), {1}),epsilon);
  EXPECT_NEAR(problem.value_function(stage(4), {3}),
            std::min(purchase_cost[1] - trade_in_value[3] + operating_cost[0] +
                         problem.value_function(stage(3), {1}),
                     operating_cost[3] + problem.value_function(stage(3), {4})),epsilon);
}

TEST(MaintenanceAndReplacementProblemMulti,
     ParameterConstructorWorks) {
       MaintenanceAndReplacementMultiParameters params;
  params.salvage_value =   { std::numeric_limits<double>::max(), 25, 17, 8, 0, 0, 0, 0};
  params.trade_in_value = {    std::numeric_limits<double>::max(), 32, 21, 11, 5, 0, 0, 0};
  params.operating_cost = {10, 13, 20, 40, 70, 100, 100, 100};
  params.purchase_cost = {0, 50};
  
  auto oldest_state = 7u;
  MaintenanceAndReplacementProblemMulti problem(4, oldest_state, 1,params);
  problem.calculate();
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(1), {i}),
              std::min(params.purchase_cost[1] - params.trade_in_value[i] +
                           params.operating_cost[0] + problem.value_function(stage(0), {1}),
                       params.operating_cost[i] + problem.value_function(stage(0), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(1), {7}), params.purchase_cost[1] - params.trade_in_value[7] +
                                   params.operating_cost[0] + problem.value_function(stage(0), {1}),epsilon);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(2), {i}),
              std::min(params.purchase_cost[1] - params.trade_in_value[i] +
                           params.operating_cost[0] + problem.value_function(stage(1), {1}),
                       params.operating_cost[i] + problem.value_function(stage(1), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(2), {7}), params.purchase_cost[1] - params.trade_in_value[7] +
                                   params.operating_cost[0] + problem.value_function(stage(1), {1}),epsilon);
  for (auto i = 1u; i < oldest_state; ++i) {
    EXPECT_NEAR(problem.value_function(stage(3), {i}),
              std::min(params.purchase_cost[1] - params.trade_in_value[i] +
                           params.operating_cost[0] + problem.value_function(stage(2), {1}),
                       params.operating_cost[i] + problem.value_function(stage(2), {i + 1})),epsilon);
  }
  EXPECT_NEAR(problem.value_function(stage(3), {7}), params.purchase_cost[1] - params.trade_in_value[7] +
                                   params.operating_cost[0] + problem.value_function(stage(2), {1}),epsilon);
  EXPECT_NEAR(problem.value_function(stage(4), {3}),
            std::min(params.purchase_cost[1] - params.trade_in_value[3] + params.operating_cost[0] +
                         problem.value_function(stage(3), {1}),
                     params.operating_cost[3] + problem.value_function(stage(3), {4})),epsilon);
}


TEST(MaintenanceAndReplacementProblemMulti, ActionMatrixCorrect) {
  auto oldest_state = 7u;
  MaintenanceAndReplacementProblemMulti problem(4, oldest_state, 1);
  problem.use_action_matrix();
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 25, 17, 8, 0, 0, 0, 0};
  std::vector<double> trade_in_value = {
      std::numeric_limits<double>::max(), 32, 21, 11, 5, 0, 0, 0};
  std::vector<double> operating_cost = {10, 13, 20, 40, 70, 100, 100, 100};
  std::vector<double> purchase_cost = {0, 50};
  problem.set_salvage_value(salvage_value);
  problem.set_trade_in_value(trade_in_value);
  problem.set_purchase_cost(purchase_cost);
  problem.set_operating_cost(operating_cost);
  problem.calculate();
  for (auto i = 1u; i < oldest_state; ++i) {
    unsigned int action =
        (unsigned int)(purchase_cost[1] - trade_in_value[i] +
                           operating_cost[0] + problem.value_function(stage(0), {1}) <
                       operating_cost[i] + problem.value_function(stage(0), {i + 1}));
    EXPECT_EQ(problem.action_matrix(stage(1), {i}), action);
  }
  EXPECT_EQ(problem.action_matrix(stage(1), {oldest_state}), 1u);
  for (auto i = 1u; i < oldest_state; ++i) {
    unsigned int action =
        (unsigned int)(purchase_cost[1] - trade_in_value[i] +
                           operating_cost[0] + problem.value_function(stage(1), {1}) <
                       operating_cost[i] + problem.value_function(stage(1), {i + 1}));
    EXPECT_EQ(problem.action_matrix(stage(2), {i}), action);
  }
  EXPECT_EQ(problem.action_matrix(stage(2), {oldest_state}), 1u);
  for (auto i = 1u; i < oldest_state; ++i) {
    unsigned int action =
        (unsigned int)(purchase_cost[1] - trade_in_value[i] +
                           operating_cost[0] + problem.value_function(stage(2), {1}) <
                       operating_cost[i] + problem.value_function(stage(2), {i + 1}));
    EXPECT_EQ(problem.action_matrix(stage(3), {i}), action);
  }
  EXPECT_EQ(problem.action_matrix(stage(3), {oldest_state}), 1u);
  EXPECT_EQ(problem.action_matrix(stage(4), {3}),
            ((unsigned int)(purchase_cost[1] - trade_in_value[3] +
                                operating_cost[0] + problem.value_function(stage(3), {1}) <
                            operating_cost[3] + problem.value_function(stage(3), {4}))));
}

std::vector<unsigned int>
generate_subsequent_state(std::vector<unsigned int> in_state,
                          std::vector<unsigned int> mod_state) {
  std::vector<unsigned int> out_state = in_state;
  for (auto i = 0u; i < in_state.size(); ++i)
    if (mod_state[i] == 0)
      out_state[i]++;
    else
      out_state[i] = 1;
  return out_state;
}

TEST(MaintenanceAndReplacementProblemMulti, MutlipleMachineProblemReplaceTwo) {
  auto oldest_state = 7u;
  auto T = 10u;
  MaintenanceAndReplacementProblemMulti problem(T, oldest_state, 3);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 25, 17, 8, 0, 0, 0, 0};
  std::vector<double> trade_in_value = {
      std::numeric_limits<double>::max(), 32, 21, 11, 5, 0, 0, 0};
  std::vector<double> operating_cost = {10, 13, 20, 40, 70, 100, 100, 100};
  // note we cannot purchase 3 machines (only 0, 1 or 2)
  std::vector<double> purchase_cost = {0, 50, 85};
  problem.set_salvage_value(salvage_value);
  problem.set_trade_in_value(trade_in_value);
  problem.set_purchase_cost(purchase_cost);
  problem.set_operating_cost(operating_cost);
  problem.calculate();
  for (auto t = 1u; t <= T; ++t) 
  {
    // in this testing loop avoid the oldest states as the test code is
    //    too simple to handle them
    StateLoop<std::vector<unsigned int>> i({1, 1, 1}, {6, 6, 6});
    for (const auto &state : i) {
      double min_cost = std::numeric_limits<double>::max();
      for (std::vector<unsigned int> subs_state :
           std::vector<std::vector<unsigned int>>({
               {0, 0, 0},
               {1, 0, 0},
               {0, 1, 0},
               {0, 0, 1},
               {1, 1, 0},
               {1, 0, 1},
               {0, 1, 1}
               // no {1, 1, 1} because we can only replace 2 at most
           })) {
        std::vector<unsigned int> i_prime =
            generate_subsequent_state(state, subs_state);

        double cost = purchase_cost[std::accumulate(subs_state.begin(),
                                                 subs_state.end(), 0u)] +
                   problem.value_function(stage(t - 1), i_prime);
        for (auto age : i_prime) {
          cost += operating_cost[age - 1];
        }
        for (auto machine = 0u; machine < 3; ++machine) {
          if (subs_state[machine] == 1)
            cost -= trade_in_value[state[machine]];
        }
        min_cost = std::min(min_cost, cost);
      }
      EXPECT_NEAR(problem.value_function(stage(t), state), min_cost,epsilon)
          << "i = " << state[0] << " " << state[1] << " "
          << state[2] << "\n";
    }
  }
}

TEST(MaintenanceAndReplacementProblemMulti, MutlipleMachineProblemReplaceTwoLowDiscount) {
  auto oldest_state = 7u;
  auto T = 10u;
  MaintenanceAndReplacementProblemMulti problem(T, oldest_state, 3);
  std::vector<double> salvage_value = {
      std::numeric_limits<double>::max(), 25, 17, 8, 0, 0, 0, 0};
  std::vector<double> trade_in_value = {
      std::numeric_limits<double>::max(), 32, 21, 11, 5, 0, 0, 0};
  std::vector<double> operating_cost = {10, 13, 20, 40, 70, 100, 100, 100};
  // note we cannot purchase 3 machines (only 0, 1 or 2)
  std::vector<double> purchase_cost = {0, 50, 98};
  problem.set_salvage_value(salvage_value);
  problem.set_trade_in_value(trade_in_value);
  problem.set_purchase_cost(purchase_cost);
  problem.set_operating_cost(operating_cost);
  problem.calculate();
  for (auto t = 1u; t <= T; ++t) {
    // in this testing loop avoid the oldest states as the test code is
    //    too simple to handle them
    StateLoop<std::vector<unsigned int>> i({1, 1, 1}, {6, 6, 6});
    for (const auto &state : i) {
      double min_cost = std::numeric_limits<int>::max();
      for (std::vector<unsigned int> subs_state :
           std::vector<std::vector<unsigned int>>({
               {0, 0, 0},
               {1, 0, 0},
               {0, 1, 0},
               {0, 0, 1},
               {1, 1, 0},
               {1, 0, 1},
               {0, 1, 1}
               // no {1, 1, 1} because we can only replace 2 at most
           })) {
        std::vector<unsigned int> i_prime =
            generate_subsequent_state(state, subs_state);

        double cost = purchase_cost[std::accumulate(subs_state.begin(),
                                                 subs_state.end(), 0u)] +
                   problem.value_function(stage(t - 1), i_prime);
        for (auto age : i_prime) {
          cost += operating_cost[age - 1];
        }
        for (auto machine = 0u; machine < 3; ++machine) {
          if (subs_state[machine] == 1)
            cost -= trade_in_value[state[machine]];
        }
        min_cost = std::min(min_cost, cost);
      }
      EXPECT_NEAR(problem.value_function(stage(t), state), min_cost,epsilon)
          << "i = " << state[0] << " " << state[1] << " "
          << state[2] << "\n";
    }
  }
}


// TEST(MaintenanceAndReplacementProblemMulti, MutlipleMachineProblemReplaceOne) {
//   auto oldest_state = 7u;
//   auto T = 20u;
//   MaintenanceAndReplacementProblemMulti problem(T, oldest_state, 4);
//   std::vector<double> salvage_value = {
//       std::numeric_limits<double>::max(), 25, 17, 8, 0, 0, 0, 0};
//   std::vector<double> trade_in_value = {
//       std::numeric_limits<double>::max(), 32, 21, 11, 5, 0, 0, 0};
//   std::vector<double> operating_cost = {10, 13, 20, 40, 70, 100, 100, 100};
//   // note we cannot purchase 3 machines (only 0, 1)
//   std::vector<double> purchase_cost = {0, 50};
//   problem.set_salvage_value(salvage_value);
//   problem.set_trade_in_value(trade_in_value);
//   problem.set_purchase_cost(purchase_cost);
//   problem.set_operating_cost(operating_cost);
//   problem.calculate();
//   for (auto t = 1u; t <= T; ++t) {
//     // in this testing loop avoid the oldest states as the test code is
//     //    too simple to handle them
//     for (StateLoop<std::vector<unsigned int>> i({1, 1, 1, 1}, {6, 6, 6, 6});
//          !i.final(); ++i) {
//       double min_cost = std::numeric_limits<double>::max();
//       for (std::vector<unsigned int> subs_state :
//            std::vector<std::vector<unsigned int>>({
//                {0, 0, 0, 0},
//                {1, 0, 0, 0},
//                {0, 1, 0, 0},
//                {0, 0, 1, 0},
//                {0, 0, 0, 1}
//                // no {1, 1, 0},
//                // no {1, 0, 1},
//                // no {0, 1, 1},
//                // no {1, 1, 1} because we can only replace 1 at most
//            })) {
//         std::vector<unsigned int> i_prime =
//             generate_subsequent_state(i.value(), subs_state);

//         double cost = purchase_cost[std::accumulate(subs_state.begin(),
//                                                  subs_state.end(), 0u)] +
//                    problem.value_function(stage(t - 1), i_prime);
//         for (auto age : i_prime) {
//           cost += operating_cost[age - 1];
//         }
//         for (auto machine = 0u; machine < 4; ++machine) {
//           if (subs_state[machine] == 1)
//             cost -= trade_in_value[i.value()[machine]];
//         }
//         min_cost = std::min(min_cost, cost);
//       }
//       EXPECT_NEAR(problem.value_function(stage(t), i.value()), min_cost,epsilon)
//           << "i = " << i.value()[0] << " " << i.value()[1] << " "
//           << i.value()[2] << " " << i.value()[3] << "\n";
//     }
//   }
// }

// TEST(MaintenanceAndReplacementProblemMulti,
//      ActionMatrixMutlipleMachineProblemReplaceTwo) {
//   auto oldest_state = 7u;
//   auto T = 10u;
//   MaintenanceAndReplacementProblemMulti problem(T, oldest_state, 3);
//   problem.use_action_matrix();
//   std::vector<double> salvage_value = {
//       std::numeric_limits<double>::max(), 25, 17, 8, 0, 0, 0, 0};
//   std::vector<double> trade_in_value = {
//       std::numeric_limits<double>::max(), 32, 21, 11, 5, 0, 0, 0};
//   std::vector<double> operating_cost = {10, 13, 20, 40, 70, 100, 100, 100};
//   // note we cannot purchase 3 machines (only 0, 1 or 2)
//   std::vector<double> purchase_cost = {0, 50, 98};
//   problem.set_salvage_value(salvage_value);
//   problem.set_trade_in_value(trade_in_value);
//   problem.set_purchase_cost(purchase_cost);
//   problem.set_operating_cost(operating_cost);
//   problem.calculate();
//   for (auto t = 1u; t <= T; ++t) {
//     // in this testing loop avoid the oldest states as the test code is
//     //    too simple to handle them
//     for (StateLoop<std::vector<unsigned int>> i({1, 1, 1}, {6, 6, 6}); !i.final();
//          ++i) {
//       double min_cost = std::numeric_limits<double>::max();
//       int action = std::numeric_limits<int>::max();
//       bool tie_break = false;
//       for (std::vector<unsigned int> subs_state :
//            std::vector<std::vector<unsigned int>>({
//                {0, 0, 0},
//                {1, 0, 0},
//                {0, 1, 0},
//                {0, 0, 1},
//                {1, 1, 0},
//                {1, 0, 1},
//                {0, 1, 1}
//                // no {1, 1, 1} because we can only replace 2 at most
//            })) {
//         std::vector<unsigned int> i_prime =
//             generate_subsequent_state(i.value(), subs_state);

//         double cost = purchase_cost[std::accumulate(subs_state.begin(),
//                                                  subs_state.end(), 0u)] +
//                    problem.value_function(stage(t - 1), i_prime);
//         for (auto age : i_prime) {
//           cost += operating_cost[age - 1];
//         }
//         for (auto machine = 0u; machine < 3; ++machine) {
//           if (subs_state[machine] == 1)
//             cost -= trade_in_value[i.value()[machine]];
//         }
//         if (cost < min_cost) {
//           min_cost = cost;
//           action =
//               (int)std::accumulate(subs_state.begin(), subs_state.end(), 0u);
//           tie_break = false;
//         } else if (cost == min_cost)
//           tie_break = true;
//         // min_cost = std::min(min_cost, cost);
//       }
//       if (!tie_break) {

//         EXPECT_EQ(problem.action_matrix(stage(t), i.value()), action)
//             << "i = " << i.value()[0] << " " << i.value()[1] << " "
//             << i.value()[2] << "\n";
//       }
//     }
//   }
// }
