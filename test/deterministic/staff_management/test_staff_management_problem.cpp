#include <gtest/gtest.h>
#include <limits>
#include <vector>

#include <staff_management_problem.h>
#include <state_loop.h>

TEST(StaffManagementProblem, StaffAtTheEndHaveNoValue) {
  StaffManagementProblem problem(StaffManagementProblem::day{0}, 9);
  EXPECT_EQ(problem.value_function(stage(0), 0), 0);
  EXPECT_EQ(problem.value_function(stage(0), 1), 0);
  EXPECT_EQ(problem.value_function(stage(0), 2), 0);
  EXPECT_EQ(problem.value_function(stage(0), 3), 0);
  EXPECT_EQ(problem.value_function(stage(0), 4), 0);
  EXPECT_EQ(problem.value_function(stage(0), 5), 0);
  EXPECT_EQ(problem.value_function(stage(0), 6), 0);
  EXPECT_EQ(problem.value_function(stage(0), 7), 0);
  EXPECT_EQ(problem.value_function(stage(0), 8), 0);
  EXPECT_EQ(problem.value_function(stage(0), 9), 0);
}

TEST(StaffManagementProblem, RequirementsHireCost) {
  int hire_cost_fixed=50;
  int hire_cost_variable=10;
  StaffManagementProblem problem(StaffManagementProblem::day{1}, 9);
  problem.set_requirements({5});
  problem.set_hire_cost(hire_cost_fixed,hire_cost_variable);
  problem.calculate();
  EXPECT_EQ(problem.value_function(stage(1), 0), hire_cost_fixed+hire_cost_variable*5);
  EXPECT_EQ(problem.value_function(stage(1), 1), hire_cost_fixed+hire_cost_variable*4);
  EXPECT_EQ(problem.value_function(stage(1), 2), hire_cost_fixed+hire_cost_variable*3);
  EXPECT_EQ(problem.value_function(stage(1), 3), hire_cost_fixed+hire_cost_variable*2);
  EXPECT_EQ(problem.value_function(stage(1), 4), hire_cost_fixed+hire_cost_variable*1);
  EXPECT_EQ(problem.value_function(stage(1), 5), 0);
  EXPECT_EQ(problem.value_function(stage(1), 6), 0);
  EXPECT_EQ(problem.value_function(stage(1), 7), 0);
  EXPECT_EQ(problem.value_function(stage(1), 8), 0);
  EXPECT_EQ(problem.value_function(stage(1), 9), 0);
}

TEST(StaffManagementProblem, RequirementsHireCostAndWageCost) {
  int hire_cost_fixed=50;
  int hire_cost_variable=10;
  int wage_cost=20;
  StaffManagementProblem problem(StaffManagementProblem::day{1}, 9);
  problem.set_requirements({5});
  problem.set_hire_cost(hire_cost_fixed,hire_cost_variable);
  problem.set_wage_cost(wage_cost);
  problem.calculate();
  EXPECT_EQ(problem.value_function(stage(1), 0), 5*wage_cost+hire_cost_fixed+hire_cost_variable*5);
  EXPECT_EQ(problem.value_function(stage(1), 1), 5*wage_cost+hire_cost_fixed+hire_cost_variable*4);
  EXPECT_EQ(problem.value_function(stage(1), 2), 5*wage_cost+hire_cost_fixed+hire_cost_variable*3);
  EXPECT_EQ(problem.value_function(stage(1), 3), 5*wage_cost+hire_cost_fixed+hire_cost_variable*2);
  EXPECT_EQ(problem.value_function(stage(1), 4), 5*wage_cost+hire_cost_fixed+hire_cost_variable*1);
  EXPECT_EQ(problem.value_function(stage(1), 5), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 6), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 7), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 8), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 9), 5*wage_cost);
}

TEST(StaffManagementProblem, RequirementsHireCostAndWageCostTwoDays) {
  int hire_cost_fixed=50;
  int hire_cost_variable=10;
  int wage_cost=40;
  StaffManagementProblem problem(StaffManagementProblem::day{2}, 9);
  problem.set_requirements({3,5});
  problem.set_hire_cost(hire_cost_fixed,hire_cost_variable);
  problem.set_wage_cost(wage_cost);
  problem.calculate();
  EXPECT_EQ(problem.value_function(stage(1), 0), 5*wage_cost+hire_cost_fixed+hire_cost_variable*5);
  EXPECT_EQ(problem.value_function(stage(1), 1), 5*wage_cost+hire_cost_fixed+hire_cost_variable*4);
  EXPECT_EQ(problem.value_function(stage(1), 2), 5*wage_cost+hire_cost_fixed+hire_cost_variable*3);
  EXPECT_EQ(problem.value_function(stage(1), 3), 5*wage_cost+hire_cost_fixed+hire_cost_variable*2);
  EXPECT_EQ(problem.value_function(stage(1), 4), 5*wage_cost+hire_cost_fixed+hire_cost_variable*1);
  EXPECT_EQ(problem.value_function(stage(1), 5), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 6), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 7), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 8), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 9), 5*wage_cost);

  EXPECT_EQ(problem.value_function(stage(2), 0), 3*wage_cost+hire_cost_fixed+hire_cost_variable*3+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 1), 3*wage_cost+hire_cost_fixed+hire_cost_variable*2+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 2), 3*wage_cost+hire_cost_fixed+hire_cost_variable*1+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 3), 3*wage_cost+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 4), 3*wage_cost+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 5), 3*wage_cost+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 6), 3*wage_cost+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 7), 3*wage_cost+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 8), 3*wage_cost+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 9), 3*wage_cost+problem.value_function(stage(1),3));
}


TEST(StaffManagementProblem, RequirementsHireCostAndWageCostThreeDays) {
  int hire_cost_fixed=50;
  int hire_cost_variable=10;
  int wage_cost=40;
  StaffManagementProblem problem(StaffManagementProblem::day{3}, 9);
  problem.set_requirements({5,3,5});
  problem.set_hire_cost(hire_cost_fixed,hire_cost_variable);
  problem.set_wage_cost(wage_cost);
  problem.calculate();
  EXPECT_EQ(problem.value_function(stage(1), 0), 5*wage_cost+hire_cost_fixed+hire_cost_variable*5);
  EXPECT_EQ(problem.value_function(stage(1), 1), 5*wage_cost+hire_cost_fixed+hire_cost_variable*4);
  EXPECT_EQ(problem.value_function(stage(1), 2), 5*wage_cost+hire_cost_fixed+hire_cost_variable*3);
  EXPECT_EQ(problem.value_function(stage(1), 3), 5*wage_cost+hire_cost_fixed+hire_cost_variable*2);
  EXPECT_EQ(problem.value_function(stage(1), 4), 5*wage_cost+hire_cost_fixed+hire_cost_variable*1);
  EXPECT_EQ(problem.value_function(stage(1), 5), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 6), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 7), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 8), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 9), 5*wage_cost);

  EXPECT_EQ(problem.value_function(stage(2), 0), 3*wage_cost+hire_cost_fixed+hire_cost_variable*3+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 1), 3*wage_cost+hire_cost_fixed+hire_cost_variable*2+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 2), 3*wage_cost+hire_cost_fixed+hire_cost_variable*1+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 3), 3*wage_cost+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 4), 3*wage_cost+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 5), 3*wage_cost+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 6), 3*wage_cost+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 7), 3*wage_cost+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 8), 3*wage_cost+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 9), 3*wage_cost+problem.value_function(stage(1),3));

  EXPECT_EQ(problem.value_function(stage(3), 0), 5*wage_cost+hire_cost_fixed+hire_cost_variable*5+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 1), 5*wage_cost+hire_cost_fixed+hire_cost_variable*4+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 2), 5*wage_cost+hire_cost_fixed+hire_cost_variable*3+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 3), 5*wage_cost+hire_cost_fixed+hire_cost_variable*2+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 4), 5*wage_cost+hire_cost_fixed+hire_cost_variable*1+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 5), 5*wage_cost+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 6), 5*wage_cost+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 7), 5*wage_cost+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 8), 5*wage_cost+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 9), 5*wage_cost+problem.value_function(stage(2),5));
}

TEST(StaffManagementProblem, RequirementsHireCostAndWageCostThreeDaysHighHireCost) {
  int hire_cost_fixed=100;
  int hire_cost_variable=10;
  int wage_cost=40;
  StaffManagementProblem problem(StaffManagementProblem::day{3}, 9);
  problem.set_requirements({5,3,5});
  problem.set_hire_cost(hire_cost_fixed,hire_cost_variable);
  problem.set_wage_cost(wage_cost);
  problem.calculate();
  EXPECT_EQ(problem.value_function(stage(1), 0), 5*wage_cost+hire_cost_fixed+hire_cost_variable*5);
  EXPECT_EQ(problem.value_function(stage(1), 1), 5*wage_cost+hire_cost_fixed+hire_cost_variable*4);
  EXPECT_EQ(problem.value_function(stage(1), 2), 5*wage_cost+hire_cost_fixed+hire_cost_variable*3);
  EXPECT_EQ(problem.value_function(stage(1), 3), 5*wage_cost+hire_cost_fixed+hire_cost_variable*2);
  EXPECT_EQ(problem.value_function(stage(1), 4), 5*wage_cost+hire_cost_fixed+hire_cost_variable*1);
  EXPECT_EQ(problem.value_function(stage(1), 5), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 6), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 7), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 8), 5*wage_cost);
  EXPECT_EQ(problem.value_function(stage(1), 9), 5*wage_cost);

  EXPECT_EQ(problem.value_function(stage(2), 0), 5*wage_cost+hire_cost_fixed+hire_cost_variable*5+problem.value_function(stage(1),5));
  EXPECT_EQ(problem.value_function(stage(2), 1), 5*wage_cost+hire_cost_fixed+hire_cost_variable*4+problem.value_function(stage(1),5));
  EXPECT_EQ(problem.value_function(stage(2), 2), 5*wage_cost+hire_cost_fixed+hire_cost_variable*3+problem.value_function(stage(1),5));
  EXPECT_EQ(problem.value_function(stage(2), 3), 3*wage_cost+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 4), 3*wage_cost+problem.value_function(stage(1),3));
  EXPECT_EQ(problem.value_function(stage(2), 5), 5*wage_cost+problem.value_function(stage(1),5));
  EXPECT_EQ(problem.value_function(stage(2), 6), 5*wage_cost+problem.value_function(stage(1),5));
  EXPECT_EQ(problem.value_function(stage(2), 7), 5*wage_cost+problem.value_function(stage(1),5));
  EXPECT_EQ(problem.value_function(stage(2), 8), 5*wage_cost+problem.value_function(stage(1),5));
  EXPECT_EQ(problem.value_function(stage(2), 9), 5*wage_cost+problem.value_function(stage(1),5));

  EXPECT_EQ(problem.value_function(stage(3), 0), 5*wage_cost+hire_cost_fixed+hire_cost_variable*5+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 1), 5*wage_cost+hire_cost_fixed+hire_cost_variable*4+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 2), 5*wage_cost+hire_cost_fixed+hire_cost_variable*3+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 3), 5*wage_cost+hire_cost_fixed+hire_cost_variable*2+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 4), 5*wage_cost+hire_cost_fixed+hire_cost_variable*1+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 5), 5*wage_cost+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 6), 5*wage_cost+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 7), 5*wage_cost+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 8), 5*wage_cost+problem.value_function(stage(2),5));
  EXPECT_EQ(problem.value_function(stage(3), 9), 5*wage_cost+problem.value_function(stage(2),5));
}

TEST(StaffManagementProblem, LowHireCostActionMatrix) {
  int hire_cost_fixed=50;
  int hire_cost_variable=10;
  int wage_cost=40;
  StaffManagementProblem problem(StaffManagementProblem::day{3}, 9);
  problem.use_action_matrix();
  problem.set_requirements({5,3,5});
  problem.set_hire_cost(hire_cost_fixed,hire_cost_variable);
  problem.set_wage_cost(wage_cost);
  problem.calculate();
  EXPECT_EQ(problem.action_matrix(stage(1), 0), 5);
  EXPECT_EQ(problem.action_matrix(stage(1), 1), 4);
  EXPECT_EQ(problem.action_matrix(stage(1), 2), 3);
  EXPECT_EQ(problem.action_matrix(stage(1), 3), 2);
  EXPECT_EQ(problem.action_matrix(stage(1), 4), 1);
  EXPECT_EQ(problem.action_matrix(stage(1), 5), 0);
  EXPECT_EQ(problem.action_matrix(stage(1), 6), -1);
  EXPECT_EQ(problem.action_matrix(stage(1), 7), -2);
  EXPECT_EQ(problem.action_matrix(stage(1), 8), -3);
  EXPECT_EQ(problem.action_matrix(stage(1), 9), -4);

  EXPECT_EQ(problem.action_matrix(stage(2), 0), 3);
  EXPECT_EQ(problem.action_matrix(stage(2), 1), 2);
  EXPECT_EQ(problem.action_matrix(stage(2), 2), 1);
  EXPECT_EQ(problem.action_matrix(stage(2), 3), 0);
  EXPECT_EQ(problem.action_matrix(stage(2), 4), -1);
  EXPECT_EQ(problem.action_matrix(stage(2), 5), -2);
  EXPECT_EQ(problem.action_matrix(stage(2), 6), -3);
  EXPECT_EQ(problem.action_matrix(stage(2), 7), -4);
  EXPECT_EQ(problem.action_matrix(stage(2), 8), -5);
  EXPECT_EQ(problem.action_matrix(stage(2), 9), -6);

  EXPECT_EQ(problem.action_matrix(stage(3), 0), 5);
  EXPECT_EQ(problem.action_matrix(stage(3), 1), 4);
  EXPECT_EQ(problem.action_matrix(stage(3), 2), 3);
  EXPECT_EQ(problem.action_matrix(stage(3), 3), 2);
  EXPECT_EQ(problem.action_matrix(stage(3), 4), 1);
  EXPECT_EQ(problem.action_matrix(stage(3), 5), 0);
  EXPECT_EQ(problem.action_matrix(stage(3), 6), -1);
  EXPECT_EQ(problem.action_matrix(stage(3), 7), -2);
  EXPECT_EQ(problem.action_matrix(stage(3), 8), -3);
  EXPECT_EQ(problem.action_matrix(stage(3), 9), -4);
}

TEST(StaffManagementProblem, HighHireCostActionMatrix) {
  int hire_cost_fixed=100;
  int hire_cost_variable=10;
  int wage_cost=40;
  StaffManagementProblem problem(StaffManagementProblem::day{3}, 9);
  problem.use_action_matrix();
  problem.set_requirements({5,3,5});
  problem.set_hire_cost(hire_cost_fixed,hire_cost_variable);
  problem.set_wage_cost(wage_cost);
  problem.calculate();
  EXPECT_EQ(problem.action_matrix(stage(1), 0), 5);
  EXPECT_EQ(problem.action_matrix(stage(1), 1), 4);
  EXPECT_EQ(problem.action_matrix(stage(1), 2), 3);
  EXPECT_EQ(problem.action_matrix(stage(1), 3), 2);
  EXPECT_EQ(problem.action_matrix(stage(1), 4), 1);
  EXPECT_EQ(problem.action_matrix(stage(1), 5), 0);
  EXPECT_EQ(problem.action_matrix(stage(1), 6), -1);
  EXPECT_EQ(problem.action_matrix(stage(1), 7), -2);
  EXPECT_EQ(problem.action_matrix(stage(1), 8), -3);
  EXPECT_EQ(problem.action_matrix(stage(1), 9), -4);

  EXPECT_EQ(problem.action_matrix(stage(2), 0), 5);
  EXPECT_EQ(problem.action_matrix(stage(2), 1), 4);
  EXPECT_EQ(problem.action_matrix(stage(2), 2), 3);
  EXPECT_EQ(problem.action_matrix(stage(2), 3), 0);
  EXPECT_EQ(problem.action_matrix(stage(2), 4), -1);
  EXPECT_EQ(problem.action_matrix(stage(2), 5), 0);
  EXPECT_EQ(problem.action_matrix(stage(2), 6), -1);
  EXPECT_EQ(problem.action_matrix(stage(2), 7), -2);
  EXPECT_EQ(problem.action_matrix(stage(2), 8), -3);
  EXPECT_EQ(problem.action_matrix(stage(2), 9), -4);

  EXPECT_EQ(problem.action_matrix(stage(3), 0), 5);
  EXPECT_EQ(problem.action_matrix(stage(3), 1), 4);
  EXPECT_EQ(problem.action_matrix(stage(3), 2), 3);
  EXPECT_EQ(problem.action_matrix(stage(3), 3), 2);
  EXPECT_EQ(problem.action_matrix(stage(3), 4), 1);
  EXPECT_EQ(problem.action_matrix(stage(3), 5), 0);
  EXPECT_EQ(problem.action_matrix(stage(3), 6), -1);
  EXPECT_EQ(problem.action_matrix(stage(3), 7), -2);
  EXPECT_EQ(problem.action_matrix(stage(3), 8), -3);
  EXPECT_EQ(problem.action_matrix(stage(3), 9), -4);
}