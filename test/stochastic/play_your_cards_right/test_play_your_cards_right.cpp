#include <gtest/gtest.h>
#include <limits>
#include <numeric>
#include <play_your_cards_right.h>
#include <state_loop.h>
#include <vector>

double epsilon = 0.01;

TEST(PlayYourCardsRight, FinalPrizeCorrect) {
  std::vector<double> prize;
  for (auto i = 0; i <= 54; ++i)
    prize.push_back(double(i * 10));
  PlayYourCardsRight problem(prize);
  EXPECT_NEAR(problem.value_function(stage(0), 1), 0.0, 0.01);
  EXPECT_NEAR(problem.value_function(stage(0), 2), 0.0, 0.01);
  EXPECT_NEAR(problem.value_function(stage(0), 3), 0.0, 0.01);
  EXPECT_NEAR(problem.value_function(stage(0), 4), 0.0, 0.01);
  EXPECT_NEAR(problem.value_function(stage(0), 5), 0.0, 0.01);
  EXPECT_NEAR(problem.value_function(stage(0), 6), 0.0, 0.01);
  EXPECT_NEAR(problem.value_function(stage(0), 7), 0.0, 0.01);
  EXPECT_NEAR(problem.value_function(stage(0), 8), 0.0, 0.01);
  EXPECT_NEAR(problem.value_function(stage(0), 9), 0.0, 0.01);
  EXPECT_NEAR(problem.value_function(stage(0), 10), 0.0, 0.01);
  EXPECT_NEAR(problem.value_function(stage(0), 11), 0.0, 0.01);
  EXPECT_NEAR(problem.value_function(stage(0), 12), 0.0, 0.01);
  EXPECT_NEAR(problem.value_function(stage(0), 13), 0.0, 0.01);
}

TEST(PlayYourCardsRight, LastRoundCorrect) {
  std::vector<double> prize;
  for (auto i = 0; i <= 54; ++i)
    prize.push_back(double(i * 10));
  PlayYourCardsRight problem(prize);
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(1), 1),
              std::max(prize[53], prize[54] * (13.0 - 1.0) / 13.0), 0.01);
  EXPECT_NEAR(problem.value_function(stage(1), 2),
              std::max(prize[53], prize[54] * (13.0 - 2.0) / 13.0), 0.01);
  EXPECT_NEAR(problem.value_function(stage(1), 3),
              std::max(prize[53], prize[54] * (13.0 - 3.0) / 13.0), 0.01);
  EXPECT_NEAR(problem.value_function(stage(1), 4),
              std::max(prize[53], prize[54] * (13.0 - 4.0) / 13.0), 0.01);
  EXPECT_NEAR(problem.value_function(stage(1), 5),
              std::max(prize[53], prize[54] * (13.0 - 5.0) / 13.0), 0.01);
  EXPECT_NEAR(problem.value_function(stage(1), 6),
              std::max(prize[53], prize[54] * (13.0 - 6.0) / 13.0), 0.01);
  EXPECT_NEAR(problem.value_function(stage(1), 7),
              std::max(prize[53], prize[54] * (13.0 - 7.0) / 13.0), 0.01);
  EXPECT_NEAR(problem.value_function(stage(1), 8),
              std::max(prize[53], prize[54] * (13.0 - 6.0) / 13.0), 0.01);
  EXPECT_NEAR(problem.value_function(stage(1), 9),
              std::max(prize[53], prize[54] * (13.0 - 5.0) / 13.0), 0.01);
  EXPECT_NEAR(problem.value_function(stage(1), 10),
              std::max(prize[53], prize[54] * (13.0 - 4.0) / 13.0), 0.01);
  EXPECT_NEAR(problem.value_function(stage(1), 11),
              std::max(prize[53], prize[54] * (13.0 - 3.0) / 13.0), 0.01);
  EXPECT_NEAR(problem.value_function(stage(1), 12),
              std::max(prize[53], prize[54] * (13.0 - 2.0) / 13.0), 0.01);
  EXPECT_NEAR(problem.value_function(stage(1), 13),
              std::max(prize[53], prize[54] * (13.0 - 1.0) / 13.0), 0.01);
}

TEST(PlayYourCardsRight, PenultimateRoundCorrect) {
  std::vector<double> prize;
  for (auto i = 0; i <= 54; ++i)
    prize.push_back(double(i * 10));
  PlayYourCardsRight problem(prize);
  problem.calculate();
  std::vector<double> last_stage_value_function;
  for (auto i = 1u; i <= 13u; ++i)
    last_stage_value_function.push_back(problem.value_function(stage(1), i));
  EXPECT_NEAR(
      problem.value_function(stage(2), 1),
      std::max(prize[52], std::accumulate(last_stage_value_function.begin() + 1,
                                          last_stage_value_function.end(), 0) *
                              1.0 / 13.0),
      0.01);
  EXPECT_NEAR(
      problem.value_function(stage(2), 2),
      std::max(prize[52], std::accumulate(last_stage_value_function.begin() + 2,
                                          last_stage_value_function.end(), 0) *
                              1.0 / 13.0),
      0.01);
  EXPECT_NEAR(
      problem.value_function(stage(2), 3),
      std::max(prize[52], std::accumulate(last_stage_value_function.begin() + 3,
                                          last_stage_value_function.end(), 0) *
                              1.0 / 13.0),
      0.01);
  EXPECT_NEAR(
      problem.value_function(stage(2), 4),
      std::max(prize[52], std::accumulate(last_stage_value_function.begin() + 4,
                                          last_stage_value_function.end(), 0) *
                              1.0 / 13.0),
      0.01);
  EXPECT_NEAR(
      problem.value_function(stage(2), 5),
      std::max(prize[52], std::accumulate(last_stage_value_function.begin() + 5,
                                          last_stage_value_function.end(), 0) *
                              1.0 / 13.0),
      0.01);
  EXPECT_NEAR(
      problem.value_function(stage(2), 6),
      std::max(prize[52], std::accumulate(last_stage_value_function.begin() + 6,
                                          last_stage_value_function.end(), 0) *
                              1.0 / 13.0),
      0.01);
  EXPECT_NEAR(
      problem.value_function(stage(2), 7),
      std::max(prize[52], std::accumulate(last_stage_value_function.begin() + 7,
                                          last_stage_value_function.end(), 0) *
                              1.0 / 13.0),
      0.01);
  EXPECT_NEAR(
      problem.value_function(stage(2), 8),
      std::max(prize[52],
               std::accumulate(last_stage_value_function.begin(),
                               last_stage_value_function.begin() + 6, 0) *
                   1.0 / 13.0),
      0.01);
  EXPECT_NEAR(
      problem.value_function(stage(2), 9),
      std::max(prize[52],
               std::accumulate(last_stage_value_function.begin(),
                               last_stage_value_function.begin() + 5, 0) *
                   1.0 / 13.0),
      0.01);
  EXPECT_NEAR(
      problem.value_function(stage(2), 10),
      std::max(prize[52],
               std::accumulate(last_stage_value_function.begin(),
                               last_stage_value_function.begin() + 4, 0) *
                   1.0 / 13.0),
      0.01);
  EXPECT_NEAR(
      problem.value_function(stage(2), 11),
      std::max(prize[52],
               std::accumulate(last_stage_value_function.begin(),
                               last_stage_value_function.begin() + 3, 0) *
                   1.0 / 13.0),
      0.01);
  EXPECT_NEAR(
      problem.value_function(stage(2), 12),
      std::max(prize[52],
               std::accumulate(last_stage_value_function.begin(),
                               last_stage_value_function.begin() + 2, 0) *
                   1.0 / 13.0),
      0.01);
  EXPECT_NEAR(
      problem.value_function(stage(2), 13),
      std::max(prize[52],
               std::accumulate(last_stage_value_function.begin(),
                               last_stage_value_function.begin() + 1, 0) *
                   1.0 / 13.0),
      0.01);
}

TEST(PlayYourCardsRight, AllOtherRoundsCorrect) {
  std::vector<double> prize;
  for (auto i = 0; i <= 54; ++i)
    prize.push_back(double(i * 10));

  PlayYourCardsRight problem(prize);
  problem.calculate();
  // for (auto t = 1u; t <= 54u; ++t)
  auto t = 43u;
  {
    std::vector<double> last_stage_value_function;
    std::vector<double> probability;
    for (auto i = 1u; i <= 13u; ++i) {
      last_stage_value_function.push_back(
          problem.value_function(stage(t - 1), i));
      probability.push_back(1.0 / 13.0);
    }
    // auto i = 1u;
    for (auto i = 1u; i <= 7u; ++i) { // higher
      double expected_value = std::inner_product(
          last_stage_value_function.begin() + i,
          last_stage_value_function.end(), probability.begin() + i, 0.0);
      EXPECT_NEAR(problem.value_function(stage(t), i),
                  std::max(prize[54 - t], expected_value), 0.01)
          << " higher t = " << t << " v( " << i
          << " ) = " << problem.value_function(stage(t), i) << " vs max( "
          << prize[54 - t] << " , " << expected_value << " )\n";
    }
    for (auto i = 8u; i <= 13u; ++i) { // lower
      double expected_value = std::inner_product(
          last_stage_value_function.begin(),
          last_stage_value_function.begin() + i - 1, probability.begin(), 0.0);
      EXPECT_NEAR(problem.value_function(stage(t), i),
                  std::max(prize[54 - t], expected_value), 0.01)
          << "lower t = " << t << " v( " << i
          << " ) = " << problem.value_function(stage(t), i) << " vs max( "
          << prize[54 - t] << " , " << expected_value << " )\n";
    }
  }
}

TEST(PlayYourCardsRight, AllOtherRoundsCorrectActionMatrix) {
  std::vector<double> prize;
  for (auto i = 0; i <= 54; ++i)
    prize.push_back(double(i * 10));
  PlayYourCardsRight problem(prize);
  problem.use_action_matrix();
  problem.calculate();
  for (auto t = 1u; t <= 54u; ++t)
  // auto t=43u;
  {
    std::vector<double> last_stage_value_function;
    std::vector<double> probability;
    for (auto i = 1u; i <= 13u; ++i) {
      last_stage_value_function.push_back(
          problem.value_function(stage(t - 1), i));
      probability.push_back(1.0 / 13.0);
    }
    // auto i = 1u;
    for (auto i = 1u; i <= 7u; ++i) { // higher
      double expected_value = std::inner_product(
          last_stage_value_function.begin() + i,
          last_stage_value_function.end(), probability.begin() + i, 0.0);
      EXPECT_EQ(problem.action_matrix(stage(t), i),
                expected_value > prize[54 - t] ? 1 : 0);
    }
    for (auto i = 8u; i <= 13u; ++i) { // lower
      double expected_value = std::inner_product(
          last_stage_value_function.begin(),
          last_stage_value_function.begin() + i - 1, probability.begin(), 0.0);
      EXPECT_EQ(problem.action_matrix(stage(t), i),
                expected_value > prize[54 - t] ? 1 : 0);
    }
  }
}

TEST(PlayYourCardsRight, Problem2Correct) {
  std::vector<double> prize;
  prize.push_back(1000);
  for (auto i = 0u; i < 54u; i++)
    prize.push_back(0.9 * prize[i]);
  std::reverse(prize.begin(), prize.end());
  PlayYourCardsRight problem(prize);
  problem.calculate();
  for (auto t = 1u; t <= 54u; ++t)
  // auto t=43u;
  {
    std::vector<double> last_stage_value_function;
    std::vector<double> probability;
    for (auto i = 1u; i <= 13u; ++i) {
      last_stage_value_function.push_back(
          problem.value_function(stage(t - 1), i));
      probability.push_back(1.0 / 13.0);
    }
    // auto i = 1u;
    for (auto i = 1u; i <= 7u; ++i) { // higher
      double expected_value = std::inner_product(
          last_stage_value_function.begin() + i,
          last_stage_value_function.end(), probability.begin() + i, 0.0);
      EXPECT_NEAR(problem.value_function(stage(t), i),
                  std::max(prize[54 - t], expected_value), 0.01)
          << "t = " << t << " v( " << i
          << " ) = " << problem.value_function(stage(t), i) << " vs max( "
          << prize[t] << " , " << expected_value << " )\n";
    }
    for (auto i = 8u; i <= 13u; ++i) { // lower
      double expected_value = std::inner_product(
          last_stage_value_function.begin(),
          last_stage_value_function.begin() + i - 1, probability.begin(), 0.0);
      EXPECT_NEAR(problem.value_function(stage(t), i),
                  std::max(prize[54 - t], expected_value), 0.01)
          << "t = " << t << " v( " << i
          << " ) = " << problem.value_function(stage(t), i) << " vs max( "
          << prize[t] << " , " << expected_value << " )\n";
    }
  }
}

TEST(PlayYourCardsRight, Problem2ActionMatrix) {
  std::vector<double> prize;
  prize.push_back(1000);
  for (auto i = 0u; i < 54u; i++)
    prize.push_back(0.9 * prize[i]);
  std::reverse(prize.begin(), prize.end());

  PlayYourCardsRight problem(prize);
  problem.use_action_matrix();
  problem.calculate();
  for (auto t = 1u; t <= 54u; ++t)
  // auto t=43u;
  {
    std::vector<double> last_stage_value_function;
    std::vector<double> probability;
    for (auto i = 1u; i <= 13u; ++i) {
      last_stage_value_function.push_back(
          problem.value_function(stage(t - 1), i));
      probability.push_back(1.0 / 13.0);
    }
    // auto i = 1u;
    for (auto i = 1u; i <= 7u; ++i) { // higher
      double expected_value = std::inner_product(
          last_stage_value_function.begin() + i,
          last_stage_value_function.end(), probability.begin() + i, 0.0);
      EXPECT_EQ(problem.action_matrix(stage(t), i),
                expected_value > prize[54 - t] ? 1 : 0);
    }
    for (auto i = 8u; i <= 13u; ++i) { // lower
      double expected_value = std::inner_product(
          last_stage_value_function.begin(),
          last_stage_value_function.begin() + i - 1, probability.begin(), 0.0);
      EXPECT_EQ(problem.action_matrix(stage(t), i),
                expected_value > prize[54 - t] ? 1 : 0);
    }
  }
}

TEST(PlayYourCardsRight, Problem3Correct) {
  std::vector<double> prize;
  prize.push_back(1000);
  for (auto i = 0u; i < 54u; i++)
    prize.push_back(0.75 * prize[i]);
  std::reverse(prize.begin(), prize.end());

  PlayYourCardsRight problem(prize);
  problem.calculate();
  for (auto t = 1u; t <= 54u; ++t)
  // auto t=43u;
  {
    std::vector<double> last_stage_value_function;
    std::vector<double> probability;
    for (auto i = 1u; i <= 13u; ++i) {
      last_stage_value_function.push_back(
          problem.value_function(stage(t - 1), i));
      probability.push_back(1.0 / 13.0);
    }
    // auto i = 1u;
    for (auto i = 1u; i <= 7u; ++i) { // higher
      double expected_value = std::inner_product(
          last_stage_value_function.begin() + i,
          last_stage_value_function.end(), probability.begin() + i, 0.0);
      EXPECT_NEAR(problem.value_function(stage(t), i),
                  std::max(prize[54 - t], expected_value), 0.01)
          << "t = " << t << " v( " << i
          << " ) = " << problem.value_function(stage(t), i) << " vs max( "
          << prize[t] << " , " << expected_value << " )\n";
    }
    for (auto i = 8u; i <= 13u; ++i) { // lower
      double expected_value = std::inner_product(
          last_stage_value_function.begin(),
          last_stage_value_function.begin() + i - 1, probability.begin(), 0.0);
      EXPECT_NEAR(problem.value_function(stage(t), i),
                  std::max(prize[54 - t], expected_value), 0.01)
          << "t = " << t << " v( " << i
          << " ) = " << problem.value_function(stage(t), i) << " vs max( "
          << prize[t] << " , " << expected_value << " )\n";
    }
  }
}

TEST(PlayYourCardsRight, Problem3ActionMatrix) {
  std::vector<double> prize;
  prize.push_back(1000);
  for (auto i = 0u; i < 54u; i++)
    prize.push_back(0.75 * prize[i]);
  std::reverse(prize.begin(), prize.end());

  PlayYourCardsRight problem(prize);
  problem.use_action_matrix();
  problem.calculate();
  for (auto t = 1u; t <= 54u; ++t)
  // auto t=43u;
  {
    std::vector<double> last_stage_value_function;
    std::vector<double> probability;
    for (auto i = 1u; i <= 13u; ++i) {
      last_stage_value_function.push_back(
          problem.value_function(stage(t - 1), i));
      probability.push_back(1.0 / 13.0);
    }
    // auto i = 1u;
    for (auto i = 1u; i <= 7u; ++i) { // higher
      double expected_value = std::inner_product(
          last_stage_value_function.begin() + i,
          last_stage_value_function.end(), probability.begin() + i, 0.0);
      EXPECT_EQ(problem.action_matrix(stage(t), i),
                expected_value > prize[54 - t] ? 1 : 0);
    }
    for (auto i = 8u; i <= 13u; ++i) { // lower
      double expected_value = std::inner_product(
          last_stage_value_function.begin(),
          last_stage_value_function.begin() + i - 1, probability.begin(), 0.0);
      EXPECT_EQ(problem.action_matrix(stage(t), i),
                expected_value > prize[54 - t] ? 1 : 0);
    }
  }
}

TEST(PlayYourCardsRight, RoundWorks) {
  std::vector<double> prize;
  prize.push_back(1000);
  for (auto i = 0u; i < 54u; i++)
    prize.push_back(0.75 * prize[i]);
  std::reverse(prize.begin(), prize.end());

  PlayYourCardsRight problem(prize);
  problem.use_action_matrix();
  problem.calculate();
  for (auto t = 1u; t < 54u; ++t)
  // auto t=43u;
  {
    std::vector<double> last_stage_value_function;
    std::vector<double> probability;
    for (auto i = 1u; i <= 13u; ++i) {
      last_stage_value_function.push_back(
          problem.value_function(round{t + 1}, i));
      probability.push_back(1.0 / 13.0);
    }
    // auto i = 1u;
    for (auto i = 1u; i <= 7u; ++i) { // higher
      double expected_value = std::inner_product(
          last_stage_value_function.begin() + i,
          last_stage_value_function.end(), probability.begin() + i, 0.0);
      EXPECT_EQ(problem.action_matrix(round{t}, i),
                expected_value > prize[t - 1] ? 1 : 0);
    }
    for (auto i = 8u; i <= 13u; ++i) { // lower
      double expected_value = std::inner_product(
          last_stage_value_function.begin(),
          last_stage_value_function.begin() + i - 1, probability.begin(), 0.0);
      EXPECT_EQ(problem.action_matrix(round{t}, i),
                expected_value > prize[t - 1] ? 1 : 0);
    }
  }
}

TEST(PlayYourCardsRight, ExpectedPrize) {
  std::vector<double> prize;
  prize.push_back(10000000);
  for (auto i = 0u; i < 54u; i++)
    prize.push_back(0.75 * prize[i]);
  std::reverse(prize.begin(), prize.end());

  PlayYourCardsRight problem(prize);
  problem.use_action_matrix();

  problem.calculate();
  double expected = 0.0;
  for (auto i = 1u; i <= 13u; ++i) {
    expected += (1.0 / 13.0) * problem.value_function(round{1}, i);
  }
  EXPECT_NEAR(problem.expected_prize(), expected, epsilon);
}