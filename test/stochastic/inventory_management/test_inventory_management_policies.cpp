#include <gtest/gtest.h>

#include "inventory_management.h"
// #include "no_pooling_policy.h"
#include "policy_nearest_neighbour.h"
#include "policy_no_pooling.h"

#include <set>
#include <state_loop_vector.h>
#include <unordered_set>
#include <vector>

double epsilon = 0.01;

TEST(InventoryManagementNoPooling, BaseStateCorrect) {
  InventoryManagementParameters params;
  params.T = 0;
  params.restock_level = {5, 5, 5};
  params.demand = {0.25, 0.25, 0.25};
  params.manufacturer_cost = {10, 10, 10};
  params.transfer_cost = {{0, 100, 100}, {100, 0, 100}, {100, 100, 0}};
  InventoryManagement<PolicyNoPooling> problem =
      InventoryManagement<PolicyNoPooling>::inventory_management_factory(
          params);
  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  for (const auto &state : state_loop) {
    EXPECT_NEAR(problem.value_function(stage(0), state), 0.0, epsilon);
  }
}

TEST(InventoryManagementNoPooling, DemandOccursOnePeriod) {
  InventoryManagementParameters params;
  params.T = 1;
  params.restock_level = {1, 1, 1};
  params.demand = {0.25, 0.25, 0.25};
  params.manufacturer_cost = {10, 10, 10};
  params.transfer_cost = {{0, 100, 100}, {100, 0, 100}, {100, 100, 0}};
  InventoryManagement<PolicyNoPooling> problem =
      InventoryManagement<PolicyNoPooling>::inventory_management_factory(
          params);
  // problem.init();
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 1}), 0.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 1, 1}), 2.5, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 0, 1}), 2.5, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 0}), 2.5, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 0, 1}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 0, 0}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 1, 0}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 0, 0}), 7.5, epsilon);
}

TEST(InventoryManagementNoPooling, DemandOccursTwoPeriods) {
  InventoryManagementParameters params;
  params.T = 2;
  params.restock_level = {2, 2, 2};
  params.demand = {0.5, 0.5, 0.5};
  params.manufacturer_cost = {10, 10, 10};
  params.transfer_cost = {{0, 100, 100}, {100, 0, 100}, {100, 100, 0}};

  InventoryManagement<PolicyNoPooling> problem =
      InventoryManagement<PolicyNoPooling>::inventory_management_factory(
          params);
  // problem.init();
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 1}), 0.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 1, 1}), 2.5, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 0, 1}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 0, 0}), 7.5, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 2, 2}), 0.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {1, 2, 2}), 0.625, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 1, 2}), 0.625, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 2, 1}), 0.625, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {0, 2, 2}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 0, 2}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 2, 0}), 5.0, epsilon);
}

TEST(InventoryManagementNoPooling, ManufacturingCostCanVary) {
  InventoryManagementParameters params;
  params.T = 2;
  params.restock_level = {2, 2, 2};
  params.demand = {0.5, 0.5, 0.5};
  params.manufacturer_cost = {15, 10, 5};
  params.transfer_cost = {{0, 100, 100}, {100, 0, 100}, {100, 100, 0}};

  InventoryManagement<PolicyNoPooling> problem =
      InventoryManagement<PolicyNoPooling>::inventory_management_factory(
          params);
  // problem.init();
  problem.calculate();

  EXPECT_NEAR(problem.value_function(stage(2), {0, 2, 2}), 7.5, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 0, 2}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 2, 0}), 2.5, epsilon);
}

TEST(InventoryManagementNoPooling, StockTransferDoesNotWork) {
  InventoryManagementParameters params;
  params.T = 1;
  params.restock_level = {1, 1, 1};
  params.demand = {0.25, 0.25, 0.25};
  params.manufacturer_cost = {20, 20, 20};
  params.transfer_cost = {{0, 5, 10}, //
                          {5, 0, 15}, //
                          {10, 15, 0}};

  InventoryManagement<PolicyNoPooling> problem =
      InventoryManagement<PolicyNoPooling>::inventory_management_factory(
          params);
  // problem.init();
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 1}), 0.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 1, 1}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 0, 1}), 10.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 0, 0}), 15.0, epsilon);
}

TEST(InventoryManagementNoPooling, EventActionsWorkOkay) {
  InventoryManagementParameters params;
  params.T = 200;
  params.restock_level = {5, 5, 5};
  params.demand = {10, 10, 10};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost,
                              manufacturer_cost};
  params.transfer_cost = {{0, 15, 25}, //
                          {15, 0, 35}, //
                          {25, 35, 0}};

  InventoryManagement<PolicyNoPooling> problem =
      InventoryManagement<PolicyNoPooling>::inventory_management_factory(
          params);
  // problem.init();
  problem.use_event_action_matrix();
  problem.calculate();

  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  // for (auto t = 1u; t <= 200u; ++t)
  for (auto t : {3u, 10u, 20u})
  {
    std::vector<unsigned int> state = {3, 3, 3};
    EXPECT_EQ(
        problem.event_action_matrix(stage(t), state, {EventEnum::no_demand, 0}),
        0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 0}),
              1)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 1}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 2}),
              3)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 3, 3};
    EXPECT_EQ(
        problem.event_action_matrix(stage(t), state, {EventEnum::no_demand, 0}),
        0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 1}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 2}),
              3)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 3, 0};
    EXPECT_EQ(
        problem.event_action_matrix(stage(t), state, {EventEnum::no_demand, 0}),
        0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 1}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 2}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
  }
}

TEST(InventoryManagementNoPooling, EventActionsSelectiveWorkOkay) {
  InventoryManagementParameters params;
  params.T = 200;
  params.restock_level = {5, 5, 5};
  params.demand = {10, 10, 10};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost,
                              manufacturer_cost};
  params.transfer_cost = {{0, 15, 25}, //
                          {15, 0, 35}, //
                          {25, 35, 0}};

  InventoryManagement<PolicyNoPooling> problem =
      InventoryManagement<PolicyNoPooling>::inventory_management_factory(
          params);
  // problem.init();
  problem.use_event_action_matrix({{EventEnum::demand_with_no_stock, 0},
                                   {EventEnum::demand_with_no_stock, 1},
                                   {EventEnum::demand_with_no_stock, 2}});
  problem.calculate();

  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  // for (auto t = 1u; t <= 200u; ++t)
  for (auto t : {3u, 10u, 20u})
  {
    std::vector<unsigned int> state = {3, 3, 3};
    EXPECT_DEATH(
        problem.event_action_matrix(stage(t), state, {EventEnum::no_demand, 0}),
        "Assertion.*failed")
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_DEATH(problem.event_action_matrix(stage(t), state,
                                             {EventEnum::demand_with_stock, 0}),
                 "Assertion.*failed")
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_DEATH(problem.event_action_matrix(stage(t), state,
                                             {EventEnum::demand_with_stock, 1}),
                 "Assertion.*failed")
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_DEATH(problem.event_action_matrix(stage(t), state,
                                             {EventEnum::demand_with_stock, 2}),
                 "Assertion.*failed")
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 3, 3};
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 3, 0};
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 2}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 0, 0};
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 1}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 2}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
  }
}

TEST(InventoryManagementNearestNeighbour, BaseStateCorrect) {
  InventoryManagementParameters params;
  params.T = 0;
  params.restock_level = {5, 5, 5};
  params.demand = {0.25, 0.25, 0.25};
  params.manufacturer_cost = {10, 10, 10};
  params.transfer_cost = {{0, 100, 100}, {100, 0, 100}, {100, 100, 0}};
  InventoryManagement<PolicyNearestNeighbour> problem =
      InventoryManagement<PolicyNearestNeighbour>::inventory_management_factory(
          params);
  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  for (const auto &state : state_loop) {
    EXPECT_NEAR(problem.value_function(stage(0), state), 0.0, epsilon);
  }
}

TEST(InventoryManagementNearestNeighbour, DemandOccursOnePeriod) {
  InventoryManagementParameters params;
  params.T = 1;
  params.restock_level = {1, 1, 1};
  params.demand = {0.25, 0.25, 0.25};
  params.manufacturer_cost = {10, 10, 10};
  params.transfer_cost = {{0, 100, 100}, {100, 0, 100}, {100, 100, 0}};
  InventoryManagement<PolicyNearestNeighbour> problem =
      InventoryManagement<PolicyNearestNeighbour>::inventory_management_factory(
          params);
  // problem.init();
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 1}), 0.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 1, 1}), 25.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 0, 1}), 25.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 0}), 25.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 0, 1}), 50.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 0, 0}), 50.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 1, 0}), 50.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 0, 0}), 7.5, epsilon);
}

TEST(InventoryManagementNearestNeighbour, DemandOccursTwoPeriods) {
  InventoryManagementParameters params;
  params.T = 2;
  params.restock_level = {2, 2, 2};
  params.demand = {0.5, 0.5, 0.5};
  params.manufacturer_cost = {10, 10, 10};
  params.transfer_cost = {{0, 100, 100}, {100, 0, 100}, {100, 100, 0}};

  InventoryManagement<PolicyNearestNeighbour> problem =
      InventoryManagement<PolicyNearestNeighbour>::inventory_management_factory(
          params);
  // problem.init();
  problem.calculate();

  EXPECT_NEAR(problem.value_function(stage(2), {2, 2, 2}), 0.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {1, 2, 2}), 6.25, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 1, 2}), 6.25, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 2, 1}), 6.25, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {0, 2, 2}), 50.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 0, 2}), 50.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 2, 0}), 50.0, epsilon);
}

TEST(InventoryManagementNearestNeighbour, ManufacturingCostCanVary) {
  InventoryManagementParameters params;
  params.T = 2;
  params.restock_level = {2, 2, 2};
  params.demand = {0.5, 0.5, 0.5};
  params.manufacturer_cost = {15, 10, 5};
  params.transfer_cost = {{0, 100, 100}, {100, 0, 100}, {100, 100, 0}};

  InventoryManagement<PolicyNearestNeighbour> problem =
      InventoryManagement<PolicyNearestNeighbour>::inventory_management_factory(
          params);
  // problem.init();
  problem.calculate();

  EXPECT_NEAR(problem.value_function(stage(2), {0, 2, 2}), 50.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 0, 2}), 50.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 2, 0}), 50.0, epsilon);
}

TEST(InventoryManagementNearestNeighbour, StockTransferWorks) {
  InventoryManagementParameters params;
  params.T = 1;
  params.restock_level = {1, 1, 1};
  params.demand = {0.25, 0.25, 0.25};
  params.manufacturer_cost = {20, 20, 20};
  params.transfer_cost = {{0, 5, 10}, //
                          {5, 0, 15}, //
                          {10, 15, 0}};

  InventoryManagement<PolicyNearestNeighbour> problem =
      InventoryManagement<PolicyNearestNeighbour>::inventory_management_factory(
          params);
  // problem.init();
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 1}), 0.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 1, 1}), 1.25, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 0, 1}), 1.25, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 0}), 2.5, epsilon);
}

TEST(InventoryManagementNearestNeighbour, StockTransfeOverManufacturerWorks) {
  InventoryManagementParameters params;
  params.T = 1;
  params.restock_level = {1, 1, 1};
  params.demand = {0.25, 0.25, 0.25};
  params.manufacturer_cost = {20, 20, 20};
  params.transfer_cost = {{0, 5, 25}, //
                          {5, 0, 25}, //
                          {25, 25, 0}};

  InventoryManagement<PolicyNearestNeighbour> problem =
      InventoryManagement<PolicyNearestNeighbour>::inventory_management_factory(
          params);
  // problem.init();
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 1}), 0.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 1, 1}), 1.25, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 0, 1}), 1.25, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 0}), 6.25, epsilon);
}

TEST(InventoryManagementNearestNeighbour,
     StockTransferOverManufacturerWorksManyStagesTwoLocation) {

  InventoryManagementParameters params;
  params.T = 200;
  params.restock_level = {10, 10};
  params.demand = {10, 10};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost};
  params.transfer_cost = {{0, 15}, //
                          {15, 0}};

  InventoryManagement<PolicyNearestNeighbour> problem =
      InventoryManagement<PolicyNearestNeighbour>::inventory_management_factory(
          params);
  // problem.init();
  problem.calculate();

  double demand_prob_at_location = 10.0 / 200.0;
  double no_demand_prob = 1.0 - (20.0 / 200.0);
  StateLoop<std::vector<unsigned int>> state_loop({0, 0}, params.restock_level);
  for (auto t = 1u; t <= 200u; ++t)
  // auto t = 2u;
  {

    for (const auto &state : state_loop) {
      if (state[0] > 0 && state[1] > 0) {
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        for (auto l = 0u; l < state.size(); ++l) {
          auto new_state = state;
          new_state[l]--;
          expected_value += demand_prob_at_location *
                            problem.value_function(stage(t - 1), new_state);
        }
        EXPECT_NEAR(problem.value_function(stage(t), state), expected_value,
                    epsilon)
            << state[0] << " " << state[1] << "\n";
      } else if (state[0] == 0 && state[1] > 0) {
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        auto new_state = state;
        new_state[1]--;
        expected_value += demand_prob_at_location *
                          (params.transfer_cost[1][0] +
                           problem.value_function(stage(t - 1), new_state));
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        EXPECT_NEAR(problem.value_function(stage(t), state), expected_value,
                    epsilon)
            << state[0] << " " << state[1] << "\n";
      }
    }
  }
}

TEST(InventoryManagementNearestNeighbour,
     StockTransferOverManufacturerWorksManyStagesThreeLocation) {
  InventoryManagementParameters params;
  params.T = 200;
  params.restock_level = {5, 5, 5};
  params.demand = {10, 10, 10};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost,
                              manufacturer_cost};
  params.transfer_cost = {{0, 15, 25}, //
                          {15, 0, 35}, //
                          {25, 35, 0}};

  InventoryManagement<PolicyNearestNeighbour> problem =
      InventoryManagement<PolicyNearestNeighbour>::inventory_management_factory(
          params);
  // problem.init();
  problem.calculate();

  double demand_prob_at_location = 10.0 / 200.0;
  double no_demand_prob = 1.0 - (30.0 / 200.0);
  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  // for (auto t = 1u; t <= 200u; ++t)
  for (auto t : {3u, 10u, 20u}) {

    for (const auto &state : state_loop) {
      if (state[0] > 0 && state[1] > 0 && state[2] > 0) {
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        for (auto l = 0u; l < state.size(); ++l) {
          auto new_state = state;
          new_state[l]--;
          expected_value += demand_prob_at_location *
                            problem.value_function(stage(t - 1), new_state);
        }
        EXPECT_DOUBLE_EQ(problem.value_function(stage(t), state),
                         expected_value)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";
      } else if (state[0] == 0 && state[1] > 0 && state[2] > 0) {
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        auto new_state = state;
        new_state[1]--;
        expected_value += demand_prob_at_location *
                          (params.transfer_cost[1][0] +
                           problem.value_function(stage(t - 1), new_state));
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        new_state = state;
        new_state[2]--;
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        EXPECT_DOUBLE_EQ(problem.value_function(stage(t), state),
                         expected_value)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";
      } else if (state[0] == 0 && state[1] > 0 && state[2] == 0) {
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        auto new_state = state;
        new_state[1]--;
        expected_value += demand_prob_at_location *
                          (params.transfer_cost[1][0] +
                           problem.value_function(stage(t - 1), new_state));
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        expected_value += demand_prob_at_location *
                          (params.transfer_cost[1][2] +
                           problem.value_function(stage(t - 1), new_state));
        EXPECT_DOUBLE_EQ(problem.value_function(stage(t), state),
                         expected_value)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";
      }
    }
  }
}

TEST(InventoryManagementNearestNeighbour, EventActionsWorkOkay) {
  InventoryManagementParameters params;
  params.T = 200;
  params.restock_level = {5, 5, 5};
  params.demand = {10, 10, 10};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost,
                              manufacturer_cost};
  params.transfer_cost = {{0, 15, 25}, //
                          {15, 0, 35}, //
                          {25, 35, 0}};

  InventoryManagement<PolicyNearestNeighbour> problem =
      InventoryManagement<PolicyNearestNeighbour>::inventory_management_factory(
          params);
  // problem.init();
  problem.use_event_action_matrix();
  problem.calculate();

  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  // for (auto t = 1u; t <= 200u; ++t)
  for (auto t : {3u, 10u, 20u})
  {
    std::vector<unsigned int> state = {3, 3, 3};
    EXPECT_EQ(
        problem.event_action_matrix(stage(t), state, {EventEnum::no_demand, 0}),
        0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 0}),
              1)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 1}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 2}),
              3)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 3, 3};
    EXPECT_EQ(
        problem.event_action_matrix(stage(t), state, {EventEnum::no_demand, 0}),
        0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 1}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 2}),
              3)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 3, 0};
    EXPECT_EQ(
        problem.event_action_matrix(stage(t), state, {EventEnum::no_demand, 0}),
        0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 1}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 2}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
  }
}

TEST(InventoryManagementNearestNeighbour, EventActionsSelectiveWorkOkay) {
  InventoryManagementParameters params;
  params.T = 200;
  params.restock_level = {5, 5, 5};
  params.demand = {10, 10, 10};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost,
                              manufacturer_cost};
  params.transfer_cost = {{0, 15, 25}, //
                          {15, 0, 35}, //
                          {25, 35, 0}};

  InventoryManagement<PolicyNearestNeighbour> problem =
      InventoryManagement<PolicyNearestNeighbour>::inventory_management_factory(
          params);
  // problem.init();
  problem.use_event_action_matrix({{EventEnum::demand_with_no_stock, 0},
                                   {EventEnum::demand_with_no_stock, 1},
                                   {EventEnum::demand_with_no_stock, 2}});
  problem.calculate();

  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  // for (auto t = 1u; t <= 200u; ++t)
  for (auto t : {3u, 10u, 20u})
  {
    std::vector<unsigned int> state = {3, 3, 3};
    EXPECT_DEATH(
        problem.event_action_matrix(stage(t), state, {EventEnum::no_demand, 0}),
        "Assertion.*failed")
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_DEATH(problem.event_action_matrix(stage(t), state,
                                             {EventEnum::demand_with_stock, 0}),
                 "Assertion.*failed")
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_DEATH(problem.event_action_matrix(stage(t), state,
                                             {EventEnum::demand_with_stock, 1}),
                 "Assertion.*failed")
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_DEATH(problem.event_action_matrix(stage(t), state,
                                             {EventEnum::demand_with_stock, 2}),
                 "Assertion.*failed")
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 3, 3};
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 3, 0};
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 2}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 0, 0};
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 1}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 2}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
  }
}

#ifdef FFFFF
#endif