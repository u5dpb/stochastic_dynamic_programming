#include <gtest/gtest.h>
#include <staggered_inventory_management.h>
#include <state_loop_vector.h>
#include <vector>

TEST(StaggeredInventoryManagementParams, RestockTFunction) {
  StaggeredInventoryManagementParameters params;
  params.restock_time = {0.3, 0.5, 0.7};
  params.T = 1000;
  EXPECT_EQ(params.restock_t()[0], 300);
  EXPECT_EQ(params.restock_t()[1], 500);
  EXPECT_EQ(params.restock_t()[2], 700);
}

TEST(StaggeredInventoryManagement, BaseStateCorrect) {
  StaggeredInventoryManagementParameters params;
  params.demand = {10, 10, 10};
  params.restock_level = {5, 5, 5};
  params.restock_time = {0.3, 0.5, 0.7};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost,
                              manufacturer_cost};
  params.transfer_cost = {{0, 15, 25}, //
                          {15, 0, 35}, //
                          {25, 35, 0}};
  params.T = 0;
  StaggeredInventoryManagement problem = StaggeredInventoryManagement<>::staggered_inventory_management_factory(params);
  // problem.init();
  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  for (const auto &state : state_loop) {
    EXPECT_DOUBLE_EQ(problem.value_function(stage(0), state), 0.0);
  }
}

TEST(StaggeredInventoryManagement, ThreeLocationTestReplenishmentNoStockouts) {
  StaggeredInventoryManagementParameters params;

  params.T = 200;
  params.restock_level = {5, 5, 5};
  params.restock_time = {0.3, 0.5, 0.7};
  params.demand = {10, 10, 10};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost,
                              manufacturer_cost};
  params.transfer_cost = {{0, 15, 25}, //
                          {15, 0, 35}, //
                          {25, 35, 0}};
  StaggeredInventoryManagement problem = StaggeredInventoryManagement<>::staggered_inventory_management_factory(params);
  // problem.init();
  const unsigned int first_replenishment =
      (unsigned int)(floor(double(params.T) * 0.3));
  problem.calculate(stage(first_replenishment));

  double demand_prob_at_location = 10.0 / 200.0;
  double no_demand_prob = 1.0 - (30.0 / 200.0);
  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  // for (auto t = 1u; t <= 200u; ++t)
  auto t = first_replenishment;
  {
    for (const auto &state : state_loop) {
      if (state[0] > 0 && state[1] > 0 && state[2] > 0) {
        auto new_state = state;
        new_state[0] = params.restock_level[0];

        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), new_state);
        for (auto l = 0u; l < state.size(); ++l) {
          new_state = state;
          new_state[0] = params.restock_level[0];
          new_state[l]--;
          expected_value += demand_prob_at_location *
                            problem.value_function(stage(t - 1), new_state);
        }
        EXPECT_DOUBLE_EQ(problem.value_function(stage(t), state),
                         expected_value)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";
        new_state = state;
        expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        for (auto l = 0u; l < state.size(); ++l) {
          new_state = state;
          new_state[l]--;
          expected_value += demand_prob_at_location *
                            problem.value_function(stage(t - 1), new_state);
        }
        EXPECT_LE(problem.value_function(stage(t), state), expected_value)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";
      }
    }
  }
}

TEST(StaggeredInventoryManagement,
     ThreeLocationTestReplenishmentReplishmentLocationStockout) {
  StaggeredInventoryManagementParameters params;

  params.T = 200;
  params.restock_level = {5, 5, 5};
  params.restock_time = {0.3, 0.5, 0.7};
  params.demand = {10, 10, 10};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost,
                              manufacturer_cost};
  params.transfer_cost = {{0, 15, 25}, //
                          {15, 0, 35}, //
                          {25, 35, 0}};
  StaggeredInventoryManagement problem = StaggeredInventoryManagement<>::staggered_inventory_management_factory(params);
  // problem.init();
  const unsigned int first_replenishment =
      (unsigned int)(floor(double(params.T) * 0.3));
  problem.calculate(stage(first_replenishment));
  double demand_prob_at_location = 10.0 / 200.0;
  double no_demand_prob = 1.0 - (30.0 / 200.0);
  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  // for (auto t = 1u; t <= 200u; ++t)
  auto t = first_replenishment;
  {
    for (const auto &state : state_loop) {
      if (state[0] == 0 && state[1] > 0 && state[2] > 0) {
        // no demand
        auto new_state = state;
        new_state[0] = params.restock_level[0];
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), new_state);
        // demand at 0
        new_state[0]--;
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        // demand at 1
        new_state[0] = params.restock_level[0];
        new_state[1]--;
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        // demand at 2
        new_state = state;
        new_state[0] = params.restock_level[0];
        new_state[2]--;
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        EXPECT_DOUBLE_EQ(problem.value_function(stage(t), state),
                         expected_value)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";

        expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        new_state = state;
        new_state[1]--;
        expected_value +=
            demand_prob_at_location *
            std::min(params.transfer_cost[1][0] +
                         problem.value_function(stage(t - 1), new_state),
                     manufacturer_cost +
                         problem.value_function(stage(t - 1), state));
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        new_state = state;
        new_state[2]--;
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        EXPECT_LE(problem.value_function(stage(t), state), expected_value)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";
      }
    }
  }
}

TEST(StaggeredInventoryManagement,
     ThreeLocationTestReplenishment2LocationStockout) {
  StaggeredInventoryManagementParameters params;

  params.T = 200;
  params.restock_level = {5, 5, 5};
  params.restock_time = {0.3, 0.5, 0.7};
  params.demand = {10, 10, 10};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost,
                              manufacturer_cost};
  params.transfer_cost = {{0, 15, 25}, //
                          {15, 0, 35}, //
                          {25, 35, 0}};
  StaggeredInventoryManagement problem = StaggeredInventoryManagement<>::staggered_inventory_management_factory(params);
  // problem.init();
  const unsigned int first_replenishment =
      (unsigned int)(floor(double(params.T) * 0.3));
  problem.calculate(stage(first_replenishment));

  double demand_prob_at_location = 10.0 / 200.0;
  double no_demand_prob = 1.0 - (30.0 / 200.0);
  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  // for (auto t = 1u; t <= 200u; ++t)
  auto t = first_replenishment;
  {
    for (const auto &state : state_loop) {
      if (state[0] == 0 && state[1] > 0 && state[2] == 0) {
        // no demand
        auto new_state = state;
        new_state[0] = params.restock_level[0];
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), new_state);
        // demand at 0
        new_state = state;
        new_state[0] = params.restock_level[0];
        new_state[0]--;
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        // demand at 1
        new_state = state;
        new_state[0] = params.restock_level[0];
        new_state[1]--;
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        // demand at 2
        new_state = state;
        new_state[0] = params.restock_level[0];
        auto state_after_transfer = new_state;
        state_after_transfer[0]--;
        expected_value +=
            demand_prob_at_location *
            std::min(
                manufacturer_cost +
                    problem.value_function(stage(t - 1), new_state),
                params.transfer_cost[0][2] +
                    problem.value_function(stage(t - 1), state_after_transfer));
        EXPECT_DOUBLE_EQ(problem.value_function(stage(t), state),
                         expected_value)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";
        expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        new_state = state;
        new_state[1]--;
        expected_value +=
            demand_prob_at_location *
            std::min(params.transfer_cost[1][0] +
                         problem.value_function(stage(t - 1), new_state),
                     manufacturer_cost +
                         problem.value_function(stage(t - 1), state));
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        expected_value +=
            demand_prob_at_location *
            std::min(params.transfer_cost[1][2] +
                         problem.value_function(stage(t - 1), new_state),
                     manufacturer_cost +
                         problem.value_function(stage(t - 1), state));
        EXPECT_GT(
            std::abs(problem.value_function(stage(t), state) - expected_value),
            0.001)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";
      }
    }
  }
}

TEST(StaggeredInventoryManagement, ThreeLocationInfiniteHorizon) {
  const epsilon stopping_epsilon(0.01);

  StaggeredInventoryManagementParameters params;
  params.restock_level = {5, 5, 5};
  params.restock_time = {0.3, 0.5, 0.7};
  params.T = 200;
  params.demand = {10, 10, 10};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost,
                              manufacturer_cost};
  params.transfer_cost = {{0, 15, 25}, //
                          {15, 0, 35}, //
                          {25, 35, 0}};

  StaggeredInventoryManagement problem = StaggeredInventoryManagement<>::staggered_inventory_management_factory(params);
  // problem.init();
  problem.calculate(stopping_epsilon);

  double demand_prob_at_location = 10.0 / 200.0;
  double no_demand_prob = 1.0 - (30.0 / 200.0);
  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  // for (auto t = 1u; t <= 200u; ++t)
  auto t = problem.T();
  {
    for (const auto &state : state_loop) {
      EXPECT_LT(std::abs((problem.value_function(stage(t), state) / double(t)) -
                         (problem.value_function(stage(t - 1), state) /
                          double(t - 1))),
                stopping_epsilon);
      if (state[0] > 0 && state[1] > 0 && state[2] > 0) {
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        for (auto l = 0u; l < state.size(); ++l) {
          auto new_state = state;
          new_state[l]--;
          expected_value += demand_prob_at_location *
                            problem.value_function(stage(t - 1), new_state);
        }
        EXPECT_DOUBLE_EQ(problem.value_function(stage(t), state),
                         expected_value)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";
      } else if (state[0] == 0 && state[1] > 0 && state[2] > 0) {
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        auto new_state = state;
        new_state[1]--;
        expected_value +=
            demand_prob_at_location *
            std::min(params.transfer_cost[1][0] +
                         problem.value_function(stage(t - 1), new_state),
                     manufacturer_cost +
                         problem.value_function(stage(t - 1), state));
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        new_state = state;
        new_state[2]--;
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        EXPECT_DOUBLE_EQ(problem.value_function(stage(t), state),
                         expected_value)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";
      } else if (state[0] == 0 && state[1] > 0 && state[2] == 0) {
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        auto new_state = state;
        new_state[1]--;
        expected_value +=
            demand_prob_at_location *
            std::min(params.transfer_cost[1][0] +
                         problem.value_function(stage(t - 1), new_state),
                     manufacturer_cost +
                         problem.value_function(stage(t - 1), state));
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        expected_value +=
            demand_prob_at_location *
            std::min(params.transfer_cost[1][2] +
                         problem.value_function(stage(t - 1), new_state),
                     manufacturer_cost +
                         problem.value_function(stage(t - 1), state));
        EXPECT_DOUBLE_EQ(problem.value_function(stage(t), state),
                         expected_value)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";
      }
    }
  }
}
