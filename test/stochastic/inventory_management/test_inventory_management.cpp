#include <gtest/gtest.h>

#include "inventory_management.h"
#include "policy_optimal.h"

#include <set>
#include <state_loop_vector.h>
#include <unordered_set>
#include <vector>

double epsilon = 0.01;

TEST(EventTypeStruct, ToInt) {
  std::set<int> collection;
  collection.insert(event_type({EventEnum::no_demand, 0}).to_int());
  EXPECT_EQ(collection.size(), 1);
  collection.insert(event_type({EventEnum::no_demand, 0}).to_int());
  EXPECT_EQ(collection.size(), 1);
  collection.insert(event_type({EventEnum::no_demand, 1}).to_int());
  EXPECT_EQ(collection.size(), 1);
  collection.insert(event_type({EventEnum::no_demand, 2}).to_int());
  EXPECT_EQ(collection.size(), 1);
  collection.insert(event_type({EventEnum::no_demand, 3}).to_int());
  EXPECT_EQ(collection.size(), 1);
  collection.insert(event_type({EventEnum::demand_with_no_stock, 0}).to_int());
  EXPECT_EQ(collection.size(), 2);
  collection.insert(event_type({EventEnum::demand_with_no_stock, 1}).to_int());
  EXPECT_EQ(collection.size(), 3);
  collection.insert(event_type({EventEnum::demand_with_no_stock, 2}).to_int());
  EXPECT_EQ(collection.size(), 4);
  collection.insert(event_type({EventEnum::demand_with_stock, 0}).to_int());
  EXPECT_EQ(collection.size(), 5);
  collection.insert(event_type({EventEnum::demand_with_stock, 1}).to_int());
  EXPECT_EQ(collection.size(), 6);
  collection.insert(event_type({EventEnum::demand_with_stock, 2}).to_int());
  EXPECT_EQ(collection.size(), 7);
}

TEST(EventTypeStruct, Equals) {
  EXPECT_TRUE(event_type({EventEnum::demand_with_stock, 1}) ==
              event_type({EventEnum::demand_with_stock, 1}));
  EXPECT_TRUE(event_type({EventEnum::demand_with_stock, 2}) ==
              event_type({EventEnum::demand_with_stock, 2}));
  EXPECT_FALSE(event_type({EventEnum::demand_with_stock, 1}) ==
               event_type({EventEnum::demand_with_stock, 2}));
  EXPECT_FALSE(event_type({EventEnum::demand_with_stock, 2}) ==
               event_type({EventEnum::demand_with_stock, 1}));
  EXPECT_TRUE(event_type({EventEnum::demand_with_no_stock, 1}) ==
              event_type({EventEnum::demand_with_no_stock, 1}));
  EXPECT_TRUE(event_type({EventEnum::demand_with_no_stock, 2}) ==
              event_type({EventEnum::demand_with_no_stock, 2}));
  EXPECT_FALSE(event_type({EventEnum::demand_with_no_stock, 1}) ==
               event_type({EventEnum::demand_with_no_stock, 2}));
  EXPECT_FALSE(event_type({EventEnum::demand_with_no_stock, 2}) ==
               event_type({EventEnum::demand_with_no_stock, 1}));
  EXPECT_FALSE(event_type({EventEnum::demand_with_no_stock, 1}) ==
               event_type({EventEnum::demand_with_stock, 1}));
  EXPECT_FALSE(event_type({EventEnum::demand_with_no_stock, 2}) ==
               event_type({EventEnum::demand_with_stock, 2}));
  EXPECT_TRUE(event_type({EventEnum::no_demand, 0}) ==
              event_type({EventEnum::no_demand, 1}));
  EXPECT_TRUE(event_type({EventEnum::no_demand, 0}) ==
              event_type({EventEnum::no_demand, 2}));
  EXPECT_TRUE(event_type({EventEnum::no_demand, 1}) ==
              event_type({EventEnum::no_demand, 2}));
  EXPECT_FALSE(event_type({EventEnum::no_demand, 0}) ==
               event_type({EventEnum::demand_with_no_stock, 0}));
  EXPECT_FALSE(event_type({EventEnum::no_demand, 0}) ==
               event_type({EventEnum::demand_with_no_stock, 1}));
  EXPECT_FALSE(event_type({EventEnum::no_demand, 0}) ==
               event_type({EventEnum::demand_with_stock, 0}));
  EXPECT_FALSE(event_type({EventEnum::no_demand, 0}) ==
               event_type({EventEnum::demand_with_stock, 1}));
}

TEST(EventTypeStruct, Hash) {
  std::unordered_set<event_type> collection;
  collection.insert(event_type({EventEnum::no_demand, 0}));
  EXPECT_EQ(collection.size(), 1);
  collection.insert(event_type({EventEnum::no_demand, 0}));
  EXPECT_EQ(collection.size(), 1);
  collection.insert(event_type({EventEnum::no_demand, 1}));
  EXPECT_EQ(collection.size(), 1);
  collection.insert(event_type({EventEnum::no_demand, 2}));
  EXPECT_EQ(collection.size(), 1);
  collection.insert(event_type({EventEnum::no_demand, 3}));
  EXPECT_EQ(collection.size(), 1);
  collection.insert(event_type({EventEnum::demand_with_no_stock, 0}));
  EXPECT_EQ(collection.size(), 2);
  collection.insert(event_type({EventEnum::demand_with_no_stock, 1}));
  EXPECT_EQ(collection.size(), 3);
  collection.insert(event_type({EventEnum::demand_with_no_stock, 2}));
  EXPECT_EQ(collection.size(), 4);
  collection.insert(event_type({EventEnum::demand_with_stock, 0}));
  EXPECT_EQ(collection.size(), 5);
  collection.insert(event_type({EventEnum::demand_with_stock, 1}));
  EXPECT_EQ(collection.size(), 6);
  collection.insert(event_type({EventEnum::demand_with_stock, 2}));
  EXPECT_EQ(collection.size(), 7);
}

TEST(InventoryManagementStruct, OverallDemandFunction) {
  InventoryManagementParameters params;
  params.demand = {10, 10, 10};
  EXPECT_DOUBLE_EQ(params.overall_demand(), 30);
}

TEST(InventoryManagementStruct, OverallDemandProbabilityFunction) {
  InventoryManagementParameters params;
  params.demand = {10, 10, 10};
  params.T = 1000;
  EXPECT_DOUBLE_EQ(params.overall_demand_probability(), 0.03);
}

TEST(InventoryManagementStruct, DemandProportionFunction) {
  InventoryManagementParameters params;
  params.demand = {30, 20, 10};
  EXPECT_DOUBLE_EQ(params.demand_proportion()[0], 0.5);
  EXPECT_DOUBLE_EQ(params.demand_proportion()[1], 1.0 / 3.0);
  EXPECT_DOUBLE_EQ(params.demand_proportion()[2], 1.0 / 6.0);
}

TEST(InventoryManagement, BaseStateCorrect) {
  InventoryManagementParameters params;
  params.T = 0;
  params.restock_level = {5, 5, 5};
  params.demand = {0.25, 0.25, 0.25};
  params.manufacturer_cost = {10, 10, 10};
  params.transfer_cost = {{0, 100, 100}, {100, 0, 100}, {100, 100, 0}};
  InventoryManagement<PolicyOptimal> problem =
      InventoryManagement<PolicyOptimal>::inventory_management_factory(params);
  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  for (const auto &state : state_loop) {
    EXPECT_NEAR(problem.value_function(stage(0), state), 0.0, epsilon);
  }
}

TEST(InventoryManagement, DemandOccursOnePeriod) {
  InventoryManagementParameters params;
  params.T = 1;
  params.restock_level = {1, 1, 1};
  params.demand = {0.25, 0.25, 0.25};
  params.manufacturer_cost = {10, 10, 10};
  params.transfer_cost = {{0, 100, 100}, {100, 0, 100}, {100, 100, 0}};
  InventoryManagement<PolicyOptimal> problem =
      InventoryManagement<PolicyOptimal>::inventory_management_factory(params);
  // problem.init();
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 1}), 0.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 1, 1}), 2.5, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 0, 1}), 2.5, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 0}), 2.5, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 0, 1}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 0, 0}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 1, 0}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 0, 0}), 7.5, epsilon);
}

TEST(InventoryManagement, DemandOccursTwoPeriods) {
  InventoryManagementParameters params;
  params.T = 2;
  params.restock_level = {2, 2, 2};
  params.demand = {0.5, 0.5, 0.5};
  params.manufacturer_cost = {10, 10, 10};
  params.transfer_cost = {{0, 100, 100}, {100, 0, 100}, {100, 100, 0}};

  InventoryManagement<PolicyOptimal> problem =
      InventoryManagement<PolicyOptimal>::inventory_management_factory(params);
  // problem.init();
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 1}), 0.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 1, 1}), 2.5, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 0, 1}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 0, 0}), 7.5, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 2, 2}), 0.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {1, 2, 2}), 0.625, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 1, 2}), 0.625, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 2, 1}), 0.625, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {0, 2, 2}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 0, 2}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 2, 0}), 5.0, epsilon);
}

TEST(InventoryManagement, ManufacturingCostCanVary) {
  InventoryManagementParameters params;
  params.T = 2;
  params.restock_level = {2, 2, 2};
  params.demand = {0.5, 0.5, 0.5};
  params.manufacturer_cost = {15, 10, 5};
  params.transfer_cost = {{0, 100, 100}, {100, 0, 100}, {100, 100, 0}};

  InventoryManagement<PolicyOptimal> problem =
      InventoryManagement<PolicyOptimal>::inventory_management_factory(params);
  // problem.init();
  problem.calculate();

  EXPECT_NEAR(problem.value_function(stage(2), {0, 2, 2}), 7.5, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 0, 2}), 5.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), {2, 2, 0}), 2.5, epsilon);
}

TEST(InventoryManagement, StockTransferWorks) {
  InventoryManagementParameters params;
  params.T = 1;
  params.restock_level = {1, 1, 1};
  params.demand = {0.25, 0.25, 0.25};
  params.manufacturer_cost = {20, 20, 20};
  params.transfer_cost = {{0, 5, 10}, //
                          {5, 0, 15}, //
                          {10, 15, 0}};

  InventoryManagement<PolicyOptimal> problem =
      InventoryManagement<PolicyOptimal>::inventory_management_factory(params);
  // problem.init();
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 1}), 0.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 1, 1}), 1.25, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 0, 1}), 1.25, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 0}), 2.5, epsilon);
}

TEST(InventoryManagement, StockTransferVsManufacturerWorks) {
  InventoryManagementParameters params;
  params.T = 1;
  params.restock_level = {1, 1, 1};
  params.demand = {0.25, 0.25, 0.25};
  params.manufacturer_cost = {20, 20, 20};
  params.transfer_cost = {{0, 5, 25}, //
                          {5, 0, 25}, //
                          {25, 25, 0}};

  InventoryManagement<PolicyOptimal> problem =
      InventoryManagement<PolicyOptimal>::inventory_management_factory(params);
  // problem.init();
  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 1}), 0.0, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {0, 1, 1}), 1.25, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 0, 1}), 1.25, epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), {1, 1, 0}), 5.0, epsilon);
}

TEST(InventoryManagement,
     StockTransferVsManufacturerWorksManyStagesTwoLocation) {

  InventoryManagementParameters params;
  params.T = 200;
  params.restock_level = {10, 10};
  params.demand = {10, 10};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost};
  params.transfer_cost = {{0, 15}, //
                          {15, 0}};

  InventoryManagement<PolicyOptimal> problem =
      InventoryManagement<PolicyOptimal>::inventory_management_factory(params);
  // problem.init();
  problem.calculate();

  double demand_prob_at_location = 10.0 / 200.0;
  double no_demand_prob = 1.0 - (20.0 / 200.0);
  StateLoop<std::vector<unsigned int>> state_loop({0, 0}, params.restock_level);
  for (auto t = 1u; t <= 200u; ++t)
  // auto t = 4u;
  {

    for (const auto &state : state_loop) {
      if (state[0] > 0 && state[1] > 0) {
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        for (auto l = 0u; l < state.size(); ++l) {
          auto new_state = state;
          new_state[l]--;
          expected_value += demand_prob_at_location *
                            problem.value_function(stage(t - 1), new_state);
        }
        EXPECT_NEAR(problem.value_function(stage(t), state), expected_value,
                    epsilon)
            << state[0] << " " << state[1] << "\n";
      } else if (state[0] == 0 && state[1] > 0) {
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        auto new_state = state;
        new_state[1]--;
        expected_value +=
            demand_prob_at_location *
            std::min(params.transfer_cost[1][0] +
                         problem.value_function(stage(t - 1), new_state),
                     manufacturer_cost +
                         problem.value_function(stage(t - 1), state));
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        EXPECT_NEAR(problem.value_function(stage(t), state), expected_value,
                    epsilon)
            << state[0] << " " << state[1] << "\n";
      }
    }
  }
}

TEST(InventoryManagement,
     StockTransferVsManufacturerWorksManyStagesThreeLocation) {
  InventoryManagementParameters params;
  params.T = 200;
  params.restock_level = {5, 5, 5};
  params.demand = {10, 10, 10};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost,
                              manufacturer_cost};
  params.transfer_cost = {{0, 15, 25}, //
                          {15, 0, 35}, //
                          {25, 35, 0}};

  InventoryManagement<PolicyOptimal> problem =
      InventoryManagement<PolicyOptimal>::inventory_management_factory(params);
  // problem.init();
  problem.calculate();

  double demand_prob_at_location = 10.0 / 200.0;
  double no_demand_prob = 1.0 - (30.0 / 200.0);
  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  // for (auto t = 1u; t <= 200u; ++t)
  auto t = 3u;
  {

    for (const auto &state : state_loop) {
      if (state[0] > 0 && state[1] > 0 && state[2] > 0) {
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        for (auto l = 0u; l < state.size(); ++l) {
          auto new_state = state;
          new_state[l]--;
          expected_value += demand_prob_at_location *
                            problem.value_function(stage(t - 1), new_state);
        }
        EXPECT_DOUBLE_EQ(problem.value_function(stage(t), state),
                         expected_value)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";
      } else if (state[0] == 0 && state[1] > 0 && state[2] > 0) {
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        auto new_state = state;
        new_state[1]--;
        expected_value +=
            demand_prob_at_location *
            std::min(params.transfer_cost[1][0] +
                         problem.value_function(stage(t - 1), new_state),
                     manufacturer_cost +
                         problem.value_function(stage(t - 1), state));
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        new_state = state;
        new_state[2]--;
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        EXPECT_DOUBLE_EQ(problem.value_function(stage(t), state),
                         expected_value)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";
      } else if (state[0] == 0 && state[1] > 0 && state[2] == 0) {
        double expected_value =
            no_demand_prob * problem.value_function(stage(t - 1), state);
        auto new_state = state;
        new_state[1]--;
        expected_value +=
            demand_prob_at_location *
            std::min(params.transfer_cost[1][0] +
                         problem.value_function(stage(t - 1), new_state),
                     manufacturer_cost +
                         problem.value_function(stage(t - 1), state));
        expected_value += demand_prob_at_location *
                          problem.value_function(stage(t - 1), new_state);
        expected_value +=
            demand_prob_at_location *
            std::min(params.transfer_cost[1][2] +
                         problem.value_function(stage(t - 1), new_state),
                     manufacturer_cost +
                         problem.value_function(stage(t - 1), state));
        EXPECT_DOUBLE_EQ(problem.value_function(stage(t), state),
                         expected_value)
            << "t = " << t << " state: " << state[0] << " " << state[1] << " "
            << state[2] << "\n";
      }
    }
  }
}

TEST(InventoryManagement, EventActionsWorkOkay) {
  InventoryManagementParameters params;
  params.T = 200;
  params.restock_level = {5, 5, 5};
  params.demand = {10, 10, 10};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost,
                              manufacturer_cost};
  params.transfer_cost = {{0, 15, 25}, //
                          {15, 0, 35}, //
                          {25, 35, 0}};

  InventoryManagement<PolicyOptimal> problem =
      InventoryManagement<PolicyOptimal>::inventory_management_factory(params);
  // problem.init();
  problem.use_event_action_matrix();
  problem.calculate();

  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  // for (auto t = 1u; t <= 200u; ++t)
  auto t = 3u;
  {
    std::vector<unsigned int> state = {3, 3, 3};
    EXPECT_EQ(
        problem.event_action_matrix(stage(t), state, {EventEnum::no_demand, 0}),
        0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 0}),
              1)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 1}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 2}),
              3)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 3, 3};
    EXPECT_EQ(
        problem.event_action_matrix(stage(t), state, {EventEnum::no_demand, 0}),
        0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 1}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 2}),
              3)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 3, 0};
    EXPECT_EQ(
        problem.event_action_matrix(stage(t), state, {EventEnum::no_demand, 0}),
        0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_stock, 1}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 2}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
  }
}

TEST(InventoryManagement, EventActionsSelectiveWorkOkay) {
  InventoryManagementParameters params;
  params.T = 200;
  params.restock_level = {5, 5, 5};
  params.demand = {10, 10, 10};
  double manufacturer_cost = 20.0;
  params.manufacturer_cost = {manufacturer_cost, manufacturer_cost,
                              manufacturer_cost};
  params.transfer_cost = {{0, 15, 25}, //
                          {15, 0, 35}, //
                          {25, 35, 0}};

  InventoryManagement<PolicyOptimal> problem =
      InventoryManagement<PolicyOptimal>::inventory_management_factory(params);
  // problem.init();
  problem.use_event_action_matrix({{EventEnum::demand_with_no_stock, 0},
                                   {EventEnum::demand_with_no_stock, 1},
                                   {EventEnum::demand_with_no_stock, 2}});
  problem.calculate();

  StateLoop<std::vector<unsigned int>> state_loop({0, 0, 0},
                                                  params.restock_level);
  // for (auto t = 1u; t <= 200u; ++t)
  auto t = 3u;
  {
    std::vector<unsigned int> state = {3, 3, 3};
    EXPECT_DEATH(
        problem.event_action_matrix(stage(t), state, {EventEnum::no_demand, 0}),
        "Assertion.*failed")
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_DEATH(problem.event_action_matrix(stage(t), state,
                                             {EventEnum::demand_with_stock, 0}),
                 "Assertion.*failed")
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_DEATH(problem.event_action_matrix(stage(t), state,
                                             {EventEnum::demand_with_stock, 1}),
                 "Assertion.*failed")
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_DEATH(problem.event_action_matrix(stage(t), state,
                                             {EventEnum::demand_with_stock, 2}),
                 "Assertion.*failed")
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 3, 3};
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 3, 0};
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              2)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 2}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    state = {0, 0, 0};
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 0}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 1}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
    EXPECT_EQ(problem.event_action_matrix(stage(t), state,
                                          {EventEnum::demand_with_no_stock, 2}),
              0)
        << "t = " << t << " state: " << state[0] << " " << state[1] << " "
        << state[2] << "\n";
  }
}
#ifdef FFFFF
#endif