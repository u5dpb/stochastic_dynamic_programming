#include <gtest/gtest.h>
#include <limits>
#include <state_loop.h>
#include <vector>

#include "stochastic_maintenance_and_replacement_problem.h"

double epsilon = 0.01;

TEST(StochasticMaintenanceAndReplacementProblem, SalvageValueCorrect) {
  StochasticMaintenanceAndReplacementProblem problem(1, 7);
  std::vector<double> salvage_value = {50, 25, 17, 8, 0, 0, 0, 0};
  problem.set_salvage_value(salvage_value);
  EXPECT_NEAR(problem.value_function(stage(0), 0u), -salvage_value[0], epsilon);
  EXPECT_NEAR(problem.value_function(stage(0), 1u), -salvage_value[1], epsilon);
  EXPECT_NEAR(problem.value_function(stage(0), 2u), -salvage_value[2], epsilon);
  EXPECT_NEAR(problem.value_function(stage(0), 3u), -salvage_value[3], epsilon);
  EXPECT_NEAR(problem.value_function(stage(0), 4u), -salvage_value[4], epsilon);
  EXPECT_NEAR(problem.value_function(stage(0), 5u), -salvage_value[5], epsilon);
  EXPECT_NEAR(problem.value_function(stage(0), 6u), -salvage_value[6], epsilon);
  EXPECT_NEAR(problem.value_function(stage(0), 7u), -salvage_value[7], epsilon);
}

TEST(StochasticMaintenanceAndReplacementProblem, UncalculatedValuesCorrect) {
  StochasticMaintenanceAndReplacementProblem problem(1u, 7u);
  std::vector<double> salvage_value = {50, 25, 17, 8, 0, 0, 0, 0};
  problem.set_salvage_value(salvage_value);
  EXPECT_NEAR(problem.value_function(stage(1), 1u), std::numeric_limits<double>::max(), epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), 7u), std::numeric_limits<double>::max(), epsilon);
}

TEST(StochasticMaintenanceAndReplacementProblem, TRangeThrows) {
  StochasticMaintenanceAndReplacementProblem problem(3u, 7u);
  EXPECT_NO_THROW(problem.value_function(stage(3), 1u));
  EXPECT_DEATH(problem.value_function(stage(4), 1u),"Assertion.*failed");
}

TEST(StochasticMaintenanceAndReplacementProblem,
     NoCostsChooseMinSalvageCostBottomNoBreakdown) {
  StochasticMaintenanceAndReplacementProblem problem(1u, 7u);
  std::vector<double> salvage_value = {50, 25, 17, 8, 0, 0, 0, 0};

  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(0);
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_repair_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_breakdown_probability({0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0});
  problem.calculate();
  for (auto i = 1u; i <= 7u; ++i) {
    EXPECT_NEAR(problem.value_function(stage(1), i), -salvage_value[1],epsilon);
  }
}

TEST(StochasticMaintenanceAndReplacementProblem,
     NoCostsChooseMinSalvageCostBottomWithBreakdown) {
  StochasticMaintenanceAndReplacementProblem problem(1u, 7u);
  std::vector<double> salvage_value = {50, 25, 17, 8, 0, 0, 0, 0};

  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(0);
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_repair_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_breakdown_probability({0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5});
  problem.calculate();
  for (auto i = 1u; i <= 7u; ++i) {
    EXPECT_NEAR(problem.value_function(stage(1), i),
                -(0.5 * salvage_value[1] + 0.5 * salvage_value[0]), epsilon);
  }
}

TEST(StochasticMaintenanceAndReplacementProblem,
     NoCostsChooseMinSalvageCostOlder) {
  StochasticMaintenanceAndReplacementProblem problem(1u, 7u);
  std::vector<double> salvage_value = {0, 1, 2, 3, 4, 5, 6, 7};
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(0);
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_repair_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_breakdown_probability({0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5});

  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(1), 1),
              -(0.5 * salvage_value[1] + 0.5 * salvage_value[2]), epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), 2),
              -(0.5 * salvage_value[2] + 0.5 * salvage_value[3]), epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), 3),
              -(0.5 * salvage_value[3] + 0.5 * salvage_value[4]), epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), 4),
              -(0.5 * salvage_value[4] + 0.5 * salvage_value[5]), epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), 5),
              -(0.5 * salvage_value[5] + 0.5 * salvage_value[6]), epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), 6),
              -(0.5 * salvage_value[6] + 0.5 * salvage_value[7]), epsilon);
  EXPECT_NEAR(problem.value_function(stage(1), 7),
              -(0.5 * salvage_value[0] + 0.5 * salvage_value[1]), epsilon);
}

TEST(StochasticMaintenanceAndReplacementProblem,
     NoCostsChooseMinSalvageCostOlderTwoStage) {
  StochasticMaintenanceAndReplacementProblem problem(2u, 7u);
  std::vector<double> salvage_value = {0, 1, 2, 3, 4, 5, 6, 7};
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(0);
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_repair_cost({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_breakdown_probability({0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5});

  problem.calculate();
  EXPECT_NEAR(problem.value_function(stage(2), 1),
              -(0.25 * salvage_value[1] + 0.5 * salvage_value[2] +
                0.25 * salvage_value[3]),
              epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), 2),
              -(0.25 * salvage_value[2] + 0.5 * salvage_value[3] +
                0.25 * salvage_value[4]),
              epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), 3),
              -(0.25 * salvage_value[3] + 0.5 * salvage_value[4] +
                0.25 * salvage_value[5]),
              epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), 4),
              -(0.25 * salvage_value[4] + 0.5 * salvage_value[5] +
                0.25 * salvage_value[6]),
              epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), 5),
              -(0.25 * salvage_value[5] + 0.5 * salvage_value[6] +
                0.25 * salvage_value[7]),
              epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), 6),
              -(0.25 * salvage_value[6] + 0.25 * salvage_value[7] +
                0.25 * salvage_value[0] + 0.25 * salvage_value[1]),
              epsilon);
  EXPECT_NEAR(problem.value_function(stage(2), 7),
              -(0.25 * salvage_value[0] + 0.5 * salvage_value[1] +
                0.25 * salvage_value[2]),
              epsilon);
}

TEST(StochasticMaintenanceAndReplacementProblem, PurchaseCostOnlyThreeStage) {
  auto oldest_state = 7u;
  StochasticMaintenanceAndReplacementProblem problem(3, oldest_state);
  std::vector<double> salvage_value = {5, 4, 3, 2, 1, 0, 0, 0};
  int purchase_cost = 5;
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(purchase_cost);
  problem.set_trade_in_value({0, 0, 0, 0, 0, 0, 0, 0});
  problem.set_repair_cost({0, 0, 0, 0, 0, 0, 0, 0});
  std::vector<double> breakdown_probability = {0.5, 0.5, 0.5, 0.5,
                                               0.5, 0.5, 0.5, 0.5};
  problem.set_breakdown_probability(breakdown_probability);

  problem.calculate();
  for (auto t = 1u; t <= 3; ++t) {

    for (auto i = 0u; i < oldest_state; ++i) {
      EXPECT_NEAR(
          problem.value_function(stage(t), i),
          std::min(
              purchase_cost + breakdown_probability[0] * problem.value_function(stage(t - 1), 0) +
                  (1 - breakdown_probability[0] * problem.value_function(stage(t - 1), 0)),
              breakdown_probability[i] * problem.value_function(stage(t - 1), i) +
                  (1 - breakdown_probability[i]) * problem.value_function(stage(t - 1), i + 1)),
          epsilon);
    }
    EXPECT_NEAR(problem.value_function(stage(t), 7),
                purchase_cost + breakdown_probability[0] * problem.value_function(stage(t - 1), 0) +
                    (1 - breakdown_probability[0]) * problem.value_function(stage(t - 1), 1),
                epsilon);
  }
}

TEST(StochasticMaintenanceAndReplacementProblem,
     PurchaseCostTradInValueThreeStage) {
  auto oldest_state = 7u;
  StochasticMaintenanceAndReplacementProblem problem(3, oldest_state);
  std::vector<double> salvage_value = {5, 4, 3, 2, 1, 0, 0, 0};
  int purchase_cost = 5;
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(purchase_cost);
  std::vector<double> trade_in_value = {0, 3, 4, 3, 2, 1, 0, 0};
  problem.set_trade_in_value(trade_in_value);
  problem.set_repair_cost({0, 0, 0, 0, 0, 0, 0, 0});
  std::vector<double> breakdown_probability = {0.5, 0.5, 0.5, 0.5,
                                               0.5, 0.5, 0.5, 0.5};
  problem.set_breakdown_probability(breakdown_probability);

  problem.calculate();
  for (auto t = 1u; t <= 3; ++t)
  // auto t=1u;
  {

    for (auto i = 0u; i < oldest_state; ++i)
    // auto i=2u;
    {
      EXPECT_NEAR(
          problem.value_function(stage(t), i),
          std::min(purchase_cost - trade_in_value[i] +
                       breakdown_probability[0] * problem.value_function(stage(t - 1), 0) +
                       (1 - breakdown_probability[0]) * problem.value_function(stage(t - 1), 1),
                   breakdown_probability[i] * problem.value_function(stage(t - 1), i) +
                       (1 - breakdown_probability[i]) *
                           problem.value_function(stage(t - 1), i + 1)),
          epsilon);
    }
    EXPECT_NEAR(problem.value_function(stage(t), 7),
                purchase_cost - trade_in_value[7] +
                    breakdown_probability[0] * problem.value_function(stage(t - 1), 0) +
                    (1 - breakdown_probability[0]) * problem.value_function(stage(t - 1), 1),
                epsilon);
  }
}

TEST(StochasticMaintenanceAndReplacementProblem,
     PurchaseCostTradeInValueRepairCostThreeStage) {
  auto oldest_state = 7u;
  StochasticMaintenanceAndReplacementProblem problem(3, oldest_state);
  std::vector<double> salvage_value = {5, 4, 3, 2, 1, 0, 0, 0};
  int purchase_cost = 5;
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(purchase_cost);
  std::vector<double> trade_in_value = {0, 3, 4, 3, 2, 1, 0, 0};
  problem.set_trade_in_value(trade_in_value);
  std::vector<double> repair_cost = {1, 2, 3, 4, 5, 6, 7, 8};
  problem.set_repair_cost(repair_cost);
  std::vector<double> breakdown_probability = {0.5, 0.5, 0.5, 0.5,
                                               0.5, 0.5, 0.5, 0.5};
  problem.set_breakdown_probability(breakdown_probability);

  problem.calculate();
  for (auto t = 1u; t <= 3; ++t)
  // auto t=1u;
  {

    for (auto i = 0u; i < oldest_state; ++i)
    // auto i=2u;
    {
      EXPECT_NEAR(
          problem.value_function(stage(t), i),
          std::min(purchase_cost - trade_in_value[i] +
                       breakdown_probability[0] *
                           (repair_cost[0] + problem.value_function(stage(t - 1), 0)) +
                       (1 - breakdown_probability[0]) * problem.value_function(stage(t - 1), 1),
                   breakdown_probability[i] *
                           (repair_cost[i] + problem.value_function(stage(t - 1), i)) +
                       (1 - breakdown_probability[i]) *
                           problem.value_function(stage(t - 1), i + 1)),
          epsilon);
    }
    EXPECT_NEAR(problem.value_function(stage(t), 7),
                purchase_cost - trade_in_value[7] +
                    breakdown_probability[0] *
                        (repair_cost[0] + problem.value_function(stage(t - 1), 0)) +
                    (1 - breakdown_probability[0]) * problem.value_function(stage(t - 1), 1),
                epsilon);
  }
}

TEST(StochasticMaintenanceAndReplacementProblem, FourStageProblem1) {
  auto oldest_state = 7u;
  StochasticMaintenanceAndReplacementProblem problem(4, oldest_state);
  std::vector<double> salvage_value = {50, 25, 17, 8, 0, 0, 0, 0};
  int purchase_cost = 50;
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(purchase_cost);
  std::vector<double> trade_in_value = {0, 32, 21, 11, 5, 0, 0, 0};
  problem.set_trade_in_value(trade_in_value);
  std::vector<double> repair_cost = {10, 13, 20, 40, 70, 100, 100, 100, 100};
  problem.set_repair_cost(repair_cost);
  std::vector<double> breakdown_probability = {0.5, 0.5, 0.5, 0.5,
                                               0.5, 0.5, 0.5, 0.5};
  problem.set_breakdown_probability(breakdown_probability);

  problem.calculate();
  for (auto t = 1u; t <= 4; ++t)
  // auto t=1u;
  {

    for (auto i = 0u; i < oldest_state; ++i)
    // auto i=2u;
    {
      EXPECT_NEAR(
          problem.value_function(stage(t), i),
          std::min(purchase_cost - trade_in_value[i] +
                       breakdown_probability[0] *
                           (repair_cost[0] + problem.value_function(stage(t - 1), 0)) +
                       (1 - breakdown_probability[0]) * problem.value_function(stage(t - 1), 1),
                   breakdown_probability[i] *
                           (repair_cost[i] + problem.value_function(stage(t - 1), i)) +
                       (1 - breakdown_probability[i]) *
                           problem.value_function(stage(t - 1), i + 1)),
          epsilon);
    }
    EXPECT_NEAR(problem.value_function(stage(t), 7),
                purchase_cost - trade_in_value[7] +
                    breakdown_probability[0] *
                        (repair_cost[0] + problem.value_function(stage(t - 1), 0)) +
                    (1 - breakdown_probability[0]) * problem.value_function(stage(t - 1), 1),
                epsilon);
  }
}

TEST(StochasticMaintenanceAndReplacementProblem, FourStageProblem2) {
  auto oldest_state = 7u;
  StochasticMaintenanceAndReplacementProblem problem(4, oldest_state);
  std::vector<double> salvage_value = {50, 25, 17, 8, 0, 0, 0, 0};
  int purchase_cost = 50;
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(purchase_cost);
  std::vector<double> trade_in_value = {0, 32, 21, 11, 5, 0, 0, 0};
  problem.set_trade_in_value(trade_in_value);
  std::vector<double> repair_cost = {10, 13, 20, 40, 70, 100, 100, 100, 100};
  problem.set_repair_cost(repair_cost);
  std::vector<double> breakdown_probability = {0.05, 0.1, 0.15, 0.2,
                                               0.3,  0.5, 0.7,  0.95};
  problem.set_breakdown_probability(breakdown_probability);

  problem.calculate();
  for (auto t = 1u; t <= 4; ++t)
  // auto t=1u;
  {

    for (auto i = 0u; i < oldest_state; ++i)
    // auto i=2u;
    {
      EXPECT_NEAR(
          problem.value_function(stage(t), i),
          std::min(purchase_cost - trade_in_value[i] +
                       breakdown_probability[0] *
                           (repair_cost[0] + problem.value_function(stage(t - 1), 0)) +
                       (1 - breakdown_probability[0]) * problem.value_function(stage(t - 1), 1),
                   breakdown_probability[i] *
                           (repair_cost[i] + problem.value_function(stage(t - 1), i)) +
                       (1 - breakdown_probability[i]) *
                           problem.value_function(stage(t - 1), i + 1)),
          epsilon);
    }
    EXPECT_NEAR(problem.value_function(stage(t), 7),
                purchase_cost - trade_in_value[7] +
                    breakdown_probability[0] *
                        (repair_cost[0] + problem.value_function(stage(t - 1), 0)) +
                    (1 - breakdown_probability[0]) * problem.value_function(stage(t - 1), 1),
                epsilon);
  }
}

TEST(StochasticMaintenanceAndReplacementProblem, FourStageProblem3) {
  auto oldest_state = 7u;
  StochasticMaintenanceAndReplacementProblem problem(4, oldest_state);
  std::vector<double> salvage_value = {50, 25, 17, 8, 0, 0, 0, 0};
  int purchase_cost = 50;
  problem.set_salvage_value(salvage_value);
  problem.set_purchase_cost(purchase_cost);
  std::vector<double> trade_in_value = {0, 32, 21, 11, 5, 0, 0, 0};
  problem.set_trade_in_value(trade_in_value);
  std::vector<double> repair_cost = {50, 53, 60, 70, 80, 100, 100, 100, 100};
  problem.set_repair_cost(repair_cost);
  std::vector<double> breakdown_probability = {0.05, 0.1, 0.15, 0.2,
                                               0.3,  0.5, 0.7,  0.95};
  problem.set_breakdown_probability(breakdown_probability);

  problem.calculate();
  for (auto t = 1u; t <= 4; ++t)
  // auto t=1u;
  {

    for (auto i = 0u; i < oldest_state; ++i)
    // auto i=2u;
    {
      EXPECT_NEAR(
          problem.value_function(stage(t), i),
          std::min(purchase_cost - trade_in_value[i] +
                       breakdown_probability[0] *
                           (repair_cost[0] + problem.value_function(stage(t - 1), 0)) +
                       (1 - breakdown_probability[0]) * problem.value_function(stage(t - 1), 1),
                   breakdown_probability[i] *
                           (repair_cost[i] + problem.value_function(stage(t - 1), i)) +
                       (1 - breakdown_probability[i]) *
                           problem.value_function(stage(t - 1), i + 1)),
          0.01);
    }
    EXPECT_NEAR(problem.value_function(stage(t), 7),
                purchase_cost - trade_in_value[7] +
                    breakdown_probability[0] *
                        (repair_cost[0] + problem.value_function(stage(t - 1), 0)) +
                    (1 - breakdown_probability[0]) * problem.value_function(stage(t - 1), 1),
                0.01);
  }
}
