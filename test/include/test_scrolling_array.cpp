#include <gtest/gtest.h>
#include <iostream>

#include "scrolling_array.h"

TEST(ScrollingArray, Contructor) {
  ScrollingArray<int> scrolling_array(10);
  EXPECT_EQ(scrolling_array.size(), 10);
  EXPECT_EQ(scrolling_array.head(), 10);
}

TEST(ScrollingArray, Initialise) {
  ScrollingArray<int> scrolling_array(10, 42);
  EXPECT_EQ(scrolling_array[0], 42);
  EXPECT_EQ(scrolling_array[1], 42);
  EXPECT_EQ(scrolling_array[5], 42);
  EXPECT_EQ(scrolling_array[8], 42);
  EXPECT_EQ(scrolling_array[9], 42);
}

TEST(ScrollingArray, ReadWrite) {
  ScrollingArray<int> scrolling_array(10);
  scrolling_array[0] = 0;
  scrolling_array[1] = scrolling_array[0] + 1;
  EXPECT_EQ(scrolling_array[1], 1);
}

TEST(ScrollingArray, InitialHead) {
  ScrollingArray<int> scrolling_array(10);
  EXPECT_EQ(scrolling_array.head(), 10);
}

TEST(ScrollingArray, HeadAfterScroll) {
  ScrollingArray<int> scrolling_array(10);
  ++scrolling_array;
  EXPECT_EQ(scrolling_array.head(), 11);
}

TEST(ScrollingArray, TestAccessAfterScroll) {
  ScrollingArray<int> scrolling_array(10);
  scrolling_array[0] = 0;
  scrolling_array[1] = scrolling_array[0] + 1;
  ++scrolling_array;
  EXPECT_EQ(scrolling_array[1], 1);
}

TEST(ScrollingArray, TestNoAccessAfterScroll) {
  ScrollingArray<int> scrolling_array(10);
  scrolling_array[0] = 0;
  scrolling_array[1] = scrolling_array[0] + 1;
  ++scrolling_array;
  EXPECT_DEATH(scrolling_array[0] = 1,"Assertion.*failed");
}

TEST(ScrollingArray, TestNoAccessIncreasedBeforeScroll) {
  ScrollingArray<int> scrolling_array(10);
  EXPECT_NO_THROW(scrolling_array[9] = 1);
  EXPECT_DEATH(scrolling_array[10] = 1,"Assertion.*failed");
  EXPECT_DEATH(scrolling_array[11] = 1,"Assertion.*failed");
}

TEST(ScrollingArray, TestResizeSmaller) {
  ScrollingArray<int> scrolling_array(10);
  EXPECT_EQ(scrolling_array.size(), 10);
  EXPECT_EQ(scrolling_array.head(), 10);
  EXPECT_NO_THROW(scrolling_array[9] = 1);
  EXPECT_DEATH(scrolling_array[10] = 1,"Assertion.*failed");
  EXPECT_DEATH(scrolling_array[11] = 1,"Assertion.*failed");
  scrolling_array.resize(2);
  EXPECT_EQ(scrolling_array.size(), 2);
  EXPECT_EQ(scrolling_array.head(), 2);
  EXPECT_NO_THROW(scrolling_array[1] = 1);
  EXPECT_DEATH(scrolling_array[2] = 1,"Assertion.*failed");
  EXPECT_DEATH(scrolling_array[3] = 1,"Assertion.*failed");
}
TEST(ScrollingArray, TestResizeBigger) {
  ScrollingArray<int> scrolling_array(2);
  scrolling_array.resize(5);
  EXPECT_EQ(scrolling_array.size(), 5);
  EXPECT_EQ(scrolling_array.head(), 5);
  EXPECT_NO_THROW(scrolling_array[4] = 1);
  EXPECT_DEATH(scrolling_array[5] = 1,"Assertion.*failed");
  EXPECT_DEATH(scrolling_array[6] = 1,"Assertion.*failed");
}

TEST(ScrollingArray, TestResizeAndInitialise) {
  ScrollingArray<int> scrolling_array(2, 42);
  scrolling_array.resize(5, 151);
  EXPECT_EQ(scrolling_array.size(), 5);
  EXPECT_EQ(scrolling_array.head(), 5);
  EXPECT_EQ(scrolling_array[0], 42);
  EXPECT_EQ(scrolling_array[1], 42);
  EXPECT_EQ(scrolling_array[2], 151);
  EXPECT_EQ(scrolling_array[3], 151);
  EXPECT_EQ(scrolling_array[4], 151);
  EXPECT_DEATH(scrolling_array[5] = 1,"Assertion.*failed");
}

TEST(ScrollingArray, TestResizeSmallerAfterScroll) {
  ScrollingArray<int> scrolling_array(5);
  ++scrolling_array;
  EXPECT_EQ(scrolling_array.size(), 5);
  EXPECT_EQ(scrolling_array.head(), 6);
  scrolling_array.resize(2);
  EXPECT_DEATH(scrolling_array[0] = 1,"Assertion.*failed");
  EXPECT_NO_THROW(scrolling_array[1] = 1);
  EXPECT_NO_THROW(scrolling_array[2] = 1);
  EXPECT_DEATH(scrolling_array[3] = 1,"Assertion.*failed");
}

TEST(ScrollingArray, TestResizeBiggerAfterScroll) {
  ScrollingArray<int> scrolling_array(2);
  ++scrolling_array;
  EXPECT_EQ(scrolling_array.size(), 2);
  EXPECT_EQ(scrolling_array.head(), 3);
  scrolling_array.resize(5);
  EXPECT_DEATH(scrolling_array[0] = 1,"Assertion.*failed");
  EXPECT_NO_THROW(scrolling_array[4] = 1);
  EXPECT_NO_THROW(scrolling_array[5] = 1);
  EXPECT_DEATH(scrolling_array[6] = 1,"Assertion.*failed");
}

TEST(ScrollingArray, TestIncreasedAccessBeforeScroll) {
  ScrollingArray<int> scrolling_array(10);
  ++scrolling_array;
  scrolling_array[10] = 1;
  EXPECT_NO_THROW(scrolling_array[9] = 1);
  EXPECT_EQ(scrolling_array[10], 1);
  EXPECT_DEATH(scrolling_array[11] = 1,"Assertion.*failed");
}

TEST(ScrollingArray, LongScroll) {
  size_t array_size = 10;
  int max_int = 1000;
  ScrollingArray<int> scrolling_array(array_size);
  for (int i = 0; i <= max_int; ++i) {
    while (uint(i) >= scrolling_array.head())
      ++scrolling_array;
    scrolling_array[uint(i)] = i;
  }
  for (uint i = 0; i < scrolling_array.size(); ++i) {
    EXPECT_EQ(scrolling_array[uint(max_int) - i], max_int - int(i));
  }
}

TEST(ScrollingArray, AutoScroll) {
  size_t array_size = 10;
  int max_int = 1000;
  ScrollingArray<int> scrolling_array(array_size, true);
  for (int i = 0; i <= max_int; ++i) {
    scrolling_array[uint(i)] = i;
  }
  for (uint i = 0; i < scrolling_array.size(); ++i) {
    EXPECT_EQ(scrolling_array[uint(max_int) - i], max_int - int(i));
  }
  EXPECT_DEATH(scrolling_array[uint(max_int) - scrolling_array.size()] = 1,"Assertion.*failed");
}