#include <gtest/gtest.h>
#include <iostream>
#include <set>
#include <vector>

#include "state_loop_vector.h"

TEST(StateLoopVectorUnsignedInt, Size) {
  StateLoop<std::vector<unsigned int>> i(std::vector<unsigned int>(2, 5),
                                         std::vector<unsigned int>(2, 10));
  EXPECT_EQ(i.size(), 6 * 6);
}

TEST(StateLoopVectorUnsignedInt, Reference) {
  StateLoop<std::vector<unsigned int>> i(std::vector<unsigned int>(2, 5),
                                         std::vector<unsigned int>(2, 10));
  EXPECT_EQ(i.reference({5, 5}), size_t(0));
  EXPECT_EQ(i.reference({6, 5}), size_t(1));
}

TEST(StateLoopVectorUnsignedInt, UniqueReferences) {
  StateLoop<std::vector<unsigned int>> i(std::vector<unsigned int>(2, 5),
                                         std::vector<unsigned int>(2, 10));
  std::set<size_t> reference_set;
  unsigned int count = 0;
  for (const auto &x : i) {
    reference_set.insert(i.reference(x));
    count++;
  }
  EXPECT_EQ(reference_set.size(), count);
}

TEST(StateLoopVectorUnsignedInt, CheckIteratorBegin) {
  StateLoop<std::vector<unsigned int>> i(std::vector<unsigned int>(2, 5),
                                         std::vector<unsigned int>(2, 10));
  EXPECT_EQ(*i.begin(), std::vector<unsigned int>({5, 5}));
}

TEST(StateLoopVectorUnsignedInt, CheckIteratorIncrement) {
  StateLoop<std::vector<unsigned int>> i(std::vector<unsigned int>(2, 5),
                                         std::vector<unsigned int>(2, 10));
  auto it = i.begin();
  EXPECT_NO_THROW(it++;);
  EXPECT_EQ(*it, std::vector<unsigned int>({6, 5}));
}

TEST(StateLoopVectorUnsignedInt, CheckIteratorEnd) {
  StateLoop<std::vector<unsigned int>> i(std::vector<unsigned int>(2, 5),
                                         std::vector<unsigned int>(2, 10));
  std::vector<unsigned int> value = {0, 0};
  for (auto v : i)
    value = v;
  EXPECT_EQ(value, std::vector<unsigned int>({10, 10}));
}

TEST(StateLoopVectorUnsignedInt, CheckIteratorTwoTimes) {
  StateLoop<std::vector<unsigned int>> i(std::vector<unsigned int>(2, 5),
                                         std::vector<unsigned int>(2, 10));
  std::vector<unsigned int> value = {0, 0};
  for (auto v : i)
    value = v;
  EXPECT_EQ(value, std::vector<unsigned int>({10, 10}));
  for (auto v : i)
    value = v;
  EXPECT_EQ(value, std::vector<unsigned int>({10, 10}));
}

TEST(StateLoopVectorUnsignedInt, CheckIteratorAllValue) {
  StateLoop<std::vector<unsigned int>> i(std::vector<unsigned int>(2, 5),
                                         std::vector<unsigned int>(2, 10));
  std::set<std::vector<unsigned int>> values;
  for (auto v : i)
    values.insert(v);

  for (unsigned int v_1 = 5; v_1 <= 10; ++v_1) {
    for (unsigned int v_2 = 5; v_2 <= 10; ++v_2) {

      EXPECT_NE(values.find(std::vector<unsigned int>({v_1, v_2})),
                values.end());
    }
  }
}

TEST(StateLoopVectorUnsignedInt, CheckIteratorCopiesOkayLoop) {
  StateLoop<std::vector<unsigned int>> i(std::vector<unsigned int>(2, 5),
                                         std::vector<unsigned int>(2, 10));
  auto it = i.begin();
  EXPECT_NO_THROW(it++;);
  EXPECT_EQ(*it, std::vector<unsigned int>({6, 5}));

  auto j = i;

  std::vector<unsigned int> value = {0, 0};
  for (auto v : j)
    value = v;
  EXPECT_EQ(value, std::vector<unsigned int>({10, 10}));
}

TEST(StateLoopVectorUnsignedInt, CheckIteratorCopiesOkayValue) {
  StateLoop<std::vector<unsigned int>> i(std::vector<unsigned int>(2, 5),
                                         std::vector<unsigned int>(2, 10));

  auto it = i.begin();
  EXPECT_NO_THROW(it++;);
  EXPECT_EQ(*it, std::vector<unsigned int>({6, 5}));
  auto j = i;
  std::set<std::vector<unsigned int>> values;
  for (auto v : j)
    values.insert(v);

  for (unsigned int v_1 = 5; v_1 <= 10; ++v_1) {
    for (unsigned int v_2 = 5; v_2 <= 10; ++v_2) {

      EXPECT_NE(values.find(std::vector<unsigned int>({v_1, v_2})),
                values.end());
    }
  }
}

TEST(StateLoopVectorUnsignedInt, CheckIteratorConstObject) {
  const StateLoop<std::vector<unsigned int>> i(
      std::vector<unsigned int>(2, 5), std::vector<unsigned int>(2, 10));
  std::set<std::vector<unsigned int>> values;
  for (auto v : i)
    values.insert(v);

  for (unsigned int v_1 = 5; v_1 <= 10; ++v_1) {
    for (unsigned int v_2 = 5; v_2 <= 10; ++v_2) {

      EXPECT_NE(values.find(std::vector<unsigned int>({v_1, v_2})),
                values.end());
    }
  }
}

TEST(StateLoopVectorUnsignedInt, TwoIterators) {
  const StateLoop<std::vector<unsigned int>> i(
      std::vector<unsigned int>(2, 0), std::vector<unsigned int>(2, 10));
  auto it_1 = i.begin();
  ++it_1;
  auto it_2 = i.begin();
  while (it_1 != i.end() && it_2 != i.end()) {
    auto v = it_2;
    EXPECT_EQ(*it_1, *(++v));
    ++it_1;
    ++it_2;
  }
}

TEST(StateLoopVectorUnsignedInt, TwoLoopTwoIterators) {
  const StateLoop<std::vector<unsigned int>> i(
      std::vector<unsigned int>(2, 0), std::vector<unsigned int>(2, 10));
  const StateLoop<std::vector<unsigned int>> j(
      std::vector<unsigned int>(2, 0), std::vector<unsigned int>(2, 10));
  auto it_1 = i.begin();
  ++it_1;
  auto it_2 = j.begin();
  while (it_1 != i.end() && it_2 != j.end()) {
    auto v = it_2;

    EXPECT_EQ(*it_1, *(++v));
    ++it_1;
    ++it_2;
  }
}