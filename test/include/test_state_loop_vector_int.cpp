#include <gtest/gtest.h>
#include <iostream>
#include <set>
#include <vector>

#include "state_loop_vector.h"

TEST(StateLoopVectorInt, Size) {
  StateLoop<std::vector<int>> i({-5, -5}, {5, 5});
  unsigned int length = 0;
  for (const auto &x : i) {
    length++;
  }
  EXPECT_EQ(i.size(), length);
}

TEST(StateLoopVectorInt, Reference) {
  StateLoop<std::vector<int>> i(std::vector<int>(2, -5),
                                std::vector<int>(2, 10));
  EXPECT_EQ(i.reference({-5, -5}), size_t(0));
  EXPECT_EQ(i.reference({-4, -5}), size_t(1));
}

TEST(StateLoopVectorInt, UniqueReferences) {
  StateLoop<std::vector<int>> i(std::vector<int>(2, -5),
                                std::vector<int>(2, 10));
  std::set<size_t> reference_set;
  unsigned int count = 0;
  for (const auto &x : i) {
    reference_set.insert(i.reference(x));
    count++;
  }
  EXPECT_EQ(reference_set.size(), count);
}

TEST(StateLoopVectorInt, CheckIteratorBegin) {
  StateLoop<std::vector<int>> i(std::vector<int>(2, -5),
                                std::vector<int>(2, 10));
  EXPECT_EQ(*i.begin(), std::vector<int>({-5, -5}));
}

TEST(StateLoopVectorInt, CheckIteratorIncrement) {
  StateLoop<std::vector<int>> i(std::vector<int>(2, -5),
                                std::vector<int>(2, 10));
  auto it = i.begin();
  EXPECT_NO_THROW(it++;);
  EXPECT_EQ(*it, std::vector<int>({-4, -5}));
  EXPECT_NO_THROW(it++;);
  EXPECT_EQ(*it, std::vector<int>({-3, -5}));
}

TEST(StateLoopVectorInt, CheckIteratorLongLoop) {
  StateLoop<std::vector<int>> i(std::vector<int>(2, -5),
                                std::vector<int>(2, 10));
  unsigned int count = 0;
  EXPECT_NO_THROW(for (auto it = i.begin(); it != i.end(); ++it) count++;)
      << count;
}

TEST(StateVectorLoopInt, CheckIteratorEnd) {
  StateLoop<std::vector<int>> i(std::vector<int>(2, -5),
                                std::vector<int>(2, 10));
  std::vector<int> value = {0, 0};
  for (auto v : i)
    value = v;
  EXPECT_EQ(value, std::vector<int>({10, 10}));
}

TEST(StateLoopVectorInt, CheckIteratorTwoTimes) {
  StateLoop<std::vector<int>> i(std::vector<int>(2, -5),
                                std::vector<int>(2, 10));
  std::vector<int> value = {0, 0};
  for (auto v : i)
    value = v;
  EXPECT_EQ(value, std::vector<int>({10, 10}));
  for (auto v : i)
    value = v;
  EXPECT_EQ(value, std::vector<int>({10, 10}));
}

TEST(StateLoopVectorInt, CheckIteratorAllValue) {
  StateLoop<std::vector<int>> i(std::vector<int>(2, -5),
                                std::vector<int>(2, 10));
  std::set<std::vector<int>> values;
  for (auto v : i)
    values.insert(v);

  for (int v_1 = -5; v_1 <= 10; ++v_1) {
    for (int v_2 = -5; v_2 <= 10; ++v_2) {

      EXPECT_NE(values.find(std::vector<int>({v_1, v_2})), values.end());
    }
  }
}

TEST(StateLoopVectorInt, CheckIteratorCopiesOkayLoop) {
  StateLoop<std::vector<int>> i(std::vector<int>(2, -5),
                                std::vector<int>(2, 10));
  auto it = i.begin();
  EXPECT_NO_THROW(it++;);
  EXPECT_EQ(*it, std::vector<int>({-4, -5}));

  auto j = i;

  std::vector<int> value = {0, 0};
  for (auto v : j)
    value = v;
  EXPECT_EQ(value, std::vector<int>({10, 10}));
}

TEST(StateLoopVectorInt, CheckIteratorCopiesOkayValue) {
  StateLoop<std::vector<int>> i(std::vector<int>(2, -5),
                                std::vector<int>(2, 10));

  auto it = i.begin();
  EXPECT_NO_THROW(it++;);
  EXPECT_EQ(*it, std::vector<int>({-4, -5}));
  auto j = i;
  std::set<std::vector<int>> values;
  for (auto v : j)
    values.insert(v);

  for (int v_1 = -5; v_1 <= 10; ++v_1) {
    for (int v_2 = -5; v_2 <= 10; ++v_2) {

      EXPECT_NE(values.find(std::vector<int>({v_1, v_2})), values.end());
    }
  }
}

TEST(StateLoopVectorInt, CheckIteratorConstObject) {
  const StateLoop<std::vector<int>> i(std::vector<int>(2, -5),
                                      std::vector<int>(2, 10));
  std::set<std::vector<int>> values;
  for (auto v : i)
    values.insert(v);

  for (int v_1 = -5; v_1 <= 10; ++v_1) {
    for (int v_2 = -5; v_2 <= 10; ++v_2) {

      EXPECT_NE(values.find(std::vector<int>({v_1, v_2})), values.end());
    }
  }
}

TEST(StateLoopVectorInt, TwoIterators) {
  const StateLoop<std::vector<int>> i(std::vector<int>(2, -5),
                                      std::vector<int>(2, 10));
  auto it_1 = i.begin();
  ++it_1;
  auto it_2 = i.begin();
  while (it_1 != i.end() && it_2 != i.end()) {
    auto v = it_2;
    EXPECT_EQ(*it_1, *(++v));
    ++it_1;
    ++it_2;
  }
}

TEST(StateLoopVectorInt, TwoLoopTwoIterators) {
  const StateLoop<std::vector<int>> i(std::vector<int>(2, -5),
                                      std::vector<int>(2, 10));
  const StateLoop<std::vector<int>> j(std::vector<int>(2, -5),
                                      std::vector<int>(2, 10));
  auto it_1 = i.begin();
  ++it_1;
  auto it_2 = j.begin();
  while (it_1 != i.end() && it_2 != j.end()) {
    auto v = it_2;

    EXPECT_EQ(*it_1, *(++v));
    ++it_1;
    ++it_2;
  }
}