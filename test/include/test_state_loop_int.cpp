#include <gtest/gtest.h>
#include <iostream>
#include <set>

#include "state_loop.h"

TEST(StateLoopInt, Size) {
  StateLoop<int> i(-5, 10);
  EXPECT_EQ(i.size(), 16);
}

TEST(StateLoopInt, Reference) {
  StateLoop<int> i(-5, 10);
  EXPECT_EQ(i.reference(-5), size_t(0));
  EXPECT_EQ(i.reference(-4), size_t(1));
}

TEST(StateLoopInt, UniqueReferences) {
  StateLoop<int> i(-5, 10);
  std::set<size_t> reference_set;
  unsigned int count = 0;
  for (const auto &x : i) {
    reference_set.insert(i.reference(x));
    count++;
  }
  EXPECT_EQ(reference_set.size(), count);
}

TEST(StateLoopInt, CheckIteratorBegin) {
  StateLoop<int> i(-5, 10);
  EXPECT_EQ(*i.begin(), -5);
}

TEST(StateLoopInt, CheckIteratorIncrement) {
  StateLoop<int> i(-5, 10);
  auto it = i.begin();
  EXPECT_NO_THROW(it++;);
  EXPECT_EQ(*it, -4);
}

TEST(StateLoopInt, CheckIteratorEnd) {
  StateLoop<int> i(-5, 10);
  int value = -10;
  for (auto v : i)
    value = v;
  EXPECT_EQ(value, 10);
}

TEST(StateLoopInt, CheckIteratorAllValue) {
  StateLoop<int> i(-5, 10);
  std::vector<int> values;
  for (auto v : i)
    values.push_back(v);
  uint index = 0;
  for (int v = -5; v <= 10; ++v) {
    EXPECT_EQ(values[index], v);
    index++;
  }
}

TEST(StateLoopInt, CheckIteratorTwoTimes) {
  StateLoop<int> i(-5, 10);
  int value = 0;
  for (auto v : i)
    value = v;
  EXPECT_EQ(value, 10u);
  for (auto v : i)
    value = v;
  EXPECT_EQ(value, 10u);
}

TEST(StateLoopInt, CheckIteratorCopiesOkayLoop) {
  StateLoop<int> i(-5, 10);
  auto it = i.begin();
  EXPECT_NO_THROW(it++;);
  EXPECT_EQ(*it, -4);
  auto j = i;
  int value = 0;
  for (auto v : j)
    value = v;
  EXPECT_EQ(value, 10u);
}

TEST(StateLoopInt, CheckIteratorCopiesOkayValue) {
  StateLoop<int> i(-5, 10);
  auto it = i.begin();
  EXPECT_NO_THROW(it++;);
  EXPECT_EQ(*it, -4);
  auto j = i;
  std::vector<int> values;
  for (auto v : j)
    values.push_back(v);
  uint index = 0;
  for (int v = -5; v <= 10; ++v) {
    EXPECT_EQ(values[index], v);
    index++;
  }
}

TEST(StateLoopInt, CheckIteratorConstObject) {
  const StateLoop<int> i(-5, 10);
  std::vector<int> values;
  for (auto v : i)
    values.push_back(v);
  uint index = 0;
  for (int v = -5; v <= 10; ++v) {
    EXPECT_EQ(values[index], v);
    index++;
  }
}

TEST(StateLoopInt, TwoIterators) {
  const StateLoop<int> i(-5, 10);
  auto it_1 = i.begin();
  ++it_1;
  auto it_2 = i.begin();
  while (it_1 != i.end() && it_2 != i.end()) {
    EXPECT_EQ(*it_1, (*it_2) + 1);
    ++it_1;
    ++it_2;
  }
}

TEST(StateLoopInt, TwoLoopTwoIterators) {
  const StateLoop<int> i(-5, 10);
  const StateLoop<int> j(-6, 10);
  auto it_1 = i.begin();
  auto it_2 = j.begin();
  while (it_1 != i.end() && it_2 != j.end()) {
    EXPECT_EQ(*it_1, (*it_2) + 1);
    ++it_1;
    ++it_2;
  }
}