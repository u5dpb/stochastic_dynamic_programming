#include <action.h>
#include <deterministic_dynamic_program.h>
#include <gtest/gtest.h>
#include <iostream>
#include <set>
#include <state_loop.h>
#include <vector>

TEST(DeterminiticDynamicProgram, TestV0SetAndRead) {
  class TestDP : public DeterministicDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : DeterministicDynamicProgram<unsigned int>(t, state_loop) {}
    double value_function(const stage &t,
                          const unsigned int &state) const override {
      return v(t, state);
    }

  private:
    std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) const override {
      return {};
    }
  };

  StateLoop<unsigned int> state_loop(0, 10);
  ASSERT_NO_THROW(TestDP dp(1, state_loop));
  TestDP dp(1, state_loop);
  for (const auto &state : state_loop) {
    EXPECT_NO_THROW(dp.set_v0(state, state));
  }
  for (const auto &state : state_loop) {
    EXPECT_EQ(dp.value_function(stage(0), state), state);
  }
}

TEST(DeterminiticDynamicProgram, TestCalculate) {
  class TestDP : public DeterministicDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : DeterministicDynamicProgram<unsigned int>(t, state_loop) {}
    double value_function(const stage &t,
                          const unsigned int &state) const override {
      return v(t, state);
    }

  private:
    std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) const override {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      return actions;
    }
  };

  StateLoop<unsigned int> state_loop(0, 10);
  TestDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  ASSERT_NO_THROW(dp.calculate(););
}

TEST(DeterminiticDynamicProgram, TestCalculateValues) {
  class TestDP : public DeterministicDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : DeterministicDynamicProgram<unsigned int>(t, state_loop) {}
    double value_function(const stage &t,
                          const unsigned int &state) const override {
      return v(t, state);
    }

  private:
    std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) const override {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      return actions;
    }
  };

  StateLoop<unsigned int> state_loop(0, 10);
  TestDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  for (auto t = 1u; t <= 5; ++t)
  // auto t=2u;
  {

    for (const auto &state : state_loop) {
      EXPECT_EQ(dp.value_function(stage(t), state),
                double(t * state) + dp.value_function(stage(t - 1), state))
          << " t = " << t << " state = " << state << "\n";
    }
  }
}

TEST(DeterminiticDynamicProgram, TestCalculateMinimum) {
  class TestDP : public DeterministicDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : DeterministicDynamicProgram<unsigned int>(t, state_loop) {}
    double value_function(const stage &t,
                          const unsigned int &state) const override {
      return v(t, state);
    }

  private:
    std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) const override {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      return actions;
    }
  };

  StateLoop<unsigned int> state_loop(0, 10);
  TestDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  for (auto t = 1u; t <= 5; ++t)
  // auto t = 2u;
  {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(t * state) +
                      dp.value_function(stage(t - 1), state - 1));
      } else {
        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(t * state) +
                      dp.value_function(stage(t - 1), state));
      }
    }
  }
}

TEST(DeterminiticDynamicProgram, TestActionMatrix) {
  class TestDP : public DeterministicDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : DeterministicDynamicProgram<unsigned int>(t, state_loop) {}
    double value_function(const stage &t,
                          const unsigned int &state) const override {
      return v(t, state);
    }

  private:
    std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) const override {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      return actions;
    }
  };

  StateLoop<unsigned int> state_loop(0, 10);
  TestDP dp(5, state_loop);
  dp.use_action_matrix(5);

  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  for (auto t = 2u; t <= 5; ++t) {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.action_matrix(stage(t), state),
                  state - 1);
      } else {
        EXPECT_EQ(dp.action_matrix(stage(t), state),
                  state);
      }
    }
  }
}

TEST(DeterminiticDynamicProgram, TestAutoScrollValueFunction) {
  class TestDP : public DeterministicDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : DeterministicDynamicProgram<unsigned int>(t, state_loop) {}
    double value_function(const stage &t,
                          const unsigned int &state) const override {
      return v(t, state);
    }

  private:
    std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) const override {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      return actions;
    }
  };

  StateLoop<unsigned int> state_loop(0, 10);
  TestDP dp(5, state_loop);
  dp.set_value_function_size(1);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  auto t = 5u;

  for (const auto &state : state_loop) {
    if (state % 2 == 1) {

      EXPECT_EQ(dp.value_function(stage(t), state),
                double(t * state) +
                    dp.value_function(stage(t - 1), state - 1));
    } else {
      EXPECT_EQ(dp.value_function(stage(t), state),
                double(t * state) +
                    dp.value_function(stage(t - 1), state));
    }
  }
  EXPECT_DEATH(dp.value_function(stage(t - 2), 5u), "Assertion.*failed");
}

TEST(DeterminiticDynamicProgram, TestAutoScrollActionMatrix) {
  class TestDP : public DeterministicDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : DeterministicDynamicProgram<unsigned int>(t, state_loop) {}
    double value_function(const stage &t,
                          const unsigned int &state) const override {
      return v(t, state);
    }

  private:
    std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) const override {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      return actions;
    }
  };

  StateLoop<unsigned int> state_loop(0, 10);
  TestDP dp(5, state_loop);
  dp.use_action_matrix(1);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  auto t = 5u;
  for (const auto &state : state_loop) {
    if (state % 2 == 1) {

      EXPECT_EQ(dp.action_matrix(stage(t), state),
                state - 1);
    } else {
      EXPECT_EQ(dp.action_matrix(stage(t), state),
                state);
    }
  }
  EXPECT_DEATH(dp.action_matrix(stage(t - 1), 5u), "Assertion.*failed");
}

TEST(DeterminiticDynamicProgram, TestCalculateUntilTime) {
  class TestDP : public DeterministicDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : DeterministicDynamicProgram<unsigned int>(t, state_loop) {}
    double value_function(const stage &t,
                          const unsigned int &state) const override {
      return v(t, state);
    }

  private:
    std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) const override {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      return actions;
    }
  };

  StateLoop<unsigned int> state_loop(0, 10);
  TestDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate(stage(100));
  for (auto t = 96u; t <= 100; ++t)
  // auto t = 2u;
  {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(t * state) +
                      dp.value_function(stage(t - 1), state - 1));
      } else {
        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(t * state) +
                      dp.value_function(stage(t - 1), state));
      }
    }
  }
}

TEST(DeterminiticDynamicProgram, TestCalculateUntilEpsilon) {
  class TestDP : public DeterministicDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : DeterministicDynamicProgram<unsigned int>(t, state_loop) {}
    double value_function(const stage &t,
                          const unsigned int &state) const override {
      return v(t, state);
    }

  private:
    std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) const override {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(state), {{state, 1.0}}, (int)state});
      if (state % 2 == 1)
        actions.push_back({double(state), {{state - 1, 1.0}}, (int)state - 1});
      return actions;
    }
  };

  StateLoop<unsigned int> state_loop(0, 10);
  const epsilon stopping_epsilon(0.01);
  TestDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate(stopping_epsilon);
  for (auto t = dp.T() - 4; t <= dp.T(); ++t)
  // auto t = 2u;
  {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(state) +
                      dp.value_function(stage(t - 1), state - 1));
      } else {
        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(state) +
                      dp.value_function(stage(t - 1), state));
      }
    }
  }
  for (const auto &state : state_loop) {
    EXPECT_LT(
        std::abs(dp.value_function(stage(dp.T()), state) / double(dp.T()) -
                 dp.value_function(stage(dp.T() - 1), state) /
                     double(dp.T() - 1)),
        stopping_epsilon);
  }
}

TEST(DeterminiticDynamicProgram, ValidCheckBasicAssignNotassign) {
  class TestDP : public DeterministicDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : DeterministicDynamicProgram<unsigned int>(t, state_loop) {}
    double value_function(const stage &t,
                          const unsigned int &state) const override {
      return v(t, state);
    }

  private:
    std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) const override {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(state), {{state, 1.0}}, (int)state});
      if (state % 2 == 1)
        actions.push_back({double(state), {{state - 1, 1.0}}, (int)state - 1});
      return actions;
    }
  };
  StateLoop<unsigned int> state_loop(0, 10);
  TestDP dp(1, state_loop);
  for (const auto &state : state_loop) {
    EXPECT_FALSE(dp.valid(stage(0), state));
  }
  for (const auto &state : state_loop) {
    dp.set_v0(state, state);
  }
  for (const auto &state : state_loop) {
    EXPECT_TRUE(dp.valid(stage(0), state));
  }
}

TEST(DeterminiticDynamicProgram, ValidCheckCaluclateAssignsAndNotassigns) {
  class TestDP : public DeterministicDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : DeterministicDynamicProgram<unsigned int>(t, state_loop) {}
    double value_function(const stage &t,
                          const unsigned int &state) const override {
      return v(t, state);
    }

  private:
    std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) const override {
      std::vector<Action<unsigned int>> actions;
      if (state + 1 < state_loop_.max())
        actions.push_back({double(t * state), {{state + 1, 1.0}}, (int)state});
      return actions;
    }
  };
  StateLoop<unsigned int> state_loop(0, 10);
  TestDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  for (auto t = 1u; t <= 5; ++t) {
    for (const auto &state : state_loop) {
      if (state + t < state_loop.max()) {
        EXPECT_TRUE(dp.valid(stage(t), state));
      } else {
        EXPECT_FALSE(dp.valid(stage(t), state));
      }
    }
  }
}

TEST(DeterminiticDynamicProgram, ValidCheckScrollingValueFunctionReturn) {
  class TestDP : public DeterministicDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : DeterministicDynamicProgram<unsigned int>(t, state_loop) {}
    double value_function(const stage &t,
                          const unsigned int &state) const override {
      return v(t, state);
    }

  private:
    std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) const override {
      std::vector<Action<unsigned int>> actions;
      if (state + 1 < state_loop_.max())
        actions.push_back({double(t * state), {{state + 1, 1.0}}, (int)state});
      return actions;
    }
  };

  StateLoop<unsigned int> state_loop(0, 10);
  TestDP dp(5, state_loop);
  dp.set_value_function_size(2);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  for (auto t = 1u; t < 3; ++t) {
    EXPECT_FALSE(dp.valid(stage(t))) << " t = " << t << "\n";
  }
  for (auto t = 3u; t <= 5; ++t) {
    EXPECT_TRUE(dp.valid(stage(t))) << " t = " << t << "\n";
  }
}

TEST(DeterminiticDynamicProgram, TestStateLoop) {
  class TestDP : public DeterministicDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : DeterministicDynamicProgram<unsigned int>(t, state_loop) {}
    double value_function(const stage &t,
                          const unsigned int &state) const override {
      return v(t, state);
    }
    unsigned int no_states() {
      unsigned int result=0;
      for (const auto &v : state_loop_)
      {
        result++;
      }
      return result;
    }
  private:
    std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) const override {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      return actions;
    }
  };

  StateLoop<unsigned int> state_loop(0, 10);
  TestDP dp(5, state_loop);
  EXPECT_EQ(dp.no_states(),state_loop.size());
}
