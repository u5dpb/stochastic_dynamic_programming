#include <gtest/gtest.h>
#include <iostream>
#include <set>

#include "state_loop.h"

TEST(StateLoopUnsignedInt, Size) {
  StateLoop<unsigned int> i(5, 10);
  EXPECT_EQ(i.size(), 6);
}

TEST(StateLoopUnsignedInt, Reference) {
  StateLoop<unsigned int> i(5, 10);
  EXPECT_EQ(i.reference(5), size_t(0));
  EXPECT_EQ(i.reference(6), size_t(1));
}

TEST(StateLoopUnsignedInt, UniqueReferences) {
  StateLoop<unsigned int> i(5, 10);
  std::set<size_t> reference_set;
  unsigned int count = 0;
  for (const auto &x : i) {
    reference_set.insert(i.reference(x));
    count++;
  }
  EXPECT_EQ(reference_set.size(), count);
}

TEST(StateLoopUnsignedInt, CheckIteratorBegin) {
  StateLoop<unsigned int> i(5, 10);
  EXPECT_EQ(*i.begin(), 5);
}

TEST(StateLoopUnsignedInt, CheckIteratorIncrement) {
  StateLoop<unsigned int> i(5, 10);
  auto it = i.begin();
  EXPECT_NO_THROW(it++;);
  EXPECT_EQ(*it, 6);
}

TEST(StateLoopUnsignedInt, CheckIteratorEnd) {
  StateLoop<unsigned int> i(5, 10);
  unsigned int value = 0;
  for (auto v : i)
    value = v;
  EXPECT_EQ(value, 10u);
}

TEST(StateLoopUnsignedInt, CheckIteratorTwoTimes) {
  StateLoop<unsigned int> i(5, 10);
  unsigned int value = 0;
  for (auto v : i)
    value = v;
  EXPECT_EQ(value, 10u);
  for (auto v : i)
    value = v;
  EXPECT_EQ(value, 10u);
}

TEST(StateLoopUnsignedInt, CheckIteratorAllValue) {
  StateLoop<unsigned int> i(5, 10);
  std::vector<unsigned int> values;
  for (auto v : i)
    values.push_back(v);
  uint index = 0;
  for (unsigned int v = 5; v <= 10; ++v) {
    EXPECT_EQ(values[index], v);
    index++;
  }
}

TEST(StateLoopUnsignedInt, CheckIteratorCopiesOkayLoop) {
  StateLoop<unsigned int> i(5, 10);
  auto it = i.begin();
  EXPECT_NO_THROW(it++;);
  EXPECT_EQ(*it, 6);
  auto j = i;
  unsigned int value = 0;
  for (auto v : j)
    value = v;
  EXPECT_EQ(value, 10u);
}

TEST(StateLoopUnsignedInt, CheckIteratorCopiesOkayValue) {
  StateLoop<unsigned int> i(5, 10);
  auto it = i.begin();
  EXPECT_NO_THROW(it++;);
  EXPECT_EQ(*it, 6);
  auto j = i;
  std::vector<unsigned int> values;
  for (auto v : j)
    values.push_back(v);
  uint index = 0;
  for (unsigned int v = 5; v <= 10; ++v) {
    EXPECT_EQ(values[index], v);
    index++;
  }
}

TEST(StateLoopUnsignedInt, CheckIteratorConstObject) {
  const StateLoop<unsigned int> i(5, 10);
  std::vector<unsigned int> values;
  for (auto v : i)
    values.push_back(v);
  uint index = 0;
  for (unsigned int v = 5; v <= 10; ++v) {
    EXPECT_EQ(values[index], v);
    index++;
  }
}

TEST(StateLoopInt, TwoIterators) {
  const StateLoop<unsigned int> i(5, 10);
  auto it_1 = i.begin();
  ++it_1;
  auto it_2 = i.begin();
  while (it_1 != i.end() && it_2 != i.end()) {
    EXPECT_EQ(*it_1, (*it_2) + 1);
    ++it_1;
    ++it_2;
  }
}

TEST(StateLoopInt, TwoLoopTwoIterators) {
  const StateLoop<unsigned int> i(5, 10);
  const StateLoop<unsigned int> j(4, 10);
  auto it_1 = i.begin();
  auto it_2 = j.begin();
  while (it_1 != i.end() && it_2 != j.end()) {
    EXPECT_EQ(*it_1, (*it_2) + 1);
    ++it_1;
    ++it_2;
  }
}