#include <action.h>
#include <event_stochastic_dynamic_program.h>
#include <gtest/gtest.h>
#include <iostream>
#include <set>
#include <state_loop.h>

TEST(EventStochasticDynamicProgram, TestV0SetAndRead) {

  class TestDP : public EventStochasticDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &T, const StateLoop<unsigned int> &state_loop)
        : EventStochasticDynamicProgram<unsigned int>(T, state_loop) {}

  private:
    virtual std::vector<Event<unsigned int>>
    generate_events(const stage &t, const unsigned int &state) const override {
      std::vector<Event<unsigned int>> events;
      std::vector<Action<unsigned int>> actions = {
          {double(t * state), {{state, 1.0}}, (int)state}};
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      events.push_back({0.5, actions, 0});
      actions = {{double(2 * t * state),
                  {{state, 1.0}},
                  (int)(state + state_loop_.max())}};
      if (state % 2 == 1)
        actions.push_back({double(2 * t * state),
                           {{state - 1, 1.0}},
                           (int)(state - 1 + state_loop_.max())});
      events.push_back({0.5, actions, 1});
      return events;
    }
  };
  StateLoop<unsigned int> state_loop(0, 10);
  ASSERT_NO_THROW(TestDP dp(1, state_loop));
  TestDP dp(1, state_loop);
  for (const auto &state : state_loop) {
    EXPECT_NO_THROW(dp.set_v0(state, state));
  }
  for (const auto &state : state_loop) {
    EXPECT_EQ(dp.value_function(stage(0), state), state);
  }
}

TEST(EventStochasticDynamicProgram, TestCalculate) {
  class TestDP : public EventStochasticDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &T, const StateLoop<unsigned int> &state_loop)
        : EventStochasticDynamicProgram<unsigned int>(T, state_loop) {}

  private:
    virtual std::vector<Event<unsigned int>>
    generate_events(const stage &t, const unsigned int &state) const override {
      std::vector<Event<unsigned int>> events;
      std::vector<Action<unsigned int>> actions = {
          {double(t * state), {{state, 1.0}}, (int)state}};
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      events.push_back({0.5, actions, 0});
      actions = {{double(2 * t * state),
                  {{state, 1.0}},
                  (int)(state + state_loop_.max())}};
      if (state % 2 == 1)
        actions.push_back({double(2 * t * state),
                           {{state - 1, 1.0}},
                           (int)(state - 1 + state_loop_.max())});
      events.push_back({0.5, actions, 1});
      return events;
    }
  };

  StateLoop<unsigned int> state_loop(0, 10);
  TestDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  ASSERT_NO_THROW(dp.calculate(););
}

TEST(EventStochasticDynamicProgram, TestAutoScrollingValueFunction) {
  class TestDP : public EventStochasticDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &T, const StateLoop<unsigned int> &state_loop)
        : EventStochasticDynamicProgram<unsigned int>(T, state_loop) {}

  private:
    virtual std::vector<Event<unsigned int>>
    generate_events(const stage &t, const unsigned int &state) const override {
      std::vector<Event<unsigned int>> events;
      std::vector<Action<unsigned int>> actions = {
          {double(t * state), {{state, 1.0}}, (int)state}};
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      events.push_back({0.5, actions, 0});
      actions = {{double(2 * t * state),
                  {{state, 1.0}},
                  (int)(state + state_loop_.max())}};
      if (state % 2 == 1)
        actions.push_back({double(2 * t * state),
                           {{state - 1, 1.0}},
                           (int)(state - 1 + state_loop_.max())});
      events.push_back({0.5, actions, 1});
      return events;
    }
  };

  StateLoop<unsigned int> state_loop(0, 10);
  TestDP dp(5, state_loop);
  dp.set_value_function_size(1);

  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  // for (auto t = 1u; t <= 5; ++t)
  auto t = 5u;
  {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(1.5 * t * state) +
                      dp.value_function(stage(t - 1), state - 1));
      } else {
        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(1.5 * t * state) +
                      dp.value_function(stage(t - 1), state));
      }
    }
  }
}

TEST(EventStochasticDynamicProgram, TestCalculateMinimumFromEvents) {
  class TestDP : public EventStochasticDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &T, const StateLoop<unsigned int> &state_loop)
        : EventStochasticDynamicProgram<unsigned int>(T, state_loop) {}

  private:
    virtual std::vector<Event<unsigned int>>
    generate_events(const stage &t, const unsigned int &state) const override {
      std::vector<Event<unsigned int>> events;
      std::vector<Action<unsigned int>> actions = {
          {double(t * state), {{state, 1.0}}, (int)state}};
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      events.push_back({0.5, actions, 0});
      actions = {{double(2 * t * state),
                  {{state, 1.0}},
                  (int)(state + state_loop_.max())}};
      if (state % 2 == 1)
        actions.push_back({double(2 * t * state),
                           {{state - 1, 1.0}},
                           (int)(state - 1 + state_loop_.max())});
      events.push_back({0.5, actions, 1});
      return events;
    }
  };
  unsigned int oldest_state = 10;
  StateLoop<unsigned int> state_loop(0, oldest_state);
  TestDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  for (auto t = 1u; t <= 5; ++t)
  // auto t = 2u;
  {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(1.5 * t * state) +
                      dp.value_function(stage(t - 1), state - 1));
      } else {
        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(1.5 * t * state) +
                      dp.value_function(stage(t - 1), state));
      }
    }
  }
}

TEST(EventStochasticDynamicProgram, TestEventActionMatrix) {
  class TestDP : public EventStochasticDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &T, const StateLoop<unsigned int> &state_loop)
        : EventStochasticDynamicProgram<unsigned int>(T, state_loop) {}

  private:
    virtual std::vector<Event<unsigned int>>
    generate_events(const stage &t, const unsigned int &state) const override {
      std::vector<Event<unsigned int>> events;
      std::vector<Action<unsigned int>> actions = {
          {double(t * state), {{state, 1.0}}, (int)state}};
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      events.push_back({0.5, actions, 0});
      actions = {{double(2 * t * state),
                  {{state, 1.0}},
                  (int)(state + state_loop_.max())}};
      if (state % 2 == 1)
        actions.push_back({double(2 * t * state),
                           {{state - 1, 1.0}},
                           (int)(state - 1 + state_loop_.max())});
      events.push_back({0.5, actions, 1});
      return events;
    }
  };
  unsigned int oldest_state = 10;
  StateLoop<unsigned int> state_loop(0, oldest_state);
  TestDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.use_event_action_matrix(5);
  dp.calculate();
  for (auto t = 2u; t <= 5; ++t) {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.event_action_matrix(stage(t), state, 0),
                  state - 1);
        EXPECT_EQ(dp.event_action_matrix(stage(t), state, 1),
                  state - 1 + oldest_state);
      } else {
        EXPECT_EQ(dp.event_action_matrix(stage(t), state, 0),
                  state);
        EXPECT_EQ(dp.event_action_matrix(stage(t), state, 1),
                  state + oldest_state);
      }
    }
  }
}

TEST(EventStochasticDynamicProgram, TestEventActionMatrixFixedEvent) {
  class TestDP : public EventStochasticDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &T, const StateLoop<unsigned int> &state_loop)
        : EventStochasticDynamicProgram<unsigned int>(T, state_loop) {}

  private:
    virtual std::vector<Event<unsigned int>>
    generate_events(const stage &t, const unsigned int &state) const override {
      std::vector<Event<unsigned int>> events;
      std::vector<Action<unsigned int>> actions = {
          {double(t * state), {{state, 1.0}}, (int)state}};
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      events.push_back({0.5, actions, 0});
      actions = {{double(2 * t * state),
                  {{state, 1.0}},
                  (int)(state + state_loop_.max())}};
      if (state % 2 == 1)
        actions.push_back({double(2 * t * state),
                           {{state - 1, 1.0}},
                           (int)(state - 1 + state_loop_.max())});
      events.push_back({0.5, actions, 1});
      return events;
    }
  };
  unsigned int oldest_state = 10;
  StateLoop<unsigned int> state_loop(0, oldest_state);
  TestDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.use_event_action_matrix(5, {0});
  dp.calculate();

  for (auto t = 2u; t <= 5; ++t) {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.event_action_matrix(stage(t), state, 0),
                  state - 1);
      } else {
        EXPECT_EQ(dp.event_action_matrix(stage(t), state, 0),
                  state);
        EXPECT_DEATH(dp.event_action_matrix(stage(t), state, 1),
                     "Assertion.*failed");
      }
    }
    EXPECT_DEATH(dp.event_action_matrix(stage(t), 5u, 1), "Assertion.*failed");
    EXPECT_DEATH(dp.event_action_matrix(stage(t), 4u, 1), "Assertion.*failed");
  }
}

TEST(EventStochasticDynamicProgram, TestScrollingEventActionMatrix) {
  class TestDP : public EventStochasticDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &T, const StateLoop<unsigned int> &state_loop)
        : EventStochasticDynamicProgram<unsigned int>(T, state_loop) {}

  private:
    virtual std::vector<Event<unsigned int>>
    generate_events(const stage &t, const unsigned int &state) const override {
      std::vector<Event<unsigned int>> events;
      std::vector<Action<unsigned int>> actions = {
          {double(t * state), {{state, 1.0}}, (int)state}};
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      events.push_back({0.5, actions, 0});
      actions = {{double(2 * t * state),
                  {{state, 1.0}},
                  (int)(state + state_loop_.max())}};
      if (state % 2 == 1)
        actions.push_back({double(2 * t * state),
                           {{state - 1, 1.0}},
                           (int)(state - 1 + state_loop_.max())});
      events.push_back({0.5, actions, 1});
      return events;
    }
  };
  unsigned int oldest_state = 10;
  StateLoop<unsigned int> state_loop(0, oldest_state);
  TestDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.use_event_action_matrix(1);
  dp.calculate();

  // for (auto t = 2u; t <= 5; ++t)
  auto t = 5u;
  {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.event_action_matrix(stage(t), state, 0),
                  state - 1);
        EXPECT_EQ(dp.event_action_matrix(stage(t), state, 1),
                  state - 1 + oldest_state);
      } else {
        EXPECT_EQ(dp.event_action_matrix(stage(t), state, 0),
                  state);
        EXPECT_EQ(dp.event_action_matrix(stage(t), state, 1),
                  state + oldest_state);
      }
    }
      EXPECT_DEATH(dp.event_action_matrix(stage(t - 1), 5u, 0),
                   "Assertion.*failed");
      EXPECT_DEATH(dp.event_action_matrix(stage(t - 1), 5u, 1),
                   "Assertion.*failed");
  }
}

TEST(EventStochasticDynamicProgram, TestEventsCalculateUntilTime) {
  class TestDP : public EventStochasticDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &T, const StateLoop<unsigned int> &state_loop)
        : EventStochasticDynamicProgram<unsigned int>(T, state_loop) {}

  private:
    virtual std::vector<Event<unsigned int>>
    generate_events(const stage &t, const unsigned int &state) const override {
      std::vector<Event<unsigned int>> events;
      std::vector<Action<unsigned int>> actions = {
          {double(state), {{state, 1.0}}, (int)state}};
      if (state % 2 == 1)
        actions.push_back(
            {double(state), {{state - 1, 1.0}}, (int)state - 1});
      events.push_back({0.5, actions, 0});
      actions = {{double(2 * state),
                  {{state, 1.0}},
                  (int)(state + state_loop_.max())}};
      if (state % 2 == 1)
        actions.push_back({double(2 * state),
                           {{state - 1, 1.0}},
                           (int)(state - 1 + state_loop_.max())});
      events.push_back({0.5, actions, 1});
      return events;
    }
  };
  unsigned int oldest_state = 10;
  StateLoop<unsigned int> state_loop(0, oldest_state);
  TestDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.use_event_action_matrix(1);
  dp.calculate(stage(100));

  for (auto t = dp.T() - 4u; t <= dp.T(); ++t)
  // auto t = 2u;
  {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(1.5 * state) +
                      dp.value_function(stage(t - 1), state - 1));
      } else {
        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(1.5 * state) +
                      dp.value_function(stage(t - 1), state));
      }
    }
  }
}

TEST(EventStochasticDynamicProgram, TestEventsCalculateUntilEpsilon) {
  class TestDP : public EventStochasticDynamicProgram<unsigned int> {
  public:
    TestDP(const unsigned int &T, const StateLoop<unsigned int> &state_loop)
        : EventStochasticDynamicProgram<unsigned int>(T, state_loop) {}

  private:
    virtual std::vector<Event<unsigned int>>
    generate_events(const stage &t, const unsigned int &state) const override {
      std::vector<Event<unsigned int>> events;
      std::vector<Action<unsigned int>> actions = {
          {double(state), {{state, 1.0}}, (int)state}};
      if (state % 2 == 1)
        actions.push_back(
            {double(state), {{state - 1, 1.0}}, (int)state - 1});
      events.push_back({0.5, actions, 0});
      actions = {{double(2 * state),
                  {{state, 1.0}},
                  (int)(state + state_loop_.max())}};
      if (state % 2 == 1)
        actions.push_back({double(2 * state),
                           {{state - 1, 1.0}},
                           (int)(state - 1 + state_loop_.max())});
      events.push_back({0.5, actions, 1});
      return events;
    }
  };
  unsigned int oldest_state = 10;
  const epsilon stopping_epsilon(0.01);
    StateLoop<unsigned int> state_loop(0, oldest_state);
  TestDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.use_event_action_matrix(1);
  dp.calculate(stopping_epsilon);

  for (auto t = dp.T() - 4u; t <= dp.T(); ++t)
  // auto t = 2u;
  {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(1.5 * state) +
                      dp.value_function(stage(t - 1), state - 1));
      } else {
        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(1.5 * state) +
                      dp.value_function(stage(t - 1), state));
      }
    }
  }
  for (const auto &state : state_loop) {
    EXPECT_LT(
        std::abs(dp.value_function(dp.T(), state) / double(dp.T()) -
                 dp.value_function(stage(dp.T() - 1), state) / double(dp.T() - 1)),
        stopping_epsilon);
  }
}
