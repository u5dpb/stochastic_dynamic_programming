#include <gtest/gtest.h>
#include <iostream>
#include <set>

#include <action.h>
#include <state_loop.h>
#include <stochastic_dynamic_program.h>

TEST(StochasticDynamicProgram, TestV0SetAndRead) {
  class TestSDP : public StochasticDynamicProgram<unsigned int> {
  public:
    TestSDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : StochasticDynamicProgram(t, state_loop) {}

  protected:
    virtual std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) {
      return {};
    }
  };
  StateLoop<unsigned int> state_loop(0, 10);
  ASSERT_NO_THROW(TestSDP dp(1, state_loop));
  TestSDP dp(1, state_loop);
  for (const auto &state : state_loop) {
    EXPECT_NO_THROW(dp.set_v0(state, state));
  }
  for (const auto &state : state_loop) {
    EXPECT_EQ(dp.value_function(stage(0), state), state);
  }
}

TEST(StochasticDynamicProgram, TestCalculate) {
  class TestSDP : public StochasticDynamicProgram<unsigned int> {
  public:
    TestSDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : StochasticDynamicProgram(t, state_loop) {}

  protected:
    virtual std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      return actions;
    }
  };
  StateLoop<unsigned int> state_loop(0, 10);
  TestSDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  ASSERT_NO_THROW(dp.calculate(););
}

TEST(StochasticDynamicProgram, TestCalculateValues) {
  class TestSDP : public StochasticDynamicProgram<unsigned int> {
  public:
    TestSDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : StochasticDynamicProgram(t, state_loop) {}

  protected:
    virtual std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      return actions;
    }
  };
  StateLoop<unsigned int> state_loop(0, 10);
  TestSDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  for (auto t = 1u; t <= 5; ++t)
  // auto t=2u;
  {

    for (const auto &state : state_loop) {
      EXPECT_EQ(dp.value_function(stage(t), state),
                double(t * state) +
                    dp.value_function(stage(t - 1), state));
    }
  }
}

TEST(StochasticDynamicProgram, TestCalculateMinimum) {
  class TestSDP : public StochasticDynamicProgram<unsigned int> {
  public:
    TestSDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : StochasticDynamicProgram(t, state_loop) {}

  protected:
    virtual std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      return actions;
    }
  };
  StateLoop<unsigned int> state_loop(0, 10);
  TestSDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  for (auto t = 1u; t <= 5; ++t)
  // auto t = 2u;
  {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(t * state) +
                      dp.value_function(stage(t - 1), state - 1));
      } else {
        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(t * state) +
                      dp.value_function(stage(t - 1), state));
      }
    }
  }
}

TEST(StochasticDynamicProgram, TestActionMatrix) {
  class TestSDP : public StochasticDynamicProgram<unsigned int> {
  public:
    TestSDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : StochasticDynamicProgram(t, state_loop) {}

  protected:
    virtual std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      return actions;
    }
  };
  StateLoop<unsigned int> state_loop(0, 10);
  TestSDP dp(5, state_loop);
  dp.use_action_matrix(5);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  for (auto t = 2u; t <= 5; ++t) {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.action_matrix(stage(t), state),
                  state - 1);
      } else {
        EXPECT_EQ(dp.action_matrix(stage(t), state), state);
      }
    }
  }
}

TEST(StochasticDynamicProgram, TestCalculateProbabilities) {
  class TestSDP : public StochasticDynamicProgram<unsigned int> {
  public:
    TestSDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : StochasticDynamicProgram(t, state_loop) {}

  protected:
    virtual std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      if (state % 2 == 1)
        actions.push_back({double(t * state),
                           {{state - 1, 0.5}, {state, 0.5}},
                           (int)state - 1});
      return actions;
    }
  };
  StateLoop<unsigned int> state_loop(0, 10);
  TestSDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  for (auto t = 1u; t <= 5; ++t)
  // auto t = 2u;
  {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(t * state) +
                      (0.5 * dp.value_function(stage(t - 1), state - 1)) +
                      (0.5 * dp.value_function(stage(t - 1), state)));
      } else {
        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(t * state) +
                      dp.value_function(stage(t - 1), state));
      }
    }
  }
}

TEST(StochasticDynamicProgram, TestAutoScrollingValueFunction) {
  class TestSDP : public StochasticDynamicProgram<unsigned int> {
  public:
    TestSDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : StochasticDynamicProgram(t, state_loop) {}

  protected:
    virtual std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      if (state % 2 == 1)
        actions.push_back({double(t * state),
                           {{state - 1, 0.5}, {state, 0.5}},
                           (int)state - 1});
      return actions;
    }
  };
  StateLoop<unsigned int> state_loop(0, 10);
  TestSDP dp(5, state_loop);
  dp.set_value_function_size(1);

  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  // for (auto t = 1u; t <= 5; ++t)
  auto t = 5u;
  {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(t * state) +
                      (0.5 * dp.value_function(stage(t - 1), state - 1)) +
                      (0.5 * dp.value_function(stage(t - 1), state)));
      } else {
        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(t * state) +
                      dp.value_function(stage(t - 1), state));
      }
    }
  }
}

TEST(StochasticDynamicProgram, TestAutoScrollActionMatrix) {
  class TestSDP : public StochasticDynamicProgram<unsigned int> {
  public:
    TestSDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : StochasticDynamicProgram(t, state_loop) {}

  protected:
    virtual std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(t * state), {{state, 1.0}}, (int)state});
      if (state % 2 == 1)
        actions.push_back(
            {double(t * state), {{state - 1, 1.0}}, (int)state - 1});
      return actions;
    }
  };
  StateLoop<unsigned int> state_loop(0, 10);
  TestSDP dp(5, state_loop);
  dp.use_action_matrix(1);

  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate();
  auto t = 5u;
  // for (auto t = 2u; t <= 5; ++t)
  {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.action_matrix(stage(t), state),
                  state - 1);
      } else {
        EXPECT_EQ(dp.action_matrix(stage(t), state), state);
      }
    }
    EXPECT_DEATH(dp.action_matrix(stage(t - 1), 5u), "Assertion.*failed");
  }
}

TEST(StochasticDynamicProgram, TestCalculateActionsUntilTime) {
  class TestSDP : public StochasticDynamicProgram<unsigned int> {
  public:
    TestSDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : StochasticDynamicProgram(t, state_loop) {}

  protected:
    virtual std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(state), {{state, 1.0}}, (int)state});
      if (state % 2 == 1)
        actions.push_back(
            {double(state), {{state - 1, 0.5}, {state, 0.5}}, (int)state - 1});
      return actions;
    }
  };
  StateLoop<unsigned int> state_loop(0, 10);
  TestSDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate(stage(100));
  for (auto t = dp.T() - 4u; t <= dp.T(); ++t)
  // auto t = 2u;
  {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(state) +
                      (0.5 * dp.value_function(stage(t - 1), state - 1)) +
                      (0.5 * dp.value_function(stage(t - 1), state)));
      } else {
        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(state) + dp.value_function(stage(t - 1), state));
      }
    }
  }
}

TEST(StochasticDynamicProgram, TestCalculateActionsUntilstopping_epsilon) {
  class TestSDP : public StochasticDynamicProgram<unsigned int> {
  public:
    TestSDP(const unsigned int &t, const StateLoop<unsigned int> &state_loop)
        : StochasticDynamicProgram(t, state_loop) {}

  protected:
    virtual std::vector<Action<unsigned int>>
    generate_actions(const stage &t, const unsigned int &state) {
      std::vector<Action<unsigned int>> actions;
      actions.push_back({double(state), {{state, 1.0}}, (int)state});
      if (state % 2 == 1)
        actions.push_back(
            {double(state), {{state - 1, 0.5}, {state, 0.5}}, (int)state - 1});
      return actions;
    }
  };
  StateLoop<unsigned int> state_loop(0, 10);
  const epsilon stopping_epsilon(0.01);
  TestSDP dp(5, state_loop);
  for (const auto &state : state_loop) {
    dp.set_v0(state, 0.0);
  }
  dp.calculate(stopping_epsilon);
  for (auto t = dp.T() - 4u; t <= dp.T(); ++t)
  // auto t = 2u;
  {

    for (const auto &state : state_loop) {
      if (state % 2 == 1) {

        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(state) +
                      (0.5 * dp.value_function(stage(t - 1), state - 1)) +
                      (0.5 * dp.value_function(stage(t - 1), state)));
      } else {
        EXPECT_EQ(dp.value_function(stage(t), state),
                  double(state) + dp.value_function(stage(t - 1), state));
      }
    }
  }
  for (const auto &state : state_loop) {
    EXPECT_LT(
        std::abs(dp.value_function(dp.T(), state) / double(dp.T()) -
                 dp.value_function(stage(dp.T() - 1), state) / double(dp.T() - 1)),
        stopping_epsilon);
  }
}
