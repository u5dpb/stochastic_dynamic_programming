#include <action.h>
#include <deterministic_dynamic_program.h>
#include <iostream>
#include <state_loop.h>
#include <vector>

unsigned int oldest_state = 7;

std::vector<double> trade_in_value = {0, 32, 21, 11, 5, 0, 0, 0};
std::vector<double> operating_cost = {10, 13, 20, 40, 70, 100, 100, 100};
double purchase_cost = 50;

class ExampleDP : public DeterministicDynamicProgram<unsigned int> {
public:
  ExampleDP(const unsigned int &T, const StateLoop<unsigned int> &state_loop)
      : DeterministicDynamicProgram(T, state_loop) {}
  double value_function(const stage &t,
                        const unsigned int &state) const override {
    return v(t, state);
  }

protected:
  std::vector<Action<unsigned int>>
  generate_actions(const stage &t, const unsigned int &state) const override {
    std::vector<Action<unsigned int>> action_set;
    action_set.push_back(
        {purchase_cost - trade_in_value[state] + operating_cost[0],
         {{1u, 1.0}},
         1u});
    if (state < oldest_state) {
      action_set.push_back({operating_cost[state], {{state + 1u, 1.0}}, 0u});
    }
    return action_set;
  }
};

int main(int argc, char **argv) {
  StateLoop<unsigned int> state_loop(1, oldest_state);
  ExampleDP ddp(10, state_loop);

  ddp.use_action_matrix();

  std::vector<double> salvage_value = {0, 25, 17, 8, 0, 0, 0, 0};
  for (const auto &state : state_loop) {
    ddp.set_v0(state, salvage_value[state]);
  }

  ddp.calculate();

  std::cout << "Cost of a 4 year old machine is "
            << ddp.value_function(stage(10), 4) << "\n";

  std::cout << "The optimal policy is\n";
  auto age = 4u;
  for (auto t = 10u; t > 0; t--) {
    std::cout << "t = " << t << ", " << age << " year old machine: ";
    if (ddp.action_matrix(stage(t), age) == 1) {
      std::cout << "replace.\n";
      age = 1;
    } else {
      std::cout << "keep.\n";
      age++;
    }
  }

  return 0;
}