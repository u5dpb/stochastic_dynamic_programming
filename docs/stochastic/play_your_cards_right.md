# Play Your Cards Right

# Play Your Cards Right API

The following `struct` is used in the class to prevent confusion between the time space of the game/problem and that of the SDP model. The first round of the game/problem is `round{1}`. The last round is `round{54}`.

```c++
struct round
{
  unsigned int number;
};
```

```c++
PlayYourCardsRight(const std::vector<double> &prize);
```
Class constructor
* `const std::vector<double> &prize` is a vector of size 54 that gives the prize for each round of the game.

---

```c++
void use_action_matrix();
```
The Dynamic Program will store the action taken for each stage/state combination. The action is defined as an `int` in the `generate_actions` function.

---

```c++
double v(const stage &t, const unsigned int &state) const;
```

Getter: returns the value function for a stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const unsigned int &state` is a state of the Dynamic Program.

---

```c++
double v(const round &r, const unsigned int &state) const;
```

Getter: returns the value function for a stage/state combination.
* `const round &r` is the round of the game where `round{1}` is the first round and `round{54}` is the last.
* `const unsigned int &state` is a state of the Dynamic Program.

---

```c++
void calculate();
```

Calculates the value function for all stage/state combinations.

---

```c++
int action_matrix(const stage &t, const unsigned int &state) const;
```

Getter: returns the action taken for a given stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const unsigned int &state` is a state of the Dynamic Program.

---

```c++
int action_matrix(const round &r, const unsigned int &state) const;
```

Getter: returns the action taken for a given stage/state combination.
* `const round &r` is the round of the game where `round{1}` is the first round and `round{54}` is the last.
* `const unsigned int &state` is a state of the Dynamic Program.

---

```c++
unsigned int no_rounds() const
```
Getter: returns the number of rounds in the game.

---

```c++
unsigned int maxmimum_card_number() const
```
Getter: returns the highest card number of the game


# The Problem

The problem is a variation on the popular 80's game show. 

1. A starting card is drawn from a standard deck. 
2. The player then chooses to guess or bank. 
  * A guess is whether the next card will be higher or lower than the current card.
    * A correct answer increases the prize. 
    * If the answer is incorrect then the player loses the game. 
  * If the player chooses bank then the current prize is awarded. 

The numerical order of the cards is used with an Ace being 1 and Jack, Queen, King being 11, 12, 13 respectively.

## Simplification

The number of possible states is unfeasibly large. 

To simplify the game:

1. The current card is shown.

2. The player decides what action to take.

3. The current card is returned to the deck.

4. The deck is shuffled.

5. The next card is drawn.


# Parameters

The stage $`t`$ of the problem is how many cards remain in the deck.

The state of the problem $`s`$, when a decision is made is the set of cards already drawn from the deck. In the simplified game this is the numerical value of the last card drawn.

The decision is whether to:
* bank, 
* guess higher or 
* guess lower.

The reward at each stage of the game is given as $`r=\{r_{54},\,r_{53},\,...,\,r_{0}\}`$ where $`r_{i+1}<r_{i}`$ for all $`i`$.

Note that the current reward/prize does not need to be part of the state of the problem as it is implied by the stage.

# Model


```math
\begin{array}{ll}
v^{t}(s)= & \max\begin{cases}
r_{t} & :bank \\
\sum_{n>s}p(n)v^{t-1}(n) & :higher \\
\sum_{n<s}p(n)v^{t-1}(n) & :lower
\end{cases} \\
\\
v^{0}(s)= & r_{0}
\end{array}
```

As this is a standard deck of cards $`p(n)=\frac{1}{13}`$ giving:

```math
\begin{array}{ll}
v^{t}(s)= & \max\begin{cases}
r_{t} & :bank\\
\frac{1}{13}\sum_{n>s}v^{t-1}(n) & :higher\\
\frac{1}{13}\sum_{n<s}v^{t-1}(n) & :lower
\end{cases}\\
\\
v^{0}(s)= & r_{0}
\end{array}
```

# Example Problem

The following example offers a prize of an extra 100 units per round played. It prints the action matrix for this prize structure.

File: `example_play_your_cards_right.cpp`

```c++
#include <play_your_cards_right.h>
#include <vector>
#include <iostream>
#include <iomanip>

int main() {
  std::vector<double> prize;
  double total=100;
  for (auto i = 1; i <= 54; i++)
  {
    prize.push_back(total);
    total+=100;
  }
  
  PlayYourCardsRight problem(prize);

  problem.use_action_matrix();

  problem.calculate();

  std::cout << "Optimal Policy\n";
  std::cout << "card/turn\n";
  for (auto card = 13u; card > 0; --card) {
    std::cout << std::setw(3) << card;
    for (auto r = 1u; r<=54; ++r) {
      std::cout << std::setw(2) << problem.action_matrix(round{r}, card);
    }
    std::cout << "\n";
  }

  exit(0);
}
```

The action matrix is as follows:

```
Optimal Policy
card/turn
 13 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 ...
 12 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ...
 11 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ...
 10 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ...
  9 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ...
  8 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ...
  7 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ...
  6 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ...
  5 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ...
  4 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ...
  3 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ...
  2 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ...
  1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 ...
```

The policy shows that even when the last card was particularly high (or low) the marginal benfit of the gamble rapidly deminishes as the game progresses.

# Implementation Details

The key part of the implementation is the `generate_actions` function. It considers all possible allocations and generates actions for them.

```c++
std::vector<Action<unsigned int>>
PlayYourCardsRight::generate_actions(const stage &t,
                                     const unsigned int &state) {
  // only need to consider one of higher or lower
  if (state > maximum_card_number / 2) { // lower
    return {make_bank(t), make_lower(state)};
  } else { // higher
    return {make_bank(t), make_higher(state)};
  }
}
```

The `make_bank`, `make_lower` and `make_higher` functions are factory functions for creating the different actions that could be taken.

# A Myopic Policy

The mypoic policy presented here evaluates the decision of the current round with the assumption that the decision in the next round will be *bank* (i.e. $`v^{t-1}(s)=r_{t-1},\,\forall s`$). The problem is symetrical so only consider states where $`s\geq7`$. The model becomes:

```math
\begin{array}{ll}
v^{t}(s)= & \max\begin{cases}
r_{t} & :bank\\
\frac{1}{13}\sum_{n<s}r_{t-1} & :lower
\end{cases}\\
\end{array}
```

Which simplifies to:

```math
\begin{array}{ll}
v^{t}(s)= & \max\begin{cases}
r_{t} & :bank\\
\frac{s-1}{13}.r_{t-1} & :lower
\end{cases}\\
\end{array}
```

The policy is to bank when $`r_{t} > \frac{s-1}{13}.r_{t-1}`$.

## Implementation of the Myopic Policy

The following code overrides the `generate_actions` function to only return the action determined by the mypoic policy.

File: `example_play_your_cards_right_myopic.cpp`

```c++
#include <iomanip>
#include <iostream>
#include <play_your_cards_right.h>
#include <vector>

class PlayYourCardsRightMyopic : public PlayYourCardsRight {
public:
  using PlayYourCardsRight::PlayYourCardsRight;

protected:
  std::vector<Action<unsigned int>>
  generate_actions(const stage &t, const unsigned int &state) override {
    if (prize(t) <
        double(state - 1) * prize(t - 1) / (double)maxmimum_card_number())
      return {make_lower(state)};
    else if (prize(t) < double(maxmimum_card_number() - state) * prize(t - 1) /
                            (double)maxmimum_card_number())
      return {make_higher(state)};
    else
      return {make_bank(t)};
  }
};

void print_action_matrix(const PlayYourCardsRight &problem);

int main() {
  std::vector<double> prize;
  double total = 100;
  for (auto i = 1; i <= 54; i++) {
    prize.push_back(total);
    total += 100;
  }

  PlayYourCardsRight problem(prize);
  problem.use_action_matrix();
  problem.calculate();

  std::cout << "Optimal Policy\n";
  std::cout << "Expected Prize = " << problem.expected_prize() << "\n";

  PlayYourCardsRightMyopic problem_myopic(prize);
  problem_myopic.use_action_matrix();
  problem_myopic.calculate();

  std::cout << "Myopic Policy\n";
  std::cout << "Expected Prize = " << problem_myopic.expected_prize() << "\n";

  std::cout << "Optimal Policy\n";
  std::cout << "card/turn\n";
  print_action_matrix(problem);
  std::cout << "Myopic Policy\n";
  std::cout << "card/turn\n";
  print_action_matrix(problem_myopic);

  exit(0);
}

void print_action_matrix(const PlayYourCardsRight &problem) {
  for (auto card = 13u; card > 0; --card) {
    std::cout << std::setw(3) << card;
    for (auto r = 1u; r <= 10; ++r) {
      std::cout << std::setw(2) << problem.action_matrix(round{r}, card);
    }
    std::cout << "\n";
  }
}
}
```

The output below show that the myopic policy is sub-optimal for the prize structure given. The difference in this case is that when the card is 7 in round 1 the myopic policy will *bank* rather than gamble *higher* or *lower*.

```
Optimal Policy
Expected Prize = 161.203
Myopic Policy
Expected Prize = 160.504
Optimal Policy
card/turn
 13 1 1 1 1 1 1 1 1 1 1
 12 1 1 1 1 1 0 0 0 0 0
 11 1 1 1 0 0 0 0 0 0 0
 10 1 1 0 0 0 0 0 0 0 0
  9 1 0 0 0 0 0 0 0 0 0
  8 1 0 0 0 0 0 0 0 0 0
  7 1 0 0 0 0 0 0 0 0 0
  6 1 0 0 0 0 0 0 0 0 0
  5 1 0 0 0 0 0 0 0 0 0
  4 1 1 0 0 0 0 0 0 0 0
  3 1 1 1 0 0 0 0 0 0 0
  2 1 1 1 1 1 0 0 0 0 0
  1 1 1 1 1 1 1 1 1 1 1
Myopic Policy
card/turn
 13 1 1 1 1 1 1 1 1 1 1
 12 1 1 1 1 1 0 0 0 0 0
 11 1 1 1 0 0 0 0 0 0 0
 10 1 1 0 0 0 0 0 0 0 0
  9 1 0 0 0 0 0 0 0 0 0
  8 1 0 0 0 0 0 0 0 0 0
  7 0 0 0 0 0 0 0 0 0 0
  6 1 0 0 0 0 0 0 0 0 0
  5 1 0 0 0 0 0 0 0 0 0
  4 1 1 0 0 0 0 0 0 0 0
  3 1 1 1 0 0 0 0 0 0 0
  2 1 1 1 1 1 0 0 0 0 0
  1 1 1 1 1 1 1 1 1 1 1
```


