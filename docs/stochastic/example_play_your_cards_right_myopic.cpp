#include <iomanip>
#include <iostream>
#include <play_your_cards_right.h>
#include <vector>

class PlayYourCardsRightMyopic : public PlayYourCardsRight {
public:
  using PlayYourCardsRight::PlayYourCardsRight;

protected:
  std::vector<Action<unsigned int>>
  generate_actions(const stage &t, const unsigned int &state) override {
    if (prize(t) <
        double(state - 1) * prize(t - 1) / (double)maxmimum_card_number())
      return {make_lower(state)};
    else if (prize(t) < double(maxmimum_card_number() - state) * prize(t - 1) /
                            (double)maxmimum_card_number())
      return {make_higher(state)};
    else
      return {make_bank(t)};
  }
};

void print_action_matrix(const PlayYourCardsRight &problem);

int main() {
  std::vector<double> prize;
  double total = 100;
  for (auto i = 0; i <= 54; i++) {
    prize.push_back(total);
    total += 100;
  }

  PlayYourCardsRight problem(prize);
  problem.use_action_matrix();
  problem.calculate();

  std::cout << "Optimal Policy\n";
  std::cout << "Expected Prize = " << problem.expected_prize() << "\n";

  PlayYourCardsRightMyopic problem_myopic(prize);
  problem_myopic.use_action_matrix();
  problem_myopic.calculate();

  std::cout << "Myopic Policy\n";
  std::cout << "Expected Prize = " << problem_myopic.expected_prize() << "\n";

  std::cout << "Optimal Policy\n";
  std::cout << "card/turn\n";
  print_action_matrix(problem);
  std::cout << "Myopic Policy\n";
  std::cout << "card/turn\n";
  print_action_matrix(problem_myopic);

  exit(0);
}

void print_action_matrix(const PlayYourCardsRight &problem) {
  for (auto card = 13u; card > 0; --card) {
    std::cout << std::setw(3) << card;
    for (auto r = 1u; r <= 10; ++r) {
      std::cout << std::setw(2) << problem.action_matrix(round{r}, card);
    }
    std::cout << "\n";
  }
}