#include <inventory_management.h>
#include <policy_nearest_neighbour.h>
#include <policy_no_pooling.h>

int main() {
  InventoryManagementParameters params;
  params.T = 1000;
  params.restock_level = {10, 10, 10};
  params.demand = {8, 8, 8};
  params.manufacturer_cost = {20, 20, 20};
  params.transfer_cost = {{0, 10, 30}, //
                          {10, 0, 25}, //
                          {30, 25, 0}};

  InventoryManagement<PolicyOptimal> problem =
      InventoryManagement<PolicyOptimal>::inventory_management_factory(params);
  problem.calculate();

  std::cout << "Optimal Policy: total cost = "
            << problem.value_function(stage(params.T), params.restock_level)
            << "\n";

  InventoryManagement<PolicyNearestNeighbour> problem_nn =
      InventoryManagement<PolicyNearestNeighbour>::inventory_management_factory(params);
  problem_nn.calculate();

  std::cout << "Nearest Neighboiur Policy: total cost = "
            << problem_nn.value_function(stage(params.T), params.restock_level)
            << "\n";

  InventoryManagement<PolicyNoPooling> problem_np =
      InventoryManagement<PolicyNoPooling>::inventory_management_factory(params);
  problem_np.calculate();

  std::cout << "No Pooling Policy: total cost = "
            << problem_np.value_function(stage(params.T), params.restock_level)
            << "\n";

  exit(0);
}