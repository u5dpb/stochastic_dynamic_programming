#include <inventory_management.h>

int main() {
  InventoryManagementParameters params;
  params.T = 1000;
  params.restock_level = {10, 10, 10};
  params.demand = {8, 8, 8};
  params.manufacturer_cost = {20, 20, 20};
  params.transfer_cost = {{0, 10, 11}, //
                          {10, 0, 25}, //
                          {11, 25, 0}};

  InventoryManagement problem=InventoryManagement<>::inventory_management_factory(params);
  problem.use_event_action_matrix({{EventEnum::demand_with_no_stock, 0}});
  problem.calculate();

  std::cout << "Optimal Policy: total cost = "
            << problem.value_function(stage(params.T), params.restock_level) << "\n";

  for (auto t : {1u, 100u, 200u, 300u}) {
    std::cout << "t = " << t << "\n";
    std::cout << "s2\\s3 ";
    for (auto stock_3 = 0u; stock_3 <= 9; ++stock_3) {
     std::cout << stock_3 << " ";
    }
    std::cout << "\n";

    for (auto stock_2 = 0u; stock_2 <= 9; ++stock_2) {
      std::cout << "   " << stock_2 << "  ";
      for (auto stock_3 = 0u; stock_3 <= 9; ++stock_3) {
        std::cout << problem.event_action_matrix(
                         stage(t), {0, stock_2, stock_3},
                         {EventEnum::demand_with_no_stock, 0})
                  << " ";
      }
      std::cout << "\n";
    }
    std::cout << "\n";
  }

  exit(0);
}