# Inventory Management

# Inventory Management Parameters API

The `InventoryManagement` class uses a parameter `struct` to pass the problem parameters into the constructor. To create the class an instance of the `struct` should be created, all parameters assigned and the `struct` passed to the constructor. See the example program for details.

```c++
struct InventoryManagementParameters {
  unsigned int T;
  std::vector<unsigned int> restock_level;
  std::vector<double> demand;
  std::vector<double> manufacturer_cost;
  std::vector<std::vector<double>> transfer_cost;
  ...
};
```

# Event Type API

The `InventoryManagement` class allows the storage of actions in response to specific events in the problem. To simplify this process `struct event_type` is used.

There are three types of event of interest:
* no customer demands
* a customer demand at a location with stock
* a customer demand at a location with no stock

The model generates actions differently for each case. This can be seen in the `generate_events` function.

The following `enum class` defines these cases.

```c++
enum class EventEnum { no_demand, demand_with_stock, demand_with_no_stock };
```

The `event_type` is defined as:

```c++
struct event_type {
  EventEnum type;
  unsigned int location;
  ...
};
```

* `EventEnum type` is the event type.
* `unsigned int location` is the location of the event. In the case of `EventEnum::no_demand` this is superfluous.

# Inventory Management API

```c++
template <class Policy>
InventoryManagement<Policy> inventory_management_factory(const InventoryManagementParameters &params)
```
Factory function
* `Policy` defines the policy class that defines the decisions made. This defaults to `PolicyOptimal`
* `const InventoryManagementParameters &params` is a `struct` that gives the parameters for the problem.

Note that this class has no public constructor.

---

```c++
double value_function(const stage &t, const StateType &state) const
```
Getter: returns the value function for a stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const StateType &state` is a state of the Dynamic Program. `StateType` is `std::vector<unsigned int>`.

---
```c++
void calculate()
```

Calculates the value function for all stage/state combinations.

---

```c++
void calculate(const stage int &max_T)
```

Calculates the value function for all stage/state combinations.
* `const stage &max_T` is the largest stage of the problem. If `max_T > params.T` then only the last `T` calculations will be stored. `stage` is a strong `unsigned int` type.

---

```c++
stage T() const
```
Getter: returns the value of `T`.  `stage` is a strong `unsigned int` type.

```c++
void use_event_action_matrix()
```
The Dynamic Program will store the action taken for each stage/state/event combination. The action is defined as an `int`.

---

```c++
void use_event_action_matrix(const std::unordered_set<event_type> &events_to_store)
```

The Dynamic Program will store the action taken for each stage/state combination for a specific subset of events. The action is defined as an `int`.

* `const std::unordered_set<event_type> &events_to_store` is the subset of events to be stored.

---

```c++
int event_action_matrix(const stage &t,
                        const StateType &state,
                        const event_type &event_type) const;
```
Getter: returns the action taken for a given stage/state/event combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const StateType &state` is a state of the Dynamic Program. `StateType` is `std::vector<unsigned int>`.
* `const event_type &event_type` is the event the action is in response to.

## protected

```c++
double v(const stage &t, const StateType &state) const
```
Getter: returns the unmodified value function for a stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const StateType &state` is a state of the Dynamic Program. `StateType` is `std::vector<unsigned int>`.

---

# The Problem

A retailer has several stores. Customers arrive at each store at a different rate and request a particular product. If the product is in stock then the customer receives the item and is happy. If the item is out of stock then the retailer has two options. Satisfy the customer request with an item from another store. Satisfy the customers request from the manufacturer. Each of these options have associated costs from transportation, potential price reduction, loss of good will, etc. Restocking of goods to the stores occurs at a fixed interval. This problem only considers a single product. Obviously (in the real world) retailers will sell more than one thing.

## Simplification

It is common for customer arrival processes to be modelled as a Poisson process. This is a continuous period of time. A Stochastic Dynamic Programming model will approximate this time period as $`T`$ “time slices”. These “time slices” are the stages of the problem. At each stage an event occurs: a single customer may (or may not) enter the system with a fixed probability. Obviously this is inaccurate because in a continuous time period two (or more) customers might enter the system during a “time slice”. The larger the value of $`T`$ the smaller this error. Mathematically this approximation models the Poisson customer arrival process using a Binomial distribution. 

Also assume that customers arrive at a constant rate. This means that the probability of a customer arriving for each "time slice" is constant. This assumption should be easy to relax provided care is taken to ensure the above approximation still holds.

# Parameters


There are $`n`$ stores. At the start of the time period the stores have $`\boldsymbol{S}=\{S_{1},\,...,\,S_{n}\}`$ stock. The Poisson arrival rate at each stores is $`\boldsymbol{\lambda}=\{\lambda_{1},\,...,\,\lambda_{n}\}`$ measured in customers per time unit. The overall arrival rate is $`\Lambda=\sum\lambda`$.

The stages of the problem is the whole time period between restocking. At each stage an event occurs: a customer may (or may not) enter the system with probability $`p=\frac{\Lambda}{T}`$. 

Each stage has a state $`\boldsymbol{i}=\{i_{1},\,...,\,i_{n}\}`$. This is the stock level for the item at each store.

Each stage begins with an event. The event is whether a customer arrives in the system. If no customer arrives then no decision or action needs to be made. If a customer arrives and there is stock at that location the action is to satisfy the customer from that locations stock. If a customer arrives and there is not stock then a decision is made between obtaining the item from the manufacturer or obtaining the item from each of the other stores that have stock.

# Model

```math
\begin{array}{ll}
v^{t}\left(\boldsymbol{i}\right)= & p\sum_{s}\phi_{s}\min\begin{cases}
v^{t-1}\left(\boldsymbol{i}-\boldsymbol{e}_{s}\right) & :\ stock\ available \\
M_{s}+v^{t-1}\left(\boldsymbol{i}\right) & :\ from\ manufacturer \\
\min_{k!=s}\left\{ T_{k,s}+v^{t-1}\left(\boldsymbol{i}-\boldsymbol{e}_{k}\right)\right\}  & :\ from\ another\ store
\end{cases} \\
 & +\left(1-p\right)v^{t-1}\left(\boldsymbol{i}\right) \\
\\
v^{0}\left(\boldsymbol{i}\right)= & 0,\ \forall\boldsymbol{i}
\end{array}
```

Where:
* $`\phi_{s}`$ is the proportion of demand at store s. So $`\phi_{s}=\frac{\lambda_{s}}{\Lambda}`$.
* $`M_{s}`$ is the total cost of satisfying the customer at store s from the manufacturer.
* $`T_{k,s}`$ is the total cost of satisfying the customer at store $`s`$ from store $`k`$.
* $`e_{s}`$ is a vector of $`0`$'s with $`1`$ in position $`s`$. So if $`\boldsymbol{i}=\{5,5,5\}`$ then $`\boldsymbol{i}-\boldsymbol{e}_{1}=\{4,5,5\}`$.

# Example Problem

The following example uses the parameters:
* $`n=3`$
* $`T=1000`$
* $`\boldsymbol{\lambda}=\{8,\,8,\,8\}`$
* $`\boldsymbol{S}=\{10,\,10,\,10\}`$
* $`\boldsymbol{M}=\{20,\,20,\,20\}`$
* $`\boldsymbol{T}=\begin{Bmatrix} 0 & 10 & 30 \\ 10 & 0 & 25 \\ 30 & 25 & 0 \\ \end{Bmatrix}`$

File: `example_inventory_management_problem.cpp`

```c++
#include <inventory_management.h>

int main() {
  InventoryManagementParameters params;
  params.T = 1000;
  params.restock_level = {10, 10, 10};
  params.demand = {8, 8, 8};
  params.manufacturer_cost = {20, 20, 20};
  params.transfer_cost = {{0, 10, 11}, //
                          {10, 0, 25}, //
                          {11, 25, 0}};

  InventoryManagement problem=InventoryManagement<>::inventory_management_factory(params);
  problem.use_event_action_matrix({{EventEnum::demand_with_no_stock, 0}});
  problem.calculate();

  std::cout << "Optimal Policy: total cost = "
            << problem.v(stage(params.T), params.restock_level) << "\n";

  for (auto t : {1u, 100u, 200u, 300u}) {
    std::cout << "t = " << t << "\n";
    std::cout << "s2\\s3 ";
    for (auto stock_3 = 0u; stock_3 <= 9; ++stock_3) {
     std::cout << stock_3 << " ";
    }
    std::cout << "\n";

    for (auto stock_2 = 0u; stock_2 <= 9; ++stock_2) {
      std::cout << "   " << stock_2 << "  ";
      for (auto stock_3 = 0u; stock_3 <= 9; ++stock_3) {
        std::cout << problem.event_action_matrix(
                         stage(t), {0, stock_2, stock_3},
                         {EventEnum::demand_with_no_stock, 0})
                  << " ";
      }
      std::cout << "\n";
    }
    std::cout << "\n";
  }

  exit(0);
}
```

The program calculates the cost when using the optimal policy and displays how a customer demand at the 1st location is handled when there is no stock. `0` means satisfying the order from the manufacturer; `2`, from the 2nd location; and `3`, from the 3rd location.

The output is as follows:

```
Optimal Policy: total cost = 18.2174
t = 1
s2\s3 0 1 2 3 4 5 6 7 8 9 
   0  0 3 3 3 3 3 3 3 3 3 
   1  2 2 2 2 2 2 2 2 2 2 
   2  2 2 2 2 2 2 2 2 2 2 
   3  2 2 2 2 2 2 2 2 2 2 
   4  2 2 2 2 2 2 2 2 2 2 
   5  2 2 2 2 2 2 2 2 2 2 
   6  2 2 2 2 2 2 2 2 2 2 
   7  2 2 2 2 2 2 2 2 2 2 
   8  2 2 2 2 2 2 2 2 2 2 
   9  2 2 2 2 2 2 2 2 2 2 

t = 100
s2\s3 0 1 2 3 4 5 6 7 8 9 
   0  0 0 3 3 3 3 3 3 3 3 
   1  0 0 3 3 3 3 3 3 3 3 
   2  2 2 2 3 3 3 3 3 3 3 
   3  2 2 2 2 2 3 3 3 3 3 
   4  2 2 2 2 2 2 2 2 2 2 
   5  2 2 2 2 2 2 2 2 2 2 
   6  2 2 2 2 2 2 2 2 2 2 
   7  2 2 2 2 2 2 2 2 2 2 
   8  2 2 2 2 2 2 2 2 2 2 
   9  2 2 2 2 2 2 2 2 2 2 

t = 200
s2\s3 0 1 2 3 4 5 6 7 8 9 
   0  0 0 0 3 3 3 3 3 3 3 
   1  0 0 0 3 3 3 3 3 3 3 
   2  0 0 0 3 3 3 3 3 3 3 
   3  2 2 2 2 3 3 3 3 3 3 
   4  2 2 2 2 2 3 3 3 3 3 
   5  2 2 2 2 2 2 2 2 2 2 
   6  2 2 2 2 2 2 2 2 2 2 
   7  2 2 2 2 2 2 2 2 2 2 
   8  2 2 2 2 2 2 2 2 2 2 
   9  2 2 2 2 2 2 2 2 2 2 

t = 300
s2\s3 0 1 2 3 4 5 6 7 8 9 
   0  0 0 0 0 3 3 3 3 3 3 
   1  0 0 0 0 3 3 3 3 3 3 
   2  0 0 0 0 3 3 3 3 3 3 
   3  0 0 0 0 3 3 3 3 3 3 
   4  2 2 2 2 2 3 3 3 3 3 
   5  2 2 2 2 2 2 3 3 3 3 
   6  2 2 2 2 2 2 2 2 3 3 
   7  2 2 2 2 2 2 2 2 2 2 
   8  2 2 2 2 2 2 2 2 2 2 
   9  2 2 2 2 2 2 2 2 2 2 
```



# Implementation Details

The key part of the implementation is the `generate_events` function. It generates a `vector` of probabilistic events along with the set of possible actions that could be taken in response to them. Factory functions are used to create instances of `struct Event`. An example implementation is below.

```c++
template <class Policy>
std::vector<Event<StateType>>
InventoryManagement<Policy>::generate_events(const stage &t,
                                             const StateType &state) const {
  (void) t;
  std::vector<Event<StateType>> events;
  events.push_back(make_event_no_demand(state));
  for (unsigned int demand_location = 0; demand_location < state.size();
       ++demand_location) {
    if (state[demand_location] > 0) {
      events.push_back(make_event_demand_with_stock(state, demand_location));
    } else {
      events.push_back(make_event_demand_no_stock(state, demand_location));
    }
  }
  return events;
}
```

Event generation is split into three sub-event generation functions `make_event_no_demand(...)`, `make_event_demand_with_stock(...)` and `make_event_demand_no_stock(...)`. The functions, given below, use the `Policy` class to create a set of actions that can be taken in response to the event.

```c++
template <class Policy>
Event<StateType>InventoryManagement<Policy>::make_event_no_demand(
    const StateType &state) const {
  return {1 - overall_demand_probability(), {policy.make_no_action(state)}, 0};
}

template <class Policy>
Event<StateType>InventoryManagement<Policy>::make_event_demand_with_stock(
    const StateType &state,
    const unsigned int &demand_location) const {
  return {overall_demand_probability() * demand_proportion()[demand_location],
          {policy.make_use_own_stock_action(state, demand_location)},
          (int)(demand_location + 1)};
}

template <class Policy>
Event<StateType>InventoryManagement<Policy>::make_event_demand_no_stock(
    const StateType &state,
    const unsigned int &demand_location) const {
  return {overall_demand_probability() * demand_proportion()[demand_location],
          policy.stockout_actions(demand_location, state),
          -(int)(demand_location + 1)};
}
```

Of particular importance in this problem is what happens when a customer requests an item that is out of stock. The `Policy` class defines a `stockout_actions` function which generates the possible actions in response to this event. These are:
* supply from the manufacturer
* supply from every another location that has stock

The `PolicyOptimal` class gives all possible actions. The numerically optimal choice will be chosen by the `InventoryManagement` class. It is this function that is overloaded should a specific policy be implemented. The specific policy implementation should force the policy's chosen action by returning it as the sole action.

```c++
std::vector<Action<StateType>>
InventoryManagement::stockout_actions(
    const unsigned int &demand_location,
    const StateType &state) const {
  std::vector<Action<StateType> actions = {
      make_manufacture_action(demand_location, state)};
  for (auto transfer_location = 0u; transfer_location < state.size();
       ++transfer_location) {
    if (state[transfer_location] > 0) {
      auto new_state = state;
      new_state[transfer_location]--;
      actions.push_back(make_transfer_stock_action(transfer_location,
                                                   demand_location, state));
    }
  }
  return actions;
}
```

# Alternate Policies

Two possible alternate policies are called *nearest neighbour* and *no pooling*.

In *nearest neighbour*, when a customer requests an item that is out of stock, the demand is satisfied by the option with lowest immediate cost (i.e. the "nearest" location with stock or the manufacturer if it is cheaper). This policy does not consider the knock on effect of moving the item of stock. The item may, for example, be the last item available at the location and highly likely to be needed where it is.

In *no pooling* no stock is transfered between locations. If a customer requests an item that is out of stock, the item must come from the manufacturer.

## Implementation of the Alternate Policies

The following code implements these two alternate policies and compares them to the optimal policy.

File: `example_inventory_management_policy_comparison.cpp`

```c++
#include <inventory_management.h>
#include <policy_nearest_neighbour.h>
#include <policy_no_pooling.h>

int main() {
  InventoryManagementParameters params;
  params.T = 1000;
  params.restock_level = {10, 10, 10};
  params.demand = {8, 8, 8};
  params.manufacturer_cost = {20, 20, 20};
  params.transfer_cost = {{0, 10, 30}, //
                          {10, 0, 25}, //
                          {30, 25, 0}};

  InventoryManagement<PolicyOptimal> problem =
      InventoryManagement<PolicyOptimal>::inventory_management_factory(params);
  problem.calculate();

  std::cout << "Optimal Policy: total cost = "
            << problem.value_function(stage(params.T), params.restock_level)
            << "\n";

  InventoryManagement<PolicyNearestNeighbour> problem_nn =
      InventoryManagement<PolicyNearestNeighbour>::inventory_management_factory(params);
  problem_nn.calculate();

  std::cout << "Nearest Neighboiur Policy: total cost = "
            << problem_nn.value_function(stage(params.T), params.restock_level)
            << "\n";

  InventoryManagement<PolicyNoPooling> problem_np =
      InventoryManagement<PolicyNoPooling>::inventory_management_factory(params);
  problem_np.calculate();

  std::cout << "No Pooling Policy: total cost = "
            << problem_np.value_function(stage(params.T), params.restock_level)
            << "\n";

  exit(0);
}
```

The output is shown below.

```
Optimal Policy: total cost = 21.1039
Nearest Neighboiur Policy: total cost = 27.3231
No Pooling Policy: total cost = 25.3133
```


