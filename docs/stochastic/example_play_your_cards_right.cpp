#include <play_your_cards_right.h>
#include <vector>
#include <iostream>
#include <iomanip>

int main() {
  std::vector<double> prize;
  double total=100;
  for (auto i = 0; i <= 54; i++)
  {
    prize.push_back(total);
    total+=100;
  }
  
  PlayYourCardsRight problem(prize);

  problem.use_action_matrix();

  problem.calculate();

  std::cout << "Optimal Policy\n";
  std::cout << "card/turn\n";
  for (auto card = 13u; card > 0; --card) {
    std::cout << std::setw(3) << card;
    for (auto r = 1u; r<=54; ++r) {
      std::cout << std::setw(2) << problem.action_matrix(round{r}, card);
    }
    std::cout << "\n";
  }

  exit(0);
}