- [A Simple Example](#a-simple-example)
  - [Deterministic Dynamic Programs](#deterministic-dynamic-programs)
  - [`DeterministicDynamicProgram`](#deterministicdynamicprogram)
  - [`StateLoop`](#stateloop)
  - [Child Class definition](#child-class-definition)
  - [Object creation](#object-creation)
  - [Base State](#base-state)
  - [Other problem parameters](#other-problem-parameters)
  - [The value function](#the-value-function)
  - [`Action`](#action)
    - [Subsequent State](#subsequent-state)
  - [Action generation](#action-generation)
    - [Warning - Invalid States](#warning---invalid-states)
  - [Solution](#solution)
  - [The Action Matrix](#the-action-matrix)

# A Simple Example

## Deterministic Dynamic Programs

The following will assumes knowledge of stages, states, decisions and other stochastic dynamic programming terminology.

An example problem.

```math
v^{t}(i)=\min\left\{P-t(i)+c(0)+v^{t-1}(1),\,c(i)+v^{t-1}(i+1)\right\} 
```

```math
v^{0}(i)=-s(i) 
```

This is a maintenance and replacement problem. Further details can be found in the **deterministic** sub-directory.

A child instance of `DeterministicDynamicProgram` will be used to solve this problem. In the example files problem classes are created which are children `DeterministicDynamicProgram` of the class.

Ordinarily the child class would contain data about the problem. To keep the derived class simple in this example global variables will be used. This makes code less robust. For an improved implementation of this problem see the `MaintenanceAndReplacementProblem` class.

## `DeterministicDynamicProgram`

The `DeterministicDynamicProgram` constructor is:
```c++
template <class State>
DeterministicDynamicProgram(const unsigned int &T,
                            const StateLoop<State> &state_loop)
```

* `State` is a type that describes the state of the problem. In this example `unsigned int` is used as the age of the machines in the problem cannot be negative.
* `T` is the number of stages in the problem.
* `StateLoop` is a template class that will loop through all possible states of the problem. In this example `StateLoop<unsigned int>` is used.

## `StateLoop`

The constructor of `StateLoop` is:
```c++
template <class T>
StateLoop(const T &min, const T &max)
```
In this problem the minimum possible age is 1 and a, perhaps arbitrary, maximum of 7 is used.

The `StateLoop` class requires of it's template type:
* `operator++`
* `operator=`
* `size_t reference(const T &value) const` converts the state `value` into a unique `size_t` for storage.

The `StateLoop` loops using an iterator as follows:
```c++
#include <state_loop.h>

StateLoop<unsigned int> state_loop(1,7);
for (auto state = state_loop.begin(); state != state_loop.end(); ++state)
{
    std::cout << state << "\n";
}
```

Or more concisely:
```c++
for (const auto &state : state_loop)
{
    std::cout << state << "\n";
}
```

The files `state_loop.h` and `state_loop_vector.h` provide implementations of `StateLoop` that will work for basic types like `int`, `unsigned int`, `vector<int>` and `vector<unsigned int>`. Other state definitions could be defined.

## Child Class definition

The child class `ExampleDP` is defined as follows. In this example three functions are definied. 
* The constructor calls the parent constructor. 
* `value_function`  is an abstract function of `DeterministicDynamicProgram`. It returns the the `value_function_` variable via a call to `v()`. Note: in some cases this value may be modified due to the problem definition.
* `generate_actions` is an abstract function of `DeterministicDynamicProgram`. It is key to the implementation of each problem.

```c++
class ExampleDP : public DeterministicDynamicProgram<unsigned int> {
public:
  ExampleDP(const unsigned int &T, const StateLoop<unsigned int> &state_loop)
      : DeterministicDynamicProgram(T, state_loop) {}
  double value_function(const stage &t,
                        const unsigned int &state) const override {
    return v(t, state);
  }
protected:
  std::vector<Action<unsigned int>>
  generate_actions(const stage &t, const unsigned int &state) override {
    // override abstract function
  }
};
```

## Object creation

The `DeterministicDynamicProgram` is then declared as:
```c++
#include <deterministic_dynamic_program.h>
#include <state_loop.h>

unsigned int oldest_state=7;

int main(int argc, char **argv) {
    StateLoop<unsigned int> state_loop(1,oldest_state);
    ExampleDP ddp(10, state_loop);
    ...
}
```

## Base State

The base state of the problem must be defined.

```math
v^{0}(i)=-s(i)
```

The following class function is used:

```c++
void set_v0(const State &state, const double &value)
```
* `state` is the state whose value is being assigned. `i` in the equation above.
* `value` is the value being assigned. `-s(i)` above.

The base state can be defined using the salvage value `s(i)`:
```c++
std::vector<double> salvage_value={0,25, 17, 8, 0, 0, 0, 0};
for (const auto &state : state_loop) {
    ddp.set_v0(state, salvage_value[state]);
}

```
## Other problem parameters

These are defined as:
```c++
unsigned int oldest_state=7;

std::vector<double> trade_in_value = {0, 32, 21, 11, 5, 0, 0, 0};
std::vector<double> operating_cost = {10, 13, 20, 40, 70, 100, 100, 100};
double purchase_cost = 50;

int main(int argc, char **argv) {
    ...
```

## The value function

The value function for all states is calculated using the following:
```c++
template <class State>
void calculate();
```
This will use the function `generate_actions` that will define what actions or decisions should be considered by the value function.

The result of the value function can be accessed by:
```c++
template <class State>
double v(const stage &t, const State &state) const;
```

`stage` is a strong type. An example of its use can be seen below. In this problem both the stage a state are non-negative integers. The use of a strong type prevents to different arguments to this function (and others) being mixed up.

## `Action`

Actions are defined in the `action.h` file as:

```c++
template <class State> struct Action {
  double cost;
  std::vector<SubsequentState<State>> subsequent_state;
  int type;
};
```
* `cost` is the immediate cost of the action.
* `subsequent_state` is a vector of possible subsequent states and their associated probabilities. The latter do not apply to deterministic problems.
* `type` is a variable used to store what type of decision is made. Encoding is user defined. This is used when generating an action matrix.

### Subsequent State

The `SubsequentState` struct is defined in `subsequent_state.h` as:

```c++
template <class State>
struct SubsequentState {
  State state;
  double probability;
};
```

## Action generation

```math
v^{t}(i)=\min\left\{P-t(i)+c(0)+v^{t-1}(1),c(i)+v^{t-1}(i+1)\right\}
```

As defined by the value function there are two actions: maintain and replace. These are returned by the `generate_actions` function with the exception of the oldest state (when replacement is forced).
```c++
 std::vector<Action<unsigned int>>
  generate_actions(const stage &t, const unsigned int &state) override {
    std::vector<Action<unsigned int>> action_set;
    action_set.push_back(
        {purchase_cost - trade_in_value[state] + operating_cost[0],
         {{1u, 1.0}},
         1u});
    if (state < oldest_state) {
      action_set.push_back({operating_cost[state], {{state + 1u, 1.0}}, 0u});
    }
    return action_set;
  }
```

The action `type` variable is `0` for replacement and `1` for maintenance.

This function is then used by the `calculate` function of `DeterministicDynamicProgram` to solve the problem.

### Warning - Invalid States

A state is **unreachable** if there is no sequence of decisions/actions from the starting state(s) that reach the state.
A state is **invalid** if there are no feasible decisions for the state.

Example: 
* A maintenance and replacement problem with 3 machines which are 4 years old. Over the next 10 years when should we replace them given only one machine can be replaced at any one time.
* unreachable states: the starting state is {4,4,4} so the next state cannot be {6,6,6}.
* invalid states: if the oldest state is 9 then we cannot reach state {9,9,9} as the subsequent states would be {10,10,10}, {1,10,10}, {10,1,10} and {10,10,1} which all violate the oldest state.

Ideally unreachable and invalid states should be eliminated but this may not always be practical. For unreachable states this shouldn't cause problems other than superfluous computation. For invalid states there is a danger.

The `DeterministicDynamicProgram` class initialises the value function to be `std::numeric_limits<double>::max()`. The `generate_actions` function for a problem should return an empty `vector` for an invalid state. `DeterministicDynamicProgram::calculate` will leave the state value as `std::numeric_limits<double>::max()`. ***Do not reply on this***. Avoid creating actions whose subsequent state is invalid. Why? Arithmetic including `std::numeric_limits<double>::max()` will lead to undefined behaviour.

## Solution

```c++
#include <action.h>
#include <deterministic_dynamic_program.h>
#include <iostream>
#include <state_loop.h>
#include <vector>

unsigned int oldest_state = 7;

std::vector<double> trade_in_value = {0, 32, 21, 11, 5, 0, 0, 0};
std::vector<double> operating_cost = {10, 13, 20, 40, 70, 100, 100, 100};
double purchase_cost = 50;

class ExampleDP : public DeterministicDynamicProgram<unsigned int> {
public:
  ExampleDP(const unsigned int &T, const StateLoop<unsigned int> &state_loop)
      : DeterministicDynamicProgram(T, state_loop) {}
  double value_function(const stage &t,
                        const unsigned int &state) const override {
    return v(t, state);
  }

protected:
  std::vector<Action<unsigned int>>
  generate_actions(const stage &t, const unsigned int &state) const override {
    std::vector<Action<unsigned int>> action_set;
    action_set.push_back(
        {purchase_cost - trade_in_value[state] + operating_cost[0],
         {{1u, 1.0}},
         1u});
    if (state < oldest_state) {
      action_set.push_back({operating_cost[state], {{state + 1u, 1.0}}, 0u});
    }
    return action_set;
  }
};

int main(int argc, char **argv) {
  StateLoop<unsigned int> state_loop(1, oldest_state);
  ExampleDP ddp(10, state_loop);

  std::vector<double> salvage_value = {0, 25, 17, 8, 0, 0, 0, 0};
  for (const auto &state : state_loop) {
    ddp.set_v0(state, salvage_value[state]);
  }

  ddp.calculate();

  std::cout << "Cost of a 4 year old machine is "
            << ddp.value_function(stage{10}, 4) << "\n";

  return 0;
}
```

## The Action Matrix

The library can create an *action matrix*. This matrix will give the action taken at each possible decision point.

To generate the action matrix in the above example call the 'use_action_matrix` function as follows:

```c++
  DeterministicDynamicProgram<unsigned int> ddp(10, state_loop);
  ddp.use_action_matrix();
```

The `DeterministicDynamicProgram` will then store which action was taken as an `int` as defined in the `generate_actions` function.

The set of decisions made or *policy* can be generated as follows:

```c++
  std::cout << "The optimal policy is\n";
  auto age = 4u;
  for (auto t = 10u; t > 0; t--) {
    std::cout << "t = " << t << ", " << age << " year old machine: ";
    if (ddp.action_matrix(stage(t), age) == 1) {
      std::cout << "replace.\n";
      age = 1;
    } else {
      std::cout << "keep.\n";
      age++;
    }
  }
```
