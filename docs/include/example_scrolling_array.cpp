#include <scrolling_array.h>
#include <iostream>

int main()
{

  ScrollingArray<int> scrolling_array(2);
  for (size_t i=0; i<scrolling_array.head(); ++i)
    scrolling_array[i]=(int)i;

  std::cout << "Size = " << scrolling_array.size() << "\n";
  std::cout << "Head = " << scrolling_array.head() << "\n";

  std::cout << "Increment\n";
  ++scrolling_array;

  std::cout << "Size now = " << scrolling_array.size() << "\n";
  std::cout << "Head now = " << scrolling_array.head() << "\n";

  std::cout << "Increment again\n";
  ++scrolling_array;

  std::cout << "Size now = " << scrolling_array.size() << "\n";
  std::cout << "Head now = " << scrolling_array.head() << "\n";

  std::cout << "Resize\n";
  scrolling_array.resize(5,-1);
  std::cout << "Size now = " << scrolling_array.size() << "\n";
  std::cout << "Head now = " << scrolling_array.head() << "\n";


  // scrolling_array[0] = 1; error
  scrolling_array[4] = 4;
  scrolling_array[5] = 5;
  scrolling_array[6] = 6;
  // scrolling_array[7] = 1; error

  std::cout << "Array is ";
  for (auto i = scrolling_array.head()-scrolling_array.size(); i <scrolling_array.head(); ++i)
  {
    std::cout << scrolling_array[i] << " ";
  }
  std::cout << "\n";
}