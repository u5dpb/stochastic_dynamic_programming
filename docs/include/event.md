# Event 

```c++
template<class State>
struct Event {
  double probability;
  std::vector<Action<State>> subsequent_actions;
  unsigned int type;
};
```

`Event` is a simple `struct` which stores:
* the `probability` of the event happening, 
* the `subsequent_actions` that could be taken in response to the event,
* and an `event_type`.

Within an Stochastic Dynamic Program (SDP) each stage may begin with a set of possible events. The probabilities of these events should add up to `1`. Different sets of actions may be possible depending on the event.

For example, in an inventory management problem each stage may begin with the events *customer arrives* and *no customer arrives*. In the case of the former, subsequent actions relating to serving the customer are considered by the SDP. In the case of the latter, a *nothing happens* action is the only subsequent action.