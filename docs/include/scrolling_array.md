
# ScrollingArray

A scrolling array is a container that has a fixed size. This size is commonly less that the referencing index. The container has a *head* value that gives the current maximum reference value. On contruction this is the size of the container. An increment operator is called that increments this *head* value. However, the size of the array does not increase. The smallest reference is no longer valid. So an array of size 10 can be referenced from 0 to 9. After an increment it can be referenced from 1 to 10 with the values referenced from 1 to 9 remaining untouched.

# ScrollingArray API

```c++
template <class T>
ScrollingArray()
```
Constructor
* `class T` is the type stored in the array.
---

```c++
template <class T>
ScrollingArray(size_t s, bool auto_scroll = false)
```
Constructor
* `class T` is the type stored in the array.
* `size_t s` is the size of the array.
* `bool auto_scroll` will automatically increase the *head* value if an assignment is made to reference greater that the current *head* value.
---

```c++
template <class T>
ScrollingArray(size_t s, const T &initial, bool auto_scroll = false)
```
Constructor
* `class T` is the type stored in the array.
* `size_t s` is the size of the array
* `const T &initial` is the initial value for elements of the array.
* `bool auto_scroll` will automatically increase the *head* value if an assignment is made to reference greater that the current *head* value.
---

```c++
template <class T>
size_t head() const
Getter. Returns the current *head* value.
```

---

```c++
template <class T>
size_t size() const
```
Getter. Returns the size of the array. Note: `size <= head`

---

```c++
template <class T>
void resize(const size_t size)
```
Setter
* `const size_t size` is the new size of the array. The *head* will increase if the size does. The head **will not** decrease if the size decreases.

---

```c++
template <class T>
void resize(const size_t size, const T &initial)
```
Setter
* `const size_t size` is the new size of the array. The *head* will increase if the size does. The head **will not** decrease if the size decreases.
* `const T &initial` is the initial value for any new elements of the array.

---

```c++
template <class T>
T &operator[](std::size_t index)
```
Returns a **non-const** reference to the array element referenced by `index`.

---

```c++
template <class T>
const T &operator[](std::size_t index) const
```
Returns a **const** reference to the array element referenced by `index`.

---

```c++
template <class T>
ScrollingArray &operator++()
```
Increments the *head* value of the array.

---

```c++
template <class T>
bool valid_index(size_t index) const
```
Returns whether `index` is a valid index for the array.

# Example

```c++
#include <scrolling_array.h>
#include <iostream>

int main()
{

  ScrollingArray<int> scrolling_array(2);
  for (size_t i=0; i<scrolling_array.head(); ++i)
    scrolling_array[i]=(int)i;

  std::cout << "Size = " << scrolling_array.size() << "\n";
  std::cout << "Head = " << scrolling_array.head() << "\n";

  std::cout << "Increment\n";
  ++scrolling_array;

  std::cout << "Size now = " << scrolling_array.size() << "\n";
  std::cout << "Head now = " << scrolling_array.head() << "\n";

  std::cout << "Increment again\n";
  ++scrolling_array;

  std::cout << "Size now = " << scrolling_array.size() << "\n";
  std::cout << "Head now = " << scrolling_array.head() << "\n";

  std::cout << "Resize\n";
  scrolling_array.resize(5,-1);
  std::cout << "Size now = " << scrolling_array.size() << "\n";
  std::cout << "Head now = " << scrolling_array.head() << "\n";


  // scrolling_array[0] = 1; error
  scrolling_array[4] = 4;
  scrolling_array[5] = 5;
  scrolling_array[6] = 6;
  // scrolling_array[7] = 1; error

  std::cout << "Array is ";
  for (auto i = scrolling_array.head()-scrolling_array.size(); i <scrolling_array.head(); ++i)
  {
    std::cout << scrolling_array[i] << " ";
  }
  std::cout << "\n";
}
```

Output:

```
Size = 2
Head = 2
Increment
Size now = 2
Head now = 3
Increment again
Size now = 2
Head now = 4
Resize
Size now = 5
Head now = 7
Array is -1 -1 4 5 6 
```