#include <state_loop_vector.h>
#include <iostream>

int main()
{
    const StateLoop<std::vector<unsigned int>> state_loop({0,0,0},{10,10,10});
    for (const auto &state : state_loop)
    {
        for (const auto &v : state)
        {
            std::cout << v << " ";
        }
        std::cout << " : " << state_loop.reference(state);
        std::cout << "\n";
    }
    exit(0);
}