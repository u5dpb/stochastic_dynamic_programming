#include <iostream>
#include <staff_management_problem.h>

int main() {
  int hire_cost_fixed = 100;
  int hire_cost_variable = 100;
  int wage_cost = 200;
  StaffManagementProblem problem(StaffManagementProblem::day{10}, 9);
  problem.use_action_matrix();
  problem.set_requirements({5, 7, 5, 9, 8, 9, 7, 7, 5, 5});
  problem.set_hire_cost(hire_cost_fixed, hire_cost_variable);
  problem.set_wage_cost(wage_cost);
  problem.calculate();

  std::cout << "Optimal Policy\n";
  auto workers = 0u;
  for (unsigned int d = 1; d <= 10; ++d) {
    int action = problem.action_matrix(StaffManagementProblem::day{d}, workers);
    if (action < 0)
      std::cout << "Week " << StaffManagementProblem::day{d} << " fire " << -action << " workers\n";
    else
      std::cout << "Week " << StaffManagementProblem::day{d} << " hire " << action << " workers\n";
    workers = (unsigned int)((int)workers + action);
  }

  exit(0);
}