#include <iostream>
#include <maintenance_and_replacement_problem_multi.h>
#include <vector>

void print_action_matrix(const MaintenanceAndReplacementProblemMulti &problem,
                         const unsigned int &t,
                         const unsigned int &oldest_state);

void print_policy(std::vector<unsigned int> ages,
                  const MaintenanceAndReplacementProblemMulti &problem,
                  const unsigned int &no_stages);

int main() {
  const auto oldest_state = 5u;
  const auto no_stages = 100u;
  const auto no_machines = 2u;

 MaintenanceAndReplacementMultiParameters params;

  params.salvage_value = {50, 33, 22, 15, 10, 7};
  params.trade_in_value = {0, 30, 20, 13, 9, 6};
  params.operating_cost = {5, 7, 10, 20, 32, 45, 70};
  params.purchase_cost = {0, 60, 110};

  MaintenanceAndReplacementProblemMulti problem(no_stages, oldest_state,
                                                no_machines, params);
  problem.use_action_matrix();

  problem.calculate();

  for (auto t = no_stages; t > no_stages - 10; --t) {
    std::cout << "t = " << t << "\n";
    print_action_matrix(problem, t, oldest_state);
  }

  print_policy({1, 1}, problem, no_stages);
  print_policy({1, 3}, problem, no_stages);

  exit(0);
}

void print_action_matrix(const MaintenanceAndReplacementProblemMulti &problem,
                         const unsigned int &t,
                         const unsigned int &oldest_state) {

  std::cout << "    ";
  for (auto machine_2_age = 1u; machine_2_age <= oldest_state;
       ++machine_2_age) {
    std::cout << machine_2_age << " ";
  }
  std::cout << "\n";
  for (auto machine_1_age = 1u; machine_1_age <= oldest_state;
       ++machine_1_age) {
    std::cout << machine_1_age << " : ";
    for (auto machine_2_age = 1u; machine_2_age <= oldest_state;
         ++machine_2_age) {
      std::cout << problem.action_matrix(stage(t), {machine_1_age, machine_2_age})
                << " ";
    }
    std::cout << "\n";
  }
  std::cout << "\n";
}

void print_policy(std::vector<unsigned int> ages,
                  const MaintenanceAndReplacementProblemMulti &problem,
                  const unsigned int &no_stages) {
  std::cout << "Policy {" << ages[0] << "," << ages[1] << "}\n";
  for (auto t = no_stages; t > no_stages - 10; --t) {
    std::cout << "  t = " << t << " {" << ages[0] << "," << ages[1]
              << "}=" << problem.action_matrix(stage(t), ages) << "\n";
    if (problem.action_matrix(stage(t), ages) == 0) {
      ++ages[0];
      ++ages[1];
    } else if (problem.action_matrix(stage(t), ages) == 1) {
      if (ages[0] > ages[1]) {
        ages[0] = 1;
        ++ages[1];
      } else {
        ages[1] = 1;
        ++ages[0];
      }
    } else if (problem.action_matrix(stage(t), ages) == 2) {
      ages[0] = 1;
      ages[1] = 1;
    }
  }
}