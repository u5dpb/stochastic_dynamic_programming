# The Staff Management Problem

# The Staff Management Problem Class API

The file `staff_management_problem.h` defines the class `class StaffManagementProblem`.

---

```c++
StaffManagementProblem(const unsigned int &no_days,
                       const unsigned int &no_workers);

```

Class constructor
* `const unsigned int &no_days` is the number of days in the problem.
* `const unsigned int &no_workers` is the maximum number of workers in the problem.
  
---

```c++
void set_requirements(const std::vector<unsigned int> &requirements);
```

Setter
* `const std::vector<unsigned int> &requirements` is a vector of length `no_days` that gives the number of workers required on each day.

---

```c++
void set_hire_cost(const double &hire_cost_fixed,
                   const double &hire_cost_variable);
```

Setter
* `const double &hire_cost_fixed` is the fixed cost whenever workers are hired.
* `const double &hire_cost_variable` is the cost of a hire **per worker**.

---

```c++
void set_wage_cost(const double &wage_cost);
```

Setter
* `const double &wage_cost` is the cost of each worker per day.

---

```c++
void use_action_matrix();
```

The Dynamic Program will store the action taken for each stage/state combination. The action is defined as an `int` in the `generate_actions` function.

---

```c++
double value_function(const stage &t, const unsigned int &state) const;
```
Getter: returns the value function for a stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const unsigned int &state` is a state of the Dynamic Program.

---

```c++
void calculate();
```

Calculates the value function for all stage/state combinations.

---

```c++
unsigned int action_matrix(const stage &t,
                           const unsigned int &state) const;
```
Getter: returns the action taken for a given stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const unsigned int &state` is a state of the Dynamic Program.

---

## protected

```c++
double v(const stage &t, const unsigned int &state) const;
```
Getter: returns the unmodified value function for a stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const unsigned int &state` is a state of the Dynamic Program.

---


# The Problem

A building contractor requires workers for the next 10 weeks.
 He hires workers from an agency at a cost of £100 + £50 per worker (per
 hire, not per week).
 Each worker is paid £200 per week.
 Firing has no cost as, in effect, the contractor says up front how many
 weeks each worker will be employed for.
 The number of workers per week is defined as:

| Week | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Workers | 5 | 7 | 5 | 9 | 8 | 9 | 7 | 7 | 5 | 5 | 

# Parameters

A Dynamic Programming (DP) model of this problem will break the problem down so that each of the weeks is a *stage* of the problem. More specifically each stage is the start of the corresponding week. The end stage is at the work period. Presumably all workers are *"fired"* at the end so this stage will incur no cost regardless of how many workers remain at the end.

A decision will be made on the basis of future demand and the current number of workers employed. The latter is the *state* of the model. The decision is how many workers to hire or fire. Some states will be invalid as we cannot have fewer workers than required. This will restrict the available actions.

The required number of workers in week *t* is $`r_{t}`$. So $`S^{t}\geq r_{t}`$

Decision: 

• to hire $`n`$ workers costs $`100+50n`$
• to fire $`n`$ workers costs $`0`$

If we have $`n`$ workers at the start of a week then there is a cost of $`200n`$ in wages. 

The decision costs can then be redefined as:

• in state $`s`$ to hire $`n`$ workers costs $`200(s+n)+100+50n=200s+100+250n`$
• in state $`s`$ to fire $`n`$ workers costs $`200(s-n)`$

We will also never hire workers if $`s\geq r_{t}`$ as the hiring decision can always be delayed to the next time period. The cost of hiring workers may mean we do not fire workers in this situation.

# Model

The mathematical model is defined as follows:

```math
\begin{array}{ll}
v^{t}(i)= & \begin{cases}
    \min_{h\,:\,i+h\geq r_{t}}\left\{ 200i+100+250h+v^{t-1}(i+h)\right\} , &i<r_{t} \\
    \min_{f\,:\,i-f\geq r_{t}}\left\{ 200(i-f)+v^{t-1}(i-f)\right\} , &otherwise
\end{cases} \\
\\
v^{0}(i)= & 0
\end{array}
```

# An Example Problem

The following is some example code that will solve a problem and display the *action matrix*.

File: `example_staff_management_problem.cpp`

```c++
#include <iostream>
#include <staff_management_problem.h>

int main() {
  int hire_cost_fixed = 100;
  int hire_cost_variable = 100;
  int wage_cost = 200;
  StaffManagementProblem problem(10, 9);
  problem.use_action_matrix();
  problem.set_requirements({5, 7, 5, 9, 8, 9, 7, 7, 5, 5});
  problem.set_hire_cost(hire_cost_fixed, hire_cost_variable);
  problem.set_wage_cost(wage_cost);
  problem.calculate();

  std::cout << "Optimal Policy\n";
  auto workers = 0u;
  for (auto t = 10u; t > 0; --t) {
    int action = problem.action_matrix(stage(t), workers);
    if (action < 0)
      std::cout << "Week " << 11 - t << " fire " << -action << " workers\n";
    else
      std::cout << "Week " << 11 - t << " hire " << action << " workers\n";
    workers = (unsigned int)((int)workers + action);
  }

  exit(0);
}
```

The output gives the optimal policy as:

```
Optimal Policy
Week 1 hire 5 workers
Week 2 hire 2 workers
Week 3 fire 2 workers
Week 4 hire 4 workers
Week 5 hire 0 workers
Week 6 hire 0 workers
Week 7 fire 2 workers
Week 8 hire 0 workers
Week 9 fire 2 workers
Week 10 hire 0 workers
```
