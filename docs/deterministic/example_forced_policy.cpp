#include <iostream>
#include <maintenance_and_replacement_problem.h>
#include <vector>

class ForcedPolicy : public MaintenanceAndReplacementProblem {
public:
  using MaintenanceAndReplacementProblem::MaintenanceAndReplacementProblem;

private:
  std::vector<Action<unsigned int>>
  generate_actions(const stage &t, const unsigned int &state) const override {
    if (state < 4)
      return {make_maintenance_action(state)};
    else
      return {make_replacement_action(state)};
  }
};

int main() {
  const auto oldest_state = 7u;
  const auto no_stages = 100u;

  MaintenanceAndReplacementParameters params;
  params.salvage_value = {50, 40, 30, 20, 10, 0, 0, 0};
  params.trade_in_value = {0, 30, 15, 5, 0, 0, 0};
  params.operating_cost = {5, 7, 11, 22, 35, 50, 75};
  params.purchase_cost = 60;

  MaintenanceAndReplacementProblem problem(no_stages, oldest_state, params);
  problem.use_action_matrix();

  problem.calculate();

  std::cout << "Optimal solution: average cost per stage is "
            << problem.value_function(stage(no_stages), 1) / double(no_stages)
            << "\n";

  for (auto state = oldest_state; state > 0; state--) {
    for (auto t = 20u; t > 0u; --t) {
      std::cout << problem.action_matrix(stage(t), state) << " ";
    }
    std::cout << "\n";
  }

  ForcedPolicy problem_forced_policy(no_stages, oldest_state, params);
  problem_forced_policy.use_action_matrix();

  problem_forced_policy.calculate();

  std::cout << "Forced policy: average cost per stage is "
            << problem_forced_policy.value_function(stage(no_stages), 1) /
                   double(no_stages)
            << "\n";

  for (auto state = oldest_state; state > 0; state--) {
    for (auto t = 20u; t > 0u; --t) {
      std::cout << problem_forced_policy.action_matrix(stage(t), state) << " ";
    }
    std::cout << "\n";
  }
  exit(0);
}