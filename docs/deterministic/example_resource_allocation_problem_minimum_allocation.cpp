#include <resource_allocation_problem.h>
#include <vector>

class ResourceAllocationProblemMinimumAllocation
    : public ResourceAllocationProblem {
public:
  using ResourceAllocationProblem::ResourceAllocationProblem;

protected:
  std::vector<Action<unsigned int>>
  generate_actions(const stage &t, const unsigned int &state) const override {
    std::vector<Action<unsigned int>> actions;
    for (auto allocation = 1u;
         allocation <= state && allocation < reward_matrix()[t - 1].size();
         ++allocation) {
      Action<unsigned int> action = make_allocation(state, allocation, t);
      if (valid(stage(t - 1), action.subsequent_state[0].state))
        actions.push_back(action);
    }
    return actions;
  }
};

int main() {
  const auto no_activities = 4u;
  const auto total_resource = 7u;
  const std::vector<std::vector<double>> reward_matrix = {
      {0, 5, 10, 12, 12, 12},  // Activity 1
      {0, 10, 18, 28, 34, 36}, // Activity 2
      {0, 14, 18, 22, 26, 28}, // Activity 3
      {0, 7, 15, 18, 22, 24}}; // Activity 4

  ResourceAllocationProblem problem(total_resource, no_activities,
                                    reward_matrix);
  problem.use_action_matrix();
  problem.calculate();

  std::cout << "Problem with no minimum allocation\n";
  std::cout << "Total reward is " << problem.value_function(stage(4), 7)
            << "\n";
  std::cout << "Allocations are:\n";
  auto remaining_resource = 7u;
  for (auto t = 4u; t > 0; --t) {
    std::cout << "  Activity " << t << " "
              << problem.action_matrix(stage(t), remaining_resource) << "\n";
    remaining_resource -=
        (unsigned int)problem.action_matrix(stage(t), remaining_resource);
  }

  ResourceAllocationProblemMinimumAllocation modified_problem(
      total_resource, no_activities, reward_matrix);
  modified_problem.use_action_matrix();
  modified_problem.calculate();

  std::cout << "Problem with minimum allocation\n";
  std::cout << "Total reward is "
            << modified_problem.value_function(stage(4), 7) << "\n";
  std::cout << "Allocations are:\n";
  remaining_resource = 7u;
  for (auto t = 4u; t > 0; --t) {
    std::cout << "  Activity " << t << " "
              << modified_problem.action_matrix(stage(t), remaining_resource)
              << "\n";
    remaining_resource -= (unsigned int)modified_problem.action_matrix(
        stage(t), remaining_resource);
  }

  exit(0);
}
