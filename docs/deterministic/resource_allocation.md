- [Resource Allocation Problems](#resource-allocation-problems)
- [The Resource Allocation Problem Class API](#the-resource-allocation-problem-class-api)
  - [protected](#protected)
- [The Problem](#the-problem)
- [Parameters](#parameters)
- [Model](#model)
- [An Example Problem](#an-example-problem)
- [Implementation Details](#implementation-details)

# Resource Allocation Problems

# The Resource Allocation Problem Class API

The file `resource_allocation_problem.h` defines the class `class ResourceAllocationProblem`.

---

```c++
ResourceAllocationProblem(const unsigned int &total_resource,
                          const unsigned int &no_activities,
                          const std::vector<std::vector<double>> &reward_matrix);
```
Class constructor.
* `const unsigned int &total_resource` is the total resoruce to be assigned to activities.
* `const unsigned int &no_activities` is the number of activities resources are assogned to.
* `const std::vector<std::vector<double>> &reward_matrix` gives the reward from assignment. `reward_matrix[a][r]` is the reward from assigning `r` resources to activity `a`.

---

```c++
void use_action_matrix();
```
The Dynamic Program will store the action taken for each stage/state combination. The action is defined as an `int` in the `generate_actions` function.

---

```c++
void calculate();
```
Calculates the value function for all stage/state combinations.

---

```c++
double value_function(const stage &t, const unsigned int &state) const
```
Getter: returns the value function for a stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const unsigned int &state` is a state of the Dynamic Program.

---


```c++
unsigned int action_matrix(const stage &t,
                           const unsigned int &state) const;
```
Getter: returns the action taken for a given stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const unsigned int &state` is a state of the Dynamic Program.

---

## protected

```c++
double v(const stage &t, const unsigned int &state) const;
```
Getter: returns the unmodifed value function for a stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const unsigned int &state` is a state of the Dynamic Program.

---


# The Problem

Edinburgh City Council have acquired several new gritting vehicles to grit the roads in snowy/icy weather. They now have 7 such machines. They are required to grit roads in 4 regions of the city: the City Center, East Edinburgh, West Edinburgh and South Edinburgh. As the number of of vehicles assigned to each area increases the number of journeys improved also increases. The table below shows this relationship for each of the regions (measured in 000's of journeys).

| Area / Vehicles | 0 | 1 | 2 | 3 | 4 | 5 |
| --- | --- | --- | --- | --- | --- | ---|
| City Center | 0 | 5 | 10 | 12 | 12 | 12 |
| South | 0 | 10 | 18 | 28 | 34 | 36 |
| West | 0 | 14 | 18 | 22 | 26 | 28 |
| East | 0 | 7 | 15 | 18 | 22 | 24 |

# Parameters

Dynamic Programming (DP) is not the only way of approaching this problem. To model the problem as a DP the problem is broken down into a sequence of decisions. Each decision corresponds to an area of the city. The order of the decisions is arbitrary. Any order will result in the same solution provided the order remains constant during the calculations. In practice all decisions are made at once.

The state of the DP model is the number of decisions remaining. The state is the amount of resource, in this case gritting vehicles, remaining. The deicision is how much resource to assign to the current decision area.

This is a problem that is commonly presented in textbooks and it is a useful example for learning about DP. However, the problem as given does not take advantage of DP. Even a deterministic DP model can take advantage of uncertainty of state transitions when implementing a solution. In this example that would mean assigning a number of vehicles only to discover more/less vehicles than expected remained after the decision. This is unrealistic given in practice all decisions are made at once.

Mathematically the only problem specific function is a reward function *r(t,a)* which gives the reward for assigning *a* resources to activity *t*. 

# Model

The mathematical model is defined as follows:

```math
v^{t}(i)=\max_{a\leq i}\left\{r(t,a)+v^{t-1}(i-a)\right\} 
```

```math
v^{0}(i)=0
```

# An Example Problem

The following is some example code that will solve the problem above and print the solution.

File: `example_resource_allocation_problem.cpp`

```c++
#include <resource_allocation_problem.h>
#include <vector>

int main() {
  const auto no_activities = 4u;
  const auto total_resource = 7u;
  const std::vector<std::vector<double>> reward_matrix = {
      {0, 5, 10, 12, 12, 12},  // Activity 1
      {0, 10, 18, 28, 34, 36}, // Activity 2
      {0, 14, 18, 22, 26, 28}, // Activity 3
      {0, 7, 15, 18, 22, 24}}; // Activity 4

  ResourceAllocationProblem problem(total_resource, no_activities,
                                    reward_matrix);
  problem.use_action_matrix();
  problem.calculate();

  std::cout << "Total reward is " << problem.value_function(stage(4), 7) << "\n";
  std::cout << "Allocations are:\n";
  auto remaining_resource = 7u;
  for (auto t = 4u; t > 0; --t) {
    std::cout << "  Activity " << t << " "
              << problem.action_matrix(stage(t), remaining_resource) << "\n";
    remaining_resource -= (unsigned int)problem.action_matrix(stage(t), remaining_resource);
  }

  exit(0);
}
```

The output of this program is as follows:

```
Total reward is 63
Allocations are:
  Activity 4 2
  Activity 3 1
  Activity 2 4
  Activity 1 0
```

# Implementation Details

The key part of the implementation is the `generate_actions` function. It considers all possible allocations and generates actions for them.

```c++
std::vector<Action<unsigned int>>
ResourceAllocationProblem::generate_actions(const stage &t,
                                            const unsigned int &state) {
  std::vector<Action<unsigned int>> actions;
  for (auto allocation = 0u;
       allocation <= state && allocation < reward_matrix[t - 1].size();
       ++allocation) {
    actions.push_back(make_allocation(state, allocation, t));
  }
  return actions;
}
```

`make_allocation` is a factory funcation that creates each action. It is defined as:

```c++
Action<unsigned int>
ResourceAllocationProblem::make_allocation(const unsigned int &state,
                                           const unsigned int &allocation,
                                           const stage &t) const {
  return {-reward_matrix[t - 1][allocation],
          {{state - allocation, 1.0}},
          (int)allocation};
}
```

Note: the value from `reward_matrix` is negative. This is because actions have a cost which is the opposite of a reward. Creating a `ResourceAllocationProblem` allows this detail to be hidden.