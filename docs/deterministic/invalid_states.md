# Invlaid States

A state is **unreachable** if there is no sequence of decisions/actions from the starting state(s) that reach the state.
A state is **invalid** if there are no feasible decisions for the state.

Example One: 
* A maintenance and replacement problem with 3 machines which are 4 years old. Over the next 10 years when should we replace them given only one machine can be replaced at any one time.
* unreachable states: the starting state is {4,4,4} so the next state cannot be {6,6,6}.
* invalid states: if the oldest state is 9 then we cannot reach state {9,9,9} as the subsequent states would be {10,10,10}, {1,10,10}, {10,1,10} and {10,10,1} which all violate the oldest state.

Example Two:
* A resource allocation problem where 7 resources are assigned to 4 activities. Each activity must have at least one resource allocated to it.
* The ending state with all 7 resources unassigned is unreachable.
* In the second stage/decision the state where only one resource remains is invalid. There is no way to satisfy the requirement that each activity receives at least one resource.

Ideally unreachable and invalid states should be eliminated but this may not always be practical. For unreachable states this shouldn't cause problems other than superfluous computation. For invalid states there is a danger.

# Multiple Machine Maintenance and Replacement Problem

The original problem is defined in [Multi Machine Maintenance and Replacement](deterministic_multi_maintenance_and_replacement.md).

The model is defined as:

```math
v^{t}(\mathbf{i})=\min_{\forall \mathbf{a}\in A}\left\{P(|\mathbf{a}|)-\sum_{\forall j\in \mathbf{i}\odot\mathbf{a}\ :\ j>0}t(j)+\sum_{\forall j\in \mathbf{i^a}}c(j)+v^{t-1}(\mathbf{i+^a})\right\}
``` (1)

```math
v^{0}(\mathbf{i})=-\sum_{\forall j\in i}s(j)
```

$`\mathbf{a}`$ is a boolean vector where $`0`$ represents maintenance and $`1`$ represents replacement and $`A`$ is the set of all possible actions.
 
 $`|\mathbf{a}|`$ is the number of $`1`$'s in the vector.

$`\mathbf{i}\odot\mathbf{a}=\left\{i_1.a_1,\,...,\,i_n.a_n\right\}`$ 

$`\mathbf{i^a}=\left\{i^a_1,\,...\,i^a_n\right\}`$ where $`i_j^a=\begin{cases}i_j&\text{if}\ a_j=0,\\0&\text{otherwise}\end{cases}`$

$`\mathbf{i+^a}=\left\{i+^a_1,\,...\,i+^a_n\right\}`$ where $`i+_j^a=\begin{cases}i_j+1&\text{if}\ a_j=0,\\1&\text{otherwise}\end{cases}`$

To modify the the model so that a maximum of $`m`$ machines can be replaced at any one time (1) is changed to limit the possible values of $`\mathbf{a}`$ as follows:

```math
v^{t}(\mathbf{i})=\min_{\forall \mathbf{a}\in A\,:\,|\mathbf{a}|\leq m}\left\{P(|\mathbf{a}|)-\sum_{\forall j\in\mathbf{i}\odot\mathbf{a}\ :\ j>0}t(j)+\sum_{\forall j\in\mathbf{i^a}}c(j)+v^{t-1}(\mathbf{i+^a})\right\}
```

This modified model will result in *invalid states* when it is implemented.

## Implementation Details

To prevent the inclusion of *invalid states* a check must be made before adding an action to the action set. The subsequent state resulting from the action must meet two conditions.

First, it must be within the states considered by the problem (i.e. the machine cannot be older than the oldest age).

Second, if a state has no possible action resulting in a valid state then it too is invalid. The DP library will assign `std::numeric_limits<double>::max()` to the value function. Therefore the subsequent state from an action must be less than `std::numeric_limits<double>::max()` to be added to the action set.

The `DeterministicDynamicProgram` class contains a function `valid(...)` that tests for both of these cases. It syntax is as follows:

```c++
bool valid(const stage &t, const State &state) const
```

The following parts of the `generate_actions` function show how this is implemented. The `choices` variable loops through all possible maintenance/replacement combinations. Note that the `no_purchases < purchase_cost.size()` conditional tests for a valid action not a valid subsequent state. In other words this conditional implements the changes to the mathematical model mentioned above.

```c++
std::vector<Action<std::vector<unsigned int>>>
MaintenanceAndReplacementProblemMulti::generate_actions(
    const stage &t, const std::vector<unsigned int> &state) {

  ...

  for (const auto &choice : choices) {
    unsigned int no_purchases = ...
    if (no_purchases < purchase_cost.size()) {

      // calculate the action cost and subsequent state of the action
      ...
      
      if (valid(stage(t - 1), subsequent_state))
        action_set.push_back(action);
    }
  }

  return action_set;
}
```

# Resource Allocation Problem

The original problem is defined in [Resource Allaction Problem](resource_allocation.md).

The model is defined as:

```math
v^{t}(i)=\max_{a\leq i}\left\{r(t,a)+v^{t-1}(i-a)\right\}
```

```math
v^{0}(i)=0
```

To modify the model so that at least one resource is allocated to each activity the general case ($`t>0`$) changes slightly to:

```math
v^{t}(i)=\max_{0<a\leq i}\left\{r(t,a)+v^{t-1}(i-a)\right\}
```


## Implementation details

The `generate_actions` function must be modified to not generate an action that allocates 0 resource. The check for valid subsequent state is similar to the previous example.

```c++
std::vector<Action<unsigned int>>
generate_actions(const stage &t, const unsigned int &state) override {
  std::vector<Action<unsigned int>> actions;
  for (auto allocation = 1u;
       allocation <= state && allocation < reward_matrix[t - 1].size();
       ++allocation) {
    Action<unsigned int> action = make_allocation(state, allocation, t);
    if (valid(stage(t-1), action.subsequent_state[0].state))
      actions.push_back(action);
  }
  return actions;
}
```

Note that the subsequent state of each action is generated in the factory function `make_allocation`. The `action.subsequent_state[0].state` argument of the `dp.valid` function could be replaced with a direct calculation (i.e. `state-allocation`) but this would repeat that code within the codebase.

The following code compares the two models and shows the effect of the minimum requirement.

File: `example_resource_allocation_problem_minimum_allocation.cpp`

```c++
#include <resource_allocation_problem.h>
#include <vector>

class ResourceAllocationProblemMinimumAllocation
    : public ResourceAllocationProblem {
public:
  using ResourceAllocationProblem::ResourceAllocationProblem;

protected:
  std::vector<Action<unsigned int>>
  generate_actions(const stage &t, const unsigned int &state) const override {
    std::vector<Action<unsigned int>> actions;
    for (auto allocation = 1u;
         allocation <= state && allocation < reward_matrix[t - 1].size();
         ++allocation) {
      Action<unsigned int> action = make_allocation(state, allocation, t);
      if (valid(stage(t - 1), action.subsequent_state[0].state))
        actions.push_back(action);
    }
    return actions;
  }
};

int main() {
  const auto no_activities = 4u;
  const auto total_resource = 7u;
  const std::vector<std::vector<double>> reward_matrix = {
      {0, 5, 10, 12, 12, 12},  // Activity 1
      {0, 10, 18, 28, 34, 36}, // Activity 2
      {0, 14, 18, 22, 26, 28}, // Activity 3
      {0, 7, 15, 18, 22, 24}}; // Activity 4

  ResourceAllocationProblem problem(total_resource, no_activities,
                                    reward_matrix);
  problem.use_action_matrix();
  problem.calculate();

  std::cout << "Problem with no minimum allocation\n";
  std::cout << "Total reward is " << problem.value_function(stage(4), 7)
            << "\n";
  std::cout << "Allocations are:\n";
  auto remaining_resource = 7u;
  for (auto t = 4u; t > 0; --t) {
    std::cout << "  Activity " << t << " "
              << problem.action_matrix(stage(t), remaining_resource) << "\n";
    remaining_resource -=
        (unsigned int)problem.action_matrix(stage(t), remaining_resource);
  }

  ResourceAllocationProblemMinimumAllocation modified_problem(
      total_resource, no_activities, reward_matrix);
  modified_problem.use_action_matrix();
  modified_problem.calculate();

  std::cout << "Problem with minimum allocation\n";
  std::cout << "Total reward is "
            << modified_problem.value_function(stage(4), 7) << "\n";
  std::cout << "Allocations are:\n";
  remaining_resource = 7u;
  for (auto t = 4u; t > 0; --t) {
    std::cout << "  Activity " << t << " "
              << modified_problem.action_matrix(stage(t), remaining_resource)
              << "\n";
    remaining_resource -= (unsigned int)modified_problem.action_matrix(
        stage(t), remaining_resource);
  }

  exit(0);
}
```

The output is a follows:

```
Problem with no minimum allocation
Total reward is 63
Allocations are:
  Activity 4 2
  Activity 3 1
  Activity 2 4
  Activity 1 0
Problem with minimum allocation
Total reward is 62
Allocations are:
  Activity 4 2
  Activity 3 1
  Activity 2 3
  Activity 1 1
```


# Summary

These examples show how `DeterministicDynamicProgram::valid(...)` can be used to avoid calculations using invalid states. This is particularly useful when these states propogate across the problem stages. This function could also be used in an `assert(...)`  to check for programming errors.