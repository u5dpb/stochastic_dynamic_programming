#include <iostream>
#include <maintenance_and_replacement_problem.h>
#include <vector>

int main() {
  const auto oldest_state = 7u;
  const auto no_stages = 100u;

  MaintenanceAndReplacementParameters params;
  params.salvage_value = {50, 40, 30, 20, 10, 0, 0, 0};
  params.trade_in_value = {0, 30, 15, 5, 0, 0, 0};
  params.operating_cost = {5, 7, 11, 22, 35, 50, 75};
  params.purchase_cost = 60;

  MaintenanceAndReplacementProblem problem(no_stages, oldest_state, params);

  problem.use_action_matrix();

  problem.calculate();

  for (auto state = oldest_state; state > 0; state--) {
    for (auto t = 20u; t > 0u; --t) {
      std::cout << problem.action_matrix(stage(t), state) << " ";
    }
    std::cout << "\n";
  }

  exit(0);
}