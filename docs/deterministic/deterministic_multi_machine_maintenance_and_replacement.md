# Deterministic Multi-Machine Maintenance and Replacement Problems

# Maintenance and Replacement Problem Multi Parameter struct

The following `struct` is used to pass the parameters to the `MaintenanceAndReplacementProblemMulti` constructor.

```c++
struct MaintenanceAndReplacementMultiParameters {
  std::vector<double> purchase_cost;
  std::vector<double> salvage_value;
  std::vector<double> trade_in_value;
  std::vector<double> operating_cost;
};
```

# The Multi-Machine Maintenance and Replacement Problem Class API

The file `maintenance_and_replacement_problem_multi.h` defines the class `class MaintenanceAndReplacementProblemMulti`.

```c++
MaintenanceAndReplacementProblemMulti(const unsigned int &T,
                                      const unsigned int &oldest_state,
                                      const unsigned int &no_machines);
```
Class Contructor.
* `const unsigned int &T` is the number of stages in the Dynamic Program.
* `const unsigned int &oldest_state` is the oldest state before a machine is forceably replaced.
* `const unsigned int &no_machines` is the number of machines in the problem.

---

```c++
MaintenanceAndReplacementProblemMulti(
      const unsigned int &T, const unsigned int &oldest_state,
      const unsigned int &no_machines,
      const MaintenanceAndReplacementMultiParameters &params);
```
Class Contructor.
* `const unsigned int &T` is the number of stages in the Dynamic Program.
* `const unsigned int &oldest_state` is the oldest state before a machine is forceably replaced.
* `const unsigned int &no_machines` is the number of machines in the problem.
* `const MaintenanceAndReplacementMultiParameters &params` are the parameters of the problem.

---

```c++
void use_action_matrix();
```

The Dynamic Program will store the action taken for each stage/state combination. The action is defined as an `unsigned int` in the `generate_actions` function.

---

```c++
void use_action_matrix(const size_t size);
```
As above but only stores the actions for the first `size` stages of the Dynamic Program.
* `const unsigned int size` is the number of stages to store actions for.

---

```c++
void set_salvage_value(const std::vector<double> &salvage_value);
```
Setter.
* `const std::vector<double> &salvage_value` is a vector that gives the salvage value. *s(i)* in the problem description below.

---

```c++
void set_purchase_cost(const std::vector<double> &purchase_cost);
```
Setter
* `const double &purchase_cost` is the purchase cost of a new machine. *P* in the problem description below.

---

```c++
void set_trade_in_value(const std::vector<double> &trade_in_value);
```
Setter
* `const std::vector<double> &trade_in_value` is a vector that gives the trade in value for a used machine. *t(i)* in the problem description below.

---

```c++
void set_operating_cost(const std::vector<double> &operating_cost);
```
Setter
* `const std::vector<double> &operating_cost` is a vector that gives the cost of operation of a machine. *c(i)*  in the problem description below.

---


```c++
double value_function(const stage &t, const std::vector<unsigned int> &state) const;
```
Getter: returns the value function for a stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const unsigned int &state` is a state of the Dynamic Program.

---

```c++
unsigned int action_matrix(const stage &t,
                           const std::vector<unsigned int> &state) const;
```
Getter: returns the action taken for a given stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const unsigned int &state` is a state of the Dynamic Program.

---

```c++
void calculate();
```
Calculates the value function for all stage/state combinations.

## protected


```c++
double v(const stage &t, const std::vector<unsigned int> &state) const;
```
Getter: returns the unmodified value function for a stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const unsigned int &state` is a state of the Dynamic Program.

---



# The Problem

The problem is the same as the Maintenance and Replacement Problem. However, that problem only considers a single machine. In the multi-machine case there may be discounts for purchases of multiple new machines. there may also be restrictions on how many machines a particular action can be performed on.

A company uses a specific piece of heavy machinary. At a regular interval, for example a year, a decision must be made about whether to perform maintenance on each of the machines or whether to trade in a machine for a new one. Older machines are more costly to maintain and run, but new machines cost money. This problem is about balancing these two factors.

As this is a deterministic problem there is no uncertainty about wear and tear or possible breakdowns. All costs are averages of the expected costs.

A machine may be used more or less over a fixed time period. It may be necessary to model run-time rather than calendar time.

# Parameters

A Dynamic Programming (DP) model for this problem will break the time period into a finite number of stages. The stage defines the time remaining. An end stage must be defined. This will represent the *assest value* of the machine given its age. This is also called the *salvage value* of the machine. It will, most likely, be an estimate. *s(i)* is the salvage value of an *i* year old machine. There are *n* machines. The state of the DP model will be a vector $`\mathbf{i}=\left\{i_{1},\,...,\,i_{n}\right\}`$ representing the age of each machine

Purchasing *n* new machines has a purchase price *P(n)*. When replacing a new machine a *trade in value* will be deducted from the purchase price. This defined as *t(i)* for an *i* year old machine.

Finally there will be the cost of operating a machine until the next stage or decision point. This will include any maintenance costs and expected breakdown costs. This cost is *c(i)* for an *i* year old machine.

# Model

The model is defined as:

```math
v^{t}(\mathbf{i})=\min_{\forall\mathbf{a}\in A}\left\{P(|\mathbf{a}|)-\sum_{\forall j\in\mathbf{i}\odot\\mathbf{a}\ :\ j>0}t(j)+\sum_{\forall j\in \mathbf{i^a}}c(j)+v^{t-1}(\mathbf{i+^a})\right\}
```

```math
v^{0}(\mathbf{i})=-\sum_{\forall j\in i}s(j))
```

 $`\mathbf{a}`$ is a boolean vector where $`0`$ represents maintenance and $`1`$ represents replacement and $`A`$ is the set of all possible actions.
 
 $`|\mathbf{a}|`$ is the number of $`1`$'s in the vector.

$`\mathbf{i}\odot\mathbf{a}=\left\{i_1.a_1,\,...,\,i_n.a_n\right\}`$ 

$`\mathbf{i^a}=\left\{i^a_1,\,...\,i^a_n\right\}`$ where $`i_j^a=\begin{cases}i_j&\text{if}\ a_j=0,\\0&\text{otherwise}\end{cases}`$

$`\mathbf{i+^a}=\left\{i+^a_1,\,...\,i+^a_n\right\}`$ where $`i+_j^a=\begin{cases}i_j+1&\text{if}\ a_j=0,\\1&\text{otherwise}\end{cases}`$

# An Example Problem

The following program will solve a problem and print the policy information.

```c++
#include <iostream>
#include <maintenance_and_replacement_problem_multi.h>
#include <vector>

void print_action_matrix(const MaintenanceAndReplacementProblemMulti &problem,
                         const unsigned int &t,
                         const unsigned int &oldest_state);

void print_policy(std::vector<unsigned int> ages,
                  const MaintenanceAndReplacementProblemMulti &problem,
                  const unsigned int &no_stages);

int main() {
  const auto oldest_state = 5u;
  const auto no_stages = 100u;
  const auto no_machines = 2u;

 MaintenanceAndReplacementMultiParameters params;

  params.salvage_value = {50, 33, 22, 15, 10, 7};
  params.trade_in_value = {0, 30, 20, 13, 9, 6};
  params.operating_cost = {5, 7, 10, 20, 32, 45, 70};
  params.purchase_cost = {0, 60, 110};

  MaintenanceAndReplacementProblemMulti problem(no_stages, oldest_state,
                                                no_machines, params);
  problem.use_action_matrix();

  problem.calculate();

  for (auto t = no_stages; t > no_stages - 10; --t) {
    std::cout << "t = " << t << "\n";
    print_action_matrix(problem, t, oldest_state);
  }

  print_policy({1, 1}, problem, no_stages);
  print_policy({1, 3}, problem, no_stages);

  exit(0);
}

void print_action_matrix(const MaintenanceAndReplacementProblemMulti &problem,
                         const unsigned int &t,
                         const unsigned int &oldest_state) {

  std::cout << "    ";
  for (auto machine_2_age = 1u; machine_2_age <= oldest_state;
       ++machine_2_age) {
    std::cout << machine_2_age << " ";
  }
  std::cout << "\n";
  for (auto machine_1_age = 1u; machine_1_age <= oldest_state;
       ++machine_1_age) {
    std::cout << machine_1_age << " : ";
    for (auto machine_2_age = 1u; machine_2_age <= oldest_state;
         ++machine_2_age) {
      std::cout << problem.action_matrix(stage(t), {machine_1_age, machine_2_age})
                << " ";
    }
    std::cout << "\n";
  }
  std::cout << "\n";
}

void print_policy(std::vector<unsigned int> ages,
                  const MaintenanceAndReplacementProblemMulti &problem,
                  const unsigned int &no_stages) {
  std::cout << "Policy {" << ages[0] << "," << ages[1] << "}\n";
  for (auto t = no_stages; t > no_stages - 10; --t) {
    std::cout << "  t = " << t << " {" << ages[0] << "," << ages[1]
              << "}=" << problem.action_matrix(stage(t), ages) << "\n";
    if (problem.action_matrix(stage(t), ages) == 0) {
      ++ages[0];
      ++ages[1];
    } else if (problem.action_matrix(stage(t), ages) == 1) {
      if (ages[0] > ages[1]) {
        ages[0] = 1;
        ++ages[1];
      } else {
        ages[1] = 1;
        ++ages[0];
      }
    } else if (problem.action_matrix(stage(t), ages) == 2) {
      ages[0] = 1;
      ages[1] = 1;
    }
  }
}
```

The output of the program shows the action matrix for the first 10 stages or decision points. The matrix shows how many of the 2 machines are replaced.

```
t = 100
    1 2 3 4 5 
1 : 0 0 0 1 1 
2 : 0 0 0 2 2 
3 : 0 0 2 2 2 
4 : 1 2 2 2 2 
5 : 1 2 2 2 2 

t = 99
    1 2 3 4 5 
1 : 0 0 1 1 1 
2 : 0 0 2 2 2 
3 : 1 2 2 2 2 
4 : 1 2 2 2 2 
5 : 1 2 2 2 2 

t = 98
    1 2 3 4 5 
1 : 0 0 1 1 1 
2 : 0 0 0 2 2 
3 : 1 0 2 2 2 
4 : 1 2 2 2 2 
5 : 1 2 2 2 2 
```

Some strange patterns can be seen in this output. For example, when *t=98* and one machine is 3 years old and the other is 1 years old. In this case 1 machine is replaces (presumably the 3 year old machine). However, if the second machine is 2 years old (i.e. one year older) neither machine is replaced.

Examining the sequence of policies for two 1 year old machines the output shows:

```
Policy {1,1}
  t = 100 {1,1}=0
  t = 99 {2,2}=0
  t = 98 {3,3}=2
  t = 97 {1,1}=0
  t = 96 {2,2}=0
  t = 95 {3,3}=2
  t = 94 {1,1}=0
  t = 93 {2,2}=0
  t = 92 {3,3}=2
  t = 91 {1,1}=0
```
At *t=100* no machines are replaced and the machines get 1 year older. The optimal policy is to wait until both machines are 3 years old and then replace both.

Examining the sequence of policies for machines of different ages shows a slightly different pattern:

```
Policy {1,3}
  t = 100 {1,3}=0
  t = 99 {2,4}=2
  t = 98 {1,1}=0
  t = 97 {2,2}=0
  t = 96 {3,3}=2
  t = 95 {1,1}=0
  t = 94 {2,2}=0
  t = 93 {3,3}=2
  t = 92 {1,1}=0
  t = 91 {2,2}=0
```

Again the optimal policy is to wait until both machines are 3 years old and then replace both. However, a different policy is followed until both machines are the same age. This is why strange cycles appear in the earlier action matrix output.

# Implementation Details

The key to implementation is the `generate_actions` function. This function overrides the abstract function in the `DeterministicDynamicProgram` class. This is a protected function that is used in the `DeterministicDynamicProgram::calculate` function.

```c++
std::vector<Action<std::vector<unsigned int>>>
MaintenanceAndReplacementProblemMulti::generate_actions(
    const stage &t, const std::vector<unsigned int> &state) {

  std::vector<Action<std::vector<unsigned int>>> action_set;
  
  StateLoop<std::vector<unsigned int>> choices(
      std::vector<unsigned int>(state.size(), 0),
      std::vector<unsigned int>(state.size(), 1));
  // choices[i] = 0 if machine i is replaced
  //            = 1 if machine i is kept
  // Note: This may be different elsewhere in the code
  //       but this notation makes some of the calculations
  //       easier
  for (const auto &choice : choices)
  {
    unsigned int no_purchases =
        no_machines_ -
        std::accumulate(choice.begin(), choice.end(), 0u);
    if (no_purchases < purchase_cost.size()) {
      Action<std::vector<unsigned int>> action;
      action.cost = 0;
      std::vector<unsigned int> subsequent_state = state;

      for (auto machine = 0u; machine < no_machines_; ++machine) {
        if (choice[machine] == 1) {
          action.cost += operating_cost[state[machine]];
          subsequent_state[machine]++;
        } else {
          action.cost += -trade_in_value[state[machine]] + operating_cost[0];
          subsequent_state[machine] = 1;
        }
      }
      action.cost += purchase_cost[no_purchases];
      action.subsequent_state = {{subsequent_state, 1.0}};
      action.type = (int)no_purchases;
      if (valid(stage(t - 1), subsequent_state))
        action_set.push_back(action);
    }
  }

  return action_set;
}

```
