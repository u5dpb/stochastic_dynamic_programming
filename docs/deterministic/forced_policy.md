# Using a Forced Policy

Finding the optimal solution to a problem is useful. However, a model is only an approximation of the real world. It is useful to make a comparison between the optimal solution and a policy that is forced on the system. This *forced policy* might be:
* an adaptation of the optimal policy that considers real world concerns that are difficult to model.
* current practice.
* benchmark practice.
* a policy derived from an approximation method.
* a heuristic policy.

To implement such a forced policy the `generate_actions` function will return a single action. The `calculate()` function will then choose the optimal action but will only have one to choose from.

# Example Problem

Using the `MaintenanceAndReplacementProblem` class from [Deterministic Maintenance and Replacement](deterministic_maintenance_and_replacement.md) a child class is created that overloads the `generate_action` function.

```c++
class ForcedPolicy : public MaintenanceAndReplacementProblem {
public:
  using MaintenanceAndReplacementProblem::MaintenanceAndReplacementProblem;

private:
  std::vector<Action<unsigned int>>
  generate_actions(const stage &t, const unsigned int &state) override {
    if (state < 4)
      return {make_maintenance_action(state)};
    else
      return {make_replacement_action(state)};
  }
};
```

In this case the `generate_actions` function forces maintenance if the state/age of the machine is less than 4; otherwise, it forces replacement.

# Calculating the Policy Cost

The problem can then be created in the same way as the parent problem. When `calculate` is called the value function will be calculated for the foreced policy.

```c++
int main() {
  const auto oldest_state = 7u;
  const auto no_stages = 100u;

  MaintenanceAndReplacementParameters params;
  params.salvage_value = {50, 40, 30, 20, 10, 0, 0, 0};
  params.trade_in_value = {0, 30, 15, 5, 0, 0, 0};
  params.operating_cost = {5, 7, 11, 22, 35, 50, 75};
  params.purchase_cost = 60;

  MaintenanceAndReplacementProblem problem(no_stages, oldest_state,params);

  problem_forced_policy.calculate();

  std::cout << "Forced policy: average cost per stage is "
            << problem_forced_policy.v(no_stages, 1) / double(no_stages)
            << "\n";
  ...
```

# Solution

The following code compares the optimal policy to the forced policy.

```c++
#include <iostream>
#include <maintenance_and_replacement_problem.h>
#include <vector>

class ForcedPolicy : public MaintenanceAndReplacementProblem {
public:
  using MaintenanceAndReplacementProblem::MaintenanceAndReplacementProblem;

private:
  std::vector<Action<unsigned int>>
  generate_actions(const stage &t, const unsigned int &state) const override {
    if (state < 4)
      return {make_maintenance_action(state)};
    else
      return {make_replacement_action(state)};
  }
};

int main() {
  const auto oldest_state = 7u;
  const auto no_stages = 100u;

  MaintenanceAndReplacementParameters params;
  params.salvage_value = {50, 40, 30, 20, 10, 0, 0, 0};
  params.trade_in_value = {0, 30, 15, 5, 0, 0, 0};
  params.operating_cost = {5, 7, 11, 22, 35, 50, 75};
  params.purchase_cost = 60;

  MaintenanceAndReplacementProblem problem(no_stages, oldest_state, params);
  problem.use_action_matrix();

  problem.calculate();

  std::cout << "Optimal solution: average cost per stage is "
            << problem.value_function(stage(no_stages), 1) / double(no_stages)
            << "\n";

  for (auto state = oldest_state; state > 0; state--) {
    for (auto t = 20u; t > 0u; --t) {
      std::cout << problem.action_matrix(stage(t), state) << " ";
    }
    std::cout << "\n";
  }

  ForcedPolicy problem_forced_policy(no_stages, oldest_state, params);
  problem_forced_policy.use_action_matrix();

  problem_forced_policy.calculate();

  std::cout << "Forced policy: average cost per stage is "
            << problem_forced_policy.value_function(stage(no_stages), 1) /
                   double(no_stages)
            << "\n";

  for (auto state = oldest_state; state > 0; state--) {
    for (auto t = 20u; t > 0u; --t) {
      std::cout << problem_forced_policy.action_matrix(stage(t), state) << " ";
    }
    std::cout << "\n";
  }
  exit(0);
}
```

The output shows the increase in cost due to the forced policy and the action matrix shows the difference between the policies.

```
Optimal solution: average cost per stage is 25.48
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 0 1 1 0 
0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 0 0 
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
Forced policy: average cost per stage is 25.85
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
```