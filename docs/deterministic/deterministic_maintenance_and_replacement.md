
- [Deterministic Maintenance and Replacement Problems](#deterministic-maintenance-and-replacement-problems)
- [Maintenance and Replacement Problem Parameter struct](#maintenance-and-replacement-problem-parameter-struct)
- [The Maintenance and Replacement Problem Class API](#the-maintenance-and-replacement-problem-class-api)
  - [protected](#protected)
- [The Problem](#the-problem)
- [Parameters](#parameters)
- [Model](#model)
- [An Example Problem](#an-example-problem)
- [Implementation Details](#implementation-details)

# Deterministic Maintenance and Replacement Problems

# Maintenance and Replacement Problem Parameter struct

The following `struct` is used to pass the parameters to the `MaintenanceAndReplacementProblem` constructor.

```c++
struct MaintenanceAndReplacementParameters {
  double purchase_cost;
  std::vector<double> salvage_value;
  std::vector<double> trade_in_value;
  std::vector<double> operating_cost;
};
```

# The Maintenance and Replacement Problem Class API

The file `maintenance_and_replacement_problem.h` defines the class `class MaintenanceAndReplacementProblem`.

---

```c++
MaintenanceAndReplacementProblem(const unsigned int &T,
                                 const unsigned int &oldest_state);
```
Class Contructor.
* `const unsigned int &T` is the number of stages in the Dynamic Program.
* `const unsigned int &oldest_state` is the oldest state before a machine is forceably replaced.

---
```c++
MaintenanceAndReplacementProblem(const unsigned int &T, 
                                 const unsigned int &oldest_state,
                                 const MaintenanceAndReplacementParameters &params);
```
Class Constructor
* `const unsigned int &T` is the number of stages in the Dynamic Program.
* `const unsigned int &oldest_state` is the oldest state before a machine is forceably replaced.
* `const MaintenanceAndReplacementParameters &params` are the parameters of the problem.

---

```c++
void use_action_matrix();
```
The Dynamic Program will store the action taken for each stage/state combination. The action is defined as an `unsigned int` in the `generate_actions` function.

---

```c++
void use_action_matrix(const size_t size);
```
As above but only stores the actions for the first `size` stages of the Dynamic Program.
* `const size_t size` is the number of stages to store actions for.

---

```c++
void set_salvage_value(const std::vector<double> &salvage_value);
```
Setter.
* `const std::vector<double> &salvage_value` is a vector that gives the salvage value. *s(i)* in the problem description below.

---

```c++
void set_purchase_cost(const double &purchase_cost);
```
Setter
* `const double &purchase_cost` is the purchase cost of a new machine. *P* in the problem description below.

---

```c++
void set_trade_in_value(const std::vector<double> &trade_in_value);
```
Setter
* `const std::vector<double> &trade_in_value` is a vector that gives the trade in value for a used machine. *t(i)* in the problem description below.

---

```c++
void set_operating_cost(const std::vector<double> &operating_cost);
```
Setter
* `const std::vector<double> &operating_cost` is a vector that gives the cost of operation of a machine. *c(i)*  in the problem description below.

---

```c++
double value_function(const stage &t, const unsigned int &state) const;
```
Getter: returns the value function for a stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const unsigned int &state` is a state of the Dynamic Program.

---

```c++
unsigned int action_matrix(const stage &t,
                           const unsigned int &state) const;
```
Getter: returns the action taken for a given stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const unsigned int &state` is a state of the Dynamic Program.

---

```c++
void calculate();
```
Calculates the value function for all stage/state combinations.

## protected

```c++
double v(const stage &t, const unsigned int &state) const;
```
Getter: returns the unmodified value function for a stage/state combination.
* `const stage &t` is a stage of the Dynamic Program. `stage` is a strong `unsigned int` type.
* `const unsigned int &state` is a state of the Dynamic Program.

---


# The Problem

A company uses a specific piece of heavy machinary. At a regular interval, for example a year, a decision must be made about whether to perform maintenance on each of the machines or whether to trade in a machine for a new one. Older machines are more costly to maintain and run, but new machines cost money. This problem is about balancing these two factors.

As this is a deterministic problem there is no uncertainty about wear and tear or possible breakdowns. All costs are averages of the expected costs.

A machine may be used more or less over a fixed time period. It may be necessary to model run-time rather than calendar time.

# Parameters

A Dynamic Programming (DP) model for this problem will break the time period into a finite number of stages. The stage defines the time remaining. An end stage must be defined. This will represent the *assest value* of the machine given its age. This is also called the *salvage value* of the machine. It will, most likely, be an estimate. *s(i)* is the salvage value of an *i* year old machine. The age *i* is the state of the DP model.

Each new machine will have a purchase price *P*. When replacing a new machine a *trade in value* will be deducted from the purchase price. This defined as *t(i)* for an *i* year old machine.

Finally there will be the cost of operating a machine until the next stage or decision point. This will include any maintenance costs and expected breakdown costs. This cost is *c(i)* for an *i* year old machine.

# Model

The model is defined as:

```math
v^{t}(\mathbf{i})=\min\left\{P-t(i)+c(0)+v^{t-1}(1),\,c(i)+v^{t-1}(i+1)\right\}
```
```math
v^0(i)=-s(i)
```

# An Example Problem

The following is some example code that will solve a problem and display part of the *action matrix*:

```c++
#include <iostream>
#include <maintenance_and_replacement_problem.h>
#include <vector>

int main() {
  const auto oldest_state = 7u;
  const auto no_stages = 100u;

  MaintenanceAndReplacementParameters params;
  params.salvage_value = {50, 40, 30, 20, 10, 0, 0, 0};
  params.trade_in_value = {0, 30, 15, 5, 0, 0, 0};
  params.operating_cost = {5, 7, 11, 22, 35, 50, 75};
  params.purchase_cost = 60;

  MaintenanceAndReplacementProblem problem(no_stages, oldest_state, params);

  problem.use_action_matrix();

  problem.calculate();

  for (auto state = oldest_state; state > 0; state--) {
    for (auto t = 20u; t > 0u; --t) {
      std::cout << problem.action_matrix(stage(t), state) << " ";
    }
    std::cout << "\n";
  }

  exit(0);
}
```

The output of this program is:

```
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 0 1 1 0 
0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 0 0 
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
```

Both the `oldest_state` and `no_stages` have been given arbitrary values. We can decide what these values should be empirically. The output above shows that machines never reach the oldest state.

Although an *asset value* has been given for the end of the planning period this does not mean that the machines will be sold at the end of this period. As `no_stages` defines the size of the planning period this value should be large enough that the base case of the DP (when `t=0`) does not effect the interpretation of the results. The output above shows that a general policy is to keep a 1 or 2 year old machine and replace anything older. However, towards the end of the planning period a different policy is followed to optimise the policy with respect to the base case.

# Implementation Details

The key to implementation is the `generate_actions` function. This function overrides the abstract function in the `DeterministicDynamicProgram` class. This is a protected function that is used in the `DeterministicDynamicProgram::calculate` function.

```c++
std::vector<Action<unsigned int>>
MaintenanceAndReplacementProblem::generate_actions(const stage &t,
                                                   const unsigned int &state) {
  if (state < oldest_state) {
    return {make_replacement_action(state), make_maintenance_action(state)};
  } else {
    return {make_replacement_action(state)};
  }
}
```

`make_replacement_action(state)` and `make_maintenance_action(state)` are factory functions that create specific `Action` structs. Although not strictly neccessary this approach is cleaner and makes overloading the `generate_actions` function easier.

```c++
Action<unsigned int> MaintenanceAndReplacementProblem::make_maintenance_action(
    const unsigned int &state) const {
  return {operating_cost[state], {{state + 1u, 1.0}}, 0u};
}

Action<unsigned int> MaintenanceAndReplacementProblem::make_replacement_action(
    const unsigned int &state) const {
  return {purchase_cost - trade_in_value[state] + operating_cost[0],
          {{1, 1.0}},
          1u};
}
```