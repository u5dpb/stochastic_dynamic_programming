# TODO

- [ ] Staggered inventory documentary
- [ ] Create abstract ActionFactory class to help tidy up the DP class.
- [ ] Explicit constructors by default
- [ ] Ensure all of the requirements of forward iterators are true for StateLoop
- [ ] InventoryManagement children are repeating code. Can this be avoided?
- [ ] const StateLoop in s_d_p.h, d_d_p.h and e_d_s_d_p.h
- [ ] inventory_management.h add holding cost
- [ ] make reference a free function and allow STL containers to store all states
- [ ] Create a class for the Secretary problem.
- [ ] Deterministic and Stochastic Actions?
- [ ] Staff management problem implementation details.
- [ ] Infinite horizon problem API 
- [ ] - deterministic
- [ ] - stochastic
- [ ] Create compiled libraries for the problem classes
- [ ] API for DeterministicDynamicProgram
- [ ] API for ScrollingArray
- [ ] API for StateLoop and StateLoopVector
- [ ] API for StochasticDynamicProgram
- [ ] Look at where member functions can be made non-member/non-friend.
- [ ] API for SubsequentState
- [ ] Update SDP API
- [ ] API on efficiency
- [ ] - store all actions/events?
- [ ] Implement the "secretary problem"
- [ ] move play your cards right strong type into the class

# TODOing


# TODOne

- [x] Create a strong type for stage (and epsilon)
- [x] Does using iterators for the state loop work better? Marginal but makes the code clearer.
- [x] Create larger problems for runtime efficiency analysis.
- [x] Would a map for the value function storage work better? No
- [x] Update APIs to reflect use of strong types.
- [x] event_enum functions test
- [x] Make DDP generate_actions const
- [x] Sort out the difference between v() and value_function().
- [x] - DDP create abstract value_function()
- [x] - DDP make v() protected and value_function private
- [x] - SDP create abstract value_function()
- [x] - SDP make v() protected and value_function private
- [x] - EDSDP create abstract value_function()
- [x] - EDSDP make v() protected and value_function private
- [x] Core guideline C.12 don't use const for member variables
- [x] play_your_cards_right.h move member vars to private
- [x] inventory_management.h make member vars private
- [x] inventory_management.h move from constructor and init to factory funcion
- [x] Use a policy based approach for an action generation class
- [x] void cast unused variables
- [x] Look for more opportunities to use typedef to tidy the code
- [x] delete commented out code
- [x] Look at "using StateType" for tidy up some of the template code.
- [x] add template param (with int default) for action type
- [x] Add "day" strong type to the staff management problem.