- [Stochastic Dynamic Programming](#stochastic-dynamic-programming)
- [Usage](#usage)
- [General Plan](#general-plan)
  - [General Notes](#general-notes)

# Stochastic Dynamic Programming

Project aim: experiment with creating C++ libraries by creating a stochastic dynamic programming library.

Note: there are existing libraries "out there" in Java and C++ but these things tend to die fast due to lack of use. The lifespan of this project is, therefore, unknown.

For an introduction to SDPs please see the slides in the introduction_to_sdp folder.

There are two sub-directories "deterministic" and "stochastic" that will contain folders that provide example code for solving specific problems. Within these sub-folders an "intro" or "docs" folder will contain some PDF slides to introduce the relevant problem.

# Usage

The library consists of several header files that can be found in the **include** directory. The library classes use templates so only these headers are required.

A simple example and overview can be found in [docs/overview.md](docs/overview.md). Please note the [**warning**](docs/overview.md#warning---invalid-states) on invalid states.

[docs/deterministic_maintenance_and_replacement.md](docs/deterministic_maintenance_and_replacement.md) demonstrates the use of a class that implements a problem using the `DeterministicDynamicProgram` class.

[docs/forced_policy.md](docs/forced_policy.md) shows how a sub-optimal policy can be forced on the problem. A comparison is made between the optimal and forced policy.

# General Plan

Start simple with Deterministic DPs then move on to Stochastic DPs.

Deterministic DP problems that could be considered include: maintenance and replacement problems, resource allocation problems, employee scheduling problems.

Stochastic DP problems include: stochastic employee scheduling, "Play Your Cards Right" decision game, Inventory Management problems.


## General Notes

Use TDD.

I need to give a good description of each of the problems along with example code.

At first glance it appears that the SDP class will use a template to define the state space ~~and will use iterators to progress through the possible states~~.

~~The state space class will need to implement these iterators and a functions to convert each state into a unique uint so that the value function can be access by direct reference.~~

For now iterators are not used as they imply that all possible states are stored in memory. This won't be feasible for large problems. It is possible to get round this but the only advantage of this is that the for loop in DeterministicDynamicProgram can be a for_each loop. ~~The (slight) extra computation is not worth it at the moment.~~ I've implemented this and on first examination it is slower than a non-iterator approach. As runtime is a big issue this is going on the back-burner.

An abstract problem class will need to be defined which will contain factory functions to create unordered sets of decision options and stochastic events.

The SDP class will contain a calculate method that will generate (some of):
* a complete set of state values for the whole problem.
* a set of values for all possible starting states.
* a complete set of decision tables.
