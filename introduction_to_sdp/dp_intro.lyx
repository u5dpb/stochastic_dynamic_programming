#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass beamer
\begin_preamble
\usetheme{Warsaw}
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "helvet" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family sfdefault
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
What is Dynamic Programming?
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Dynamic Programming (DP) is an optimisation method where problems are broken
 down into recorsive subproblem.
 In effect consists of a sequential decision process.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
Examples:
\end_layout

\begin_layout Itemize
SatNav or Google Maps directions; 
\end_layout

\begin_layout Itemize
baseball and cricket strategy;
\end_layout

\begin_layout Itemize
investment decisions.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
This process is best described with a deterministic example but we are really
 interested in problems with uncertainty.
\end_layout

\begin_layout Standard

\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
The Polwarth Music Festival
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
The Polwarth Music Festival covers 3 days of the coming weekend.
 We are to plan to beer stocks for the 3 days.
\end_layout

\begin_layout Itemize
Festival goers will drink 1 barrel of beer per day.
 
\end_layout

\begin_layout Itemize
An order for more beer can be placed the day before to be delivered before
 the start of the next day's activities.
 A cost of 
\begin_inset Formula $\text{£50+10s}$
\end_inset

 is incurred per order (where 
\begin_inset Formula $s$
\end_inset

 is the order size).
 
\end_layout

\begin_layout Itemize
Should any barrels be left on site overnight festival security will charge
 £20 per barrel to ensure nobody steals it.
\end_layout

\begin_layout Itemize
Assume any beer left over from the festival is thrown away.
\end_layout

\begin_layout Standard
We need to decide on which mornings deliveries will be made and how many
 barrels of beer will be delivered.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
This is an inventory management problem.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Approaches to Solving the Problem
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Try all possible combinations.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
Model as an Integer Program.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
Dynamic programming approach:
\end_layout

\begin_layout Itemize
treat each order as a sequential decision.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Terminology
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Stages: 
\end_layout

\begin_layout Itemize
usually in time;
\end_layout

\begin_layout Itemize
when the decision are made;
\end_layout

\begin_layout Itemize
in our example: the start of days 0, 1, 2 and 3 of the music festival (after
 the delivery).
\end_layout

\begin_layout Standard
State:
\end_layout

\begin_layout Itemize
the state of the system for a stage;
\end_layout

\begin_layout Itemize
in our example: the number of barrels of beer in stock (1,2 or 3).
\end_layout

\begin_layout Standard
Decision:
\end_layout

\begin_layout Itemize
also known as a 
\series bold
policy
\series default
;
\end_layout

\begin_layout Itemize
action that moves from a state in one stage to a state in the next stage;
\end_layout

\begin_layout Itemize
in our example: no order or the order size.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
State Diagram
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\align center
\begin_inset Graphics
	filename figures/state.eps
	scale 50

\end_inset


\end_layout

\begin_layout Standard
Note that we could eliminate some of these states as non-optimal just by
 applying some common sense.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Backwards Recursion
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
We solve the problem for the last decision.
\end_layout

\begin_layout Itemize
We do not know how many barrels will have been stored over-night so consider
 every possibility.
 
\end_layout

\begin_layout Itemize
For each possible state: find the minimum cost order.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
Move back to the previous decision.
\end_layout

\begin_layout Itemize
For each possible state: find the minimum cost order including the minimum
 cost from the subsequent last decision.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
Continue moving backwards until we reach the starting stage.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Optimal Policy - Day 2
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\align center
\begin_inset Graphics
	filename figures/state_2.eps
	scale 35

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="2" columns="2">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="left" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Cost of transition = 
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
(over night holding costs per barrel of beer)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+ (ordering cost for new barrels)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="5" columns="7">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="1" alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Order Size
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
opt.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Day 2 state
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
min
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
policy
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
x
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
60
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
70
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
80
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
60
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
20
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
80
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
90
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
x
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
20
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
40
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
100
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
x
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
x
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
40
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Optimal Policy - Day 1
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\align center
\begin_inset Graphics
	filename figures/state_1.eps
	scale 35

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="2">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="left" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Cost of transition = 
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
(over night holding costs per barrel of beer)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+ (ordering cost for new barrels)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+ (minimum cost from the resulting state)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="5" columns="7">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="1" alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Order Size
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
opt.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Day 1 state
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
min
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
policy
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
x
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
60+60
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
70+20
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
80+40
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
90
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
20+60
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
80+20
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
90+40
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
x
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
80
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
40+20
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
100+40
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
x
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
x
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
60
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Optimal Policy - Day 0
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\align center
\begin_inset Graphics
	filename figures/state_0.eps
	scale 40

\end_inset


\end_layout

\begin_layout Standard
Cost of transition (ordering cost for new barrels) + (minimum cost from
 the resulting state)
\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="2" columns="2">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="left" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Cost of transition = 
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
(ordering cost for new barrels)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+ (minimum cost from the resulting state)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="6">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="1" alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Order Size
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
opt.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Day 0 state
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
min
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
policy
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
60+90
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
70+80
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
80+60
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
140
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Optimal Policy - Overall
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
From the tables:
\end_layout

\begin_layout Itemize
Day 0 - optimal policy is to order 3 barrels.
\end_layout

\begin_layout Itemize
Day 1 - optimal policy from starting state 3 is to order no barrels.
\end_layout

\begin_layout Itemize
Day 2 - optimal policy from starting state 2 is to order no barrels.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Backward Recursion
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Difficult/counter-intuitive.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
Need to consider all possible states of a stage:
\end_layout

\begin_layout Itemize
because we 
\series bold
don't know
\series default
 what state we will eventually end up in.
\end_layout

\begin_layout Standard
We 
\series bold
do know
\series default
 the subsequent effect of each decision.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
Forward recursion:
\end_layout

\begin_layout Itemize
know the effect of past decisions;
\end_layout

\begin_layout Itemize
use this to optimise future decisions.
\end_layout

\begin_layout Standard
Backward recursion:
\end_layout

\begin_layout Itemize
know the effect of possible future decisions;
\end_layout

\begin_layout Itemize
use this to optimise past decisions.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
The Principle of Optimality
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Given a possible future state the optimal cost from that point onwards is
 independent of how we get to that state.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
In other words, when optimising a sub-problem:
\end_layout

\begin_layout Itemize
the state we are in has a 
\series bold
direct effect
\series default
 on the optimal solution;
\end_layout

\begin_layout Itemize
how we got into that state has 
\series bold
no effect
\series default
.
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
If this principle does not hold we cannot use dynamic programming.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
The principle allows us to break the problem down into sub-problems which
 can be solved recursively.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Why Use Backward Recursion?
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
It allows us to deal with uncertainty in the problem.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
In the inventory management example:
\end_layout

\begin_layout Itemize
demand unexpectedly increases;
\end_layout

\begin_layout Itemize
no need to recalculate our values.
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
In fact, state transitions may be stochastic.
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
We can build a decision table defining the optimal decision for all future
 states.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
Forward recursion can be useful:
\end_layout

\begin_layout Itemize
if the end conditions are uncertain;
\end_layout

\begin_layout Itemize
e.g.
 we discover we can return unused beer for a small profit.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
The Curse of Dimensionality
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
In an inventory problem:
\end_layout

\begin_layout Itemize
one type of product - states are 1, 2 and 3.
\end_layout

\begin_layout Itemize
two types of product - states are {1,1}, {1,2}, {1,3}, {2,1}, {2,2}, {2,3},
 {3,1}, {3,2}, {3,3}.
\end_layout

\begin_layout Itemize
three types of product - ...
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
Large number of states.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
...and a large number of decisions/policies which move between them.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
(Common) Mathematical Notation
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Formula $T$
\end_inset

 - the number of stages.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $S^{n}$
\end_inset

 - the set of possible states at stage 
\begin_inset Formula $n$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $K_{i}^{n}$
\end_inset

 - the set of possible decisions at stage 
\begin_inset Formula $n$
\end_inset

 when in state 
\begin_inset Formula $i$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $r_{i,k}^{n}$
\end_inset

 or 
\begin_inset Formula $c_{i,k}^{n}$
\end_inset

 - the reward/cost of decision 
\begin_inset Formula $k$
\end_inset

 from 
\begin_inset Formula $K_{i}^{n}$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $j_{i,k}^{n}$
\end_inset

 - the state we will move into if we make decision 
\begin_inset Formula $k$
\end_inset

 from 
\begin_inset Formula $K_{i}^{n}$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $v_{i}^{n}$
\end_inset

 - the optimal reward/cost from state 
\begin_inset Formula $i$
\end_inset

 in stage 
\begin_inset Formula $n$
\end_inset

 onwards.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $v_{i}^{0}$
\end_inset

 - the reward cost in the final stage when we end up in state 
\begin_inset Formula $i$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $v_{i}^{n}$
\end_inset

 is often written as 
\begin_inset Formula $v^{n}(i)$
\end_inset

 and is known as the 
\series bold
value function
\series default
.
\end_layout

\end_deeper
\end_body
\end_document
