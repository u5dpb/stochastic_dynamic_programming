#include <algorithm>
#include <math.h>

#include "inventory_management.h"

struct StaggeredInventoryManagementParameters : InventoryManagementParameters {

  std::vector<double> restock_time;

  std::vector<unsigned int> restock_t() const {
    std::vector<unsigned int> return_vector;
    std::transform(restock_time.begin(), restock_time.end(),
                   std::back_inserter(return_vector), [&](const double &r_t) {
                     return (unsigned int)(floor(r_t * double(T)));
                   });
    return return_vector;
  }
};

template <class Policy = PolicyOptimal>
class StaggeredInventoryManagement : public InventoryManagement<Policy> {

  typedef std::vector<unsigned int> StateType;

public:
  StaggeredInventoryManagement() = delete;
  static StaggeredInventoryManagement<Policy>
  staggered_inventory_management_factory(
      const StaggeredInventoryManagementParameters &params);

protected:
  std::vector<Event<StateType>>
  generate_events(const stage &t, const StateType &state) const override;
  StaggeredInventoryManagement(
      const StaggeredInventoryManagementParameters &params);

private:
  std::vector<unsigned int> restock_t;
  std::vector<unsigned int> restock_level;
};

template <class Policy>
StaggeredInventoryManagement<Policy>::StaggeredInventoryManagement(
    const StaggeredInventoryManagementParameters &params)
    : InventoryManagement<Policy>(params), restock_t(params.restock_t()),
      restock_level(params.restock_level) {}

template <class Policy>
StaggeredInventoryManagement<Policy>
StaggeredInventoryManagement<Policy>::staggered_inventory_management_factory(
    const StaggeredInventoryManagementParameters &params) {
  StaggeredInventoryManagement factory_instance =
      StaggeredInventoryManagement(params);
  factory_instance.init();
  return factory_instance;
}

template <class Policy>
std::vector<Event<StaggeredInventoryManagement<>::StateType>>
StaggeredInventoryManagement<Policy>::generate_events(
    const stage &t, const StateType &state) const {
  (void)t;
  std::vector<Event<StateType>> events;
  auto state_after_replenishment = state;
  for (auto location = 0u; location < state_after_replenishment.size();
       ++location) {
    if (restock_t[location] == t) {
      state_after_replenishment[location] = restock_level[location];
    }
  }
  return InventoryManagement<Policy>::generate_events(
      t, state_after_replenishment);
}
