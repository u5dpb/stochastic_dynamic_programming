#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\begin_preamble
\date{}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "helvet" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family sfdefault
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement H
\paperfontsize default
\spacing onehalf
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type authoryear
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 2cm
\topmargin 2cm
\rightmargin 2cm
\bottommargin 2cm
\secnumdepth 2
\tocdepth 2
\paragraph_separation skip
\defskip smallskip
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle plain
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
An Inventory Management Problem
\end_layout

\begin_layout Section
Problem description
\end_layout

\begin_layout Standard
A retailer has several stores.
 Customers arrive at each store at a different rate and request a particular
 product.
 If the product is in stock then the customer receives the item and is happy.
 If the item is out of stock then the retailer has two options.
 Satisfy the customer request with an item from another store.
 Satisfy the customers request from the manufacturer.
 Each of these options have associated costs from transportation, potential
 price reduction, loss of good will, etc.
 Restocking of goods to the stores occurs at a fixed interval.
 This problem only considers a single product.
 Obviously (in the real world) retailers will sell more than one thing.
\end_layout

\begin_layout Section
Problem Definition
\end_layout

\begin_layout Standard
There are 
\begin_inset Formula $n$
\end_inset

 stores.
 It is common for customer arrival processes to be modelled as a Poisson
 process.
 The arrival rate at each stores is 
\begin_inset Formula $\boldsymbol{\lambda}=\{\lambda_{1},\,...,\,\lambda_{n}\}$
\end_inset

 measured in customers per time unit.
 The overall arrival rate is 
\begin_inset Formula $\Lambda=\sum\lambda$
\end_inset

.
\end_layout

\begin_layout Standard
The stages of the problem is the whole time period between restocking.
 This is a continuous period of time.
 A Stochastic Dynamic Programming model will approximate this time period
 as 
\begin_inset Formula $T$
\end_inset

 
\begin_inset Quotes eld
\end_inset

time slices
\begin_inset Quotes erd
\end_inset

.
 These 
\begin_inset Quotes eld
\end_inset

time slices
\begin_inset Quotes erd
\end_inset

 are the 
\emph on
stages
\emph default
 of the problem.
 At each stage an event occurs: a customer may (or may not) enter the system
 with probability 
\begin_inset Formula $p=\frac{\Lambda}{T}$
\end_inset

.
 Obviously this is inaccurate because in a continuous time period two (or
 more) customers might enter the system during a 
\begin_inset Quotes eld
\end_inset

time slice
\begin_inset Quotes erd
\end_inset

.
 The larger the value of 
\begin_inset Formula $T$
\end_inset

 the smaller this error.
 Mathematically this approximation models the Poisson customer arrival process
 using a Binomial distribution.
\end_layout

\begin_layout Standard
Each stage has a state 
\begin_inset Formula $\boldsymbol{i}=\{i_{1},\,...,\,i_{n}\}$
\end_inset

.
 This is the stock level for the item at each store.
\end_layout

\begin_layout Standard
Each stage begins with an 
\emph on
event
\emph default
.
 The event is whether a customer arrives in the system.
 If no customer arrives then no 
\emph on
decision
\emph default
 or 
\emph on
action 
\emph default
needs to be made.
 If a customer arrives and there is stock at that location the action is
 to satisfy the customer from that locations stock.
 If a customer arrives and there is not stock then a decision is made between
 obtaining the item from the manufacturer or obtaining the item from each
 of the other stores that have stock.
\end_layout

\begin_layout Section
Mathematical Definition
\end_layout

\begin_layout Standard
\noindent
\align center
\begin_inset Formula $\begin{array}[t]{ll}
v^{t}\left(\boldsymbol{i}\right)= & p\sum_{s}\phi_{s}\min\begin{cases}
v^{t-1}\left(\boldsymbol{i}-\boldsymbol{e}_{s}\right) & :\ stock\ available\\
M_{s}+v^{t-1}\left(\boldsymbol{i}\right) & :\ from\ manufacturer\\
\min_{k!=s}\left\{ T_{k,s}+v^{t-1}\left(\boldsymbol{i}-\boldsymbol{e}_{k}\right)\right\} 
\end{cases}\\
 & +\left(1-p\right)v^{t-1}\left(\boldsymbol{i}\right)\\
\\
v^{0}\left(\boldsymbol{i}\right)= & 0,\ \forall\boldsymbol{i}
\end{array}$
\end_inset


\end_layout

\begin_layout Standard
Where:
\end_layout

\begin_layout Itemize
\begin_inset Formula $\phi_{s}$
\end_inset

 is the proportion of demand at store 
\begin_inset Formula $s$
\end_inset

.
 So 
\begin_inset Formula $\phi_{s}=\frac{\lambda_{s}}{\Lambda}$
\end_inset

.
\end_layout

\begin_layout Itemize
\begin_inset Formula $M_{s}$
\end_inset

 is the total cost of satisfying the customer at store 
\begin_inset Formula $s$
\end_inset

 from the manufacturer.
\end_layout

\begin_layout Itemize
\begin_inset Formula $T_{k,s}$
\end_inset

 is the total cost of satisfying the customer at store 
\begin_inset Formula $s$
\end_inset

 from store 
\begin_inset Formula $k$
\end_inset

.
\end_layout

\begin_layout Itemize
\begin_inset Formula $e_{s}$
\end_inset

 is a vector of 
\begin_inset Formula $0$
\end_inset

's with 
\begin_inset Formula $1$
\end_inset

 in position 
\begin_inset Formula $s$
\end_inset

.
 So if 
\begin_inset Formula $\boldsymbol{i}=\{5,5,5\}$
\end_inset

 then 
\begin_inset Formula $\boldsymbol{i}-\boldsymbol{e}_{1}=\{4,5,5\}$
\end_inset

.
\end_layout

\end_body
\end_document
