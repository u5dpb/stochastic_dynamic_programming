#ifndef INVENTORY_MANAGEMENT_PARAMETERS_H_
#define INVENTORY_MANAGEMENT_PARAMETERS_H_

#include <vector>
#include <numeric>

struct InventoryManagementParameters {
  unsigned int T;
  std::vector<unsigned int> restock_level;
  std::vector<double> demand;
  std::vector<double> manufacturer_cost;
  std::vector<std::vector<double>> transfer_cost;

  double overall_demand() const {
    return std::accumulate(demand.begin(), demand.end(), 0.0);
  }
  double overall_demand_probability() const {
    return overall_demand() / double(T);
  }
  std::vector<double> demand_proportion() const {
    std::vector<double> demand_proportion = demand;
    for (auto i = 0u; i < demand_proportion.size(); ++i) {
      demand_proportion[i] /= overall_demand();
    }
    return demand_proportion;
  }
};

#endif // INVENTORY_MANAGEMENT_PARAMETERS_H_