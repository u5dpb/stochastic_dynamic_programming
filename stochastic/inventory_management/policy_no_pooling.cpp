#include "policy_no_pooling.h"

std::vector<Action<PolicyNoPooling::StateType>>
PolicyNoPooling::stockout_actions(
    const unsigned int &demand_location,
    const StateType &state) const {
  return {make_manufacture_action(demand_location, state)};
}
