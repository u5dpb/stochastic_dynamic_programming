#ifndef POLICY_NEAREST_NEIGHBOUR_H_
#define POLICY_NEAREST_NEIGHBOUR_H_

#include <action.h>
#include <event.h>
#include <strong_types.h>
#include <vector>

#include "inventory_management_parameters.h"
#include "policy_optimal.h"

class PolicyNearestNeighbour : public PolicyOptimal {

  typedef std::vector<unsigned int> StateType;

public:
  using PolicyOptimal::PolicyOptimal;

  virtual std::vector<Action<StateType>>
  stockout_actions(const unsigned int &demand_location,
                   const StateType &state) const override;
};

#endif // POLICY_NEAREST_NEIGHBOUR_H_