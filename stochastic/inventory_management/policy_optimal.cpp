#include "policy_optimal.h"

PolicyOptimal::PolicyOptimal(const InventoryManagementParameters &params)
    : params_(params) {}

std::vector<Action<PolicyOptimal::StateType>>
PolicyOptimal::stockout_actions(const unsigned int &demand_location,
                                const StateType &state) const {
  std::vector<Action<StateType>> actions;
  actions.reserve(state.size() + 1);
  actions.push_back(make_manufacture_action(demand_location, state));
  for (auto transfer_location = 0u; transfer_location < state.size();
       ++transfer_location) {
    if (state[transfer_location] > 0) {
      auto new_state = state;
      new_state[transfer_location]--;
      actions.push_back(make_transfer_stock_action(transfer_location,
                                                   demand_location, state));
    }
  }
  return actions;
}

Action<PolicyOptimal::StateType>
PolicyOptimal::make_no_action(const StateType &state) const {
  return {0.0, {{state, 1.0}}, 0};
}

Action<PolicyOptimal::StateType> PolicyOptimal::make_use_own_stock_action(
    const StateType &state, const unsigned int &demand_location) const {
  auto new_state = state;
  new_state[demand_location]--;
  return {0.0, {{new_state, 1.0}}, (int)demand_location + 1};
}

Action<PolicyOptimal::StateType>
PolicyOptimal::make_transfer_stock_action(const unsigned int &transfer_location,
                                          const unsigned int &demand_location,
                                          const StateType &state) const {
  auto new_state = state;
  new_state[transfer_location]--;
  return {transfer_cost()[transfer_location][demand_location],
          {{new_state, 1.0}},
          (int)transfer_location + 1};
}

Action<PolicyOptimal::StateType>
PolicyOptimal::make_manufacture_action(const unsigned int &demand_location,
                                       const StateType &state) const {
  return {manufacturer_cost()[demand_location], {{state, 1.0}}, 0};
}