#ifndef INVENTORY_MANAGEMENT_H_
#define INVENTORY_MANAGEMENT_H_

#include <action.h>
#include <event.h>
#include <event_stochastic_dynamic_program.h>
#include <functional>
#include <state_loop_vector.h>
#include <unordered_set>
#include <vector>

#include "inventory_management_parameters.h"
#include "policy_optimal.h"

enum class EventEnum { no_demand, demand_with_stock, demand_with_no_stock };

struct event_type {
  EventEnum type;
  unsigned int location;
  int to_int() const {
    if (type == EventEnum::no_demand)
      return 0;
    else if (type == EventEnum::demand_with_stock)
      return (int)(location + 1);
    else
      return -(int)(location + 1);
  }
};

inline bool operator==(const event_type &lhs, const event_type &rhs) {

  return lhs.to_int() == rhs.to_int();
}

template <> struct std::hash<event_type> {
  std::size_t operator()(event_type const &e) const noexcept {

    return std::hash<int>{}(e.to_int());
  }
};

template <class Policy = PolicyOptimal>
class InventoryManagement
    : public EventStochasticDynamicProgram<std::vector<unsigned int>> {

public:
  typedef std::vector<unsigned int> StateType;

  InventoryManagement() = delete;

  static InventoryManagement<Policy>
  inventory_management_factory(const InventoryManagementParameters &params);

  void use_event_action_matrix();
  void use_event_action_matrix(
      const std::unordered_set<event_type> &events_to_store);
  int event_action_matrix(const stage &t, const StateType &state,
                          const event_type &event_type) const;

protected:
  InventoryManagement(const InventoryManagementParameters &params);
  void init();

  double overall_demand_probability() const {
    return overall_demand_probability_;
  }
  std::vector<double> demand_proportion() const { return demand_proportion_; }
  std::vector<double> manufacturer_cost() const { return manufacturer_cost_; }
  std::vector<std::vector<double>> transfer_cost() const {
    return transfer_cost_;
  }

  virtual std::vector<Event<StateType>>
  generate_events(const stage &t, const StateType &state) const;

private:
  Event<StateType> make_event_no_demand(const StateType &state) const;

  Event<StateType>
  make_event_demand_with_stock(const StateType &state,
                               const unsigned int &demand_location) const;

  Event<StateType>
  make_event_demand_no_stock(const StateType &state,
                             const unsigned int &demand_location) const;

  double overall_demand_probability_;
  std::vector<double> demand_proportion_;
  std::vector<double> manufacturer_cost_;
  std::vector<std::vector<double>> transfer_cost_;

  Policy policy;
};

template <class Policy>
InventoryManagement<Policy>::InventoryManagement(
    const InventoryManagementParameters &params)
    : EventStochasticDynamicProgram<StateType>(
          params.T, StateLoop(StateType(params.restock_level.size(), 0),
                              params.restock_level)),
      overall_demand_probability_(params.overall_demand_probability()),
      demand_proportion_(params.demand_proportion()),
      manufacturer_cost_(params.manufacturer_cost),
      transfer_cost_(params.transfer_cost), policy(params) {
  assert(params.demand.size() == params.restock_level.size());
  assert(params.manufacturer_cost.size() == params.restock_level.size());
  assert(params.transfer_cost.size() == params.restock_level.size());
}

template <class Policy> void InventoryManagement<Policy>::init() {
  for (const auto &state : state_loop_) {
    set_v0(state, 0.0);
  }
}

template <class Policy>
InventoryManagement<Policy>
InventoryManagement<Policy>::inventory_management_factory(
    const InventoryManagementParameters &params) {
  InventoryManagement<Policy> factory_instance =
      InventoryManagement<Policy>(params);
  factory_instance.init();
  return factory_instance;
}

template <class Policy>
void InventoryManagement<Policy>::use_event_action_matrix() {
  EventStochasticDynamicProgram<StateType>::use_event_action_matrix(T());
}

template <class Policy>
void InventoryManagement<Policy>::use_event_action_matrix(
    const std::unordered_set<event_type> &events_to_store) {
  std::unordered_set<int> events_set;
  for (auto e : events_to_store) {
    events_set.insert(e.to_int());
  }
  EventStochasticDynamicProgram<StateType>::use_event_action_matrix(T(),
                                                                    events_set);
}

template <class Policy>
int InventoryManagement<Policy>::event_action_matrix(
    const stage &t, const StateType &state,
    const event_type &event_type) const {
  return EventStochasticDynamicProgram<StateType>::event_action_matrix(
      t, state, event_type.to_int());
}

// in the following function const unsigned int &t is unused but must
// be included in the function definition

template <class Policy>
std::vector<Event<InventoryManagement<>::StateType>>
InventoryManagement<Policy>::generate_events(const stage &t,
                                             const StateType &state) const {
  (void)t;
  std::vector<Event<StateType>> events;
  events.push_back(make_event_no_demand(state));
  for (unsigned int demand_location = 0; demand_location < state.size();
       ++demand_location) {
    if (state[demand_location] > 0) {
      events.push_back(make_event_demand_with_stock(state, demand_location));
    } else {
      events.push_back(make_event_demand_no_stock(state, demand_location));
    }
  }
  return events;
}

template <class Policy>
Event<InventoryManagement<>::StateType>
InventoryManagement<Policy>::make_event_no_demand(
    const StateType &state) const {
  return {1 - overall_demand_probability(), {policy.make_no_action(state)}, 0};
}

template <class Policy>
Event<InventoryManagement<>::StateType>
InventoryManagement<Policy>::make_event_demand_with_stock(
    const StateType &state, const unsigned int &demand_location) const {
  return {overall_demand_probability() * demand_proportion()[demand_location],
          {policy.make_use_own_stock_action(state, demand_location)},
          (int)(demand_location + 1)};
}

template <class Policy>
Event<InventoryManagement<>::StateType>
InventoryManagement<Policy>::make_event_demand_no_stock(
    const StateType &state, const unsigned int &demand_location) const {
  return {overall_demand_probability() * demand_proportion()[demand_location],
          policy.stockout_actions(demand_location, state),
          -(int)(demand_location + 1)};
}

#endif // INVENTORY_MANAGEMENT_H_