#ifndef POLICY_OPTIMAL_H_
#define POLICY_OPTIMAL_H_

#include <action.h>
#include <event.h>
#include <strong_types.h>
#include <vector>

#include "inventory_management_parameters.h"

class PolicyOptimal {

  typedef std::vector<unsigned int> StateType;

public:
  PolicyOptimal(const InventoryManagementParameters &params);

  virtual std::vector<Action<StateType>>
  stockout_actions(const unsigned int &demand_location,
                   const StateType &state) const;
  virtual Action<StateType> make_no_action(const StateType &state) const;
  virtual Action<StateType>
  make_use_own_stock_action(const StateType &state,
                            const unsigned int &demand_location) const;

protected:
  std::vector<double> manufacturer_cost() const {
    return params_.manufacturer_cost;
  }
  std::vector<std::vector<double>> transfer_cost() const {
    return params_.transfer_cost;
  }
  double overall_demand_probability() const {
    return params_.overall_demand_probability();
  }
  std::vector<double> demand_proportion() const {
    return params_.demand_proportion();
  }

  Action<StateType>
  make_transfer_stock_action(const unsigned int &transfer_location,
                             const unsigned int &demand_location,
                             const StateType &state) const;
  Action<StateType> make_manufacture_action(const unsigned int &demand_location,
                                            const StateType &state) const;

private:
  InventoryManagementParameters params_;
};

#endif // POLICY_OPTIMAL_H_