#ifndef POLICY_NO_POOLING_H_
#define POLICY_NO_POOLING_H_

#include "policy_optimal.h"

class PolicyNoPooling : public PolicyOptimal {

  typedef std::vector<unsigned int> StateType;
public:
  using PolicyOptimal::PolicyOptimal;

  // protected:
  std::vector<Action<StateType>>
  stockout_actions(const unsigned int &demand_location,
                   const StateType &state) const override;
};

#endif // POLICY_NO_POOLING_H_