#include "policy_nearest_neighbour.h"

std::vector<Action<PolicyNearestNeighbour::StateType>>
PolicyNearestNeighbour::stockout_actions(const unsigned int &demand_location,
                                         const StateType &state) const {
  std::vector<Action<StateType>> actions;
  actions.reserve(state.size() + 1);
  bool transfer_possible = false;
  unsigned int best_transfer_location;
  for (auto transfer_location = 0u; transfer_location < state.size();
       ++transfer_location) {
    if (state[transfer_location] > 0) {
      if (!transfer_possible ||
          transfer_cost()[transfer_location][demand_location] <
              transfer_cost()[best_transfer_location][demand_location]) {
        best_transfer_location = transfer_location;
      }
      transfer_possible = true;
    }
  }
  if (transfer_possible) {
    auto new_state = state;
    new_state[best_transfer_location]--;
    actions.push_back(make_transfer_stock_action(best_transfer_location,
                                                 demand_location, state));

  } else {
    actions.push_back(make_manufacture_action(demand_location, state));
  }
  return actions;
}
