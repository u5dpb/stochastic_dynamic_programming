#include "play_your_cards_right.h"

PlayYourCardsRight::PlayYourCardsRight(const std::vector<double> &a_prize)
    : 
    StochasticDynamicProgram(
          54, StateLoop<StateType>(1, 13)),
    no_rounds_{54}, 
    maxmimum_card_number_{13}, 
    prize_(a_prize)
       {
  assert(prize_.size() == no_rounds_ + 1);
  for (const auto &state : state_loop_) {
    set_v0(state, -prize_[0]);
  }
}

void PlayYourCardsRight::use_action_matrix() {
  StochasticDynamicProgram::use_action_matrix(no_rounds_ + 1);
}

double PlayYourCardsRight::value_function(const stage &t,
                             const StateType &state) const {
  return -StochasticDynamicProgram::v(t, state);
}

double PlayYourCardsRight::value_function(const round &r, const StateType &state) const {
  return -StochasticDynamicProgram::v(round_to_stage(r), state);
}

int PlayYourCardsRight::action_matrix(const round &r,
                                      const StateType &state) const {
  return StochasticDynamicProgram::action_matrix(round_to_stage(r), state);
}

double PlayYourCardsRight::expected_prize() {
  double return_double = 0.0;
  for (const auto & state : state_loop_) {
    return_double += value_function(round{1}, state);
  }
  return return_double / (double)state_loop_.size();
}

std::vector<PlayYourCardsRight::ActionType>
PlayYourCardsRight::generate_actions(const stage &t,
                                     const StateType &state) {
  // only need to consider one of higher or lower
  if (state > maxmimum_card_number_ / 2) { // lower
    return {make_bank(t), make_lower(state)};
  } else { // higher
    return {make_bank(t), make_higher(state)};
  }
}

PlayYourCardsRight::ActionType
PlayYourCardsRight::make_bank(const stage &t) const {
  return ActionType({-prize(t), {}, 0});
}

PlayYourCardsRight::ActionType
PlayYourCardsRight::make_lower(const StateType &state) const {
  std::vector<SubsequentState<StateType>> subsequent_state;
  for (auto i = 1u; i < state; ++i) {
    subsequent_state.push_back({i, 1.0 / (double)maxmimum_card_number_});
  }
  return {0.0, subsequent_state, 1};
}

PlayYourCardsRight::ActionType
PlayYourCardsRight::make_higher(const StateType &state) const {
  std::vector<SubsequentState<StateType>> subsequent_state;
  for (auto i = state + 1; i <= maxmimum_card_number_; ++i) {
    subsequent_state.push_back({i, 1.0 / (double)maxmimum_card_number_});
  }
  return {0.0, subsequent_state, 1};
}