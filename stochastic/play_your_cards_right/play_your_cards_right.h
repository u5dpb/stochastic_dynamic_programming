#ifndef PLAY_YOUR_CARDS_RIGHT_H_
#define PLAY_YOUR_CARDS_RIGHT_H_

#include <action.h>
#include <cassert>
#include <state_loop.h>
#include <stochastic_dynamic_program.h>
#include <subsequent_state.h>
#include <vector>

struct round {
  explicit round(unsigned int v) { number = v; }
  operator unsigned int() { return number; }

  unsigned int number;
};

class PlayYourCardsRight : public StochasticDynamicProgram<unsigned int> {

  typedef unsigned int StateType;
  using ActionType = Action<StateType>;

public:
  PlayYourCardsRight() = delete;
  PlayYourCardsRight(const std::vector<double> &prize);

  void use_action_matrix();

  double value_function(const stage &t, const StateType &state) const override;
  double value_function(const round &r, const StateType &state) const;

  using StochasticDynamicProgram<StateType>::action_matrix;
  int action_matrix(const round &r, const StateType &state) const;

  double expected_prize();

  unsigned int no_rounds() const { return no_rounds_; }
  unsigned int maxmimum_card_number() const { return maxmimum_card_number_; }

protected:
  double prize(size_t t) const { return prize_[no_rounds_ - t]; }
  double prize(round r) const { return prize_[r - 1]; }

  virtual std::vector<ActionType>
  generate_actions(const stage &t, const StateType &state) override;

  ActionType make_bank(const stage &t) const;
  ActionType make_lower(const StateType &state) const;
  ActionType make_higher(const StateType &state) const;

private:
  unsigned int no_rounds_;
  unsigned int maxmimum_card_number_;
  std::vector<double> prize_;

  stage round_to_stage(round r) const { return stage(no_rounds_ - r + 1); }
};

#endif // PLAY_YOUR_CARDS_RIGHT_H_