#ifndef STOCHASTIC_MAINTENANCE_AND_REPLACEMENT_PROBLEM_H_
#define STOCHASTIC_MAINTENANCE_AND_REPLACEMENT_PROBLEM_H_

#include <stochastic_dynamic_program.h>
#include <vector>

#include "state_loop.h"

class StochasticMaintenanceAndReplacementProblem
    : public StochasticDynamicProgram<unsigned int> {

  typedef unsigned int StateType;

public:
  StochasticMaintenanceAndReplacementProblem() = delete;
  StochasticMaintenanceAndReplacementProblem(const unsigned int &T,
                                             const unsigned int &oldest_state);

  void set_salvage_value(const std::vector<double> &salvage_value);
  void set_purchase_cost(const double &purchase_cost);
  void set_trade_in_value(const std::vector<double> &trade_in_value);
  void set_repair_cost(const std::vector<double> &repair_cost);
  void
  set_breakdown_probability(const std::vector<double> &breakdown_probability);

private:
  StateType oldest_state;

  double purchase_cost;
  std::vector<double> trade_in_value;
  std::vector<double> repair_cost;
  std::vector<double> breakdown_probability;

  std::vector<Action<StateType>> generate_actions(const stage &t,
                                                  const StateType &state);
};

#endif // STOCHASTIC_MAINTENANCE_AND_REPLACEMENT_PROBLEM_H_