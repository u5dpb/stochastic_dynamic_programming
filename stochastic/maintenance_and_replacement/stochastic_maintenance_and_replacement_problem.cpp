#include "stochastic_maintenance_and_replacement_problem.h"

StochasticMaintenanceAndReplacementProblem::
    StochasticMaintenanceAndReplacementProblem(const unsigned int &T,
                                               const unsigned int &oldest_state)
    : StochasticDynamicProgram(T, StateLoop<unsigned int>(0u, oldest_state)),
      oldest_state(oldest_state) {}

void StochasticMaintenanceAndReplacementProblem::set_salvage_value(
    const std::vector<double> &salvage_value) {
  for (const auto &state : state_loop_) {
    set_v0(state, -salvage_value[state]);
  }
}

void StochasticMaintenanceAndReplacementProblem::set_purchase_cost(
    const double &purchase_cost) {
  this->purchase_cost = purchase_cost;
}

void StochasticMaintenanceAndReplacementProblem::set_trade_in_value(
    const std::vector<double> &trade_in_value) {
  this->trade_in_value = trade_in_value;
}

void StochasticMaintenanceAndReplacementProblem::set_repair_cost(
    const std::vector<double> &repair_cost) {
  this->repair_cost = repair_cost;
}

void StochasticMaintenanceAndReplacementProblem::set_breakdown_probability(
    const std::vector<double> &breakdown_probability) {
  this->breakdown_probability = breakdown_probability;
}

// in the following function const unsigned int &t is unused but must
// be included in the function definition

std::vector<Action<StochasticMaintenanceAndReplacementProblem::StateType>>
StochasticMaintenanceAndReplacementProblem::generate_actions(
    const stage &t, const StateType &state) {

  (void)t;

  std::vector<Action<StateType>> action_set;
  if (state > 0)
    action_set.push_back(
        {purchase_cost - trade_in_value[state] +
             breakdown_probability[0] * repair_cost[0],
         {{0, breakdown_probability[0]}, {1, 1 - breakdown_probability[0]}},
         1u});
  if (state < oldest_state) {
    action_set.push_back({breakdown_probability[state] * repair_cost[state],
                          {{state, breakdown_probability[state]},
                           {state + 1, 1 - breakdown_probability[state]}},
                          0u});
  }
  return action_set;
}